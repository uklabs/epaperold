package com.bhaskar.epaper.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnLoginListener;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.ui.MobileVerificationActivity;
import com.bhaskar.epaper.utils.Utility;

public class SignInFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_CODE_FORGOT_PASSWORD = 1020;
    private boolean isPasswordVisible = false;
    private String password, emailOrMobile;
    private EditText emailEdt, passwrdEdt;

    private OnLoginListener onLoginListener;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment getInstance(String gaAction) {
        SignInFragment signInFragment = new SignInFragment();

        Bundle bundle = new Bundle();
        bundle.putString("gaaction", gaAction);
        signInFragment.setArguments(bundle);

        return signInFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onLoginListener = (OnLoginListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        view.setSaveFromParentEnabled(false);

        emailEdt = view.findViewById(R.id.signin_email);

        if (Constants.COUNTRY_IS_INDIA) {
            emailEdt.setHint(getString(R.string.email));
        } else {
            emailEdt.setHint(getString(R.string.email_txt));
        }

        passwrdEdt = view.findViewById(R.id.signin_password);
        final TextView forgetpasswordTV = view.findViewById(R.id.forgetpassword);
        TextView dontHaveAccountTV = view.findViewById(R.id.dont_have_account);

        /*forgot password clickable*/
        String forgotPasswordText = getString(R.string.forgetpassword);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(forgotPasswordText));
        spannableStringBuilder.setSpan(new MySpannable(getActivity()) {
            @Override
            public void onClick(View widget) {
                startActivityForResult(new Intent(getActivity(), MobileVerificationActivity.class).putExtra("forgotPwd", true), REQUEST_CODE_FORGOT_PASSWORD);
            }
        }, 0, Html.fromHtml(forgotPasswordText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        forgetpasswordTV.setMovementMethod(LinkMovementMethod.getInstance());
        forgetpasswordTV.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

        /*register now clickable*/
        String registerNow = "Register Now";
        String dontHaveAccountText = getString(R.string.don_t_have_an_account, registerNow);
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(dontHaveAccountText));
        spannableStringBuilder2.setSpan(new MySpannable(getActivity()) {
            @Override
            public void onClick(View widget) {
//                view pager set position to 1
                if (getActivity() != null) {
                    ((LoginRegistrationActivity) getActivity()).viewpager.setCurrentItem(1);
                }
            }
        }, Html.fromHtml(dontHaveAccountText).toString().indexOf(registerNow), Html.fromHtml(dontHaveAccountText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        dontHaveAccountTV.setMovementMethod(LinkMovementMethod.getInstance());
        dontHaveAccountTV.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
        view.findViewById(R.id.iv_pass_visible).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPasswordVisible = !isPasswordVisible;
                if (isPasswordVisible) {
                    passwrdEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ((ImageView) view.findViewById(R.id.iv_pass_visible)).setImageResource(R.drawable.ic_visibility_on);
                } else {
                    passwrdEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    ((ImageView) view.findViewById(R.id.iv_pass_visible)).setImageResource(R.drawable.ic_visibility_off);
                }
                passwrdEdt.setTypeface(Typeface.DEFAULT);
                passwrdEdt.setSelection(passwrdEdt.getText().length());
            }
        });
        view.findViewById(R.id.signin_Btn).setOnClickListener(this);
        view.findViewById(R.id.facebook_loginBtn).setOnClickListener(this);
        view.findViewById(R.id.google_loginBtn).setOnClickListener(this);

        return view;
    }

    boolean isMobile = false;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signin_Btn:
                if (Utility.isConnectingToInternet(getActivity())) {
                    if (checkValidation()) {
                        if (onLoginListener != null)
                            onLoginListener.onLogin(emailOrMobile, password);
                    }
                    hideKeyBoard();
                } else {
                    Utility.setNetworkErrorToast(getActivity());
                }
                break;

            case R.id.facebook_loginBtn:
                if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                    if (onLoginListener != null)
                        onLoginListener.onFBClick();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_error_), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.google_loginBtn:
                if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                    if (onLoginListener != null)
                        onLoginListener.onGoogleClick();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_error_), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void hideKeyBoard() {
        if (getView() != null && getActivity() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    private boolean checkValidation() {
        emailOrMobile = emailEdt.getText().toString();
        password = passwrdEdt.getText().toString();
        if (TextUtils.isEmpty(emailOrMobile)) {
            emailEdt.setError("Please enter Email Or Mobile");
            return false;
        } else if (TextUtils.isEmpty(password)) {
            passwrdEdt.setError("Please enter password");
            return false;
        }

        if (TextUtils.isDigitsOnly(emailOrMobile)) {
            if (!Patterns.PHONE.matcher(emailOrMobile).matches()) {
                emailEdt.setError("Please enter valid Input");
                return false;
            } else {
                isMobile = true;
            }
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(emailOrMobile).matches()) {
                emailEdt.setError("Please enter valid Email");
                return false;
            } else {
                isMobile = false;
            }
        }
        return true;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    public class MySpannable extends ClickableSpan {
        private Context mContext;

        public MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {

        }
    }

}
