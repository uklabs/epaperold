package com.bhaskar.epaper.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnLoginListener;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.TermConditionActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.ui.MobileVerificationActivity;
import com.bhaskar.epaper.utils.AuthConst;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PasswordValidator;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;

public class SignUpFragment extends Fragment implements View.OnClickListener {

    public static final int REQUEST_CODE_MOBILE_VERIFY = 1100;


    private boolean btnClicked = false;
    private boolean isAgree = false;
    private int loginType = AuthConst.LoginType.MANUAL;
    private String email, password, signUpId, profilePic;


    private EditText emailEdt, passwordEdt, confirmPwdEdt;
    private EditText phoneEDt;
    private TextView passwordValidateTv, loginFailureTv;
    private OnLoginListener onLoginListener;

    private String mobileNumber;
    private boolean isPasswordVisible = false;
    private boolean isCnfPasswordVisible = false;

    public static SignUpFragment getInstance(String gaAction) {
        SignUpFragment signUpFragment = new SignUpFragment();

        /*Arguments*/
        Bundle bundle = new Bundle();
        bundle.putString("gaaction", gaAction);
        signUpFragment.setArguments(bundle);

        return signUpFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context != null) {
            onLoginListener = (OnLoginListener) context;
        }
    }

    String privacyPolicy = "Privacy Policy";
    String cookiePolicy = "Cookie Policy";
    String termsAndCond = "Terms and Conditions";
    final int privacyOption = 1;
    final int cookieOption = 2;
    final int termAndCondOption = 3;
    private View rootView;
    AppCompatButton signupbtn;
    AppCompatCheckBox appCompatCheckBox;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        rootView.setSaveFromParentEnabled(false);


        View phoneSeparator = rootView.findViewById(R.id.phone_separator);


        phoneEDt = rootView.findViewById(R.id.signup_phone_et);
        emailEdt = rootView.findViewById(R.id.singup_email);
        passwordEdt = rootView.findViewById(R.id.singup_password);
        confirmPwdEdt = rootView.findViewById(R.id.confirm_password);
        passwordValidateTv = rootView.findViewById(R.id.password_validate_tv);
        loginFailureTv = rootView.findViewById(R.id.login_failure_tv);
        signupbtn = rootView.findViewById(R.id.signup_btn);
        appCompatCheckBox = rootView.findViewById(R.id.agree_check_box);

        if (EpaperPrefernces.getInstance(getActivity()).getBooleanValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_ACTIVE, false)) {
            rootView.findViewById(R.id.text_dash).setVisibility(View.VISIBLE);
        } else {
            rootView.findViewById(R.id.text_dash).setVisibility(View.GONE);
        }

        setTermsAndCond();

        signupbtn.setEnabled(false);
        appCompatCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    isAgree = true;
                    signupbtn.setEnabled(true);
                    signupbtn.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_theme_bg));
                } else {
                    isAgree = false;
                    signupbtn.setEnabled(false);
                    signupbtn.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.btn_grey_bg));
                }
            }
        });


        rootView.findViewById(R.id.facebook_loginBtn).setOnClickListener(this);
        rootView.findViewById(R.id.google_loginBtn).setOnClickListener(this);


        signupbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!btnClicked) {

                    if (Utility.isConnectingToInternet(getActivity())) {
                        if (checkValidation()) {
                            btnClicked = true;
                            checkUserExistOrNot();
                        }
                    } else {
                        Utility.setNetworkErrorToast(getActivity());
                    }
                }
            }
        });

        if (Constants.COUNTRY_IS_INDIA) {
            phoneEDt.setVisibility(View.VISIBLE);
            phoneSeparator.setVisibility(View.VISIBLE);
            rootView.findViewById(R.id.mobile_number_layout).setVisibility(View.VISIBLE);
        } else {
            phoneEDt.setVisibility(View.GONE);
            phoneSeparator.setVisibility(View.GONE);
            rootView.findViewById(R.id.mobile_number_layout).setVisibility(View.GONE);
        }

        rootView.findViewById(R.id.iv_pass_visible).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPasswordVisible = !isPasswordVisible;
                if (isPasswordVisible) {
                    passwordEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ((ImageView) rootView.findViewById(R.id.iv_pass_visible)).setImageResource(R.drawable.ic_visibility_on);
                } else {
                    passwordEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    ((ImageView) rootView.findViewById(R.id.iv_pass_visible)).setImageResource(R.drawable.ic_visibility_off);
                }
                passwordEdt.setTypeface(Typeface.DEFAULT);
                passwordEdt.setSelection(passwordEdt.getText().length());
            }
        });
        rootView.findViewById(R.id.iv_cnf_pass_visible).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCnfPasswordVisible = !isCnfPasswordVisible;
                if (isCnfPasswordVisible) {
                    confirmPwdEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ((ImageView) rootView.findViewById(R.id.iv_cnf_pass_visible)).setImageResource(R.drawable.ic_visibility_on);
                } else {
                    confirmPwdEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    ((ImageView) rootView.findViewById(R.id.iv_cnf_pass_visible)).setImageResource(R.drawable.ic_visibility_off);
                }
                confirmPwdEdt.setTypeface(Typeface.DEFAULT);
                confirmPwdEdt.setSelection(confirmPwdEdt.getText().length());
            }
        });
        return rootView;
    }

    private void setTermsAndCond() {
        TextView termConditionTv = rootView.findViewById(R.id.term_condition_tv);
        TextView haveAccountTV = rootView.findViewById(R.id.have_account);
        String agreeTermsConditionText = getString(R.string.agree_terms_and_condition, privacyPolicy, cookiePolicy, termsAndCond);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(agreeTermsConditionText));
        int priPoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(privacyPolicy);
        int cookiePoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(cookiePolicy);
        int termAndCndIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(termsAndCond);

        spannableStringBuilder.setSpan(new MySpannable(getActivity(), privacyOption), priPoIndx, priPoIndx + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(getActivity(), cookieOption), cookiePoIndx, cookiePoIndx + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(getActivity(), termAndCondOption), termAndCndIndx, termAndCndIndx + termsAndCond.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termConditionTv.setMovementMethod(LinkMovementMethod.getInstance());
        termConditionTv.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

        /*register now clickable*/
        String loginNow = "Login Now";
        String haveAccountText = getString(R.string.have_an_account, loginNow);
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(haveAccountText));
        spannableStringBuilder2.setSpan(new MySpannable(getActivity()) {
            @Override
            public void onClick(View widget) {
//                view pager set position to 1
                if (getActivity() != null) {
                    ((LoginRegistrationActivity) getActivity()).viewpager.setCurrentItem(0);
                }
            }
        }, Html.fromHtml(haveAccountText).toString().indexOf(loginNow), Html.fromHtml(haveAccountText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        haveAccountTV.setMovementMethod(LinkMovementMethod.getInstance());
        haveAccountTV.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
    }


    private void checkUserExistOrNot() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            if (Constants.COUNTRY_IS_INDIA) {
                jsonObject.put("mobileno", mobileNumber);
            }
            jsonObject.put("login_type", loginType);
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SIGN_UP_VERIFY_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        userReadyToInsert();
                    } else {
                        btnClicked = false;
                        Utility.setToast(getActivity(), response.getString("msg"));
                    }
                } catch (Exception ignored) {
                    btnClicked = false;
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                btnClicked = false;
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    public void userReadyToInsert() {
        Intent mobileVerficationIntent = new Intent(getActivity(), MobileVerificationActivity.class);
        mobileVerficationIntent.putExtra("forVerify", true);
        mobileVerficationIntent.putExtra("email", email);
        mobileVerficationIntent.putExtra("password", password);
        mobileVerficationIntent.putExtra("login_type", loginType);
        mobileVerficationIntent.putExtra("sign_up_id", signUpId);
        mobileVerficationIntent.putExtra("profile_pic", profilePic);
        if (Constants.COUNTRY_IS_INDIA) {
            mobileVerficationIntent.putExtra("mobileno", phoneEDt.getText().toString().trim());
        }
        startActivityForResult(mobileVerficationIntent, REQUEST_CODE_MOBILE_VERIFY);
    }

    public void setRegistrationData(String email, String signUpId, String profilePic, int loginType) {
        this.email = email;
        this.signUpId = signUpId;
        this.loginType = loginType;
        this.profilePic = profilePic;
        loginFailureTv.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(email)) {
            emailEdt.setText(email);
            emailEdt.setEnabled(false);
        }
    }

    private boolean checkValidation() {
        email = emailEdt.getText().toString();
        password = passwordEdt.getText().toString();
        mobileNumber = phoneEDt.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            emailEdt.requestFocus();
            emailEdt.setError("Please enter Email Id");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailEdt.requestFocus();
            emailEdt.setError("Please enter valid Email Id");
            return false;
        } else if (Constants.COUNTRY_IS_INDIA && TextUtils.isEmpty(phoneEDt.getText().toString().trim())) {
            phoneEDt.requestFocus();
            phoneEDt.setError("Please enter Mobile number");
            return false;
        } else if (Constants.COUNTRY_IS_INDIA && phoneEDt.getText().toString().trim().length() != 10) {
            phoneEDt.requestFocus();
            phoneEDt.setError("Please enter valid Mobile number");
            return false;
        } else {
            String confirmPassword = confirmPwdEdt.getText().toString();
            if (TextUtils.isEmpty(password)) {
                passwordEdt.requestFocus();
                passwordEdt.setError("Please enter password");
                return false;
            } else if (!new PasswordValidator().validate(password)) {
                passwordValidateTv.setVisibility(View.VISIBLE);
                return false;
            } else if (TextUtils.isEmpty(confirmPassword)) {
                passwordValidateTv.setVisibility(View.GONE);
                confirmPwdEdt.requestFocus();
                confirmPwdEdt.setError("Please enter password");
                return false;
            } else if (!new PasswordValidator().validate(confirmPassword)) {
                passwordValidateTv.setVisibility(View.VISIBLE);
                return false;
            } else if (!password.equalsIgnoreCase(confirmPassword)) {
                passwordValidateTv.setVisibility(View.GONE);
                confirmPwdEdt.requestFocus();
                confirmPwdEdt.setError("Password and confirm password did not matched");
                return false;
            } else if (!isAgree) {
                passwordValidateTv.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Please check Term & Conditions", Toast.LENGTH_SHORT).show();
                return false;
            }
            passwordValidateTv.setVisibility(View.GONE);
            confirmPwdEdt.setError(null);
            return true;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.facebook_loginBtn:
                if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                    if (onLoginListener != null)
                        onLoginListener.onFBClick();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_error_), Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.google_loginBtn:
                if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                    if (onLoginListener != null)
                        onLoginListener.onGoogleClick();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.internet_connection_error_), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_MOBILE_VERIFY) {
            if (resultCode == RESULT_OK) {
                if (getActivity() != null) {
                    ((LoginRegistrationActivity) getActivity()).finishLoginActivity(true);
                }
            }
        }
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;
        private int position;

        public MySpannable(Context context, int position) {
            this.mContext = context;
            this.position = position;
        }

        public MySpannable(Context context) {
            this.mContext = context;
            this.position = 0;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {
            switch (position) {
                case privacyOption:
                    startActivity(new Intent(getActivity(), TermConditionActivity.class).putExtra("title", privacyPolicy).putExtra("url", Urls.PRIVACY_POLICY_URL));
                    break;
                case cookieOption:
                    startActivity(new Intent(getActivity(), TermConditionActivity.class).putExtra("title", cookiePolicy).putExtra("url", Urls.COOKIE_POLICY_URL));
                    break;
                case termAndCondOption:
                    startActivity(new Intent(getActivity(), TermConditionActivity.class).putExtra("title", termsAndCond).putExtra("url", Urls.TERM_AND_CONDITION_URL));
                    break;
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        btnClicked = false;
    }
}
