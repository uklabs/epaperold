package com.bhaskar.epaper.fragments;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.HomeItemAdapter;
import com.bhaskar.epaper.epaperv2.interfaces.OnSetHomePageListener;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.MainDataModel;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasterPageStateFragment extends Fragment {

    private EpaperPrefernces mPref;
    private String statename = "";
    private List<MasterModel> stateEditionList = new ArrayList<>();
    private RecyclerView recycler;
    private AVLoadingIndicatorView progressBar;
    private String channelSlno="";
    private String isPaid;
    private FloatingActionButton setAsHome;
    private OnSetHomePageListener onSetHomePageListener;
    private TextView holidayTxt;
    boolean isHoliday = false;

    public static MasterPageStateFragment getInstance(int position) {
        MasterPageStateFragment masterPageStateFragment = new MasterPageStateFragment();
        Bundle argument = new Bundle();
        argument.putInt("position", position);
        masterPageStateFragment.setArguments(argument);
        return masterPageStateFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*BUndle data*/
        Bundle arguments = getArguments();
        int position = 0;
        if (arguments != null) {
            position = arguments.getInt("position");
        }

        try {
            MainDataModel mainDataModel = Epaper.getInstance().getModelManager().getStateList().get(position);
            this.statename = mainDataModel.statename;
            this.channelSlno = mainDataModel.channelSlno;
            this.isPaid = mainDataModel.paidFlag;
            //isHoliday = ModelManager.getInstance().getHolidayList().contains(statename);
            if (ModelManager.getInstance().getHolidayList().contains(statename))
                isHoliday = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_state_master, container, false);
        rootView.setSaveFromParentEnabled(false);

        mPref = EpaperPrefernces.getInstance(getActivity());
        setAsHome = rootView.findViewById(R.id.set_as_home);
        recycler = rootView.findViewById(R.id.homeRecyclerList);
        holidayTxt = rootView.findViewById(R.id.holidayTxt);
        progressBar = rootView.findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        int columns = Epaper.getInstance().getModelManager().getColumnCount();
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), columns));
        progressBar.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startWork();
            }
        }, 300);

        if (isHoliday) {
            holidayTxt.setText(ModelManager.getInstance().getHolidaymsz());
            holidayTxt.setVisibility(View.VISIBLE);
        } else {
            holidayTxt.setVisibility(View.GONE);
            holidayTxt.setText(ModelManager.getInstance().getHolidaymsz());
        }

        return rootView;
    }

    private void startWork() {
        if (HomeContent.getHomeContent().listMain.containsKey(statename)) {
            recycler.setAdapter(new HomeItemAdapter(getActivity(), mPref, statename, channelSlno, isPaid));
            progressBar.setVisibility(View.GONE);
        } else {
            callStateWiseData();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setStateAsHome();
    }

    private void setStateAsHome() {

        final String setAsDefaultString = mPref.getStringValue(PreferncesConstant.setAsDefaultData, "");
        if (getContext() != null) {
            if (!TextUtils.isEmpty(setAsDefaultString) && setAsDefaultString.contains(statename)) {
                setAsHome.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark)));
            } else {
                setAsHome.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.default_float_btn)));
            }
        }

        setAsHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSetHomePageListener != null) {
                    onSetHomePageListener.showProgress();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (setAsDefaultString.equalsIgnoreCase(statename)) {
                            mPref.setStringValue(PreferncesConstant.setAsDefaultData, "");
                            Utility.setToast(getActivity(), statename + " removed as Home");
                            if (onSetHomePageListener != null) {
                                onSetHomePageListener.removeHomeState();
                            }
                        } else {
                            mPref.setStringValue(PreferncesConstant.setAsDefaultData, statename);
                            Utility.setToast(getActivity(), statename + " set as Home");
                            if (onSetHomePageListener != null) {
                                onSetHomePageListener.changeHomeState(statename);
                            }
                        }
                        setStateAsHome();
                    }
                }, 500);

            }
        });
    }


    private void callStateWiseData() {
        String dateSetter = DateUtility.setDateForAll;
        if (getActivity() != null) {
            if (ConnectionDetector.isConnectingToInternet(getActivity())) {
                String filename = "jx-master/json/" + statename.toUpperCase() + "/" + dateSetter + "/";
                String url = Urls.BASE_URL + filename;
                JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            progressBar.setVisibility(View.GONE);
                            parseStateWiseData(response);
                        } catch (Exception e) {
                        e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            progressBar.setVisibility(View.GONE);
                            Toast toast = Toast.makeText(getActivity(), "NO Data Found", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("api-key", "bh@@$k@r-D");
                        headers.put("Content-Type", "application/json charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }
                };

                request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
            } else {
                Utility.setNetworkErrorToast(getActivity());
            }
        }
    }

    private void parseStateWiseData(JSONArray response) {
        stateEditionList = Arrays.asList(new Gson().fromJson(response.toString(), MasterModel[].class));
        HomeContent.getHomeContent().listMain.put(statename, stateEditionList);
        recycler.setAdapter(new HomeItemAdapter(getActivity(), mPref, statename, channelSlno, isPaid));
    }

    public void setHomeListener(OnSetHomePageListener onSetHomePageListener) {
        this.onSetHomePageListener = onSetHomePageListener;
    }
}