package com.bhaskar.epaper.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnSavePdfListener;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.ui.ImageCropperActivity;
import com.bhaskar.epaper.ui.ImageCropperShareActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.JsonElement;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ViewPagerFragment extends Fragment {

    private static final int REQUEST_CODE_BOOKMARK = 1232;
    private AVLoadingIndicatorView progressBar;
    private TextView txt_loader;
    private SwipeRefreshLayout swipeLayout;
    private ImageView imgPlaceholder;
    private Epaper mApp;
    private ImageViewTouch image;
    private EpaperPrefernces mPref;
    private Bitmap imagebitmap = null;
    private MasterPageModel masteDetailModel;
    private OnSavePdfListener onSavePdfListener;
    private String imgThumb = "";
    private ProgressDialog pd;
    private FloatingActionMenu floatingActionMenu;
    private RelativeLayout retryLay;
    private int count =0;

    public ViewPagerFragment() {
    }

    public static ViewPagerFragment getPagerFragment(MasterPageModel masterPageModel, int position) {
        ViewPagerFragment viewPagerFragment = new ViewPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", masterPageModel);
        bundle.putInt("position", position);
        viewPagerFragment.setArguments(bundle);
        return viewPagerFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onSavePdfListener = (OnSavePdfListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle argument = getArguments();
        masteDetailModel = (MasterPageModel) argument.getSerializable("data");
        int position = argument.getInt("position");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pagerlayout, container, false);
        v.setSaveFromParentEnabled(false);

        mApp = Epaper.getInstance();
        mPref = EpaperPrefernces.getInstance(getActivity());
        floatingActionMenu = v.findViewById(R.id.menu_item);
        image = v.findViewById(R.id.pager_imageview);
        FloatingActionButton save_menu_btn = v.findViewById(R.id.save_menu_btn);
        FloatingActionButton share_menu_btn = v.findViewById(R.id.share_menu_btn);
        FloatingActionButton book_menu_btn = v.findViewById(R.id.book_menu_btn);
        FloatingActionButton crop_menu_btn = v.findViewById(R.id.crop_menu_btn);
        retryLay = v.findViewById(R.id.retryLay);

        progressBar = v.findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        txt_loader = v.findViewById(R.id.txt_loader);
        swipeLayout = v.findViewById(R.id.swipeLayout);
        imgPlaceholder = v.findViewById(R.id.imgPlaceholder);

        v.findViewById(R.id.imageview).setOnTouchListener(new ImageMatrixTouchHandler(v.getContext()));

        progressBar.bringToFront();
        txt_loader.bringToFront();

        if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            book_menu_btn.setVisibility(View.VISIBLE);
        } else {
            book_menu_btn.setVisibility(View.GONE);
        }

        crop_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if (ImageCropperShareActivity.isShowTime) {
                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                    String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                    startActivity(
                            new Intent(getActivity(), ImageCropperShareActivity.class)
                                    .putExtra("editioncode", masteDetailModel.editioncode)
                                    .putExtra("pageno", masteDetailModel.pageno)
                                    .putExtra("description", description)
                                    .putExtra("editionname", masteDetailModel.description)
                                    .putExtra("imgpath", masteDetailModel.imagepath)
                    );
                } else {
                    if (imagebitmap != null) {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                        startActivity(
                                new Intent(getActivity(), ImageCropperActivity.class)
                                        .putExtra("pageno", masteDetailModel.pageno)
                                        .putExtra("description", description)
                                        .putExtra("editionname", masteDetailModel.description)
                                        .putExtra("imgpath", masteDetailModel.imagepath)
                        );
                    } else {
                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                    }
                }

                floatingActionMenu.close(true);*/
                IsImageSaveAPI("crop");
            }
        });

        save_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* if (onSavePdfListener != null) {
                    onSavePdfListener.saveAsPdf(position);
                }*/

              /*  try {
                    PdfCreator.getPdfCreator(mApp).createSinglePagePdf(masteDetailModel.imagelarge, state);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }*/

                IsImageSaveAPI("download");

            }
        });

        share_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if (imagebitmap != null) {
                    String description = getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + masteDetailModel.description + " Page No:" + masteDetailModel.pageno;
                    ImageUtility.saveSharedImage(imagebitmap, getActivity(), description);
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }
                floatingActionMenu.close(true);*/

                IsImageSaveAPI("share");
            }
        });

        book_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
                    if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                        setBookmarkdata();
                    } else {
                        startActivityForResult(new Intent(getActivity(), LoginRegistrationActivity.class), REQUEST_CODE_BOOKMARK);
                    }
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }
                floatingActionMenu.close(true);
            }
        });

        retryLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                retryLay.setVisibility(View.GONE);
                /*final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .imageScaleType(ImageScaleType.NONE)
                        .displayer(new FadeInBitmapDisplayer(300))
                        .bitmapConfig(Bitmap.Config.ARGB_4444)
                        .build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                        getActivity())
                        .defaultDisplayImageOptions(opts)
                        .memoryCache(new WeakMemoryCache())
                        .build();

                if (!ImageLoader.getInstance().isInited())
                    ImageLoader.getInstance().init(config);
                loadImages(image, opts,"click");*/
                loadImages(image,"click");
            }
        });

        /*final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE)
                .displayer(new FadeInBitmapDisplayer(300))
                .bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .defaultDisplayImageOptions(opts)
                .memoryCache(new WeakMemoryCache())
                .build();

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(config);
*/
        loadImages(image,"");

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadImages(image,"");
            }
        });

        //==========================
        return v;
    }

    public void loadImages(final ImageView image,String chkClick) {
        txt_loader.setText("Please wait...");

        String imgPath = "http://app.dbclmatrix.com/nopicture.png",
                tmpImgThumb = "http://app.dbclmatrix.com/nopicture.png";

        if (masteDetailModel != null) {
            if (masteDetailModel.imagepath != null) {
                imgPath = masteDetailModel.imagepath;
//            } else if (masteDetailModel.imagepath != null) {
//                imgPath = masteDetailModel.imagepath;
            }
            if (masteDetailModel.imagethumb != null) {
                tmpImgThumb = masteDetailModel.imagethumb;
            }
        }

        String urls="";
        /*if (chkClick.equalsIgnoreCase("click"))
            urls =imgPath;
        else
            urls = "";*/
        // final String imgThumb = tmpImgThumb;
        imgThumb = tmpImgThumb;
        //FINAL_IMAGLOAD_CODE
        Glide.with(this)
                .asBitmap()
                .load(imgPath)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);
                        /*DisplayImageOptions opts = new DisplayImageOptions.Builder()
                                .cacheInMemory(true)
                                .cacheOnDisk(true)
                                .imageScaleType(ImageScaleType.NONE)
                                .displayer(new FadeInBitmapDisplayer(300))
                                .bitmapConfig(Bitmap.Config.ARGB_4444)
                                .build();
                        count=count+1;
                        if (count==1){
                            retryLay.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
*/
                        retryLay.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                            loadImages(image,"");
//                            count=0;
                       /* }
                        else {
                            loadImages(image,opts,"");
                        }*/
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);
                        imagebitmap = resource;
                        return false;
                    }
                })
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                        progressBar.setVisibility(View.GONE);

                        swipeLayout.setRefreshing(false);
                        swipeLayout.setEnabled(false);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });

       /*
        Glide.with(this)
                .asDrawable()
                .load(imgPath)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.splash_bg)
                .dontTransform()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);

                        return false;
                    }
                })
                .into(image);*/


       /* ImageLoader.getInstance().displayImage(imgPath, image, opts, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {

                if (swipeLayout.isRefreshing()) {
                    swipeLayout.setRefreshing(false);
                }

                //preview
                imgPlaceholder.setVisibility(View.VISIBLE);

                progressBar.setVisibility(View.VISIBLE);
                ImageUtil.setImage(getActivity(), imgThumb, 0, imgPlaceholder);
                 //preview
                txt_loader.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                @SuppressWarnings("ThrowableResultOfMethodCallIgnored")

                Throwable cause = failReason.getCause();
                String message = cause != null ? cause.getMessage() : failReason.getType().name();

                // preview
                txt_loader.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                imagebitmap = loadedImage;

                image.setImageBitmap(imagebitmap);
                image.setVisibility(View.VISIBLE);

                swipeLayout.setRefreshing(false);
                swipeLayout.setEnabled(false);

                // preview
                txt_loader.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                imgPlaceholder.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {
            }
        });*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_BOOKMARK && resultCode == Activity.RESULT_OK) {
            if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                setBookmarkdata();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void setBookmarkdata() {
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait ...");
        pd.setCanceledOnTouchOutside(false);
        String email = mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, "");
        String edition_name = masteDetailModel.page;
        String edition_date = Utility.getCurrentDate();
        String pageno = masteDetailModel.pageno;
        String org_url = masteDetailModel.imagepath;
        String thum_url = masteDetailModel.imagethumb;
        String larg_url = masteDetailModel.imagelarge;

        mApp.getServices().setBookmarkData(email, edition_name, edition_date, org_url, thum_url, larg_url, pageno, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                String res = jsonElement.toString();
                pd.dismiss();
                if (res.contains("1")) {
                    Utility.setToast(getActivity(), "Successfully Bookmarked");
                } else {
                    if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                        Utility.setToast(getActivity(), "Bookmark Already Exist...");
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
                Utility.setNetworkErrorToast(getActivity());
            }
        });
    }

    //API IS IMAGE SAVE
    public void IsImageSaveAPI(final String chkClick) {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", mPref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
            jsonObject.put("img", imgThumb);
            jsonObject.put("type", chkClick);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.IS_IMAGE_SAVE, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !getActivity().isFinishing()) {
                    pd.dismiss();
                }

                try {
                    String img = response.getString("img");
                    //String img = "";
                    if (img != null)
                    {
                        if (!img.equalsIgnoreCase("")) {
                            if (imagebitmap != null) {
                                switch (chkClick) {
                                    case "download":
                                        ImageUtility.saveImage(imagebitmap, getActivity());
                                        break;
                                    case "share":
                                        if (imagebitmap != null) {
                                            String description = getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + masteDetailModel.description + " Page No:" + masteDetailModel.pageno;
                                            ImageUtility.saveSharedImage(imagebitmap, getActivity(), description);
                                        } else {
                                            Utility.setToast(getActivity(), "Please Wait For Image Download");
                                        }
                                        floatingActionMenu.close(true);
                                        break;
                                    case "crop":
                                        if (ImageCropperShareActivity.isShowTime) {
                                            ImageCropperShareActivity.imagebitmap = imagebitmap;
                                            String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                                            startActivity(
                                                    new Intent(getActivity(), ImageCropperShareActivity.class)
                                                            .putExtra("editioncode", masteDetailModel.editioncode)
                                                            .putExtra("pageno", masteDetailModel.pageno)
                                                            .putExtra("description", description)
                                                            .putExtra("editionname", masteDetailModel.description)
                                                            .putExtra("imgpath", masteDetailModel.imagepath)
                                            );
                                        } else {
                                            if (imagebitmap != null) {
                                                ImageCropperActivity.imagebitmap = imagebitmap;
                                                String description = masteDetailModel.description + " Page " + masteDetailModel.pageno;
                                                startActivity(
                                                        new Intent(getActivity(), ImageCropperActivity.class)
                                                                .putExtra("pageno", masteDetailModel.pageno)
                                                                .putExtra("description", description)
                                                                .putExtra("editionname", masteDetailModel.description)
                                                                .putExtra("imgpath", masteDetailModel.imagepath)
                                                                .putExtra("pageDate", ""));

                                            } else {
                                                Utility.setToast(getActivity(), "Please Wait For Image Download");
                                            }
                                        }

                                        floatingActionMenu.close(true);
                                        break;
                                }

                            } else {
                                Utility.setToast(getActivity(), "Please Wait For Image Download");
                            }
                        } else {
                            showSaveImgDialog(chkClick, response.getString("message"));
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                floatingActionMenu.close(true);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!getActivity().isFinishing()) {
            pd.show();
        }
    }

    private void showSaveImgDialog(String chkClick, String message) {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.save_image_dialog, null);
        exitDialogBuilder.setView(dialogView);
        TextView mszTxt = dialogView.findViewById(R.id.mszTxt);
        TextView titleDialog = dialogView.findViewById(R.id.titleDialog);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);
        final AlertDialog mszDialog = exitDialogBuilder.create();
        mszDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mszDialog.setCancelable(true);
        String upperString = chkClick.substring(0, 1).toUpperCase() + chkClick.substring(1);
        titleDialog.setText(upperString + " Epaper");
        mszTxt.setText(message);
        if (okBtn != null)
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mszDialog.dismiss();

                }
            });

        mszDialog.show();
    }
}
