package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazineRecyclerAdapter;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.Utility;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class MagazinePagerFragment extends Fragment {
    private AVLoadingIndicatorView progressBar;
    private List<MagazineMasterModel> modelList = new ArrayList<>();
    private String language="";

    public MagazinePagerFragment() {
    }

    @SuppressLint("ValidFragment")
    public MagazinePagerFragment(List<MagazineMasterModel> pages,String lang) {
        this.modelList = pages;
        this.language =lang;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.activity_sub_magazine_master, container, false);
        progressBar = rootView.findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(INPUT_METHOD_SERVICE);
                //hides keyboard
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
                //checks edittext content
                return false;
            }
        });

        RecyclerView recycler = rootView.findViewById(R.id.homeRecyclerList);
        int columns = Epaper.getInstance().getModelManager().getColumnCount();//Utility.calculateNoOfColumns(getActivity());
        recycler.setLayoutManager(new GridLayoutManager(getActivity(), columns));
        Collections.sort(modelList, new Comparator<MagazineMasterModel>() {
            public int compare(MagazineMasterModel o1, MagazineMasterModel o2) {
                if (o1 == null || o1.getPagedate() == null || o2 == null || o2.getPagedate() == null)
                    return 0;
                return o1.getPagedate().compareTo(o2.getPagedate());
            }
        });
        Collections.reverse(modelList);
        recycler.setAdapter(new MagazineRecyclerAdapter(getActivity(), modelList,language, true));
        progressBar.setVisibility(View.GONE);
        return rootView;
    }

}