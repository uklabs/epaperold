package com.bhaskar.epaper.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.ui.ImageCropperActivity;
import com.bhaskar.epaper.ui.ImageCropperShareActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

public class MagazinePageDetailViewpager extends Fragment {


    private FloatingActionButton save_menu_btn;
    private FloatingActionButton share_menu_btn;
    private FloatingActionButton book_menu_btn;
    private FloatingActionMenu menuItem;
    private AVLoadingIndicatorView progressBar;
    private SwipeRefreshLayout swipeLayout;
    private TextView txt_loader;
    private FloatingActionButton crop_menu_btn;

    Epaper mApp;
    EpaperPrefernces mPref;
    MagazineMasterModel masteDetailModel;
    Bitmap imagebitmap = null;
    private ProgressDialog pd;
    private String imgThumb="";

    public MagazinePageDetailViewpager() {
    }


    @SuppressLint("ValidFragment")
    public MagazinePageDetailViewpager(MagazineMasterModel magazineMasterModel) {
        this.masteDetailModel = magazineMasterModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pagerlayout, container, false);
        v.setSaveFromParentEnabled(false);
        Log.e("Called ", "magazine page detail");

        mApp = (Epaper) getActivity().getApplicationContext();
        mPref = EpaperPrefernces.getInstance(getActivity());

        save_menu_btn = v.findViewById(R.id.save_menu_btn);
        share_menu_btn = v.findViewById(R.id.share_menu_btn);
        book_menu_btn = v.findViewById(R.id.book_menu_btn);
        menuItem = v.findViewById(R.id.menu_item);
        progressBar = v.findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        swipeLayout = v.findViewById(R.id.swipeLayout);
        txt_loader = v.findViewById(R.id.txt_loader);
        crop_menu_btn = v.findViewById(R.id.crop_menu_btn);
        menuItem.removeMenuButton(book_menu_btn);

        final ImageViewTouch image = v.findViewById(R.id.pager_imageview);

        crop_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsImageSaveAPI("crop");
              /*  if (ImageCropperShareActivity.isShowTime) {
                    ImageCropperShareActivity.imagebitmap = imagebitmap;
                    String description = *//*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + *//*masteDetailModel.getDescription() + masteDetailModel.getPageNo();
                    Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                    startActivity(new Intent(getActivity(), ImageCropperShareActivity.class)
                            .putExtra("imgpath", masteDetailModel.getImagelarge())
                            .putExtra("pageno", masteDetailModel.getPageNo())
                            .putExtra("description", description)
                            .putExtra("editioncode", masteDetailModel.getEditioncode())
                            .putExtra("editionname", masteDetailModel.getDescription())
                    );
                } else {
                    if (imagebitmap != null) {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        String description = *//*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + *//*masteDetailModel.getDescription() + masteDetailModel.getPageNo();
                        Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                        startActivity(new Intent(getActivity(), ImageCropperActivity.class).putExtra("imgpath", masteDetailModel.getImagelarge()).putExtra("pageno", masteDetailModel.getPageNo()).putExtra("description", description).putExtra("editionname", masteDetailModel.getMagazine()).putExtra("pageDate", masteDetailModel.getPagedate()));

//                    startActivity(new Intent(getActivity(), ImageCropperActivity.class));
                    } else {
                        Utility.setToast(getActivity(), "Please Wait For Image Download");
                    }
                }*/

            }
        });

        save_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsImageSaveAPI("download");
                /*if (imagebitmap != null) {
//                    ImageUtility.saveImage(ImageCropperActivity.imagebitmap, getActivity());
                    ImageUtility.saveImage(imagebitmap, getActivity());
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }*/
            }
        });

        share_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IsImageSaveAPI("share");
               /* if (imagebitmap != null) {
                    String description = getResources().getString(R.string.dainikbhaskar) + "\n Edition" + masteDetailModel.getDescription() + " Page No:" + masteDetailModel.getPagedate();
                    ImageUtility.saveSharedImage(imagebitmap, getActivity(), description);
                } else {
                    Utility.setToast(getActivity(), "Please Wait For Image Download");
                }*/

            }
        });


//==================================== UNIVERSAL IMAGE ===========================
/*        final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE).bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getActivity())
                .defaultDisplayImageOptions(opts)
                .memoryCache(new WeakMemoryCache())
                .build();

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(config);*/

        loadImages(image);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadImages(image);
            }
        });

        return v;
    }

   /* public void loadImage(ImageViewTouch image, DisplayImageOptions options) {
        if (masteDetailModel != null) {
            ImageLoader.getInstance().displayImage(masteDetailModel.getImagepath(), image, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.e("image", "=>" + imageUri);
                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")

                    Throwable cause = failReason.getCause();
                    String message = cause != null ? cause.getMessage() : failReason.getType().name();
                    Toast.makeText(Epaper.getInstance(), "Error: " + message, Toast.LENGTH_LONG).show();
                    swipeLayout.setRefreshing(true);
                    swipeLayout.setEnabled(true);


                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
               *//* pbPercent.setVisibility(View.GONE);
                tvPerc.setVisibility(View.GONE);*//*
                    imagebitmap = loadedImage;
                    progressBar.setVisibility(View.GONE);
                    txt_loader.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    swipeLayout.setRefreshing(false);
                    swipeLayout.setEnabled(false);

                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
//                Toast.makeText(getActivity(), this.getClass().getSimpleName() + " : OnLoadingCancelled", Toast.LENGTH_LONG).show();
//                doBar();
                    if (swipeLayout.isRefreshing()) {
                        swipeLayout.setRefreshing(false);
                    }

                }
                // NOTE: .cacheOnDisk(true) must be set in the options or this Listener won't work
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {
//                    int percent = (current * 100) / 1112000;
//                    progressBar.setProgress(percent);

                }
            });
        }
    }*/

    public void loadImages(final ImageView image) {
        txt_loader.setText("Please wait...");

        String imgPath = "http://app.dbclmatrix.com/nopicture.png",
                tmpImgThumb = "http://app.dbclmatrix.com/nopicture.png";

        if (masteDetailModel != null) {
            if (masteDetailModel.getImagepath() != null) {
                imgPath = masteDetailModel.getImagepath();
//            } else if (masteDetailModel.imagepath != null) {
//                imgPath = masteDetailModel.imagepath;
            }
            if (masteDetailModel.getImagethumb() != null) {
                tmpImgThumb = masteDetailModel.getImagethumb();
            }
        }

        String urls = "";
        /*if (chkClick.equalsIgnoreCase("click"))
            urls =imgPath;
        else
            urls = "";*/
        // final String imgThumb = tmpImgThumb;
        imgThumb = tmpImgThumb;
        //FINAL_IMAGLOAD_CODE
        Glide.with(this)
                .asBitmap()
                .load(imgPath)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);
                        /*DisplayImageOptions opts = new DisplayImageOptions.Builder()
                                .cacheInMemory(true)
                                .cacheOnDisk(true)
                                .imageScaleType(ImageScaleType.NONE)
                                .displayer(new FadeInBitmapDisplayer(300))
                                .bitmapConfig(Bitmap.Config.ARGB_4444)
                                .build();
                        count=count+1;
                        if (count==1){
                            retryLay.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
*/
                        txt_loader.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        loadImages(image);
//                            count=0;
                       /* }
                        else {
                            loadImages(image,opts,"");
                        }*/
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        swipeLayout.setRefreshing(false);
                        imagebitmap = resource;
                        return false;
                    }
                })
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                        progressBar.setVisibility(View.GONE);

                        swipeLayout.setRefreshing(false);
                        swipeLayout.setEnabled(false);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }
    //API IS IMAGE SAVE
    public void IsImageSaveAPI(final String chkClick)
    {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", mPref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
            jsonObject.put("img", imagebitmap);
            jsonObject.put("type", chkClick);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.IS_IMAGE_SAVE, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !getActivity().isFinishing()) {
                    pd.dismiss();
                }

                try {
                    String img = response.getString("img");
                    //String img = "";
                    if (img != null) {
                        if (!img.equalsIgnoreCase("")) {
                            if (imagebitmap != null) {
                                switch (chkClick) {
                                    case "download":
                                        if (imagebitmap != null) {
//                    ImageUtility.saveImage(ImageCropperActivity.imagebitmap, getActivity());
                                            ImageUtility.saveImage(imagebitmap, getActivity());
                                        } else {
                                            Utility.setToast(getActivity(), "Please Wait For Image Download");
                                        }
                                        break;
                                    case "share":
                                        if (imagebitmap != null) {
                                            String description = getResources().getString(R.string.dainikbhaskar) + "\n Edition" + masteDetailModel.getDescription() + " Page No:" + masteDetailModel.getPagedate();
                                            ImageUtility.saveSharedImage(imagebitmap, getActivity(), description);
                                        } else {
                                            Utility.setToast(getActivity(), "Please Wait For Image Download");
                                        }
                                        break;
                                    case "crop":
                                        if (ImageCropperShareActivity.isShowTime) {
                                            ImageCropperShareActivity.imagebitmap = imagebitmap;
                                            String description = /*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + */masteDetailModel.getDescription() + masteDetailModel.getPageNo();
                                            Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                                            startActivity(new Intent(getActivity(), ImageCropperShareActivity.class)
                                                    .putExtra("imgpath", masteDetailModel.getImagelarge())
                                                    .putExtra("pageno", masteDetailModel.getPageNo())
                                                    .putExtra("description", description)
                                                    .putExtra("editioncode", masteDetailModel.getEditioncode())
                                                    .putExtra("editionname", masteDetailModel.getDescription())
                                            );
                                        } else {
                                            if (imagebitmap != null) {
                                                ImageCropperActivity.imagebitmap = imagebitmap;
                                                String description = /*getResources().getString(R.string.dainikbhaskar) + "\n Edition: " + */masteDetailModel.getDescription() + masteDetailModel.getPageNo();
                                                Log.e("ImageURL Magazine", masteDetailModel.getImagepath());

                                                startActivity(new Intent(getActivity(), ImageCropperActivity.class).putExtra("imgpath", masteDetailModel.getImagelarge()).putExtra("pageno", masteDetailModel.getPageNo()).putExtra("description", description).putExtra("editionname", masteDetailModel.getMagazine()).putExtra("pageDate", masteDetailModel.getPagedate()));

//                    startActivity(new Intent(getActivity(), ImageCropperActivity.class));
                                            } else {
                                                Utility.setToast(getActivity(), "Please Wait For Image Download");
                                            }
                                        }
                                        break;
                                }

                            } else {
                                Utility.setToast(getActivity(), "Please Wait For Image Download");
                            }
                        } else {
                            showSaveImgDialog(chkClick, response.getString("message"));
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!getActivity().isFinishing()) {
            pd.show();
        }
    }

    private void showSaveImgDialog(String chkClick, String message) {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.save_image_dialog, null);
        exitDialogBuilder.setView(dialogView);
        TextView mszTxt = dialogView.findViewById(R.id.mszTxt);
        TextView titleDialog = dialogView.findViewById(R.id.titleDialog);
        TextView okBtn = dialogView.findViewById(R.id.okBtn);
        final AlertDialog mszDialog = exitDialogBuilder.create();
        mszDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mszDialog.setCancelable(true);
        String upperString = chkClick.substring(0, 1).toUpperCase() + chkClick.substring(1);
        titleDialog.setText(upperString + " Epaper");
        mszTxt.setText(message);
        if (okBtn != null)
            okBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mszDialog.dismiss();

                }
            });

        mszDialog.show();
    }
}
