package com.bhaskar.epaper.manager;

import com.bhaskar.epaper.epaperv2.payment.PaymentSubscriptionBean;
import com.bhaskar.epaper.model.KeyBenefitsInfo;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.model.MainDataModel;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.model.SearchModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelManager {

    private static ModelManager ourInstance = null;

    private List<MainDataModel> stateList = new ArrayList<>();
  private List<String> holidayList = new ArrayList<>();
  private String holidaymsz="";
    private Map<String, PaymentSubscriptionBean.Data.Plans> stringPlansMap = new HashMap<>();
    private ArrayList<SearchModel> searchList = new ArrayList<>();
    private List<MasterPageModel> listdata = new ArrayList<>();
    private List<MagazineMasterModel> masterModels = new ArrayList<>();
    private Map<String, List<KeyBenefitsInfo>> stringListMap = new HashMap<>();
    private ArrayList<String> allowStateList = new ArrayList<>();
    private ArrayList<String> allowMagazineList = new ArrayList<>();
    private PaymentSubscriptionBean mPaymentSubscriptionBean;
    private int columnCount = 2;

    public static ModelManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new ModelManager();
        }
        return ourInstance;
    }

    private ModelManager() {
    }

    public List<MainDataModel> getStateList() {
        if (stateList == null)
            return new ArrayList<>();
        return stateList;
    }

    public void setStateList(List<MainDataModel> stateList) {
        this.stateList.clear();
        this.stateList.addAll(stateList);
    }

    public void setYearlyPlan(PaymentSubscriptionBean.Data.Plans localPlan, String channelId) {
        this.stringPlansMap.put(channelId, localPlan);

    }

    public PaymentSubscriptionBean.Data.Plans getYearlyPlan(String channelId) {
        return this.stringPlansMap.get(channelId);
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public void setSearchList(ArrayList<SearchModel> searchList) {
        this.searchList = searchList;
    }

    public ArrayList<SearchModel> getSearchList() {
        return searchList;
    }

    public List<MasterPageModel> getListdata() {
        return listdata;
    }

    public void setListdata(List<MasterPageModel> listdata) {
        this.listdata = listdata;
    }

    public List<MagazineMasterModel> getMasterModels() {
        return masterModels;
    }

    public void setMasterModels(List<MagazineMasterModel> masterModels) {
        this.masterModels = masterModels;
    }

    public List<KeyBenefitsInfo> getStringList(String channelId) {
        return stringListMap.get(channelId);
    }

    public void setStringListMap(String channelId, List<KeyBenefitsInfo> stringList) {
        this.stringListMap.put(channelId, stringList);
    }

    public ArrayList<String> getAllowStateList() {
        return allowStateList;
    }

    public void setAllowStateList(ArrayList<String> allowStateList) {
        this.allowStateList = allowStateList;
    }

    public ArrayList<String> getAllowMagazineList() {
        return allowMagazineList;
    }

    public void setAllowMagazineList(ArrayList<String> allowMagazineList) {
        this.allowMagazineList = allowMagazineList;
    }

    public PaymentSubscriptionBean getmPaymentSubscriptionBean() {
        return mPaymentSubscriptionBean;
    }

    public void setmPaymentSubscriptionBean(PaymentSubscriptionBean mPaymentSubscriptionBean) {
        this.mPaymentSubscriptionBean = mPaymentSubscriptionBean;
    }

    public List<String> getHolidayList() {
        return holidayList;
    }

    public void setHolidayList(List<String> holidayList) {
        this.holidayList = holidayList;
    }

    public String getHolidaymsz() {
        return holidaymsz;
    }

    public void setHolidaymsz(String holidaymsz) {
        this.holidaymsz = holidaymsz;
    }

    public static void clearAllData() {
       /* stateList.clear();
        yearlyPlan = null;
        searchList.clear();
        listdata.clear();
        masterModels.clear();*/
        ourInstance = null;
    }
}
