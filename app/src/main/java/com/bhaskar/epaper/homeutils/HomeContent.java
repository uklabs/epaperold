package com.bhaskar.epaper.homeutils;

import com.bhaskar.epaper.model.MasterModel;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by db on 8/29/2R.drawable.ic_mytodo16.
 */
public class HomeContent implements Serializable {

    public static HomeContent homeContent = null;

    public static HomeContent getHomeContent() {
        if (homeContent == null) {
            homeContent = new HomeContent();
        }
        return homeContent;
    }

    public HomeContent() {
    }

    public Map<String, List<MasterModel>> listMain = new HashMap<>();


}
