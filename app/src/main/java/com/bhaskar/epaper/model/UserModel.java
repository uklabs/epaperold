package com.bhaskar.epaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by db on 12/9/2016.
 */
public class UserModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("dat")
    @Expose
    private String dat;
    @SerializedName("last_visited")
    @Expose
    private String lastVisited;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("mob_verf")
    @Expose
    private String mobVerf;
    @SerializedName("mob_verf_date")
    @Expose
    private String mobVerfDate;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("otp_date")
    @Expose
    private String otpDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDat() {
        return dat;
    }

    public void setDat(String dat) {
        this.dat = dat;
    }

    public String getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(String lastVisited) {
        this.lastVisited = lastVisited;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMobVerf() {
        return mobVerf;
    }

    public void setMobVerf(String mobVerf) {
        this.mobVerf = mobVerf;
    }

    public String getMobVerfDate() {
        return mobVerfDate;
    }

    public void setMobVerfDate(String mobVerfDate) {
        this.mobVerfDate = mobVerfDate;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpDate() {
        return otpDate;
    }

    public void setOtpDate(String otpDate) {
        this.otpDate = otpDate;
    }
}
