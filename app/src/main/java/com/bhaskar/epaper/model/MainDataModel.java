package com.bhaskar.epaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MainDataModel implements Serializable {
    @SerializedName("statename")
    @Expose
    public String statename;

//    @SerializedName("languagecode")
//    @Expose
//    public String languagecode;

    @SerializedName("channel_slno")
    @Expose
    public String channelSlno;

    @SerializedName("paid_flag")
    @Expose
    public String paidFlag;

    public MainDataModel(String statename) {
        this.statename = statename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainDataModel that = (MainDataModel) o;
        return statename.equalsIgnoreCase(that.statename);
    }
}
