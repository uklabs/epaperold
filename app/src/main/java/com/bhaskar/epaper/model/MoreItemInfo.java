package com.bhaskar.epaper.model;

public class MoreItemInfo {
    public int imageId;
    public String name;
    public String action;

    public MoreItemInfo(String name, int imageId, String action) {
        this.name = name;
        this.imageId = imageId;
        this.action = action;
    }


}
