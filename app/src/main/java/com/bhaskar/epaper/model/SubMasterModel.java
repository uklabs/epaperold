package com.bhaskar.epaper.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by db on 9/15/2016.
 */
public class SubMasterModel implements Parcelable {

    public String description;
    public String priorty;
    public String editioncode;
    public String parent;
    public String imagepath;
    public String imagethumb;
    public String imagelarge;

    public SubMasterModel(String description, String priorty,String editioncode, String parent, String imagepath, String imagethumb, String imagelarge) {
        this.parent = parent;
        this.editioncode = editioncode;
        this.description = description;
        this.priorty = priorty;
        this.imagepath = imagepath;
        this.imagethumb = imagethumb;
        this.imagelarge = imagelarge;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeString(this.priorty);
        dest.writeString(this.editioncode);
        dest.writeString(this.parent);
        dest.writeString(this.imagepath);
        dest.writeString(this.imagethumb);
        dest.writeString(this.imagelarge);
    }

    protected SubMasterModel(Parcel in) {
        this.description = in.readString();
        this.priorty = in.readString();
        this.editioncode = in.readString();
        this.parent = in.readString();
        this.imagepath = in.readString();
        this.imagethumb = in.readString();
        this.imagelarge = in.readString();
    }

    public static final Parcelable.Creator<SubMasterModel> CREATOR = new Parcelable.Creator<SubMasterModel>() {
        @Override
        public SubMasterModel createFromParcel(Parcel source) {
            return new SubMasterModel(source);
        }

        @Override
        public SubMasterModel[] newArray(int size) {
            return new SubMasterModel[size];
        }
    };
}
