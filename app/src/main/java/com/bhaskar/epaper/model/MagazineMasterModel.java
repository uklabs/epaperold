package com.bhaskar.epaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by db on 1/16/2017.
 */
public class MagazineMasterModel {

    @SerializedName("magazine")
    @Expose
    private String magazine;

    public String getChannelSlno() {
        return channelSlno;
    }

    public void setChannelSlno(String channelSlno) {
        this.channelSlno = channelSlno;
    }

    @SerializedName("channel_slno")
    @Expose
    private String channelSlno;
    @SerializedName("editioncode")
    @Expose
    private String editioncode;
    @SerializedName("pagedate")
    @Expose
    private String pagedate;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("priorty")
    @Expose
    private Integer priorty;
    @SerializedName("imagepath")
    @Expose
    private String imagepath;
    @SerializedName("imagethumb")
    @Expose
    private String imagethumb;
    @SerializedName("imagelarge")
    @Expose
    private String imagelarge;


    private String PageNo = "0";


    public String getPageNo() {
        return PageNo;
    }

    public void setPageNo(String pageNo) {
        PageNo = pageNo;
    }

    public String getMagazine() {
        return magazine;
    }

    public void setMagazine(String magazine) {
        this.magazine = magazine;
    }

    public String getEditioncode() {
        return editioncode;
    }

    public void setEditioncode(String editioncode) {
        this.editioncode = editioncode;
    }

    public String getPagedate() {
        return pagedate;
    }

    public void setPagedate(String pagedate) {
        this.pagedate = pagedate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriorty() {
        return priorty;
    }

    public void setPriorty(Integer priorty) {
        this.priorty = priorty;
    }

    public String getImagepath() {
        return imagepath;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public String getImagethumb() {
        return imagethumb;
    }

    public void setImagethumb(String imagethumb) {
        this.imagethumb = imagethumb;
    }

    public String getImagelarge() {
        return imagelarge;
    }

    public void setImagelarge(String imagelarge) {
        this.imagelarge = imagelarge;
    }
}
