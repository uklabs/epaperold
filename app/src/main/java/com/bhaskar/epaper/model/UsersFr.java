package com.bhaskar.epaper.model;

import java.io.Serializable;

/**
 * Created by db on 10/14/2016.
 */
public class UsersFr implements Serializable {
    public String gcmkey;
    public String udid;
    public String userEmail;
    public String username;
    public String photoUrl;
    public String userId;


    public UsersFr() {
    }

    public UsersFr(String gcmkey, String udid, String userEmail, String username, String photoUrl, String userId) {
        this.gcmkey = gcmkey;
        this.udid = udid;
        this.userEmail = userEmail;
        this.username = username;
        this.photoUrl = photoUrl;
        this.userId = userId;
    }

    public UsersFr(String gcmkey, String udid) {
        this.gcmkey = gcmkey;
        this.udid = udid;
    }

    public String getGcmkey() {
        return gcmkey;
    }

    public void setGcmkey(String gcmkey) {
        this.gcmkey = gcmkey;
    }

    public String getUdid() {
        return udid;
    }

    public void setUdid(String udid) {
        this.udid = udid;
    }
}
