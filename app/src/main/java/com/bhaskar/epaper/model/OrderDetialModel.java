package com.bhaskar.epaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class OrderDetialModel {
    private String status;
    private String code;
    private String title;
    private String price;
    private String discount;
    private String subtotal;
    private String taxes;
    private String taxes2;
    private String total;
    private String currency;
    private String price_title;
    private String discount_title;
    private String subtotal_title;
    private String taxes_title;
    private String total_title;

    @SerializedName("desc")
    @Expose
    private DescModel descModel;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }

    public String getTaxes2() {
        return taxes2;
    }

    public void setTaxes2(String taxes2) {
        this.taxes2 = taxes2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrice_title() {
        return price_title;
    }

    public void setPrice_title(String price_title) {
        this.price_title = price_title;
    }

    public String getDiscount_title() {
        return discount_title;
    }

    public void setDiscount_title(String discount_title) {
        this.discount_title = discount_title;
    }

    public String getSubtotal_title() {
        return subtotal_title;
    }

    public void setSubtotal_title(String subtotal_title) {
        this.subtotal_title = subtotal_title;
    }

    public String getTaxes_title() {
        return taxes_title;
    }

    public void setTaxes_title(String taxes_title) {
        this.taxes_title = taxes_title;
    }

    public String getTotal_title() {
        return total_title;
    }

    public void setTotal_title(String total_title) {
        this.total_title = total_title;
    }

    public DescModel getDescModel() {
        return descModel;
    }

    public void setDescModel(DescModel descModel) {
        this.descModel = descModel;
    }
}
