package com.bhaskar.epaper.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DescModel {

    String desc;

     @SerializedName("list")
    @Expose
    private ArrayList<ListModel> listModelArrayList;


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public ArrayList<ListModel> getListModelArrayList() {
        return listModelArrayList;
    }

    public void setListModelArrayList(ArrayList<ListModel> listModelArrayList) {
        this.listModelArrayList = listModelArrayList;
    }
}
