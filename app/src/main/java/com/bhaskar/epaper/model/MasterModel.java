package com.bhaskar.epaper.model;

import java.io.Serializable;

/**
 * Created by db on 9/15/2016.
 */
public class MasterModel implements Serializable {
    public String editioncode;
    public String description;
    public String imagepath;
    public String imagethumb;
    public String imagelarge;
    public String id;
    public String channelSlno;
    public String priorty;
    public MasterModel(String id, String pageno, String edition, String org_url, String thumb_url, String large_url, String s) {
        this.id = id;
        this.editioncode = pageno;
        this.imagepath = org_url;
        this.imagethumb = thumb_url;
        this.imagelarge = large_url;
        this.description = edition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterModel that = (MasterModel) o;
        return (editioncode != null ? editioncode.equals(that.editioncode) : that.editioncode == null);
    }
}
