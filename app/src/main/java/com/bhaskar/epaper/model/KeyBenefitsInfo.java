package com.bhaskar.epaper.model;

import com.google.gson.annotations.SerializedName;

public class KeyBenefitsInfo {
    @SerializedName("name")
    public String name;
    @SerializedName("desc")
    public String desc;
    @SerializedName("icon")
    public String imagePath;
    
    public boolean isHeader = false;

    public KeyBenefitsInfo(String name, String desc, String imagePath) {
        this.name = name;
        this.desc = desc;
        this.imagePath = imagePath;
    }

    public KeyBenefitsInfo(boolean isHeader) {
        this.isHeader = isHeader;
    }
}
