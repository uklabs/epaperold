package com.bhaskar.epaper;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.services.BaseServices;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp.StethoInterceptor;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.okhttp.OkHttpClient;

import io.fabric.sdk.android.Fabric;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by db on 9/15/2016.
 */
public class Epaper extends Application {

    BaseServices services;
    public static String BASEURL = "https://appfeedlight.bhaskar.com/epaper";
    public static String NO_IMG_FOUND = "https://i10.dainikbhaskar.com/epaper/nopicture.png";
    private static Epaper mInstance;
    //private Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

//        AnalyticsTrackers.initialize(this);
        //AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

        initRetrofitApiClient();

        initStetho();

        Fabric.with(this, new Crashlytics());
        try {
            String publisherId = getString(R.string.comscore_publisher_id);
            String secretId = getString(R.string.comscore_secret_id);
            boolean isBlockedCountry = EpaperPrefernces.getInstance(this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
            Util.initialiseComscore(this, publisherId, secretId, isBlockedCountry);
        } catch (Exception e) {
        }
    }

    public void initRetrofitApiClient() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.networkInterceptors().add(new StethoInterceptor());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASEURL)
                .setClient(new OkClient(okHttpClient))
                .build();
        services = restAdapter.create(BaseServices.class);
    }

    private void initRetrofitApiClientOne() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.networkInterceptors().add(new StethoInterceptor());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASEURL)
                .setClient(new OkClient(okHttpClient))
                .build();
        services = restAdapter.create(BaseServices.class);
    }

    private void initStetho() {
        if (Urls.IS_TESTING) {
            Stetho.initializeWithDefaults(this);
        }
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public BaseServices getServices() {
        return services;
    }

    public static synchronized Epaper getInstance() {
        return mInstance;
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        Tracker defaultTracker = analyticsTrackers.get(AnalyticsTrackers.Target.APP);
        EpaperPrefernces epaperPrefernces = EpaperPrefernces.getInstance(getApplicationContext());
        /*cd1 user type*/
        int userType = epaperPrefernces.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        if (userType > 0)
            defaultTracker.set("&cd1", Util.getUserType(this));
        /*cd2 userId*/
        String userId = epaperPrefernces.getStringValue(QuickPreferences.UserInfo.USER_ID, "");
        if (!TextUtils.isEmpty(userId))
            defaultTracker.set("&cd2", userId);

        return defaultTracker;
    }

    public void trackScreenView(Context context,String screenName) {
        try {
            Systr.println("Tracking : GA Screen :- " + screenName);

           /* Tracker t = getGoogleAnalyticsTracker();
            t.setScreenName(screenName);
            t.send(new HitBuilders.ScreenViewBuilder().build());
            GoogleAnalytics.getInstance(this).dispatchLocalHits();*/

            // GTM
            Bundle bundle = new Bundle();
            bundle.putString("ga_screen_name", screenName);


            FirebaseAnalytics instance = FirebaseAnalytics.getInstance(context);
            instance.logEvent("screen_visit", bundle);

            // Google Analytics
//        if (gaTracker != null) {
//            gaTracker.set("&cd4", publisherIdAndName);
//            gaTracker.setScreenName(value);
//            gaTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        }


        } catch (Exception ignored) {

        }
    }

    public void trackEvent(Context mContext,String category, String action, String label) {
        Systr.println("Tracking : GA Event :- cat : " + category + ", act : " + action + ", lab : " + label);

       /* Trackcder t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
        GoogleAnalytics.getInstance(this).dispatchLocalHits();*/

        Bundle bundle = new Bundle();
        bundle.putString("ev_cat", category);
        bundle.putString("ev_action", action);
        bundle.putString("ev_label", label);

//        bundle.putString("GA_DBID", TrackingData.getDbORDeviceId(mContext));
//        bundle.putString("latest_Install_DT", TrackingData.getAppInstallDate(mContext));
//        bundle.putString("first_Install_DT", TrackingData.getFirstInstallDate(mContext));
//        bundle.putString("campaing_info", "C:" + campaign);

        FirebaseAnalytics instance = FirebaseAnalytics.getInstance(mContext);
        instance.logEvent("ev_custom", bundle);

    }

    /* instance*/
    public ModelManager getModelManager() {
        return ModelManager.getInstance();
    }

    public String getChannelSlno() {
        return EpaperPrefernces.getInstance(this).getStringValue(QuickPreferences.CommonPref.CHANNEL_SLNO, "");
    }

    public void setChannelSlno(String channel) {
        EpaperPrefernces.getInstance(this).setStringValue(QuickPreferences.CommonPref.CHANNEL_SLNO, channel);
    }
}
