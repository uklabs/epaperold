package com.bhaskar.epaper.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.google.android.gms.analytics.CampaignTrackingReceiver;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;


public class ManyInstallReferrerReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            String data = intent.getStringExtra("referrer");
            try {
                data = URLDecoder.decode(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                data = null;
            }
            if (data != null) {
                Map<String, String> paramsMap = new HashMap<>();
                for (String s : data.split("&")) {
                    paramsMap.put(s.split("=")[0], s.split("=")[1]);
                }
                String utm_source = paramsMap.get(Constants.UTM_SOURCE);
                String utm_medium = paramsMap.get(Constants.UTM_MEDIUM);
                String utm_campaign = paramsMap.get(Constants.UTM_CAMPAIGN);
                // String utm_term = intent.getStringExtra("utm_term");
                // String utm_content = intent.getStringExtra("utm_content");

                // set all values when direct comes
                EpaperPrefernces prefs = EpaperPrefernces.getInstance(context);
                if (prefs.getStringValue(Constants.UTM_SOURCE, "").length() == 0 || prefs.getStringValue(Constants.UTM_CAMPAIGN, "").equalsIgnoreCase(Constants.UTM_DIRECT)) {

                    if (!utm_source.contains("(not set)"))
                        prefs.setStringValue(Constants.UTM_SOURCE_TEMP, utm_source);
                    if (!utm_medium.contains("(not set)"))
                        prefs.setStringValue(Constants.UTM_MEDIUM_TEMP, utm_medium);
                    if (!utm_campaign.contains("(not set)"))
                        prefs.setStringValue(Constants.UTM_CAMPAIGN_TEMP, utm_campaign);
                }

                new CampaignTrackingReceiver().onReceive(context, intent);
            }
        } catch (Exception e) {
        }
    }
}