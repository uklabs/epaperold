package com.bhaskar.epaper.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

public class ForceUpdateActivity extends BaseActivity {

    static final int UPDATE_REQUEST = 1;
    boolean isGdpr = false;
    TextView messageTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_update);

        TextView btnUpdateNow = findViewById(R.id.textUpdateNow);
        messageTv = findViewById(R.id.txt_dia);

        String updatePopupMessage = EpaperPrefernces.getInstance(ForceUpdateActivity.this).getStringValue(QuickPreferences.AppUpdate.UPDATE_POPUP_MESSAGE, "");
        if (!Utility.isValidString(updatePopupMessage)) {
            updatePopupMessage = getResources().getString(R.string.app_update_popup_default_message);
        }

        if (getIntent().hasExtra("isGdpr") && getIntent().getBooleanExtra("isGdpr", false)) {
            isGdpr = true;
            btnUpdateNow.setVisibility(View.GONE);
            findViewById(R.id.btn_layout).setVisibility(View.GONE);
            if (getIntent().hasExtra("message")) {
                updatePopupMessage = getIntent().getStringExtra("message");
                if (TextUtils.isEmpty(updatePopupMessage)) {
                    updatePopupMessage = getResources().getString(R.string.gdpr_default_message);
                }
            }
            messageTv.setMovementMethod(new ScrollingMovementMethod());
        }

        messageTv.setText(Html.fromHtml(updatePopupMessage));

        btnUpdateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent pickContactIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.APP_SHARE_URL));
                    startActivityForResult(pickContactIntent, UPDATE_REQUEST);
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == UPDATE_REQUEST) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
        //finish();
    }

    @Override
    public void onBackPressed() {
        if (isGdpr)
            finish();
    }
}