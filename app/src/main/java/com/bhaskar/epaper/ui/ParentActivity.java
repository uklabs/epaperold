package com.bhaskar.epaper.ui;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.utils.EpaperPrefernces;

public class ParentActivity extends AppCompatActivity {
    Epaper mApp;
    EpaperPrefernces epref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApp = (Epaper) getApplicationContext();
        epref = EpaperPrefernces.getInstance(ParentActivity.this);

    }

    public void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            try {
                final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_action_back);
                getSupportActionBar().setHomeAsUpIndicator(upArrow);
            } catch (Exception e) {

            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isBlockedCountry = EpaperPrefernces.getInstance(ParentActivity.this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
        Util.onResume(ParentActivity.this, isBlockedCountry);
    }

    @Override
    protected void onPause() {
        super.onPause();
        boolean isBlockedCountry = EpaperPrefernces.getInstance(ParentActivity.this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
        Util.onPause(ParentActivity.this, isBlockedCountry);
    }
}
