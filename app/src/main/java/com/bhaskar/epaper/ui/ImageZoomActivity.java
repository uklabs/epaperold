package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.imagezoom.ImageViewTouch;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.Utility;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.github.clans.fab.FloatingActionButton;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ImageZoomActivity extends BaseActivity {

    Epaper mApp;
    EpaperPrefernces mPref;

    String imageUrl, editioncode, editionname;

    Bitmap imagebitmap = null;
    public static MasterModel clickModel;
    ImageViewTouch image;
    Target mTarget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_imagezoom);

        FloatingActionButton crop_menu_btn = findViewById(R.id.crop_menu_btn);
        FloatingActionButton save_menu_btn = findViewById(R.id.save_menu_btn);
        FloatingActionButton share_menu_btn = findViewById(R.id.share_menu_btn);
        FloatingActionButton book_menu_btn = findViewById(R.id.book_menu_btn);

        setToolbar();

        imageUrl = getIntent().getStringExtra("largeUrl");

        editioncode = getIntent().getStringExtra("editioncode");
        editionname = getIntent().getStringExtra("editionname");

        Log.e("Zoom activity ", imageUrl);
//        imageUrl = "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSpQgSz-aG4cX1EmyuBcKQKokszgyBPJmS_OHZMZXObGC6eZDrItw";
        mPref = EpaperPrefernces.getInstance(ImageZoomActivity.this);

        image = findViewById(R.id.pager_imageview);

//        book_menu_btn.setVisibility(View.GONE);

        String url = imageUrl.replace(" ", "%20");
        Log.e("Zoom activity 1", url);
        try {
//            loadImages(url);
            loadImage(url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        book_menu_btn.setVisibility(View.GONE);


        crop_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {

                    if (ImageCropperShareActivity.isShowTime) {
                        ImageCropperShareActivity.imagebitmap = imagebitmap;
                        Log.e("name", "=> ImageCropperShareActivity" + clickModel.description + "=" + clickModel.id);
                        Intent intent = new Intent(ImageZoomActivity.this, ImageCropperShareActivity.class);
                        intent.putExtra("imgpath", imageUrl);
                        intent.putExtra("editioncode", editioncode);
                        intent.putExtra("editionname", editionname);
                        startActivity(intent);
                    } else {
                        ImageCropperActivity.imagebitmap = imagebitmap;
                        Log.e("name", "=>" + clickModel.description + "=" + clickModel.id);
                        Intent intent = new Intent(ImageZoomActivity.this, ImageCropperActivity.class);
                        intent.putExtra("imgpath", imageUrl);
                        intent.putExtra("pageDate", "");
                        startActivity(intent);
                    }

                    //startActivity(new Intent(ImageZoomActivity.this, ImageCropperActivity.class));
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }
            }
        });

        save_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
//                    ImageUtility.saveImage(ImageCropperActivity.imagebitmap, ImageZoomActivity.this);
                    ImageUtility.saveImage(imagebitmap, ImageZoomActivity.this);
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }
            }
        });

        share_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebitmap != null) {
//                    ImageUtility.saveSharedImage(ImageCropperActivity.imagebitmap, ImageZoomActivity.this);
                    ImageUtility.saveSharedImage(imagebitmap, ImageZoomActivity.this);
                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Wait For Image Download");
                }

            }
        });

        book_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                    if (clickModel != null) {
                        removeBookMark(clickModel.id);
                    }

                } else {
                    Utility.setToast(ImageZoomActivity.this, "Please Login..");
                }

            }
        });
    }

/*
    public void loadImages(String imgUrl) {
        final DisplayImageOptions opts = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.NONE)
                .displayer(new FadeInBitmapDisplayer(300))
                .bitmapConfig(Bitmap.Config.ARGB_4444)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(ImageZoomActivity.this)
                .defaultDisplayImageOptions(opts)
                .memoryCache(new WeakMemoryCache())
                .build();

        if (!ImageLoader.getInstance().isInited())
            ImageLoader.getInstance().init(config);


        ImageLoader.getInstance().displayImage(imgUrl, image, opts, new ImageLoadingListener() {

            @Override
            public void onLoadingStarted(String imageUri, View view) {
                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
                image.setImageDrawable(getResources().getDrawable(R.drawable.img));
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                image.setImageBitmap(loadedImage);
                ImageCropperActivity.imagebitmap = loadedImage;
                imagebitmap = loadedImage;
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        }, new ImageLoadingProgressListener() {
            @Override
            public void onProgressUpdate(String imageUri, View view, int current, int total) {

            }
        });
    }*/

    public void loadImage(final String imageUrl) {

        String imgPath = "http://app.dbclmatrix.com/nopicture.png",
                tmpImgThumb = "http://app.dbclmatrix.com/nopicture.png";

            if (image != null) {
                imgPath = imageUrl;
//            } else if (masteDetailModel.imagepath != null) {
//                imgPath = masteDetailModel.imagepath;
            }
            //FINAL_IMAGLOAD_CODE
        Glide.with(this)
                .asBitmap()
                .load(imgPath)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {

                        loadImage(imageUrl);
//                            count=0;
                       /* }
                        else {
                            loadImages(image,opts,"");
                        }*/
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                        imagebitmap = resource;
                        return false;
                    }
                })
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        image.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_bar_back);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    public void removeBookMark(String id) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Please Wait ..");
        pd.show();

        Epaper mApp = (Epaper) getApplicationContext();
        mApp.getServices().removeMyBookmarkData(id, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                Log.e("Response", "=>" + jsonElement.toString());
                String status = "", msg = "";
                try {
                    JSONObject jb = new JSONObject(jsonElement.toString());
                    status = jb.getString("status");
                    msg = jb.getString("msg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.contains("1")) {
                    Utility.setToast(ImageZoomActivity.this, msg);
                    finish();
                } else {
                    Utility.setToast(ImageZoomActivity.this, msg);
                }
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {

                pd.dismiss();
            }
        });

    }
}



    /*public void loadImage(String imgUrl) {

        mTarget = new SimpleTarget<Bitmap>() {
            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
                Log.e("picasso zoom ", "onBitmapFailed");
            }

            @Override
            public void onLoadStarted(Drawable placeholder) {
                super.onLoadStarted(placeholder);
                ImageCropperActivity.imagebitmap = null;
                imagebitmap = null;
                image.setImageDrawable(getResources().getDrawable(R.drawable.img));
                Log.e("picasso zoom ", "onPrepareLoad");
            }

            @Override
            public void onResourceReady(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                image.setImageBitmap(bitmap);
                ImageCropperActivity.imagebitmap = bitmap;
                imagebitmap = bitmap;
                Glide.with(ImageZoomActivity.this).clear(mTarget);
                Log.e("picasso zoom ", "onBitmapLoaded");
            }
        };

        Glide.with(this)
                .load(imgUrl)
                .into(mTarget);

    }*/
