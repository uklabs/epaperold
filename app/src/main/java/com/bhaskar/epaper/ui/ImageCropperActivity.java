package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.my_cropper.CropImageView;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Utility;
import com.google.android.gms.ads.AdView;


public class ImageCropperActivity extends BaseActivity {

    private CropImageView cropImageView;
    private ImageView setCropperImage;
    private Button crop_btn;
    private Button share_btn;
    private Button save_btn;
    private Button reset_btn;
    private RelativeLayout fullRelativeImage;
    private ScrollView layRelViewHolder;
    private AdView adview;
    private LinearLayout bottom_ll;
    private RelativeLayout relativeImage;
    private TextView editionDetailTxt;
    private ImageView logo_banner;

    private String imageUrl;
    private String editionname;
    private String description;
    private String pageno;
    private String state;
    private String imgpath;
    private String pageDate="";

    public static Bitmap imagebitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cropper);

        cropImageView = findViewById(R.id.cropImageView);
        setCropperImage = findViewById(R.id.setCropperImage);
        crop_btn = findViewById(R.id.crop_activity_btn);
        share_btn = findViewById(R.id.share_activity_btn);
        save_btn = findViewById(R.id.save_activity_btn);
        reset_btn = findViewById(R.id.reset_activity_btn);
        fullRelativeImage = findViewById(R.id.fullRelative_image);
        layRelViewHolder = findViewById(R.id.layRelViewHolder);
        adview = findViewById(R.id.adView);
        bottom_ll = findViewById(R.id.bottom_ll);
        relativeImage = findViewById(R.id.relative_image);
        editionDetailTxt = findViewById(R.id.edition_detail_txt);
        logo_banner = findViewById(R.id.logo_banner);

        Intent intent = getIntent();
        if (intent !=null){
            imageUrl = getIntent().getStringExtra("largeUrl");
            System.out.print("Image URL " + imageUrl);
            editionname = getIntent().getStringExtra("editionname");
            description = getIntent().getStringExtra("description");
            pageno = getIntent().getStringExtra("pageno");
            imgpath = getIntent().getStringExtra("imgpath");
            pageDate = getIntent().getStringExtra("pageDate");


            if (pageDate.equalsIgnoreCase("")){
                 String setasDate = EpaperPrefernces.getInstance(ImageCropperActivity.this).getStringValue(PreferncesConstant.setDateAsDefult, "");
                editionDetailTxt.setText(DateUtility.parseDateToddMMyyyy(setasDate) + "\n" + description);
            }
            else {
                editionDetailTxt.setText(DateUtility.parseDateToddMMyyyy(pageDate) + "\n" + description);
            }

            if (!TextUtils.isEmpty(imgpath)) {
                if (imgpath.startsWith("http://digitalimages.bhaskar.com/gujarat") || imgpath.contains("/gujarat/")) {
                    logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divyabhaskar_logo));
                } else if (imgpath.startsWith("http://digitalimages.bhaskar.com/divyamarathi")) {
                    logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divya_logo));
                } else {
                    logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.dainik_logo));
                }
            }
        }

        setToolbar();

        if (Util.isShowAds(ImageCropperActivity.this)) {
            final AdUtility adUtility = new AdUtility(this);

            adUtility.loadBannerAd(adview);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    adUtility.loadInterstitial();
                }
            }, 72000);
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(imageUrl)) {
            try {
                cropImageView.setImageBitmap(imagebitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setCropperImage.setImageBitmap(imagebitmap);
            setAccessVisibility(false);
        } else {
            cropImageView.setVisibility(View.GONE);
            setCropperImage.setVisibility(View.GONE);

            Log.i("imageUrl", "=>" + imageUrl);
            cropImageView.setImageUriAsync(Uri.parse(imageUrl));
            cropImageView.setVisibility(View.VISIBLE);
            setAccessVisibility(false);
        }

        crop_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {
                            relativeImage.setVisibility(View.VISIBLE);
                            fullRelativeImage.setVisibility(View.GONE);
                            setAccessVisibility(true);

                        } catch (Exception e) {
                            Utility.setToast(ImageCropperActivity.this, "Please Work Properly");
                            e.printStackTrace();
                        }
                    }
                });

        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setCropperImage.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                fullRelativeImage.setVisibility(View.VISIBLE);
                relativeImage.setVisibility(View.GONE);

                try {
                    cropImageView.setImageBitmap(imagebitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setAccessVisibility(false);
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layRelViewHolder.setVisibility(View.GONE);
                adview.setVisibility(View.GONE);
                bottom_ll.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveImage(bt, ImageCropperActivity.this);

                layRelViewHolder.setVisibility(View.VISIBLE);
                adview.setVisibility(View.VISIBLE);
                bottom_ll.setVisibility(View.VISIBLE);
                //scrollView.setVisibility(View.VISIBLE);vi
            }
        });

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                Log.e("permission", "=>share_btn");
                ImageUtility.saveSharedImage(bt, ImageCropperActivity.this, description);
            }
        });
    }

    private void setToolbar() {
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("E-Paper");
    }

    public void setAccessVisibility(Boolean flag) {

        if (flag) {
            save_btn.setVisibility(View.VISIBLE);
            share_btn.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            save_btn.setLayoutParams(layoutParams);
            share_btn.setLayoutParams(layoutParams);
            cropImageView.setVisibility(View.VISIBLE);
            setCropperImage.setVisibility(View.VISIBLE);
            setCropperImage.setImageBitmap(cropImageView.getCroppedImage());
        } else {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    4.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);

            share_btn.setVisibility(View.GONE);
            save_btn.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Epaper.getInstance().trackScreenView(ImageCropperActivity.this,"ImageCropperActivity");
    }
}
