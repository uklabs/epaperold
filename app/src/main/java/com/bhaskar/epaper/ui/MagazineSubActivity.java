package com.bhaskar.epaper.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazineRecyclerAdapter;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.RequestHandler;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MagazineSubActivity extends ParentActivity {

    private RecyclerView recyclerView;
    private AVLoadingIndicatorView progress;

    String editioncode,magazineLang, magazineName;
    EpaperPrefernces mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);
        mPref = EpaperPrefernces.getInstance(MagazineSubActivity.this);
        setToolbar();

        recyclerView = findViewById(R.id.homeRecyclerList);
        progress = findViewById(R.id.progress);
        progress.setIndicatorColor(ContextCompat.getColor(MagazineSubActivity.this, R.color.colorPrimaryDark));

        int column = Epaper.getInstance().getModelManager().getColumnCount();
        recyclerView.setLayoutManager(new GridLayoutManager(this, column));
        editioncode = getIntent().getStringExtra("editioncode");
        magazineLang = getIntent().getStringExtra("magazinelang");
        magazineName = getIntent().getStringExtra("magazinename");
        getSubMagazineDetail();
        if (Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()) == null) {
            RequestHandler.makeYearlyPlanHit(MagazineSubActivity.this, Epaper.getInstance().getChannelSlno());
        }

    }

    public void getSubMagazineDetail() {
        mApp.getServices().getMagazineSubMasterData(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String jsonResponse = ConnectionDetector.convertResponseToString(response);
                Log.e("Jsonresponse", "=>" + jsonResponse);
                Type listType = new TypeToken<List<MagazineMasterModel>>() {
                }.getType();
                List<MagazineMasterModel> magazinemodel = new Gson().fromJson(jsonResponse, listType);
                setRecyclerView(magazinemodel);

            }

            @Override
            public void failure(RetrofitError error) {
                Utility.setNetworkErrorToast(MagazineSubActivity.this);

            }
        });
    }

    private void setRecyclerView(List<MagazineMasterModel> magazinemodel) {
        if (magazinemodel == null) {
            Utility.setToast(this, "No Data Found");
        } else {
            List<MagazineMasterModel> MagazineDetailList = new ArrayList<>();
            for (MagazineMasterModel mmm : magazinemodel) {
                if (mmm.getEditioncode().equals(editioncode)) {
                    MagazineDetailList.add(mmm);
                    Log.e("Jsonresponse", "=>" + mmm.getMagazine());
                }
            }
            progress.setVisibility(View.GONE);
            recyclerView.setAdapter(new MagazineRecyclerAdapter(MagazineSubActivity.this, MagazineDetailList, magazineLang,false));
            //String gaScreen= EpaperSplashActivity.getInstance(MagazineSubActivity.this).modulePrefix+"-"+ MagazineMasterActivity.magazinePrefix+"-"+magazineLang+"-"+magazineName;
           // Tracking.trackGAScreen(MagazineSubActivity.this, EpaperSplashActivity.getInstance(MagazineSubActivity.this).defaultTracker, gaScreen, EpaperSplashActivity.getInstance(MagazineSubActivity.this).source, EpaperSplashActivity.getInstance(MagazineSubActivity.this).medium, EpaperSplashActivity.getInstance(MagazineSubActivity.this).campaign);
            Epaper.getInstance().trackScreenView(MagazineSubActivity.this, "MAGAZINE "+magazineLang+"-"+magazineName);
        }
    }

}
