package com.bhaskar.epaper.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MasterPageStateAdapter;
import com.bhaskar.epaper.adapter.SearchAdapter;
import com.bhaskar.epaper.epaperv2.interfaces.OnSetHomePageListener;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.PaymentPlanActivity;
import com.bhaskar.epaper.epaperv2.payment.TransactionHistoryActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.AccountSettingsActivity;
import com.bhaskar.epaper.epaperv2.profile.ContactActivity;
import com.bhaskar.epaper.epaperv2.profile.FeedbackActivity;
import com.bhaskar.epaper.epaperv2.profile.ProfileActivity;
import com.bhaskar.epaper.epaperv2.profile.TermConditionActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.RequestHandler;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.gdpr.GDPRControllerActivity;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.mobile_verify.MobileVerifyActivity;
import com.bhaskar.epaper.model.MainDataModel;
import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.bhaskar.epaper.ui.DetailSwipeActivity.SUBSCRIBE_REQUEST_CODE;

public class MasterActivity extends AppCompatActivity
        implements
        NavigationView.OnNavigationItemSelectedListener, RequestHandler.OnRequestListener, OnSetHomePageListener {
    private int REQUEST_CODE_LOGIN = 1010;
    public static int REQUEST_CODE_PAYMENT = 1011;
    private static final int REQUEST_CODE_WD_CONSENT = 2020;

    private AutoCompleteTextView searchTxt;
    private ViewPager masterPageViewpager;
    private TabLayout masterPageTabs;
    private DrawerLayout drawer;
    private View navHeaderView;
    private Menu menu;
    private TextView userName;
    private TextView userEmail;
    private CircleImageView userImage;
    private Dialog trialEndDialog;

    private EpaperPrefernces pref;
    private MasterActivity mActivity;
    private LinearLayout searchLayout;
    private NavigationView navigationView;
    private MasterPageStateAdapter adapter;
    private DatePicker datepicker;
    DatePickerDialog dpd;
    int yearSave, monthOfYearSave, dayOfMonthSave;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_v2);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        statusbar();
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        menu = navigationView.getMenu();

        startMaster();

        masterPageViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                Epaper.getInstance().trackScreenView(MasterActivity.this, String.valueOf(adapter.getPageTitle(position)));
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (adapter.getCount() > 0)
            Epaper.getInstance().trackScreenView(MasterActivity.this, String.valueOf(adapter.getPageTitle(0)));

    }


    private void startMaster() {
        initMaster();
        setAdView();
        setNavigationView();
        setAutoCompleteText();
        setPagerData();
        initAppUpdatePopup();
        LocalBroadcastManager.getInstance(this).registerReceiver(userLoggedInReceiver, new IntentFilter(QuickPreferences.USER_LOGGED_IN_INTENT));

        if (!TextUtils.isEmpty(pref.getStringValue(QuickPreferences.CommonPref.USER_ALERT_TITLE, ""))
                && !TextUtils.isEmpty(pref.getStringValue(QuickPreferences.CommonPref.USER_ALERT_MSG, ""))
                && !pref.getBooleanValue(QuickPreferences.CommonPref.USER_ALERT_SHOWN, false)) {
            showUserAlertDialog();
        }

        if (TextUtils.isEmpty(pref.getStringValue(QuickPreferences.UserInfo.MOBILE, ""))
                && pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            showMobileVerifyForExitsingUser();
        }

        RequestHandler.requestForIsUserSubscribed(this, this);
        if (!pref.getBooleanValue(QuickPreferences.CommonPref.APP_SOURCE_HIT, false)) {
            RequestHandler.appSourceTracking(MasterActivity.this);
        }
        Epaper.getInstance().getModelManager().setColumnCount(Utility.calculateNoOfColumns(MasterActivity.this));
    }

    private void initMaster() {
        mActivity = this;
        pref = EpaperPrefernces.getInstance(mActivity);

        searchTxt = findViewById(R.id.search_editor_actxt);
        ImageView magazineMenuBtn = findViewById(R.id.magazine_menu_btn);
        ImageView searchBtn = findViewById(R.id.search_btn);
        ImageView closeBtn = findViewById(R.id.close_search);
        searchLayout = findViewById(R.id.search_layout);
        masterPageViewpager = findViewById(R.id.master_page_viewpager);
        masterPageTabs = findViewById(R.id.master_page_tabs);
        magazineMenuBtn.setVisibility(View.VISIBLE);
        searchBtn.setVisibility(View.VISIBLE);
        masterPageViewpager.setOffscreenPageLimit(1);
        magazineMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mActivity, MagazineMasterActivity.class));
            }
        });
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp();
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchLayout.getVisibility() == View.VISIBLE) {
                    slideUp();
                    hideKeyBoard();
                } else {
                    slideDown();
                    showKeyboard();
                }
            }
        });
    }

    private void slideUp() {
        Animation animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
        searchLayout.startAnimation(animSlideUp);
        animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                searchLayout.setVisibility(View.GONE);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        hideKeyBoard();
    }

    private void slideDown() {
        Animation animSlideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
        searchLayout.startAnimation(animSlideDown);
        animSlideDown.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                searchLayout.setVisibility(View.VISIBLE);
                searchTxt.setFocusable(true);
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        searchLayout.setVisibility(View.VISIBLE);
    }

    private void setAdView() {
//        AdView adview = findViewById(R.id.adView);
//        if (Util.isShowAds(MasterActivity.this)) {
//            adview.setVisibility(View.VISIBLE);
//            new AdUtility(mActivity).loadBannerAd(adview);
//            AdUtility adUtility = new AdUtility(mActivity);
//            adUtility.loadBannerAd(adview);
//        } else {
//            adview.setVisibility(View.GONE);
//        }
    }

    private void setAutoCompleteText() {
        /*search Data*/
        ArrayList<SearchModel> searchList = Epaper.getInstance().getModelManager().getSearchList();
        searchTxt.clearFocus();
        if (searchList != null && searchList.size() > 0) {
            searchTxt.setVisibility(View.VISIBLE);
            searchTxt.setThreshold(1);
            searchTxt.setAdapter(new SearchAdapter(mActivity, searchList));
        } else {
            searchTxt.setVisibility(View.GONE);
            searchTxt.setText("No Network Connection");
            searchTxt.setEnabled(false);
        }
    }

    private void setPagerData() {
        HomeContent.getHomeContent().listMain.clear();

        adapter = new MasterPageStateAdapter(getSupportFragmentManager(), MasterActivity.this);
        masterPageViewpager.setAdapter(adapter);

        // masterPageViewpager.setAdapter(new MasterPageStateAdapter(getSupportFragmentManager(), MasterActivity.this));
        masterPageTabs.setupWithViewPager(masterPageViewpager);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(masterPageViewpager.getAdapter()).notifyDataSetChanged();
        }
    }

    private void setNavigationView() {
        // NavigationView navigationView = findViewById(R.id.nav_view);
        navHeaderView = navigationView.getHeaderView(0);
        navHeaderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                    startActivity(new Intent(MasterActivity.this, ProfileActivity.class));
//                } else {
//                    showChannelChooserDialog();

                }
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        userImage = navHeaderView.findViewById(R.id.user_image);
        userName = navHeaderView.findViewById(R.id.user_name);
        userEmail = navHeaderView.findViewById(R.id.user_email);

        setSubscribedMenu();
    }

    private void showChannelChooserDialog() {
        final AlertDialog.Builder channelDialogBuilder = new AlertDialog.Builder(this);
        channelDialogBuilder.setCancelable(true);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_login_channel_chooser_dialog, null);

        Button continueBtn = dialogView.findViewById(R.id.confirm_btn);
        final RadioGroup channelRadioGroup = dialogView.findViewById(R.id.channel_radio_group);
        channelDialogBuilder.setView(dialogView);
        final AlertDialog channelDialog = channelDialogBuilder.create();
        channelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        channelDialog.setCanceledOnTouchOutside(true);
        /*do some work here*/

        channelRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String channel = (checkedId == R.id.dainik_bhaskar) ? Constants.DAINIK_BHASKAR : Constants.DIVYA_BHASKAR;
                Epaper.getInstance().setChannelSlno(channel);
            }
        });
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (channelRadioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(MasterActivity.this, "Please choose Channel First", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MasterActivity.this, LoginRegistrationActivity.class);
                    intent.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.LEFT_MENU);
                    startActivityForResult(intent, REQUEST_CODE_LOGIN);
                    channelDialog.dismiss();
                }
            }
        });

        channelDialog.show();
    }

    private void setSubscribedMenu() {
        boolean isLoggedIn = pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false);
        int regType = pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        if (Constants.SUBSCRIBED_COUNTRY) {
            menu.findItem(R.id.order_history).setVisible(isLoggedIn && regType != Constants.SubscriptionStatus.DOMAIN_USER);
            menu.findItem(R.id.account_settings).setVisible(isLoggedIn);
            menu.findItem(R.id.bookmarks).setVisible(isLoggedIn);
            menu.findItem(R.id.edition).setVisible(isLoggedIn);
            menu.findItem(R.id.change_pass).setVisible(isLoggedIn);
            menu.findItem(R.id.logout).setVisible(isLoggedIn);
            menu.findItem(R.id.subscription_plan).setVisible(isLoggedIn && regType != Constants.SubscriptionStatus.DOMAIN_USER);
            navHeaderView.setVisibility(View.VISIBLE);
        } else {
            navHeaderView.setVisibility(View.GONE);
            menu.findItem(R.id.order_history).setVisible(false);
            menu.findItem(R.id.account_settings).setVisible(false);
            menu.findItem(R.id.bookmarks).setVisible(false);
            menu.findItem(R.id.edition).setVisible(false);
            menu.findItem(R.id.change_pass).setVisible(false);
            menu.findItem(R.id.logout).setVisible(false);
            menu.findItem(R.id.subscription_plan).setVisible(false);
        }

        String profilePic = pref.getStringValue(QuickPreferences.UserInfo.PROFILE_PIC, "");
        try {
            if (isLoggedIn) {
                ImageUtil.setImage(this, profilePic, R.drawable.user_default_pic, userImage);
            } else {
                ImageUtil.setImage(this, "xyz", R.drawable.user_default_pic, userImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String dbId = pref.getStringValue(QuickPreferences.UserInfo.DB_ID, "");
        if (!TextUtils.isEmpty(dbId)) {
            menu.findItem(R.id.db_id).setTitle(getString(R.string.db_id_text, dbId));
        } else {
            menu.findItem(R.id.db_id).setVisible(false);
        }
        String appVersion = Util.getAppVersion(MasterActivity.this);
        if (!TextUtils.isEmpty(appVersion)) {
            menu.findItem(R.id.app_version).setTitle(getString(R.string.app_version_text, appVersion));
        } else {
            menu.findItem(R.id.app_version).setVisible(false);
        }
        setUserName();

        if (pref.getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false)) {
            menu.findItem(R.id.with_draw_consent).setVisible(true);
        } else {
            menu.findItem(R.id.with_draw_consent).setVisible(false);
        }
        setTextColorForMenuItem(menu.findItem(R.id.db_id), R.color.md_grey_500);
        setTextColorForMenuItem(menu.findItem(R.id.app_version), R.color.md_grey_500);

    }

    private void setTextColorForMenuItem(MenuItem menuItem, @ColorRes int color) {
        SpannableString spanString = new SpannableString(menuItem.getTitle().toString());
        spanString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, color)), 0, spanString.length(), 0);
        menuItem.setTitle(spanString);
    }

    private void setUserName() {
        String username = "";
        String useremail = "";
        if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            username = pref.getStringValue(QuickPreferences.UserInfo.NAME, "");
            useremail = pref.getStringValue(QuickPreferences.UserInfo.EMAIL, "");
        }
        username = (TextUtils.isEmpty(username)) ? getString(R.string.nav_header_title) : username;
        userName.setText(username);
        if (!TextUtils.isEmpty(useremail)) {
            userEmail.setText(useremail);
            userEmail.setVisibility(View.VISIBLE);
        } else {
            userEmail.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        // Handle navigation view item clicks here.
        drawer.closeDrawer(GravityCompat.START);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (item.getItemId()) {
                    case R.id.date:
                        onChangeDate();
                        break;

                    case R.id.magazines:
                        onMagazines();
                        break;

                    case R.id.edition:
                        onMyEditions();
                        break;

                    case R.id.bookmarks:
                        onBookmarks();
                        break;

                    case R.id.change_pass:
                        onChange_password();
                        break;

                    case R.id.account_settings:
                        onAccount_settings();
                        break;
                    case R.id.subscription_plan:
                        showTrialEndDialog();
                        break;
                    case R.id.tnc:
                        onTerms_condition();
                        break;

                    case R.id.rate_us:
                        onRate_us();
                        break;

                    case R.id.contact_us:
                        onContact_us();
                        break;
                    case R.id.feedback:
                        onFeedback();
                        break;

                    case R.id.logout:
                        onLogout();
                        break;

                    case R.id.order_history:
                        onOrderHistory();
                        break;

                    case R.id.privacy_policy:
                        onPrivacyPolicy();
                        break;
                    case R.id.refund_policy:
                        onRefundPolicy();
                        break;
                    case R.id.cookie_policy:
                        onCookiePolicy();
                        break;
                    case R.id.share_app:
                        onShareApp();
                        break;
                    case R.id.with_draw_consent:
                        onWithDrawConsent();
                        break;
                }
            }
        }, 200);


        return true;
    }

    private void onWithDrawConsent() {
        Intent intent = new Intent(MasterActivity.this, GDPRControllerActivity.class);
        if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false) && !TextUtils.isEmpty(pref.getStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, ""))) {
            intent.putExtra(Constants.Gdpr.CONSENT_VALUE, Constants.Gdpr.WD_SUBSCR_CONSENT);
        } else if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            intent.putExtra(Constants.Gdpr.CONSENT_VALUE, Constants.Gdpr.WD_LOGIN_CONSENT);

        } else {
            intent.putExtra(Constants.Gdpr.CONSENT_VALUE, Constants.Gdpr.WD_CONSENT);
        }
        startActivityForResult(intent, REQUEST_CODE_WD_CONSENT);
    }

    private void onShareApp() {
        Util.shareApp(MasterActivity.this);
    }

    private void onLogout() {
        pref.setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false);
        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false);
        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false);
        pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        pref.setStringValue(QuickPreferences.UserInfo.MOBILE, "");
        pref.setStringValue(QuickPreferences.UserInfo.EMAIL, "");
        pref.setStringValue(QuickPreferences.UserInfo.NAME, "");
        pref.setStringValue(QuickPreferences.UserInfo.PROFILE_PIC, "");
        pref.setStringValue(QuickPreferences.UserInfo.USER_ID, "");
        pref.setStringValue(QuickPreferences.UserInfo.CITY, "");
        pref.setStringValue(QuickPreferences.UserInfo.COUNTRY, "");
        pref.delete(QuickPreferences.PaymentPref.IS_DOMAIN_USER_VERIFIED);

        Constants.isUserComeFromRegistration = false;
        Constants.isTrialEndsDialogShown = false;
        Constants.FREE_TRIAL_DURATION_MESSAGE = "";

        setSubscribedMenu();
        setAdView();
//        RequestHandler.requestForDBId(MasterActivity.this);
//        if (AccessToken.getCurrentAccessToken() != null) {
//            LoginManager.getInstance().logOut();
//        }

        try {
            checkAndLogout();
        } catch (Exception ignored) {
        }

    }

    private GoogleApiClient mGoogleApiClient = null;

    private void checkAndLogout() {
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        } else {
            if (mGoogleApiClient == null) {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getResources().getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build();

                mGoogleApiClient = new GoogleApiClient.Builder(MasterActivity.this)
                        .enableAutoManage(MasterActivity.this, null)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
                //mGoogleApiClient.clearDefaultAccountAndReconnect();
            }
            if (mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }
        }
    }

    private void onMagazines() {
        Intent intent = new Intent(this, MagazineMasterActivity.class);
        startActivity(intent);
    }

    private void onChangeDate() {
        callDatePicker();
    }

    public void callDatePicker() {
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        final Calendar calendarGlobal = Calendar.getInstance();
        dpd = new DatePickerDialog(mActivity, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                datepicker = new DatePicker(MasterActivity.this);

                datepicker.init(year, monthOfYear + 1, dayOfMonth, null);
                yearSave = year;
                monthOfYearSave = monthOfYear;
                dayOfMonthSave = dayOfMonth;

                SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                calendarGlobal.set(Calendar.MONTH, monthOfYear);
                calendarGlobal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                calendarGlobal.set(Calendar.YEAR, year);
                String strDate = dateFormatter.format(calendarGlobal.getTime());

                holidayMessageService(strDate);
//                holidayMessageService("2019-09-18 ");

                //startActivity(new Intent(mActivity, SplashActivity.class).putExtra("date", strDate));
                //finish();
               /* if (strDate.equalsIgnoreCase("2019-08-15"))
                    Toast.makeText(MasterActivity.this, "15 AUGUST NOT AVIALABLE", Toast.LENGTH_SHORT).show();
                else {
                    RequestHandler.makeDefaultDateRequest(MasterActivity.this, strDate, new RequestHandler.OnChangeDateListener() {
                        @Override
                        public void success() {
                            setPagerData();
                        }

                        @Override
                        public void fail() {

                        }
                    });
                }*/


            }
        }, year, month, day);


        if (datepicker != null) {
            dpd.updateDate(datepicker.getYear(), datepicker.getMonth() - 1, datepicker.getDayOfMonth());

        }
      /*  if (yearSave !=0)
            dpd.updateDate(yearSave, monthOfYearSave - 1, dayOfMonthSave);*/

        Calendar calNow = Calendar.getInstance();
        // adding -1 month
        calNow.add(Calendar.MONTH, -12);
        dpd.getDatePicker().setMinDate(calNow.getTimeInMillis());
        dpd.getDatePicker().setMaxDate(getMaxDate().getTime());
        dpd.show();
        //dpd.updateDate(Calendar.YEAR, Calendar.MONTH - 1, Calendar.DAY_OF_MONTH);

        //int daya = getApplicationContext().getResources().getIdentifier("android:id/day", null, null);
       /* int daya = Calendar.MONTH;
        if(daya != 0){
            View dayPicker = findViewById(daya);
            if(dayPicker != null){
                //Set Day view visibility Off/Gone
                dayPicker.setVisibility(View.GONE);
            }
            Toast.makeText(mActivity, "null Day picker", Toast.LENGTH_SHORT).show();
            //dpd.getDatePicker().setCalendarViewShown(false);
        }*/

    }

    public Date getMaxDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if (!TextUtils.isEmpty(pref.getStringValue(PreferncesConstant.setDateAsMax, ""))) {
                String currDate = pref.getStringValue(PreferncesConstant.setDateAsMax, "");
                Date date = simpleDateFormat.parse(currDate);
                return date;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Date();
    }

    private void onMyEditions() {
        if (!TextUtils.isEmpty(pref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""))) {
            startActivity(new Intent(mActivity, MyEditionActivity.class));
        } else {
            pref.logOut(mActivity);
            Utility.setToast(mActivity, "Please Login First. ");
        }
    }

    private void onBookmarks() {
        if (!TextUtils.isEmpty(pref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""))) {
            startActivity(new Intent(mActivity, MyBookmarkActivity.class));
        } else {
            pref.logOut(mActivity);
            Utility.setToast(mActivity, "Please Login First. ");
        }
    }

    private void onRate_us() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.APP_SHARE_URL)));
    }

    private void onTerms_condition() {
        startActivity(new Intent(mActivity, TermConditionActivity.class).putExtra("title", "Terms & Condition").putExtra("url", Urls.TERM_AND_CONDITION_URL));
    }

    private void onContact_us() {
        startActivity(new Intent(mActivity, ContactActivity.class));
    }

    private void onFeedback() {
        Epaper.getInstance().trackEvent(MasterActivity.this, GAConst.Category.FEEDBACK, GAConst.Action.CLICK, GAConst.Label.LEFT_MENU);
        startActivity(new Intent(mActivity, FeedbackActivity.class));
    }

    private void onChange_password() {
        startActivity(new Intent(mActivity, ChangePasswordActivity.class).putExtra("isChangePassword", true));
    }

    private void onAccount_settings() {
        startActivity(new Intent(this, AccountSettingsActivity.class));
    }

    private void onPrivacyPolicy() {
        startActivity(new Intent(mActivity, TermConditionActivity.class).putExtra("title", "Privacy Policy").putExtra("url", Urls.PRIVACY_POLICY_URL));
    }

    private void onRefundPolicy() {
        startActivity(new Intent(mActivity, TermConditionActivity.class).putExtra("title", "Refund Policy").putExtra("url", Urls.REFUND_POLICY_URL));
    }

    private void onCookiePolicy() {
        startActivity(new Intent(mActivity, TermConditionActivity.class).putExtra("title", "Cookie Policy").putExtra("url", Urls.COOKIE_POLICY_URL));
    }

    private void onOrderHistory() {
        startActivity(new Intent(this, TransactionHistoryActivity.class));
    }

    boolean epaperChoosed = false;

    private void showTrialEndDialog() {
        trialEndDialog = new Dialog(MasterActivity.this);
        trialEndDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        trialEndDialog.setContentView(R.layout.subscription_and_trial_end);
        trialEndDialog.setCancelable(false);

        TextView txtTrialEndsMsg = trialEndDialog.findViewById(R.id.txt_trial_ends_msg);
        TextView btnBuySubscriptionHindi = trialEndDialog.findViewById(R.id.buy_subscription_hindi);
        TextView btnBuySubscriptionGujrati = trialEndDialog.findViewById(R.id.buy_subscription_gujrati);
        TextView btnLogin = trialEndDialog.findViewById(R.id.not_now);
        final ImageView iv_dainik_epaper = trialEndDialog.findViewById(R.id.iv_dainik_epaper);
        final ImageView iv_divya_epaper = trialEndDialog.findViewById(R.id.iv_divya_epaper);
        // text come from back end in appcontrol
        String trialEndsDaysMessage = pref.getStringValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_DAYS_MESSAGE, "");
        txtTrialEndsMsg.setText("Choose Epaper");
        LinearLayout dainik_bhaskar_epaper = trialEndDialog.findViewById(R.id.dainik_bhaskar_epaper);
        LinearLayout divya_bhaskar_epaper = trialEndDialog.findViewById(R.id.divya_bhaskar_epaper);
        TextView buy_subscription = trialEndDialog.findViewById(R.id.buy_subscription);

        buy_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (epaperChoosed) {
                    Epaper.getInstance().trackEvent(MasterActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                    startActivityForResult(new Intent(MasterActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                    trialEndDialog.dismiss();
                } else {
                    Toast.makeText(MasterActivity.this, "Please Choose Epaper", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dainik_bhaskar_epaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = true;
                Epaper.getInstance().setChannelSlno(Constants.DAINIK_BHASKAR);
                iv_dainik_epaper.setImageResource(R.drawable.ic_radio_checked);
                iv_divya_epaper.setImageResource(R.drawable.ic_radio_unchecked);
            }
        });
        divya_bhaskar_epaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = true;
                Epaper.getInstance().setChannelSlno(Constants.DIVYA_BHASKAR);
                iv_dainik_epaper.setImageResource(R.drawable.ic_radio_unchecked);
                iv_divya_epaper.setImageResource(R.drawable.ic_radio_checked);
            }
        });
        btnBuySubscriptionHindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Epaper.getInstance().setChannelSlno(Constants.DAINIK_BHASKAR);
                Epaper.getInstance().trackEvent(MasterActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                startActivityForResult(new Intent(MasterActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                trialEndDialog.dismiss();
            }
        });
        btnBuySubscriptionGujrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Epaper.getInstance().setChannelSlno(Constants.DIVYA_BHASKAR);
                Epaper.getInstance().trackEvent(MasterActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                startActivityForResult(new Intent(MasterActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                trialEndDialog.dismiss();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = false;
                trialEndDialog.dismiss();
            }
        });
        trialEndDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    epaperChoosed = false;
                    trialEndDialog.dismiss();
                }
                return true;
            }
        });
        trialEndDialog.show();
        int regType = pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        Epaper.getInstance().trackEvent(MasterActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.VIEW + "_" + regType, GAConst.Label.IMPRESSION);
        Constants.isTrialEndsDialogShown = false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            showExitDialog();
        }
    }

    private void showExitDialog() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.app_exit_popup_layout, null);
        exitDialogBuilder.setView(dialogView);
        TextView btnYes = dialogView.findViewById(R.id.textyes);
        TextView btnNo = dialogView.findViewById(R.id.textno);
        final AlertDialog exitDialog = exitDialogBuilder.create();
        exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitDialog.setCancelable(true);

        if (btnYes != null)
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exitDialog.dismiss();
                    finish();
                }
            });
        if (btnNo != null)
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exitDialog.dismiss();
                }
            });
        exitDialog.show();
    }


    private void initAppUpdatePopup() {
        // fetch value from preference
        int sessionCount = pref.getIntValue(QuickPreferences.AppUpdate.APP_UPDATE_SESSION_COUNT, 0);
        int curAppVersion = pref.getIntValue(QuickPreferences.AppUpdate.APP_CURRENT_VERSION, 0);
        int appUpdateVersion = pref.getIntValue(QuickPreferences.AppUpdate.UPDATE_POPUP_VERSION_CODE, 0);
        long lastPopupShownDate = pref.getLongValue(QuickPreferences.AppUpdate.APP_UPDATE_POP_UP_SHOWN_DATE, (long) 0);

        long threshold = Constants.AppCommon.UPDATE_POPUP_DAYS_COUNT * 24 * 60 * 60 * 1000L;
        long today = new Date().getTime();

        if (pref.getIntValue(QuickPreferences.AppUpdate.FORCE_UPDATE_VERSION, 0) == 1 && curAppVersion < appUpdateVersion) {
            startActivity(new Intent(MasterActivity.this, ForceUpdateActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        } else if (curAppVersion < appUpdateVersion && sessionCount == 0) {
            // normal Update
            updatePopupCheck();
        } else if (sessionCount > Constants.AppCommon.UPDATE_POPUP_SESSION_COUNT || (today - lastPopupShownDate >= threshold)) {
            // later uopdate || session count || days count
            updatePopupCheck();
        }
    }

    public void updatePopupCheck() {
        final Dialog updateVersionDialog = new Dialog(MasterActivity.this);
        updateVersionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        updateVersionDialog.setContentView(R.layout.app_update_popup_layout);
        updateVersionDialog.setCancelable(false);
        updateVersionDialog.show();

        TextView btnRemindMeLater = updateVersionDialog.findViewById(R.id.remind_later_tv);
        TextView btnUpdateNow = updateVersionDialog.findViewById(R.id.update_now_tv);
        TextView txt_description = updateVersionDialog.findViewById(R.id.txt_dia);

        String updatePopupMessage = pref.getStringValue(QuickPreferences.AppUpdate.UPDATE_POPUP_MESSAGE, "");
        if (!Utility.isValidString(updatePopupMessage)) {
            txt_description.setText(getResources().getString(R.string.app_update_popup_default_message));
        } else {
            txt_description.setText(pref.getStringValue(QuickPreferences.AppUpdate.UPDATE_POPUP_MESSAGE, ""));
        }

        final Date lastShownDate = new Date();
        btnRemindMeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // store last pop up shown date
                pref.setLongValue(QuickPreferences.AppUpdate.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());

                // set session count 0
                pref.setIntValue(QuickPreferences.AppUpdate.APP_UPDATE_SESSION_COUNT, 1);

                updateVersionDialog.dismiss();
            }
        });

        btnUpdateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // store last pop up shown date
                pref.setLongValue(QuickPreferences.AppUpdate.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());

                // set session count 0
                pref.setIntValue(QuickPreferences.AppUpdate.APP_UPDATE_SESSION_COUNT, 1);

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Urls.APP_SHARE_URL)));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                updateVersionDialog.dismiss();
            }
        });
    }

    private BroadcastReceiver userLoggedInReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.isEmpty(action) && action.equalsIgnoreCase(QuickPreferences.USER_LOGGED_IN_INTENT)) {
                setSubscribedMenu();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CODE_PAYMENT || requestCode == REQUEST_CODE_LOGIN || requestCode == SUBSCRIBE_REQUEST_CODE) && resultCode == RESULT_OK) {
            setAdView();
        } else if (requestCode == REQUEST_CODE_WD_CONSENT && resultCode == RESULT_OK) {
            pref.setBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, false);
            onLogout();
            startActivity(new Intent(MasterActivity.this, SplashActivity.class));
            finish();
        }
    }

    @Override
    public void onCompleteRequest() {

    }

    private void showMobileVerifyForExitsingUser() {
        long today = new Date().getTime();
        Long lastMobileVerifyPopupShownDate = pref.getLongValue(QuickPreferences.CommonPref.MOBILE_VERIFY_DAY, today);
        long threshold = Constants.AppCommon.MOBILE_POPUP_DAYS_COUNT * 24 * 60 * 60 * 1000L;
        if ((today - lastMobileVerifyPopupShownDate >= threshold)) {
            pref.setLongValue(QuickPreferences.CommonPref.MOBILE_VERIFY_DAY, new Date().getTime());
            startActivity(new Intent(MasterActivity.this, MobileVerifyActivity.class));
        }
    }

    private void showUserAlertDialog() {
        final AlertDialog.Builder userAlertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.user_alert_popup_layout, null);
        userAlertDialogBuilder.setView(dialogView);

        TextView txt_head = dialogView.findViewById(R.id.txt_head);
        TextView txt_message = dialogView.findViewById(R.id.txt_message);
        txt_head.setText(Html.fromHtml(pref.getStringValue(QuickPreferences.CommonPref.USER_ALERT_TITLE, "")));
        txt_message.setText(Html.fromHtml(pref.getStringValue(QuickPreferences.CommonPref.USER_ALERT_MSG, "")));
        TextView btnYes = dialogView.findViewById(R.id.textyes);

        final AlertDialog exitDialog = userAlertDialogBuilder.create();
        exitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        exitDialog.setCancelable(true);
        exitDialog.setCanceledOnTouchOutside(false);
        exitDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    pref.setBooleanValue(QuickPreferences.CommonPref.USER_ALERT_SHOWN, true);
                    exitDialog.dismiss();
                }
                return true;
            }
        });
        if (btnYes != null)
            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pref.setBooleanValue(QuickPreferences.CommonPref.USER_ALERT_SHOWN, true);
                    exitDialog.dismiss();
                }
            });
        exitDialog.show();
    }

    @Override
    public void removeHomeState() {
        hideProgress();
    }

    @Override
    public void changeHomeState(String stateName) {
        String setAsDefaultString = pref.getStringValue(PreferncesConstant.setAsDefaultData, "");
        List<MainDataModel> stateList = Epaper.getInstance().getModelManager().getStateList();
        if (TextUtils.isEmpty(setAsDefaultString) && !stateList.get(0).statename.equalsIgnoreCase(stateName)) {
            if (masterPageViewpager.getAdapter() != null) {
                masterPageViewpager.getAdapter().notifyDataSetChanged();
            }
            hideProgress();
        } else {
            if (stateList.size() > 0 && !TextUtils.isEmpty(setAsDefaultString)) {
                int index = stateList.indexOf(new MainDataModel(setAsDefaultString));
                if (index > -1) {
                    MainDataModel mainDataModel = stateList.get(index);
                    List<MainDataModel> localList = new ArrayList<>(stateList);
                    localList.remove(mainDataModel);
                    localList.add(0, mainDataModel);
                    Epaper.getInstance().getModelManager().setStateList(localList);
                    setPagerData();
                    hideProgress();
                }
            }
        }
    }

    @Override
    public void showProgress() {
        findViewById(R.id.overlay_layout).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        findViewById(R.id.overlay_layout).setVisibility(View.GONE);
    }

    public void hideKeyBoard() {

        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

    }

    public void showKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        searchTxt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                searchTxt.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(searchTxt, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        searchTxt.requestFocus();

        /*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/
    }

    private void statusbar() {
        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.getAttributes().flags &= (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(Color.RED);
        }
    }

    //API IS HOLIDAY MESSAGE
    public void holidayMessageService(final String strDate) {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("date", strDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.HOLIDAY_MSZ, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !MasterActivity.this.isFinishing()) {
                    pd.dismiss();
                }

                try {
                    JSONArray arrJson = response.getJSONArray("states");
                    String[] statesList = new String[arrJson.length()];
                    for (int i = 0; i < arrJson.length(); i++)
                        statesList[i] = arrJson.getString(i);
                    List<String> stringList = new ArrayList<String>(Arrays.asList(statesList)); //new ArrayList is only needed if you absolutely need an ArrayList
                    //List<MainDataModel> stateList = Arrays.asList(new Gson().fromJson(response.optJSONArray("states").toString(), MainDataModel[].class));

                    Epaper.getInstance().getModelManager().setHolidayList(stringList);
                    String msz = response.getString("message");
                    Epaper.getInstance().getModelManager().setHolidaymsz(msz);
                    RequestHandler.makeDefaultDateRequest(MasterActivity.this, strDate, new RequestHandler.OnChangeDateListener() {
                        @Override
                        public void success() {
                            setPagerData();
                        }
                        @Override
                        public void fail() {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(MasterActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(MasterActivity.this).addToRequestQueue(request);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(MasterActivity.this);
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!MasterActivity.this.isFinishing()) {
            pd.show();
        }

    }
}
