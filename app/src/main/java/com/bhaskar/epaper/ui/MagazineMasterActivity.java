package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazinePagerAdapter;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.lang.reflect.Type;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.bhaskar.epaper.ui.DetailSwipeActivity.SUBSCRIBE_REQUEST_CODE;

public class MagazineMasterActivity extends ParentActivity {

    private ViewPager magazine_viewpager;
    private TabLayout tabs;
    private AVLoadingIndicatorView progressBar;

    public static int REQUEST_CODE_PAYMENT = 1010;
    private Epaper mApp;
    private  MagazinePagerAdapter magazinePagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magazine);
        setToolbar();
        mApp = Epaper.getInstance();
        magazine_viewpager = findViewById(R.id.magazine_viewpager);
        tabs = findViewById(R.id.tabs);
        progressBar = findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(MagazineMasterActivity.this, R.color.colorPrimaryDark));
        if (ConnectionDetector.isConnectingToInternet(this)) {

            if (Epaper.getInstance().getModelManager().getMasterModels().isEmpty()) {
                getMasterMagazineData();
            } else {
                setPagerData();
            }
        } else {
            Utility.setNetworkErrorToast(this);
        }
        setAdView();

        magazine_viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Epaper.getInstance().trackScreenView(MagazineMasterActivity.this,"MAGAZINE"+"-"+ String.valueOf(magazinePagerAdapter.getPageTitle(position)));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void setAdView() {
//        if (Util.isShowAds(MagazineMasterActivity.this)) {
//            AdView adview = findViewById(R.id.adView);
//            adview.setVisibility(View.VISIBLE);
//            new AdUtility(MagazineMasterActivity.this).loadBannerAd(adview);
//            new AdUtility(MagazineMasterActivity.this).loadInterstitial();
//        } else {
//            findViewById(R.id.adView).setVisibility(View.GONE);
//        }
    }

    public void getMasterMagazineData() {

        mApp.getServices().getMagazineMasterData(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String jsonRespose = ConnectionDetector.convertResponseToString(response);
                Type listType = new TypeToken<List<MagazineMasterModel>>() {
                }.getType();
                List<MagazineMasterModel> masterModels = new Gson().fromJson(jsonRespose, listType);
                Epaper.getInstance().getModelManager().setMasterModels(masterModels);
                setPagerData();
                if(magazinePagerAdapter.getCount()>0){
                    Epaper.getInstance().trackScreenView(MagazineMasterActivity.this,"MAGAZINE "+ String.valueOf(magazinePagerAdapter.getPageTitle(0)));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Utility.setToast(MagazineMasterActivity.this, error.getMessage());
            }
        });
    }

    private void setPagerData() {
        progressBar.setVisibility(View.GONE);
        magazine_viewpager.setOffscreenPageLimit(2);
         magazinePagerAdapter = new MagazinePagerAdapter(getSupportFragmentManager(), Epaper.getInstance().getModelManager().getMasterModels(), MagazineMasterActivity.this);
        magazine_viewpager.setAdapter(magazinePagerAdapter);
        tabs.setupWithViewPager(magazine_viewpager);
  if(magazinePagerAdapter.getCount()>0){
            // Tracking.trackGAScreen(MagazineMasterActivity.this, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).defaultTracker, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).modulePrefix+"-"+magazinePrefix+"-"+magazinePagerAdapter.getPageTitle(0), EpaperSplashActivity.getInstance(MagazineMasterActivity.this).source, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).medium, EpaperSplashActivity.getInstance(MagazineMasterActivity.this).campaign);
            Epaper.getInstance().trackScreenView(MagazineMasterActivity.this,"MAGAZINE"+"-"+ String.valueOf(magazinePagerAdapter.getPageTitle(0)));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == REQUEST_CODE_PAYMENT || requestCode == SUBSCRIBE_REQUEST_CODE) && resultCode == RESULT_OK) {
            setAdView();
        }
    }

}
