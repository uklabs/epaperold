package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.MasterPageModel;
import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;


public class EditionDetailActivity extends BaseActivity {

    Epaper epaper;
    String stateName;
    String cityName;
    String geteditioncode;
    String currentDate;
    String searchDate;
    String description;

    private RecyclerView homeRecyclerList;
    private RecyclerView stateSuggetion;
    private AVLoadingIndicatorView progressBar;

    public ArrayList<SubMasterModel> listdata;
    public ArrayList<SearchModel> searchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);

        homeRecyclerList = findViewById(R.id.homeRecyclerList);
        stateSuggetion = findViewById(R.id.stateSuggetion);
        progressBar = findViewById(R.id.progress);
        AppBarLayout abLay = findViewById(R.id.appbar);
        progressBar.setIndicatorColor(ContextCompat.getColor(EditionDetailActivity.this, R.color.colorPrimaryDark));
        epaper = (Epaper) getApplicationContext();
        stateName = getIntent().getStringExtra("statename"); //"RAJSATHAN";//
        cityName = getIntent().getStringExtra("city_name");
        geteditioncode = getIntent().getStringExtra("editioncode");//"14";
        currentDate = getIntent().getStringExtra("currentdate");
        searchDate = getIntent().getStringExtra("searchdate");
        description = getIntent().getStringExtra("editionname");
        String chkComes = getIntent().getStringExtra("check_comes");
            if (chkComes.equalsIgnoreCase("homeSubmaster"))
                abLay.setVisibility(View.GONE);
            else
                abLay.setVisibility(View.VISIBLE);

        if (getIntent().getParcelableArrayListExtra("pages") != null) {
            listdata = getIntent().getParcelableArrayListExtra("pages");
        }
        setToolbar();
        searchList = Epaper.getInstance().getModelManager().getSearchList();
        callXmlsetter();
    }

    private void setToolbar() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_back);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void callXmlsetter() {
        progressBar.setVisibility(View.VISIBLE);

        if (ConnectionDetector.isConnectingToInternet(this)) {
            String filename = null;
            if (!TextUtils.isEmpty(currentDate)) {
                filename = "jx-edtdetails/xml/" + stateName + "/" + DateUtility.getDate() + "/";
            } else {
                filename = "jx-edtdetails/xml/" + stateName + "/" + DateUtility.setDateForAll + "/";
            }

            if (!TextUtils.isEmpty(searchDate)) {
                filename = "jx-edtdetails/xml/" + stateName + "/" + searchDate + "/";
            }

            String url = Urls.BASE_URL + filename;
            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String res) {
                    setXmlParsingMaster(res);
                    progressBar.setVisibility(View.GONE);
                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(EditionDetailActivity.this).addToRequestQueue(request);
        } else {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setXmlParsingMaster(String xmlres) {
        XMLParser xmlparser = new XMLParser();
        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        if (nl !=null) {
            List<MasterPageModel> stateEditionList = new ArrayList<>();
            for (int i = 0; i < nl.getLength(); i++) {
                Element e = (Element) nl.item(i);
                String editioncode = xmlparser.getValue(e, "editioncode"); // name child value
                String pageno = xmlparser.getValue(e, "pageno"); // name child value
                String page = xmlparser.getValue(e, "page"); // cost child value
                String priorty = xmlparser.getValue(e, "priorty"); // editionname child value
                String imagePath = xmlparser.getValue(e, "imagepath"); // editionname child value
                String imageThumb = xmlparser.getValue(e, "imagethumb"); // editionname child value
                String imageLarge = xmlparser.getValue(e, "imagelarge"); // editionname child value
                stateEditionList.add(new MasterPageModel(editioncode, pageno, page, priorty, imagePath, imageThumb, imageLarge, description));
            }
            setAdapter(stateEditionList);
        }
    }

    private void setAdapter(List<MasterPageModel> stateEditionList) {
        int columno = Epaper.getInstance().getModelManager().getColumnCount();//Utility.calculateNoOfColumns(EditionDetailActivity.this);
        homeRecyclerList.setLayoutManager(new GridLayoutManager(this, columno));
        List<MasterPageModel> filterEdition = new ArrayList<>();

        for (MasterPageModel pgm : stateEditionList) {
            if (pgm.editioncode.equals(geteditioncode)) {
                filterEdition.add(pgm);
            }
        }

        Epaper.getInstance().getModelManager().setListdata(filterEdition);
        homeRecyclerList.setItemAnimator(new DefaultItemAnimator());
        Intent intent = new Intent(EditionDetailActivity.this, DetailSwipeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("editionname", description);
        intent.putExtra("position", 0);
//        intent.putParcelableArrayListExtra("searchList", searchList);
        intent.putParcelableArrayListExtra("pages", listdata);
        intent.putExtra("state", stateName);
        intent.putExtra("city_name", cityName);

        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

}
