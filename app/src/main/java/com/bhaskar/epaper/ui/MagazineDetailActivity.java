package com.bhaskar.epaper.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MagazinePageDetailAdapter;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.BillingInfoActivity;
import com.bhaskar.epaper.epaperv2.payment.PaymentPlanActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.AccountSettingsActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Utility;
import com.google.android.gms.ads.AdView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MagazineDetailActivity extends ParentActivity {
    public static final int LOGIN_REQUEST_CODE = 1010;
    public static final int SUBSCRIBE_REQUEST_CODE = 1020;
    public final static int SUBSCRIBE_REG_REQUEST_CODE = 1030;
    private final static int BILLING_REQUEST_CODE = 1040;

    private AVLoadingIndicatorView progressBar;
    private ViewPager viewpager;
    private TabLayout tabs;
    private String editioncode, editiondate, magazine, magazineLang, magazineName;
    private EpaperPrefernces pref;
    private MagazinePageDetailAdapter magazinePageDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_swipe);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        progressBar = findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(MagazineDetailActivity.this, R.color.colorPrimaryDark));
        viewpager = findViewById(R.id.dailog_viewpager);
        tabs = findViewById(R.id.tabs);
        AdView adView = findViewById(R.id.adView);
        ImageView homeIcon = findViewById(R.id.homeIcon);

        progressBar.setVisibility(View.VISIBLE);
        if (Util.isShowAds(MagazineDetailActivity.this)) {
            new AdUtility(this).loadBannerAd(adView);
        } else {
            adView.setVisibility(View.GONE);
        }
        editioncode = getIntent().getStringExtra("editioncode");
        editiondate = getIntent().getStringExtra("editiondate");
        magazine = getIntent().getStringExtra("magazine");
        magazineLang = getIntent().getStringExtra("magazinelang");
        magazineName = getIntent().getStringExtra("magazinename");

        Log.e("EditionCode", "=>" + editioncode + "editiondate" + editiondate);

        pref = EpaperPrefernces.getInstance(MagazineDetailActivity.this);
        if (ConnectionDetector.isConnectingToInternet(this)) {
            getDetailMagazineData();
        } else {
            Utility.setNetworkErrorToast(this);
        }

        homeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EpaperPrefernces.getInstance(getApplicationContext()).setHome();
            }
        });
        viewPaymentInfo();

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                //String gaScreen= EpaperSplashActivity.getInstance(MagazineDetailActivity.this).modulePrefix+"-"+MagazineMasterActivity.magazinePrefix+"-"+magazineLang+"-"+magazineName+"-PAGE"+(position+1);
                // Tracking.trackGAScreen(MagazineDetailActivity.this, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).defaultTracker, gaScreen, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).source, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).medium, EpaperSplashActivity.getInstance(MagazineDetailActivity.this).campaign);
                Epaper.getInstance().trackScreenView(MagazineDetailActivity.this, "MAGAZINE" + "-" + magazineLang + "-" + magazineName + "-PAGE" + (position + 1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void viewPaymentInfo() {
        TextView trailMessage = findViewById(R.id.trail_message);
        if (pref.getBooleanValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_ACTIVE, false) && pref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false) && !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            trailMessage.setVisibility(View.VISIBLE);
            String registerNow = "register here";
            String resendOtpText = getString(R.string.trial_message, registerNow);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(resendOtpText));
            spannableStringBuilder2.setSpan(new MySpannable(MagazineDetailActivity.this) {
                @Override
                public void onClick(View widget) {
                    Intent intent = new Intent(MagazineDetailActivity.this, LoginRegistrationActivity.class);
                    intent.putExtra("IS_LOGIN", false);
                    startActivityForResult(intent, SUBSCRIBE_REG_REQUEST_CODE);
                }
            }, Html.fromHtml(resendOtpText).toString().indexOf(registerNow), Html.fromHtml(resendOtpText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            trailMessage.setMovementMethod(LinkMovementMethod.getInstance());
            trailMessage.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
        } else {
            checkAnnualPackDialog();
        }
    }

    private void checkAnnualPackDialog() {
        if (pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.DOMAIN_USER
                && !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)
                && pref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false)) {
            showAnnualPackDialog();
        }
    }

    private void showAnnualPackDialog() {
        final android.support.v7.app.AlertDialog.Builder exitDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.annual_pack_dialog, null);
        exitDialogBuilder.setView(dialogView);

        TextView annual_plan_title = dialogView.findViewById(R.id.annual_plan_title);
        TextView annual_desc = dialogView.findViewById(R.id.annual_desc);
        Button buy_now_btn = dialogView.findViewById(R.id.buy_now_btn);

        annual_plan_title.setText(Html.fromHtml(getString(R.string.annaual_pack_plan_title, Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()).getPlan_rs())));
        annual_desc.setText(Html.fromHtml(getString(R.string.annaual_pack_plan_desc)));

        final AlertDialog annaulPackDialog = exitDialogBuilder.create();
        annaulPackDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        annaulPackDialog.setCancelable(true);

        if (buy_now_btn != null)
            buy_now_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(MagazineDetailActivity.this, BillingInfoActivity.class);
                    intent.putExtra(BillingInfoActivity.EXTRA_ID, Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()).getId());
                    startActivityForResult(intent, BILLING_REQUEST_CODE);

//                    Intent intent = new Intent(DetailSwipeActivity.this, PaymentPlanActivity.class);
//                    intent.putExtra("push_annual_plan", true);
//                    startActivity(intent);
                    annaulPackDialog.dismiss();
                }
            });
        annaulPackDialog.show();
    }

    private void getDetailMagazineData() {
        mApp.getServices().getMagazineDetailData(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                String jsonResponse = ConnectionDetector.convertResponseToString(response);
                Log.e("Jsonresponse", "=>" + jsonResponse);
                progressBar.setVisibility(View.GONE);
                Type listType = new TypeToken<List<MagazineMasterModel>>() {
                }.getType();
                List<MagazineMasterModel> magazinemodel = new Gson().fromJson(jsonResponse, listType);
                List<MagazineMasterModel> MagazineDetailList = new ArrayList<>();
                for (MagazineMasterModel mmm : magazinemodel) {
                    if (mmm.getEditioncode().equals(editioncode) && mmm.getPagedate().equals(editiondate)) {
                        MagazineDetailList.add(mmm);
                        Log.e("magazine_Detail", "=>" + mmm.getDescription() + "====" + mmm.getPagedate());
                    }
                }
                /*viewpager.setAdapter(new MagazinePageDetailAdapter(getSupportFragmentManager(), MagazineDetailList));
                tabs.setupWithViewPager(viewpager);*/
                magazinePageDetailAdapter = new MagazinePageDetailAdapter(getSupportFragmentManager(), MagazineDetailList);
                viewpager.setAdapter(magazinePageDetailAdapter);
                tabs.setupWithViewPager(viewpager);

                if (magazinePageDetailAdapter.getCount() > 0) {
                    Epaper.getInstance().trackScreenView(MagazineDetailActivity.this, "MAGAZINE" + "-" + magazineLang + "-" + magazineName + "-PAGE" + "1");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Utility.setToast(MagazineDetailActivity.this, error.getMessage());
            }
        });
    }

    Dialog subscriptinoAndLoginDialog;

    public void showSubscriptionAndLoginDialog() {
        subscriptinoAndLoginDialog = new Dialog(MagazineDetailActivity.this);
        subscriptinoAndLoginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscriptinoAndLoginDialog.setContentView(R.layout.subscription_and_login);
        subscriptinoAndLoginDialog.setCancelable(false);

        LinearLayout buttonLayout = subscriptinoAndLoginDialog.findViewById(R.id.button_layout);
        TextView verifyEmail = subscriptinoAndLoginDialog.findViewById(R.id.verify_email);
        TextView btnSubscription = subscriptinoAndLoginDialog.findViewById(R.id.subscription);
        TextView btnLogin = subscriptinoAndLoginDialog.findViewById(R.id.login);

        if (!pref.getBooleanValue(QuickPreferences.PaymentPref.IS_DOMAIN_USER_VERIFIED, true)) {
            buttonLayout.setVisibility(View.GONE);
            verifyEmail.setVisibility(View.VISIBLE);

        } else {
            /*Subscription Button Visibility*/
            buttonLayout.setVisibility(View.VISIBLE);
            verifyEmail.setVisibility(View.GONE);

            if (!pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)) {
                btnSubscription.setVisibility(View.VISIBLE);
            } else {
                btnSubscription.setVisibility(View.GONE);
            }

            /*Login Button Visibility*/
            if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                btnLogin.setVisibility(View.GONE);
            } else {
                btnLogin.setVisibility(View.VISIBLE);
            }

            if (btnSubscription.getVisibility() == View.VISIBLE && btnLogin.getVisibility() == View.VISIBLE) {
                subscriptinoAndLoginDialog.findViewById(R.id.or).setVisibility(View.VISIBLE);
            } else {
                subscriptinoAndLoginDialog.findViewById(R.id.or).setVisibility(View.GONE);
            }
        }

        verifyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MagazineDetailActivity.this, AccountSettingsActivity.class), SUBSCRIBE_REQUEST_CODE);
            }
        });
        btnSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(MagazineDetailActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MagazineDetailActivity.this, LoginRegistrationActivity.class);
                intent.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.SUBSCRIPTION_POPUP);
                startActivityForResult(intent, LOGIN_REQUEST_CODE);
            }
        });
        subscriptinoAndLoginDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    subscriptinoAndLoginDialog.dismiss();
                }
                return true;
            }
        });
        subscriptinoAndLoginDialog.show();
    }

    private boolean isShowSubscripionLoginDialog() {
//        Systr.println("APP CONTROL : " + Constants.allowMagazineList.toString() + ", mag : " + magazine);
        if (Epaper.getInstance().getModelManager().getAllowMagazineList().contains(magazine)) {
            return false;
        }
        return !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.TRIAL_USER
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.PAID_USER
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.DOMAIN_USER;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGIN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    subscriptinoAndLoginDialog.dismiss();
                    if (isShowSubscripionLoginDialog())
                        showSubscriptionAndLoginDialog();
                }
                break;
            case SUBSCRIBE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    subscriptinoAndLoginDialog.dismiss();
                    if (isShowSubscripionLoginDialog())
                        showSubscriptionAndLoginDialog();
                }
                break;

        }
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;

        public MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {

        }
    }
}
