package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PasswordValidator;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePasswordActivity extends BaseActivity {

    private EditText OldEdt;
    private EditText NewEdt;
    private EditText ConfrmEdt;
    private AppCompatButton Confirm_Btn;
    private TextView oldPasswordTv;

    Epaper mApp;
    EpaperPrefernces pref;
    private boolean isChangePassword = false;
    private String userid, otp, email;
    private String mobileno;
    private boolean isOldPasswordVisible = false;
    private boolean isNewPasswordVisible = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        mApp = (Epaper) getApplicationContext();
        pref = EpaperPrefernces.getInstance(this);

        OldEdt = findViewById(R.id.change_old_edt);
        NewEdt = findViewById(R.id.change_new_edt);
        ConfrmEdt = findViewById(R.id.confirm_new_edt);
        Confirm_Btn = findViewById(R.id.changepswrd_btn);
        oldPasswordTv = findViewById(R.id.old_password_tv);
        findViewById(R.id.iv_old_pass_visible).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isOldPasswordVisible = !isOldPasswordVisible;
                if (isOldPasswordVisible) {
                    OldEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ((ImageView) findViewById(R.id.iv_old_pass_visible)).setImageResource(R.drawable.ic_visibility_on);
                } else {
                    OldEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    ((ImageView) findViewById(R.id.iv_old_pass_visible)).setImageResource(R.drawable.ic_visibility_off);
                }
                OldEdt.setTypeface(Typeface.DEFAULT);
                OldEdt.setSelection(OldEdt.getText().length());
            }
        });
        findViewById(R.id.iv_new_pass_visible).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNewPasswordVisible = !isNewPasswordVisible;
                if (isNewPasswordVisible) {
                    NewEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    ((ImageView) findViewById(R.id.iv_new_pass_visible)).setImageResource(R.drawable.ic_visibility_on);
                } else {
                    NewEdt.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    ((ImageView) findViewById(R.id.iv_new_pass_visible)).setImageResource(R.drawable.ic_visibility_off);
                }
                NewEdt.setTypeface(Typeface.DEFAULT);
                NewEdt.setSelection(NewEdt.getText().length());
            }
        });
        isChangePassword = getIntent().getBooleanExtra("isChangePassword", false);
        otp = getIntent().getStringExtra("otp");
        email = getIntent().getStringExtra("email");
        mobileno = getIntent().getStringExtra("mobileno");

        if (!isChangePassword) {
            setToolbar("Set Password");
            findViewById(R.id.change_old_layout).setVisibility(View.GONE);
            oldPasswordTv.setVisibility(View.GONE);
        } else {
            OldEdt.setVisibility(View.VISIBLE);
            oldPasswordTv.setVisibility(View.VISIBLE);
            setToolbar("Change Password");
        }

        userid = pref.getStringValue(QuickPreferences.UserInfo.USER_ID, "");

        Confirm_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(ChangePasswordActivity.this)) {
                    if (checkValidation()) {
                        ConfirmClicker();
                    }
                } else {
                    Utility.setNetworkErrorToast(ChangePasswordActivity.this);
                }
            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public boolean checkValidation() {
        String oldPassword = OldEdt.getText().toString();
        String newPassword = NewEdt.getText().toString();
        String confirmPassword = ConfrmEdt.getText().toString();
        if (isChangePassword) {
            if (TextUtils.isEmpty(oldPassword)) {
                OldEdt.setError("Please enter old password");
                return false;
            } else if (!oldPassword.equalsIgnoreCase(pref.getStringValue(QuickPreferences.UserInfo.PASSWORD, ""))) {
                OldEdt.setError("Old password did not matched");
                return false;
            }
        }

        if (TextUtils.isEmpty(newPassword)) {
            NewEdt.setError("Please enter password");
            return false;
        } else if (TextUtils.isEmpty(confirmPassword)) {
            ConfrmEdt.setError("Please enter password");
            return false;
        } else if (!new PasswordValidator().validate(newPassword)) {
            NewEdt.setError(getResources().getString(R.string.password_validation_message));
            findViewById(R.id.password_validate_tv).setVisibility(View.VISIBLE);
            return false;
        } else if (!new PasswordValidator().validate(confirmPassword)) {
            ConfrmEdt.setError("Please enter valid Password");
            findViewById(R.id.con_password_validate_tv).setVisibility(View.VISIBLE);
            return false;
        } else if (!newPassword.equals(confirmPassword)) {
            ConfrmEdt.setError("Password and Confirm password do not match.");
            return false;
        }
        if (isChangePassword && oldPassword.equalsIgnoreCase(confirmPassword)) {
            NewEdt.setError("Old password and New Password should not be same.");
            return false;
        }

        findViewById(R.id.password_validate_tv).setVisibility(View.GONE);
        findViewById(R.id.con_password_validate_tv).setVisibility(View.GONE);
        return true;
    }

    ProgressDialog pd;

    private void setProgressbar() {
        pd = new ProgressDialog(ChangePasswordActivity.this);
        pd.setMessage("Please Wait,We are verifying your mobile number");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    private void ConfirmClicker() {
        String newString = NewEdt.getText().toString();
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("password", newString);

            if (isChangePassword) {
                jsonObject.put("user_id", userid);
            } else {
                jsonObject.put("email", email);
                jsonObject.put("mobileno", mobileno);
                jsonObject.put("otp", otp);
                jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.RESET_PASSWORD_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null) {
                    pd.dismiss();
                }

                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        finish();
                    }
                    Utility.setToast(ChangePasswordActivity.this, response.optString("msg"));
                } catch (Exception e) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(ChangePasswordActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(ChangePasswordActivity.this).addToRequestQueue(request);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void setToolbar(String title) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

}
