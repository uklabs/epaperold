package com.bhaskar.epaper.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.PagerAdapter;
import com.bhaskar.epaper.epaperv2.interfaces.OnSavePdfListener;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.BillingInfoActivity;
import com.bhaskar.epaper.epaperv2.payment.PaymentPlanActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.AccountSettingsActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

public class DetailSwipeActivity extends BaseActivity implements OnSavePdfListener {

    private ViewPager viewpager;
    private TabLayout tabs;
    private ImageView leftimg;
    private ImageView rightimg;
    private AppCompatImageView homeIcon;
    private TabLayout edition_tabs;
    private String description, state, cityName = "";

    public ArrayList<SubMasterModel> listdata;
    EpaperPrefernces pref;
    public static final int LOGIN_REQUEST_CODE = 1010;
    public static final int SUBSCRIBE_REQUEST_CODE = 1020;
    public final static int SUBSCRIBE_REG_REQUEST_CODE = 1030;
    private final static int BILLING_REQUEST_CODE = 1040;
    private TextView trailMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_swipe);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        pref = EpaperPrefernces.getInstance(DetailSwipeActivity.this);

        int position = getIntent().getIntExtra("position", 0);
        description = getIntent().getStringExtra("editionname");
        state = getIntent().getStringExtra("state");
        cityName = getIntent().getStringExtra("city_name");
        if (getIntent().getParcelableArrayListExtra("pages") != null) {
            listdata = getIntent().getParcelableArrayListExtra("pages");
        }

        initViews();

        if (Epaper.getInstance().getModelManager().getListdata() != null && Epaper.getInstance().getModelManager().getListdata().size() > 0) {
            viewpager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
            if (cityName == null) {
                cityName = description;
            }
            Epaper.getInstance().trackScreenView(DetailSwipeActivity.this, state + "-" + cityName + "-" + description + "-PAGE1");
        }
        viewpager.setOffscreenPageLimit(1);
        viewpager.setCurrentItem(position);
        tabs.setupWithViewPager(viewpager);

        if (listdata != null && listdata.size() > 0) {
            for (int i = 0; i < listdata.size(); i++) {
                if (!TextUtils.isEmpty(description)) {
                    if (description.contains(listdata.get(i).description)) {
                        Collections.swap(listdata, i, 0);
                    }
                }
            }
            edition_tabs.setVisibility(View.VISIBLE);
        } else {
            edition_tabs.setVisibility(View.GONE);
        }

        if (edition_tabs.getVisibility() == View.VISIBLE) {
            for (int i = 0; i < listdata.size(); i++) {
                edition_tabs.addTab(edition_tabs.newTab().setText(listdata.get(i).description));
            }
        }

        edition_tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                startActivity(new Intent(DetailSwipeActivity.this, EditionDetailActivity.class).putExtra("statename", state)
                        .putExtra("position", 0)
                        .putExtra("check_comes", "homeSubmaster")
                        .putExtra("editionname", listdata.get(tab.getPosition()).description)
                        .putExtra("editioncode", listdata.get(tab.getPosition()).editioncode)
                        .putParcelableArrayListExtra("pages", listdata)
                );
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                startActivity(new Intent(
                        DetailSwipeActivity.this, EditionDetailActivity.class)
                        .putExtra("statename", state)
                        .putExtra("position", 0)
                        .putExtra("check_comes", "")
                        .putExtra("editionname", listdata.get(tab.getPosition()).description)
                        .putExtra("editioncode", listdata.get(tab.getPosition()).editioncode)
                        .putParcelableArrayListExtra("pages", listdata)
                );
                finish();
            }
        });

        setUpAds();
        homeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EpaperPrefernces.getInstance(getApplicationContext()).setHome();
            }
        });

        rightimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewpagercurrent = viewpager.getCurrentItem();
                if (viewpagercurrent < Epaper.getInstance().getModelManager().getListdata().size()) {
                    viewpager.setCurrentItem(++viewpagercurrent);
                }
            }
        });

        leftimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewpagercurrent = viewpager.getCurrentItem();
                if (viewpagercurrent > 0) {
                    viewpager.setCurrentItem(--viewpagercurrent);
                }
            }
        });
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Epaper.getInstance().trackScreenView(DetailSwipeActivity.this, state + "-" + cityName + "-" + description + "-PAGE" + (position + 1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void initViews() {
        viewpager = findViewById(R.id.dailog_viewpager);
        tabs = findViewById(R.id.tabs);
        leftimg = findViewById(R.id.dailog_left_arrow_img);
        rightimg = findViewById(R.id.dailog_right_arrow_img);
        homeIcon = findViewById(R.id.homeIcon);
        edition_tabs = findViewById(R.id.edition_tabs);
        viewPaymentInfo();
    }

    private void viewPaymentInfo() {
        trailMessage = findViewById(R.id.trail_message);
        if (pref.getBooleanValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_ACTIVE, false)
                && pref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false)
                && !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            trailMessage.setVisibility(View.VISIBLE);
            String registerNow = "register here";
            String resendOtpText = getString(R.string.trial_message, registerNow);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(resendOtpText));
            spannableStringBuilder2.setSpan(new MySpannable(DetailSwipeActivity.this) {
                @Override
                public void onClick(View widget) {
                    Intent intent = new Intent(DetailSwipeActivity.this, LoginRegistrationActivity.class);
                    intent.putExtra("IS_LOGIN", false);
                    startActivityForResult(intent, SUBSCRIBE_REG_REQUEST_CODE);
                }
            }, Html.fromHtml(resendOtpText).toString().indexOf(registerNow), Html.fromHtml(resendOtpText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            trailMessage.setMovementMethod(LinkMovementMethod.getInstance());
            trailMessage.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
        } else {
            checkAnnualPackDialog();
        }
    }

    private void checkAnnualPackDialog() {
        if (pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.DOMAIN_USER
                && !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)
                && pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)
                && pref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false)) {
            showAnnualPackDialog();
        }
    }

    private void showAnnualPackDialog() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.annual_pack_dialog, null);
        exitDialogBuilder.setView(dialogView);

        TextView annual_plan_title = dialogView.findViewById(R.id.annual_plan_title);
        TextView annual_desc = dialogView.findViewById(R.id.annual_desc);
        Button buy_now_btn = dialogView.findViewById(R.id.buy_now_btn);

        annual_plan_title.setText(Html.fromHtml(getString(R.string.annaual_pack_plan_title, Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()).getPlan_rs())));
        annual_desc.setText(Html.fromHtml(getString(R.string.annaual_pack_plan_desc)));

        final AlertDialog annaulPackDialog = exitDialogBuilder.create();
        annaulPackDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        annaulPackDialog.setCancelable(true);

        if (buy_now_btn != null)
            buy_now_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    annaulPackDialog.dismiss();

                    Intent intent = new Intent(DetailSwipeActivity.this, BillingInfoActivity.class);
                    intent.putExtra(BillingInfoActivity.EXTRA_ID, Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()).getId());
                    startActivityForResult(intent, BILLING_REQUEST_CODE);
                }
            });
        annaulPackDialog.show();
    }

    private void setUpAds() {
        if (Util.isShowAds(DetailSwipeActivity.this)) {
            findViewById(R.id.adView).setVisibility(View.VISIBLE);
            new AdUtility(this).loadBannerAd((AdView) findViewById(R.id.adView));
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }
    }

    protected void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
                fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    Dialog subscriptinoAndLoginDialog;

    public void showSubscriptionAndLoginDialog() {
        subscriptinoAndLoginDialog = new Dialog(DetailSwipeActivity.this);
        subscriptinoAndLoginDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        subscriptinoAndLoginDialog.setContentView(R.layout.subscription_and_login);
        subscriptinoAndLoginDialog.setCancelable(false);

        LinearLayout buttonLayout = subscriptinoAndLoginDialog.findViewById(R.id.button_layout);
        TextView verifyEmail = subscriptinoAndLoginDialog.findViewById(R.id.verify_email);
        TextView btnSubscription = subscriptinoAndLoginDialog.findViewById(R.id.subscription);
        TextView btnLogin = subscriptinoAndLoginDialog.findViewById(R.id.login);

        if (!pref.getBooleanValue(QuickPreferences.PaymentPref.IS_DOMAIN_USER_VERIFIED, true)) {
            buttonLayout.setVisibility(View.GONE);
            verifyEmail.setVisibility(View.VISIBLE);

        } else {
            /*Subscription Button Visibility*/
            buttonLayout.setVisibility(View.VISIBLE);
            verifyEmail.setVisibility(View.GONE);

            if (!pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)) {
                btnSubscription.setVisibility(View.VISIBLE);
            } else {
                btnSubscription.setVisibility(View.GONE);
            }

            /*Login Button Visibility*/
            if (pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                btnLogin.setVisibility(View.GONE);
            } else {
                btnLogin.setVisibility(View.VISIBLE);
            }

            if (btnSubscription.getVisibility() == View.VISIBLE && btnLogin.getVisibility() == View.VISIBLE) {
                subscriptinoAndLoginDialog.findViewById(R.id.or).setVisibility(View.VISIBLE);
            } else {
                subscriptinoAndLoginDialog.findViewById(R.id.or).setVisibility(View.GONE);
            }
        }

        verifyEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DetailSwipeActivity.this, AccountSettingsActivity.class), SUBSCRIBE_REQUEST_CODE);
            }
        });
        btnSubscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(DetailSwipeActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailSwipeActivity.this, LoginRegistrationActivity.class);
                intent.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.SUBSCRIPTION_POPUP);
                startActivityForResult(intent, LOGIN_REQUEST_CODE);
            }
        });
        subscriptinoAndLoginDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    subscriptinoAndLoginDialog.dismiss();
                }
                return true;
            }
        });
        subscriptinoAndLoginDialog.show();
    }

    private boolean isShowSubscripionLoginDialog() {
        if (Epaper.getInstance().getModelManager().getAllowStateList().contains(state)) {
            return false;
        }

        return !pref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.TRIAL_USER
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.PAID_USER
                && pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.DOMAIN_USER;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGIN_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    subscriptinoAndLoginDialog.dismiss();
                    if (isShowSubscripionLoginDialog())
                        showSubscriptionAndLoginDialog();
                }
                break;
            case SUBSCRIBE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    subscriptinoAndLoginDialog.dismiss();
                    if (isShowSubscripionLoginDialog())
                        showSubscriptionAndLoginDialog();
                }
                break;
            case SUBSCRIBE_REG_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    trailMessage.setVisibility(View.GONE);
                }
                break;
//            case BILLING_REQUEST_CODE:
//                checkAnnualPackDialog();
//                break;

        }
        setUpAds();
    }


    @Override
    public void saveAsPdf(int position) {
        showSavePdfDialog(position);
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;

        MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {

        }
    }

    private void showSavePdfDialog(int position) {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(DetailSwipeActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_save_pdf_dialog, null);

        Button singlePage = dialogView.findViewById(R.id.single_page);
        Button allPages = dialogView.findViewById(R.id.all_pages);

        exitDialogBuilder.setView(dialogView);
        final AlertDialog gdprDialog = exitDialogBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*do some work here*/

        singlePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        allPages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        gdprDialog.show();
    }
}
