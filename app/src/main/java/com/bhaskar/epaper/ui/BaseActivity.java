package com.bhaskar.epaper.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.utils.EpaperPrefernces;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isBlockedCountry = EpaperPrefernces.getInstance(BaseActivity.this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
        Util.onResume(BaseActivity.this, isBlockedCountry);
    }

    @Override
    protected void onPause() {
        super.onPause();
        boolean isBlockedCountry = EpaperPrefernces.getInstance(BaseActivity.this).getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false);
        Util.onPause(BaseActivity.this, isBlockedCountry);
    }
}
