package com.bhaskar.epaper.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MyEditionAdapter;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonElement;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyEditionActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private AVLoadingIndicatorView progress;

    Epaper mApp;
    EpaperPrefernces mPref;
    TextView noDataTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookmark);

        setToolbar();

        recyclerView = findViewById(R.id.homeRecyclerList);
        progress = findViewById(R.id.progress);
        progress.setIndicatorColor(ContextCompat.getColor(MyEditionActivity.this, R.color.colorPrimaryDark));
        noDataTv = findViewById(R.id.no_data_tv);
        mApp = (Epaper) getApplicationContext();
        mPref = EpaperPrefernces.getInstance(MyEditionActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(MyEditionActivity.this));

        if (Util.isShowAds(MyEditionActivity.this)) {
            AdUtility adUtility = new AdUtility(this);
            adUtility.loadBannerAd((AdView) findViewById(R.id.adView));
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerAdapter();
        Epaper.getInstance().trackScreenView(MyEditionActivity.this,"MyEditionActivity");
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyEditionActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("My Edition");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecyclerAdapter() {
        mApp.getServices().getMyEditionData(mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""), new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                Log.i("Response", "=>" + jsonElement.toString());
                try {
                    setParsingJson(jsonElement.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

    }

    private void setParsingJson(String response) throws Exception {
        int columno = Epaper.getInstance().getModelManager().getColumnCount();//Utility.calculateNoOfColumns(this);
        progress.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new GridLayoutManager(this, columno));
        List<EditionModel> setEditionModelList = new ArrayList<>();
        JSONObject jb = new JSONObject(response);

        if (jb.getString("status").equals("1")) {
            JSONObject jbData = jb.getJSONObject("data");
            JSONArray dataArray = jbData.getJSONArray("data");
            if (dataArray == null || dataArray.length() == 0) {
                Utility.setToast(MyEditionActivity.this, "No Data Found");
            }
            Log.e("myedition", dataArray.length() + "");
            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject datavalue = (JSONObject) dataArray.get(i);
                String id = datavalue.optString("id");
                String state = datavalue.optString("state");
                String center = datavalue.optString("center");
                String edtname = datavalue.optString("edtname");
                String edtcode = datavalue.optString("edtcode");
                String org_url = datavalue.optString("org_path");
                String thumb_url = datavalue.optString("thumb_path");
                String large_url = datavalue.optString("large_path");
                setEditionModelList.add(new EditionModel(id, state, center, edtname, edtcode, org_url, thumb_url, large_url));
//                setEditionModelList.add(new EditionModel(id, state, center, edtname, edtcode, org_url, getImagePath(thumb_url), large_url));
            }
            recyclerView.setAdapter(new MyEditionAdapter(MyEditionActivity.this, setEditionModelList));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            noDataTv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            noDataTv.setVisibility(View.VISIBLE);
            noDataTv.setText(getString(R.string.no_edition_available));
            recyclerView.setVisibility(View.GONE);
        }
    }

    public String getImagePath(String imageurl) {

        if (imageurl.contains("epaper.dbpost.com")) {
            String imagepath = "http://epaper.dbpost.com/epaper_images/";
            String[] arrayName = imageurl.split("/");
            String stateName = null;

            for (int i = 0; i < arrayName.length; i++) {
                Log.e("split", "split=>" + arrayName[i] + "---" + i);
            }
            int sizeofdate = getDateEditionSize(arrayName[6]);
            if (sizeofdate <= 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(1);
                Log.e("statename", "=>sub <=10=" + stateName);
            } else if (sizeofdate > 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(2);
                Log.e("statename", "=>10=>sub" + stateName);
            }
            return imagepath + arrayName[4] + "/epaperimages/" + DateUtility.getMyEditionCurrentDate() + "/" + stateName;
//            return "";

        } else if (imageurl.contains("dnaindia.com")) {
            return "http://app.dbclmatrix.com/dna.jpg";
        } else {
            String imagepath = "http://digitalimages.bhaskar.com/";
            String[] arrayName = imageurl.split("/");
            String stateName = null;// = arrayName[arrayName.length - 1];

            for (int i = 0; i < arrayName.length; i++) {
                Log.e("split", "split=>" + arrayName[i] + "---" + i);
            }

            int sizeofdate = getDateEditionSize(arrayName[5]);
            if (sizeofdate <= 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(1);
                Log.e("statename", "=>sub <=10=" + stateName);
            } else if (sizeofdate > 10) {
                stateName = (getDateEditionSize(DateUtility.getMyEditionCurrentDate()) - 1) + arrayName[arrayName.length - 1].substring(2);
                Log.e("statename", "=>10=>sub" + stateName);
            }
            return imagepath + arrayName[3] + "/EpaperImages/" + DateUtility.getMyEditionCurrentDate() + "/" + stateName;
        }


    }

    public int getDateEditionSize(String date) {
        return Integer.parseInt(date.substring(0, 2));
    }

}
