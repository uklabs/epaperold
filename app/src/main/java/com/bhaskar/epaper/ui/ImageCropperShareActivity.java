package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.my_cropper.CropImageView;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtility;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Utility;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonElement;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class ImageCropperShareActivity extends BaseActivity {
    private CropImageView cropImageView;
    private ImageView setCropperImage;
    private Button crop_btn;
    private Button share_btn;
    private Button save_btn;
    private Button reset_btn;
    private RelativeLayout fullRelativeImage;
    private ScrollView layRelViewHolder;
    private AdView adview;
    private LinearLayout bottom_ll;
    private RelativeLayout relativeImage;
    private TextView editionDetailTxt;
    private ImageView logo_banner;
    private EditText edtComment;
    private LinearLayout layCommnetContainer;

    String imageUrl;
    String editionname;
    String editioncode;
    String description;
    String pageno;
    String state;
    String imgpath;

    ImageCropperShareActivity mActivity;

    public static Bitmap imagebitmap;
    public static boolean isShowTime = false;

    ProgressDialog progressDialog;
    private Epaper mApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_cropper_share);
        mActivity = this;

        mApp = (Epaper) getApplicationContext();
        cropImageView = findViewById(R.id.cropImageView);
        setCropperImage = findViewById(R.id.setCropperImage);
        crop_btn = findViewById(R.id.crop_activity_btn);
        share_btn = findViewById(R.id.share_activity_btn);
        save_btn = findViewById(R.id.save_activity_btn);
        reset_btn = findViewById(R.id.reset_activity_btn);
        fullRelativeImage = findViewById(R.id.fullRelative_image);
        layRelViewHolder = findViewById(R.id.layRelViewHolder);
        adview = findViewById(R.id.adView);
        bottom_ll = findViewById(R.id.bottom_ll);
        relativeImage = findViewById(R.id.relative_image);
        editionDetailTxt = findViewById(R.id.edition_detail_txt);
        logo_banner = findViewById(R.id.logo_banner);
        edtComment = findViewById(R.id.edtComment);
        layCommnetContainer = findViewById(R.id.layCommnetContainer);


        progressDialog = new ProgressDialog(mActivity);
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(false);

        layCommnetContainer.bringToFront();
        edtComment.setSelected(false);

        imageUrl = getIntent().getStringExtra("largeUrl");
        editionname = getIntent().hasExtra("editionname") ? getIntent().getStringExtra("editionname") : "";
        editioncode = getIntent().hasExtra("editioncode") ? getIntent().getStringExtra("editioncode") : "";
        description = getIntent().getStringExtra("description");
        pageno = getIntent().getStringExtra("pageno");
        imgpath = getIntent().getStringExtra("imgpath");

        Toast.makeText(this, "editionname :" + editionname + " , editioncode:" + editioncode, Toast.LENGTH_LONG).show();

        String setasDate = EpaperPrefernces.getInstance(mActivity).getStringValue(PreferncesConstant.setDateAsDefult, "");
        editionDetailTxt.setText(DateUtility.parseDateToddMMyyyy(setasDate) + "\n" + description);

        if (imgpath != null) {
            if (imgpath.startsWith("http://digitalimages.bhaskar.com/gujarat")) {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divyabhaskar_logo));
            } else if (imgpath.startsWith("http://digitalimages.bhaskar.com/divyamarathi")) {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.divya_logo));
            } else {
                logo_banner.setImageDrawable(getResources().getDrawable(R.drawable.dainik_logo));
            }
        }

        setToolbar();

        if (Util.isShowAds(ImageCropperShareActivity.this)) {
            final AdUtility adUtility = new AdUtility(this);

            adUtility.loadBannerAd(adview);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    adUtility.loadInterstitial();
                }
            }, 72000);
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(imageUrl)) {
            try {
                cropImageView.setImageBitmap(imagebitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
            setCropperImage.setImageBitmap(imagebitmap);
            setAccessVisibility(false);
        } else {

            cropImageView.setVisibility(View.GONE);
            setCropperImage.setVisibility(View.GONE);
            Log.i("imageUrl", "=>" + imageUrl);
            cropImageView.setImageUriAsync(Uri.parse(imageUrl));
            cropImageView.setVisibility(View.VISIBLE);
            setAccessVisibility(false);

        }


        crop_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        try {

                            relativeImage.setVisibility(View.VISIBLE);
                            fullRelativeImage.setVisibility(View.GONE);
                            setAccessVisibility(true);

                        } catch (Exception e) {

                            Utility.setToast(mActivity, "Please Work Properly");
                            e.printStackTrace();

                        }


                    }
                });


        reset_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setCropperImage.setVisibility(View.GONE);
                cropImageView.setVisibility(View.VISIBLE);
                fullRelativeImage.setVisibility(View.VISIBLE);
                relativeImage.setVisibility(View.GONE);

                try {
                    cropImageView.setImageBitmap(imagebitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                setAccessVisibility(false);
            }
        });

        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layRelViewHolder.setVisibility(View.GONE);
                adview.setVisibility(View.GONE);
                bottom_ll.setVisibility(View.GONE);
                //scrollView.setVisibility(View.GONE);
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                ImageUtility.saveImage(bt, mActivity);

                layRelViewHolder.setVisibility(View.VISIBLE);
                adview.setVisibility(View.VISIBLE);
                bottom_ll.setVisibility(View.VISIBLE);
                String comments = edtComment.getText().toString();


                setBestStory(comments, editioncode, editionname, imageUrl, getBitmapToFilePath(bt));


                //scrollView.setVisibility(View.VISIBLE);
            }
        });

        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bt = ImageUtility.getBitmapFromView(relativeImage);
                Log.e("permission", "=>share_btn");
                ImageUtility.saveSharedImage(bt, mActivity, description);
            }
        });

    }

    private void setToolbar() {
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("E-Paper");
    }


    public void setAccessVisibility(Boolean flag) {


        if (flag) {

            save_btn.setVisibility(View.VISIBLE);
            share_btn.setVisibility(View.VISIBLE);
            layCommnetContainer.setVisibility(View.VISIBLE);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    2.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            save_btn.setLayoutParams(layoutParams);
            share_btn.setLayoutParams(layoutParams);
            cropImageView.setVisibility(View.VISIBLE);
            setCropperImage.setVisibility(View.VISIBLE);
            setCropperImage.setImageBitmap(cropImageView.getCroppedImage());


        } else {

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    4.0f
            );

            crop_btn.setLayoutParams(layoutParams);
            reset_btn.setLayoutParams(layoutParams);
            layCommnetContainer.setVisibility(View.GONE);

            share_btn.setVisibility(View.GONE);
            save_btn.setVisibility(View.GONE);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Epaper.getInstance().trackScreenView(this,"ImageCropperActivity");
    }


    private String getBitmapToFilePath(Bitmap bitmap) {

        String path = "";
        File myDir;

        if (ImageUtility.hasStorage(true)) {

            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            myDir = new File(root + "/" + getString(R.string.foldername));

        } else {

            ContextWrapper cw = new ContextWrapper(this);
            myDir = cw.getDir(getString(R.string.foldername), Context.MODE_PRIVATE);
        }

        myDir.mkdirs();

        Random generator = new Random();
        int n = 100000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        if (file.exists())
            file.delete();

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            //ImageUtility.galleryAddPic(file.getAbsolutePath(), this);
            out.flush();
            out.close();
            path = file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return path;
    }


    private void setBestStory(String comment, String edition_code, String edition_name, String img_org_path, String filePath) {


        Log.d("filePath", filePath);
        final File imageFile = new File(filePath);
        TypedFile typedFile = null;

        if (imageFile != null) {
            Log.i("sendfile", "=>" + imageFile.getAbsolutePath());
            try {

                typedFile = new TypedFile("multipart/form-data", new File(imageFile.getAbsolutePath()));

                progressDialog.show();
                mApp.getServices().setBestStory(comment, edition_code, edition_name, img_org_path, typedFile, new Callback<JsonElement>() {

                    @Override
                    public void success(JsonElement jsonElement, Response response) {

                        if (response != null) {
                            Log.d("setBestStory", response.toString());
                            Toast.makeText(mActivity, response.toString(), Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                        if (imageFile.exists()) {
                            imageFile.delete();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(mActivity, error.getMessage(), Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        if (imageFile.exists()) {
                            imageFile.delete();
                        }
                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
