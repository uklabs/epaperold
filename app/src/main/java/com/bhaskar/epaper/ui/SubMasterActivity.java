package com.bhaskar.epaper.ui;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.HomeSubMasterAdapter;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.RequestHandler;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.XMLParser;
import com.google.android.gms.ads.AdView;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class SubMasterActivity extends BaseActivity {

    Epaper epaper;
    String stateName, editioncode, desc;

    private RecyclerView homeRecyclerList;
    private AVLoadingIndicatorView progressBar;

    public static List<SubMasterModel> filterEditionList = new ArrayList<>();
    EpaperPrefernces mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_master);
        mPref = EpaperPrefernces.getInstance(SubMasterActivity.this);
        homeRecyclerList = findViewById(R.id.homeRecyclerList);
        progressBar = findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(SubMasterActivity.this, R.color.colorPrimaryDark));
        handleDeepLinks(getIntent());
        setToolbar();
        if (Util.isShowAds(SubMasterActivity.this)) {
            AdUtility adUtility = new AdUtility(this);
            adUtility.loadBannerAd((AdView) findViewById(R.id.adView));
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }

        epaper = (Epaper) getApplicationContext();
        stateName = getIntent().getStringExtra("statename"); //"RAJSATHAN";//
        desc = getIntent().getStringExtra("description");
        editioncode = getIntent().getStringExtra("editioncode");//"14";
        callXmlsetter();
        if (Epaper.getInstance().getModelManager().getYearlyPlan(Epaper.getInstance().getChannelSlno()) == null) {
            if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
                RequestHandler.makeYearlyPlanHit(SubMasterActivity.this, Epaper.getInstance().getChannelSlno());

            } else {
                if (!stateName.equalsIgnoreCase("MAHARASHTRA"))
                    Utility.setToast(SubMasterActivity.this, "Please Login..");
            }

        }
    }

    private void handleDeepLinks(Intent intent) {

        if (intent != null) {
            String action = intent.getAction();
            String data = intent.getDataString();
            if (Intent.ACTION_MAIN.equals(action) && data != null) {
                Toast.makeText(this, data, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "No data", Toast.LENGTH_LONG).show();
        }

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_action_back);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowHomeEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeAsUpIndicator(upArrow);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void callXmlsetter() {
        progressBar.setVisibility(View.VISIBLE);

        if (ConnectionDetector.isConnectingToInternet(this)) {

            String filename = "jx-submaster/xml/" + stateName + "/" + DateUtility.setDateForAll + "/";
            Log.d("filename", "=>" + filename);

            String url = Urls.BASE_URL + filename;
            StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
                @Override
                public void onResponse(String res) {
                    Log.d("filename", "=>" + res);
                    setXmlParsingMaster(res);
                    progressBar.setVisibility(View.GONE);

                }
            }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
                }
            });
            request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(SubMasterActivity.this).addToRequestQueue(request);

        } else {
            progressBar.setVisibility(View.GONE);

            Toast.makeText(getApplicationContext(), "Check Network Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void setXmlParsingMaster(String xmlres) {
        XMLParser xmlparser = new XMLParser();
        Document doc = xmlparser.getDomElement(xmlres);
        NodeList nl = doc.getElementsByTagName("data");
        List<SubMasterModel> stateEditionList = new ArrayList<>();
        for (int i = 0; i < nl.getLength(); i++) {
            Element e = (Element) nl.item(i);
//            String description = xmlparser.getValue(e, "editionname"); // cost child value
            String editioncode = xmlparser.getValue(e, "editioncode"); // cost child value
            String priorty = xmlparser.getValue(e, "priorty"); // editionname child value
            String parent = xmlparser.getValue(e, "parent"); // name child value
            String imagepath = xmlparser.getValue(e, "imagepath"); // editionname child value
            String imagethumb = xmlparser.getValue(e, "imagethumb"); // editionname child value
            String imagelarge = xmlparser.getValue(e, "imagelarge"); // editionname child value
            String description = xmlparser.getValue(e, "description"); // editionname child value
            Log.e("edition", "=>" + editioncode);
            stateEditionList.add(new SubMasterModel(description, priorty, editioncode, parent, imagepath, imagethumb, imagelarge));
        }
        setAdapter(stateEditionList);

    }


    private void setAdapter(List<SubMasterModel> stateEditionList) {
        int columno = Epaper.getInstance().getModelManager().getColumnCount();//Utility.calculateNoOfColumns(this);

        homeRecyclerList.setLayoutManager(new GridLayoutManager(this, columno));
        ArrayList<SubMasterModel> filterEdition = new ArrayList<>();
        filterEditionList.clear();
        for (SubMasterModel sm : stateEditionList) {
            Log.d("Edition", sm.parent + "===" + editioncode);
            if (sm.parent.equals(editioncode)) {
                filterEdition.add(sm);
                filterEditionList.add(sm);
            }
        }

        homeRecyclerList.setAdapter(new HomeSubMasterAdapter(SubMasterActivity.this, filterEdition, stateName, desc));
        homeRecyclerList.setItemAnimator(new DefaultItemAnimator());
        Epaper.getInstance().trackScreenView(SubMasterActivity.this, stateName + "-" + desc);
    }

}
