package com.bhaskar.epaper.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.RequestHandler;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.gdpr.GDPRControllerActivity;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.KeyBenefitsInfo;
import com.bhaskar.epaper.model.MainDataModel;
import com.bhaskar.epaper.model.SearchModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SplashActivity extends BaseActivity {

    private static final int REQUEST_CODE_GDPR_CONSENT = 1010;
    private TextView progres_txt;
    private ProgressBar progress_bar;
    private ImageView retlyImgView;
    private RelativeLayout retryLay;

    private String setDate = "";
    private Epaper mApp;
    private EpaperPrefernces pref;
    public static String getDate = null;
    private boolean isAppControlParse = false;
    private boolean isHeaderParse = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_splash);
        ModelManager.clearAllData();
        /*if date is given or comming from other activity than takes it*/
        if (getIntent() != null) {
            if (getIntent().getStringExtra("date") != null) {
                if (!TextUtils.isEmpty(getIntent().getStringExtra("date"))) {
                    setDate = getDate = getIntent().getStringExtra("date");
                }
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startSplash();
            }
        }, 300);

    }

    private void startSplash() {
        pref = EpaperPrefernces.getInstance(SplashActivity.this);
        mApp = (Epaper) getApplicationContext();
        progres_txt = findViewById(R.id.progres_txt);
        progress_bar = findViewById(R.id.progress_bar);
        retlyImgView = findViewById(R.id.retlyImgView);
        retryLay = findViewById(R.id.retryLay);

        //clear app data
        Util.checkDateAndClearCache(SplashActivity.this);
        HomeContent.getHomeContent().listMain.clear();
        startAppControl();
    }

    private void startAppControl() {
        if (Utility.isConnectingToInternet(SplashActivity.this)) {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, String.format(Urls.APP_CONTROL_API_URL, Constants.HOST_ID),
                    new com.android.volley.Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            if (!TextUtils.isEmpty(response.toString())) {
                                try {
                                    parseAppControl(response.getJSONObject("data"));
                                    parseHeaders(response.getJSONObject("headers"));
                                } catch (Exception e) {
                                    onAppControlFailure();
                                }
                            } else {
                                onAppControlFailure();
                            }
                        }
                    }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    onAppControlFailure();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("api-key", Constants.API_KEY_VALUE);
                    headers.put("User-Agent", Constants.USER_AGENT);
                    headers.put("encrypt", "0");
                    headers.put("Content-Type", "application/json charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                        JSONObject jsonResponse = new JSONObject();
                        jsonResponse.put("data", new JSONObject(jsonString));
                        jsonResponse.put("headers", new JSONObject(response.headers));
                        return com.android.volley.Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return com.android.volley.Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return com.android.volley.Response.error(new ParseError(je));
                    }
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjReq);
        } else {
            setErrorText(1);
        }
    }

    private void onAppControlFailure() {
        isAppControlParse = true;
        isHeaderParse = true;
        pref.setBooleanValue(QuickPreferences.GdprConst.IS_FAIL_VERSION, true);
        afterAppControl();
    }

    private void parseAppControl(JSONObject mainObject) {
        try {
            if (TextUtils.isEmpty(setDate))
                setDate = getDate = mainObject.getString("date");
            JSONObject dainikJsonObject = mainObject.getJSONObject(Constants.AppIds.DAINIK_CHANNEL);
            try {
                if (dainikJsonObject.has("subscription")) {
                    JSONObject subscriptionJsonObject = dainikJsonObject.getJSONObject("subscription");
                    String subscriptionHeader = subscriptionJsonObject.optString("subscription_header");
                    String subscriptionTitle = subscriptionJsonObject.optString("free_subscription_title");
                    String subscriptionDesc = subscriptionJsonObject.optString("free_subscription_desc");
                    String msgFreeTrailEnd = subscriptionJsonObject.optString("msg_free_trail_end");
                    String msgSubscriptionEnd = subscriptionJsonObject.optString("msg_subscription_end");
                    String videoHelpUrl = subscriptionJsonObject.optString("help_url");
                    String videoHelpTitle = subscriptionJsonObject.optString("help_title");
                    String appInstallHeader = subscriptionJsonObject.optString("app_install_header");
                    String appInstallLink = subscriptionJsonObject.optString("app_install_link");
                    String appInstallImg = subscriptionJsonObject.optString("app_install_img");
                    String keyBenefitsDainik = subscriptionJsonObject.optString("key_benefits");

                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_TRIAL_END_TEXT, Constants.AppIds.DAINIK_CHANNEL), msgFreeTrailEnd);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.SUBSCRIPTION_END_TEXT, Constants.AppIds.DAINIK_CHANNEL), msgSubscriptionEnd);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.SUBSCRIPTION_HEADER_TEXT, Constants.AppIds.DAINIK_CHANNEL), subscriptionHeader);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_HEADER_TITLE, Constants.AppIds.DAINIK_CHANNEL), subscriptionTitle);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_HEADER_DESC, Constants.AppIds.DAINIK_CHANNEL), subscriptionDesc);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.VIDEO_HELP_URL, Constants.AppIds.DAINIK_CHANNEL), videoHelpUrl);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.VIDEO_HELP_TITLE, Constants.AppIds.DAINIK_CHANNEL), videoHelpTitle);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_HEADER, Constants.AppIds.DAINIK_CHANNEL), appInstallHeader);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_LINK, Constants.AppIds.DAINIK_CHANNEL), appInstallLink);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_IMG, Constants.AppIds.DAINIK_CHANNEL), appInstallImg);
                    ArrayList<KeyBenefitsInfo> keyBenefitsInfosDainik = new ArrayList<>(Arrays.asList(new Gson().fromJson(keyBenefitsDainik, KeyBenefitsInfo[].class)));
                    keyBenefitsInfosDainik.add(0, new KeyBenefitsInfo(true));
                    Epaper.getInstance().getModelManager().setStringListMap(Constants.AppIds.DAINIK_CHANNEL, keyBenefitsInfosDainik);
                }
            } catch (Exception ignored) {
            }
            JSONObject divyaJsonObject = mainObject.getJSONObject(Constants.AppIds.DIVYA_CHANNEL);
            try {
                if (divyaJsonObject.has("subscription")) {
                    JSONObject subscriptionJsonObject = divyaJsonObject.getJSONObject("subscription");
                    String subscriptionHeader = subscriptionJsonObject.optString("subscription_header");
                    String subscriptionTitle = subscriptionJsonObject.optString("free_subscription_title");
                    String subscriptionDesc = subscriptionJsonObject.optString("free_subscription_desc");
                    String msgFreeTrailEnd = subscriptionJsonObject.optString("msg_free_trail_end");
                    String msgSubscriptionEnd = subscriptionJsonObject.optString("msg_subscription_end");
                    String videoHelpUrl = subscriptionJsonObject.optString("help_url");
                    String videoHelpTitle = subscriptionJsonObject.optString("help_title");
                    String appInstallHeader = subscriptionJsonObject.optString("app_install_header");
                    String appInstallLink = subscriptionJsonObject.optString("app_install_link");
                    String appInstallImg = subscriptionJsonObject.optString("app_install_img");
                    String keyBenefitsDivya = subscriptionJsonObject.optString("key_benefits");

                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_TRIAL_END_TEXT, Constants.AppIds.DIVYA_CHANNEL), msgFreeTrailEnd);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.SUBSCRIPTION_END_TEXT, Constants.AppIds.DIVYA_CHANNEL), msgSubscriptionEnd);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.SUBSCRIPTION_HEADER_TEXT, Constants.AppIds.DIVYA_CHANNEL), subscriptionHeader);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_HEADER_TITLE, Constants.AppIds.DIVYA_CHANNEL), subscriptionTitle);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_HEADER_DESC, Constants.AppIds.DIVYA_CHANNEL), subscriptionDesc);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.VIDEO_HELP_URL, Constants.AppIds.DIVYA_CHANNEL), videoHelpUrl);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.VIDEO_HELP_TITLE, Constants.AppIds.DIVYA_CHANNEL), videoHelpTitle);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_HEADER, Constants.AppIds.DIVYA_CHANNEL), appInstallHeader);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_LINK, Constants.AppIds.DIVYA_CHANNEL), appInstallLink);
                    pref.setStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_IMG, Constants.AppIds.DIVYA_CHANNEL), appInstallImg);
                    ArrayList<KeyBenefitsInfo> keyBenefitsInfosDivya = new ArrayList<>(Arrays.asList(new Gson().fromJson(keyBenefitsDivya, KeyBenefitsInfo[].class)));
                    keyBenefitsInfosDivya.add(0, new KeyBenefitsInfo(true));
                    Epaper.getInstance().getModelManager().setStringListMap(Constants.AppIds.DIVYA_CHANNEL, keyBenefitsInfosDivya);
                }
            } catch (Exception ignored) {
            }

            /* App Update */
            JSONObject jsonObject = mainObject.getJSONObject("data");
            try {
                if (jsonObject.has("update_popup")) {
                    JSONObject updatePopupJsonObject = jsonObject.getJSONObject("update_popup");
                    if (updatePopupJsonObject != null) {
                        String app_version = updatePopupJsonObject.optString("version");
                        String force_update = updatePopupJsonObject.optString("force_update");
                        String message = updatePopupJsonObject.optString("message");
                        pref.setIntValue(QuickPreferences.AppUpdate.UPDATE_POPUP_VERSION_CODE, Integer.parseInt(app_version));
                        pref.setIntValue(QuickPreferences.AppUpdate.FORCE_UPDATE_VERSION, Integer.parseInt(force_update));
                        pref.setStringValue(QuickPreferences.AppUpdate.UPDATE_POPUP_MESSAGE, message);
                    }
                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("subscription")) {
                    JSONObject subsJsonObject = jsonObject.getJSONObject("subscription");

                    boolean freeSubsActive = subsJsonObject.optString("free_subscription_active").equalsIgnoreCase("1");
                    pref.setBooleanValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_ACTIVE, freeSubsActive);

                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("user_alert")) {
                    JSONObject userAlertJsonObject = jsonObject.getJSONObject("user_alert");
                    String userAlertTitle = userAlertJsonObject.optString("user_alert_title");
                    String userAlertMsg = userAlertJsonObject.optString("user_alert_message");
                    pref.setStringValue(QuickPreferences.CommonPref.USER_ALERT_TITLE, userAlertTitle);
                    pref.setStringValue(QuickPreferences.CommonPref.USER_ALERT_MSG, userAlertMsg);

                }
            } catch (Exception ignored) {
            }

            /*feedback_option*/
            try {
                if (jsonObject.has("feedback_option")) {
                    JSONObject feedbackOption = jsonObject.getJSONObject("feedback_option");
                    String feedbackOptionJSONArray = feedbackOption.optString("value");
                    JSONArray jsonArray = new JSONArray(feedbackOptionJSONArray);
                    if (jsonArray.length() > 0) {
                        Constants.feedbackTopics = new String[jsonArray.length() + 1];
                        Constants.feedbackTopics[0] = "Select Topic";
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Constants.feedbackTopics[i + 1] = jsonArray.getString(i);
                        }
                    }
                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("contact_address1")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("contact_address1");
                    pref.setStringValue(QuickPreferences.ContactInfo.ADDRESS_1, jsonObj.toString());
                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("contact_address2")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("contact_address2");
                    pref.setStringValue(QuickPreferences.ContactInfo.ADDRESS_2, jsonObj.toString());
                }
            } catch (JSONException ignored) {
            }

            try {
                if (jsonObject.has("contact_email")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("contact_email");
                    pref.setStringValue(QuickPreferences.ContactInfo.SEND_EMAIL, jsonObj.toString());
                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("contact_feedback")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("contact_feedback");
                    pref.setStringValue(QuickPreferences.ContactInfo.FEEDBACK, jsonObj.toString());
                }
            } catch (Exception ignored) {
            }

           /* try {
                if (jsonObject.has("gdpr")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("gdpr");
                    gdprMessage = jsonObj.getString("text");
                }
            } catch (Exception ignored) {
            }*/
            try {
                if (jsonObject.has("ad_control")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("ad_control");
                    String isActive = jsonObj.getString("isactive");
                    pref.setBooleanValue(QuickPreferences.CommonPref.IS_AD_ACTIVE, isActive.equalsIgnoreCase("1"));
                }
            } catch (Exception ignored) {
            }
            try {
                if (jsonObject.has("cache_control")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("cache_control");
                    String isValue = jsonObj.getString("value");
                    pref.setStringValue(QuickPreferences.CommonPref.CACHE_VALUE, isValue);
                }
            } catch (Exception ignored) {
            }

            try {
                if (jsonObject.has("feed_urls")) {
                    JSONObject jsonObj = jsonObject.getJSONObject("feed_urls");

                    String subsInfo = jsonObj.getString("subs_info");
                    String subsHistory = jsonObj.getString("subs_history");
                    String isUserSubs = jsonObj.getString("is_user_subs");

                    pref.setStringValue(QuickPreferences.Urls.SUBS_INFO, subsInfo);
                    pref.setStringValue(QuickPreferences.Urls.SUBS_HISTORY, subsHistory);
                    pref.setStringValue(QuickPreferences.Urls.IS_USER_SUBSCRIBED, isUserSubs);
                }
            } catch (Exception ignored) {
            }

            // Allow states
            try {
                if (jsonObject.has("allow_states")) {
                    JSONObject allowStates = jsonObject.getJSONObject("allow_states");
                    String allowStatesString = allowStates.optString("values");
                    JSONArray jsonArray = new JSONArray(allowStatesString);
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Epaper.getInstance().getModelManager().getAllowStateList().add(jsonArray.getString(i));
//                            Constants.allowStateList.add(jsonArray.getString(i));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Allow magazine
            try {
                if (jsonObject.has("allow_magazine")) {
                    JSONObject allowMagazine = jsonObject.getJSONObject("allow_magazine");
                    String allowMagazineString = allowMagazine.optString("values");
                    JSONArray jsonArray = new JSONArray(allowMagazineString);
                    if (jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Epaper.getInstance().getModelManager().getAllowMagazineList().add(jsonArray.getString(i));
//                            Constants.allowMagazineList.add(jsonArray.getString(i));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception ignored) {
        }
        isAppControlParse = true;
        afterAppControl();
    }

    private void parseHeaders(JSONObject headers) {
        try {
//            @SuppressWarnings("unused") boolean value = headers.has("Subscription") && headers.getString("Subscription").equalsIgnoreCase("true");
            Constants.SUBSCRIBED_COUNTRY = true;

            boolean value1 = headers.has("country_india") && headers.getString("country_india").equalsIgnoreCase("true");
            Constants.COUNTRY_IS_INDIA = value1;

            boolean gdprValue = headers.has("GDPR") && headers.getString("GDPR").equalsIgnoreCase("Yes");
            pref.setBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, gdprValue);
        } catch (Exception ignored) {
        }
        isHeaderParse = true;
        afterAppControl();
    }

    private void afterAppControl() {
        if (isAppControlParse && isHeaderParse) {
//            if (pref.getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false)) {
//                finish();
//                startActivity(new Intent(SplashActivity.this, ForceUpdateActivity.class).putExtra("isGdpr", true).putExtra("message", gdprMessage));
//            } else {
            pref.setStringValue(PreferncesConstant.setDateAsDefault, getDate);
            //makeDefaultDateRequest("");
            holidayMessageService(getDate);
            RequestHandler.requestForDBId(this);
            checkForUpdate();
//            }
        }
    }

    private void makeDefaultDateRequest(final String date) {
        String url = Urls.STATE_LIST_URL + ((TextUtils.isEmpty(date)) ? "" : date + "/");
        Systr.println("URL= > " + url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!TextUtils.isEmpty(response.toString())) {
                            try {
                                parseDateParsing(response);
                            } catch (Exception e) {
                                onAppControlFailure();
                            }
                        } else {
                            onAppControlFailure();
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null && error.networkResponse.statusCode == 404) {
                    String preDate = getPreviousDate();
                    pref.setStringValue(PreferncesConstant.setDateAsDefault, preDate);
                    holidayMessageService(preDate);
                   // holidayMessageService("2019-09-18");
                } else {
                    setErrorText(2);
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", Constants.API_KEY_VALUE);
                headers.put("User-Agent", Constants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjReq);
    }

    private void parseDateParsing(JSONObject response) {
        pref.setStringValue(PreferncesConstant.setDateAsDefault, setDate);
        Systr.println("DATE WISE DATA=> " + response.optJSONArray("data"));
        List<MainDataModel> stateList = Arrays.asList(new Gson().fromJson(response.optJSONArray("data").toString(), MainDataModel[].class));

        String setAsDefaultString = pref.getStringValue(PreferncesConstant.setAsDefaultData, "");
        if (stateList.size() > 0 && !TextUtils.isEmpty(setAsDefaultString)) {
            int index = stateList.indexOf(new MainDataModel(setAsDefaultString));
            if (index > -1) {
                MainDataModel mainDataModel = stateList.get(index);
                List<MainDataModel> localList = new ArrayList<>(stateList);
                localList.remove(mainDataModel);
                localList.add(0, mainDataModel);
                Epaper.getInstance().getModelManager().setStateList(localList);
            }
        } else {
            Epaper.getInstance().getModelManager().setStateList(stateList);
        }
        DateUtility.setDateForAll = setDate;

        getSearchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // Epaper.getInstance().trackScreenView(SplashActivity.this,"SplashActivity");
    }

    public String getPreviousDate() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormatter.format(cal.getTime());
    }

    public void setErrorText(final int apiHit) {
        findViewById(R.id.error_view).setVisibility(View.VISIBLE);
        progres_txt.setText(R.string.retry);
        progress_bar.setVisibility(View.GONE);
        // progres_txt.setBackgroundResource(R.drawable.square);
        // progres_txt.setTextColor(Color.WHITE);

        retryLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.isConnectingToInternet(SplashActivity.this)) {
                    progress_bar.setVisibility(View.VISIBLE);
                    progres_txt.setBackgroundColor(Color.TRANSPARENT);
                    progres_txt.setTextColor(Color.BLACK);
                    progres_txt.setText(R.string.please_wait_1);
                    findViewById(R.id.error_view).setVisibility(View.GONE);

                    if (apiHit == 1) {
                        startAppControl();
                    } else if (apiHit == 2) {
                       // makeDefaultDateRequest("");
                         holidayMessageService("");
                    } else if (apiHit == 3) {
                        getSearchData();
                    }
                } else {
                    Toast.makeText(mApp, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                    findViewById(R.id.error_view).setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void finishSplash() {
        if (pref.getBooleanValue(QuickPreferences.GdprConst.IS_BLOCKED_COUNTRY, false)
                && !pref.getBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, false)) {
            Intent intent = new Intent(SplashActivity.this, GDPRControllerActivity.class);
            intent.putExtra(Constants.Gdpr.CONSENT_VALUE, Constants.Gdpr.USER_CONSENT);
            startActivityForResult(intent, REQUEST_CODE_GDPR_CONSENT);
        } else {
            Intent intent = new Intent(SplashActivity.this, MasterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
        }

    }

    public void checkForUpdate() {
        //fetch value from preference for update popup
        int curAppVersion = 0;

        try {
            curAppVersion = pref.getIntValue(QuickPreferences.AppUpdate.APP_CURRENT_VERSION, 0);
            int curVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            if (curAppVersion == 0)
                curAppVersion = curVersion;
            pref.setIntValue(QuickPreferences.AppUpdate.APP_CURRENT_VERSION, curVersion);
        } catch (Exception ignored) {
        }

        int appUpdateVersion = pref.getIntValue(QuickPreferences.AppUpdate.UPDATE_POPUP_VERSION_CODE, 0);
        if (curAppVersion < appUpdateVersion) {
            int sessionCount = pref.getIntValue(QuickPreferences.AppUpdate.APP_UPDATE_SESSION_COUNT, 0);
            if (sessionCount > 0)
                sessionCount++;
            pref.setIntValue(QuickPreferences.AppUpdate.APP_UPDATE_SESSION_COUNT, sessionCount);
        } else {
            Date lastShownDate = new Date();
            pref.setLongValue(QuickPreferences.AppUpdate.APP_UPDATE_POP_UP_SHOWN_DATE, lastShownDate.getTime());
        }
    }

    private void getSearchData() {
        mApp.getServices().getSearchData(new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

                String responsedata = ConnectionDetector.convertResponseToString(response);
                pref.setStringValue(PreferncesConstant.searchList, responsedata);
                Type listType = new TypeToken<List<SearchModel>>() {
                }.getType();

                ArrayList<SearchModel> searchList = new Gson().fromJson(responsedata, listType);
                Epaper.getInstance().getModelManager().setSearchList(searchList);

            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finishSplash();
            }
        }, 500);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GDPR_CONSENT && resultCode == RESULT_OK) {
            finishSplash();
        }
    }

    //API IS HOLIDAY MESSAGE
    public void holidayMessageService(final String preDate) {


        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("date",preDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.HOLIDAY_MSZ, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    JSONArray arrJson = response.getJSONArray("states");
                    String[] statesList = new String[arrJson.length()];
                    for(int i = 0; i < arrJson.length(); i++)
                        statesList[i] = arrJson.getString(i);
                    List<String> stringList = new ArrayList<String>(Arrays.asList(statesList)); //new ArrayList is only needed if you absolutely need an ArrayList
                    //List<MainDataModel> stateList = Arrays.asList(new Gson().fromJson(response.optJSONArray("states").toString(), MainDataModel[].class));

                    Epaper.getInstance().getModelManager().setHolidayList(stringList);
                    String msz = response.getString("message");
                    Epaper.getInstance().getModelManager().setHolidaymsz(msz);
                    makeDefaultDateRequest(preDate);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Utility.setNetworkErrorToast(SplashActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(SplashActivity.this).addToRequestQueue(request);
    }
}