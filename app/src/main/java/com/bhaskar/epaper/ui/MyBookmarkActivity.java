package com.bhaskar.epaper.ui;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.adapter.MyBookmarkAdapter;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.AdUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Utility;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonElement;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyBookmarkActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private AVLoadingIndicatorView progressBar;

    Epaper mApp;
    EpaperPrefernces mPref;
    TextView noDataTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bookmark);
        recyclerView = findViewById(R.id.homeRecyclerList);
        toolbar = findViewById(R.id.toolbar);
        noDataTv = findViewById(R.id.no_data_tv);

        progressBar = findViewById(R.id.progress);
        progressBar.setIndicatorColor(ContextCompat.getColor(MyBookmarkActivity.this, R.color.colorPrimaryDark));
        setToolbar();
        mApp = (Epaper) getApplicationContext();
        mPref = EpaperPrefernces.getInstance(MyBookmarkActivity.this);

        recyclerView.setLayoutManager(new LinearLayoutManager(MyBookmarkActivity.this));

        if (Util.isShowAds(MyBookmarkActivity.this)) {
            AdUtility adUtility = new AdUtility(this);
            adUtility.loadBannerAd((AdView) findViewById(R.id.adView));
        } else {
            findViewById(R.id.adView).setVisibility(View.GONE);
        }
    }

    private void setRecyclerAdapter() {
        mApp.getServices().getMyBookmarkData(mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""), new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                try {
                    setParsingJson(jsonElement.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });

    }

    private void setParsingJson(String response) throws Exception {

        int columno = Epaper.getInstance().getModelManager().getColumnCount();//Utility.calculateNoOfColumns(this);
        progressBar.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new GridLayoutManager(this, columno));

        List<MasterModel> stateEditionList = new ArrayList<>();
        JSONObject jb = new JSONObject(response);

        if (jb.getString("status").equals("1")) {
            JSONObject jbData = jb.getJSONObject("data");
            JSONArray dataArray = jbData.getJSONArray("data");

            for (int i = 0; i < dataArray.length(); i++) {
                JSONObject datavalue = (JSONObject) dataArray.get(i);
                String edition = datavalue.getString("editionname");
                String pageno = datavalue.getString("pageno");
                String org_url = datavalue.getString("org_url");
                String thumb_url = datavalue.getString("thumb_url");
                String large_url = datavalue.getString("large_url");
                String id = datavalue.getString("id");
                stateEditionList.add(new MasterModel(id, pageno, edition, org_url, thumb_url, large_url, ""));
            }

            recyclerView.setAdapter(new MyBookmarkAdapter(MyBookmarkActivity.this, stateEditionList));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            noDataTv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            Utility.setToast(this, jb.getString("msg"));
            noDataTv.setVisibility(View.VISIBLE);
            noDataTv.setText(getString(R.string.no_bookmark_available));
            recyclerView.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerAdapter();
        Epaper.getInstance().trackScreenView(MyBookmarkActivity.this,"MyBookmarkActivity");
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("My Bookmarks");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


}
