package com.bhaskar.epaper.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.utils.AuthConst;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class MobileVerificationActivity extends BaseActivity {

    private EditText verifyEmailMobile, verify_otp;
    private Button verify_btn;

    private ProgressDialog pd;
    private EpaperPrefernces pref;

    private boolean comeForVerification = false;
    private String userid, otp, email, password;
    private String mobileno, signUpId, profilePic;
    private int loginType;
    private boolean forgotPwd;
    private TextView resendBtn;
    private TextView verificationHeadTv;
    private Button send_Otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verification);

        verifyEmailMobile = findViewById(R.id.verify_email_mobile);
        send_Otp = findViewById(R.id.send_Otp);
        verify_btn = findViewById(R.id.verify_btn);
        verify_otp = findViewById(R.id.verify_otp);
        resendBtn = findViewById(R.id.resend_Btn);
        verificationHeadTv = findViewById(R.id.verification_head_tv);

        pref = EpaperPrefernces.getInstance(this);
        comeForVerification = getIntent().getBooleanExtra("forVerify", false);
        forgotPwd = getIntent().getBooleanExtra("forgotPwd", false);

        initVerification();
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = verify_otp.getText().toString();
                if (Utility.isConnectingToInternet(MobileVerificationActivity.this)) {
                    if (!TextUtils.isEmpty(otp)) {
                        if (comeForVerification) {
                            makeSignUpHit();
                        } else {
                            setVerifyOtp();
                        }
                        hideKeyBoard();
                    } else {
                        verify_otp.setError("Wrong OTP");
                    }
                } else {
                    Utility.setNetworkErrorToast(MobileVerificationActivity.this);
                }
            }
        });

        resendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comeForVerification) {
                    makeGetOtpHit();
                } else {
                    sendOtp();
                }

            }
        });
        send_Otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp();
            }
        });
    }

    private void initVerification() {
        if (comeForVerification) {
            setToolbar("Account Verification");
            email = getIntent().getStringExtra("email");
            password = getIntent().getStringExtra("password");
            loginType = getIntent().getIntExtra("login_type", 0);
            signUpId = getIntent().getStringExtra("sign_up_id");
            profilePic = getIntent().getStringExtra("profile_pic");

            if (Constants.COUNTRY_IS_INDIA) {
                mobileno = getIntent().getStringExtra("mobileno");
                verifyEmailMobile.setHint(getString(R.string.email));
                if (!TextUtils.isEmpty(mobileno)) {
                    verificationHeadTv.setText(getString(R.string.verification_code_sms, mobileno));
                } else {
                    verificationHeadTv.setText(getString(R.string.verification_code_sms, "91XXXXXXXX"));
                }
                verifyEmailMobile.setText(mobileno);
            } else {
                verifyEmailMobile.setHint(getString(R.string.email_txt));
                if (!TextUtils.isEmpty(email)) {
                    verificationHeadTv.setText(getString(R.string.verification_code_sms, email));
                } else {
                    verificationHeadTv.setText(getString(R.string.verification_code_sms, "guest@abc.com"));
                }
                verifyEmailMobile.setText(email);
            }

            verifyEmailMobile.setEnabled(false);
            send_Otp.setVisibility(View.GONE);
            findViewById(R.id.mobile_number_layout).setVisibility(View.GONE);
            makeGetOtpHit();

        } else {
            setToolbar("Forgot Password");

            userid = pref.getStringValue(QuickPreferences.UserInfo.USER_ID, "");
            verificationHeadTv.setVisibility(View.GONE);
            if (Constants.COUNTRY_IS_INDIA) {
                verifyEmailMobile.setHint(getString(R.string.forgot_password_mobile));
            } else {
                verifyEmailMobile.setHint(getString(R.string.forgot_password_email));
            }

            send_Otp.setVisibility(View.VISIBLE);
            resendBtn.setVisibility(View.GONE);
            verify_btn.setEnabled(false);
            verify_btn.setBackground(getResources().getDrawable(R.drawable.btn_theme_bg));
            findViewById(R.id.mobile_number_layout).setVisibility(View.VISIBLE);
        }
    }

    public void hideKeyBoard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    String var;

    private void sendOtp() {
        if (Utility.isConnectingToInternet(MobileVerificationActivity.this)) {
            if (TextUtils.isDigitsOnly(verifyEmailMobile.getText())) {
                var = verifyEmailMobile.getText().toString();
            } else {
                var = verifyEmailMobile.getText().toString();
            }
            if (checkValidation()) {
                makeGetOtpHit();
            } else {
                verifyEmailMobile.setError("Please enter valid email");
            }
        } else {
            Utility.setNetworkErrorToast(MobileVerificationActivity.this);
        }
    }

    private boolean checkValidation() {

        if (TextUtils.isEmpty(var)) {
            verifyEmailMobile.setError("Please enter valid input.");
            return false;
        }

        if (TextUtils.isDigitsOnly(var)) {
            if (!Patterns.PHONE.matcher(var).matches()) {
                verifyEmailMobile.setError("Please enter valid mobile number");
                return false;
            } else {
                mobileno = var;
                email = "";
            }
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(var).matches()) {
                verifyEmailMobile.setError("Please enter valid email id");
                return false;
            } else {
                mobileno = "";
                email = var;
            }
        }

        return true;
    }

    private void makeGetOtpHit() {
        setProgressbar();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);

            if (Constants.COUNTRY_IS_INDIA) {
                jsonObject.put("mobileno", mobileno);
            }

            if (comeForVerification) {
                jsonObject.put("login_type", loginType);
            } else {
                jsonObject.put("user_id", userid);
            }
            if (forgotPwd) {
                jsonObject.put("forgot_password", "1");
            }

            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SEND_OTP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null) {
                    pd.dismiss();
                }
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        Epaper.getInstance().trackEvent(MobileVerificationActivity.this,GAConst.Category.OTP, GAConst.Action.SEND, userid);

                        verify_btn.setEnabled(true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            verify_btn.setBackground(getResources().getDrawable(R.drawable.btn_theme_bg));
                        }
                    } else {
                        if (comeForVerification) {
                            finish();
                        }
                    }
                    Utility.setToast(MobileVerificationActivity.this, response.optString("msg"));
                } catch (Exception ignored) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (comeForVerification) {
                    finish();
                }
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(MobileVerificationActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(MobileVerificationActivity.this).addToRequestQueue(request);
    }

    public void makeSignUpHit() {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            if (Constants.COUNTRY_IS_INDIA) {
                jsonObject.put("mobileno", mobileno);
            }
            jsonObject.put("login_type", loginType);

            if (loginType == AuthConst.LoginType.GOOGLE) {
                jsonObject.put("g_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB) {
                jsonObject.put("fb_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB_MANUAL) {
                jsonObject.put("fb_id", signUpId);
            }

            jsonObject.put("password", password);
            jsonObject.put("otp", otp);
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            jsonObject.put("app_version", Util.getAppVersion(MobileVerificationActivity.this));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SIGNUP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("SignUp Response : " + response);
                if (pd != null) {
                    pd.dismiss();
                }
                try {
                    if (response.optString("code").equalsIgnoreCase("200")) {
                        if (response.optString("status").equalsIgnoreCase("Success")) {
                            JSONObject mainObject = response.getJSONObject("data");
                            Constants.isUserComeFromRegistration = true;
                            Constants.FREE_TRIAL_DURATION_MESSAGE = response.optString("freeTrialDateMessage");

                            pref.setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, mainObject.optBoolean("is_trial"));
                            pref.setStringValue(QuickPreferences.UserInfo.EMAIL, mainObject.optString("email"));
                            pref.setStringValue(QuickPreferences.UserInfo.NAME, mainObject.optString("name"));
                            pref.setStringValue(QuickPreferences.UserInfo.MOBILE, mainObject.optString("mobileno"));
                            pref.setStringValue(QuickPreferences.UserInfo.USER_ID, mainObject.optString("user_id"));
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, mainObject.optInt("email_verified") == 1);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_MOBILE_VERIFIED, mainObject.optInt("mobile_verified") == 1);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, mainObject.optString("subscribed").equalsIgnoreCase("1"));
                            pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, mainObject.optInt("reg_type"));

                            pref.setStringValue(QuickPreferences.UserInfo.PASSWORD, password);
                            pref.setStringValue(QuickPreferences.UserInfo.PROFILE_PIC, profilePic);
                            pref.setStringValue(QuickPreferences.UserInfo.SIGN_UP_ID, signUpId);
                            pref.setIntValue(QuickPreferences.UserInfo.LOGIN_TYPE, loginType);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, true);
                            //
                            Epaper.getInstance().trackEvent(MobileVerificationActivity.this,GAConst.Category.REGISTER, GAConst.Action.CLICK, "" + loginType);
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                    Utility.setToast(MobileVerificationActivity.this, response.optString("msg"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(MobileVerificationActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(MobileVerificationActivity.this).addToRequestQueue(request);
    }

    private void setVerifyOtp() {
        //Utility.setToast(MobileVerificationActivity.this, "Please wait, we are verifying your account");
        setProgressbar();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("otp", otp);
            jsonObject.put("email", email);
            jsonObject.put("mobileno", mobileno);
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.VERIFY_OTP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null) {
                    pd.dismiss();
                }
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        startActivity(new Intent(MobileVerificationActivity.this, ChangePasswordActivity.class).putExtra("isChangePassword", false).putExtra("otp", otp).putExtra("email", email).putExtra("mobileno", mobileno));
                        finish();
                    }
                    Utility.setToast(MobileVerificationActivity.this, response.optString("msg"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(MobileVerificationActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(MobileVerificationActivity.this).addToRequestQueue(request);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Epaper.getInstance().trackScreenView(MobileVerificationActivity.this,"MobileVerificationActivity");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }


    private void setProgressbar() {
        pd = new ProgressDialog(MobileVerificationActivity.this);
        pd.setMessage("Please Wait,We are verifying your account");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void setToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobileVerificationActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }
}
