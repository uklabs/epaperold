package com.bhaskar.epaper.mobile_verify;

public interface MobileOtpListener {
    void sendOtp(String mobileNumber);

    void verifyOtp(String mobileNumber);

    void finishVerification(String otp);

    void finishScreen();

    void skipMobileInput();

    void handleProgress(boolean isProgressShown);

}
