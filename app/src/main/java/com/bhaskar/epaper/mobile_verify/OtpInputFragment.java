package com.bhaskar.epaper.mobile_verify;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtpInputFragment extends Fragment {

    TextView resendOtpTv;
    private AppCompatButton verify_btn;
    private String mobileno;
    private String otp;
    private boolean forVerification;

    private EditText edtOTP1, edtOTP2, edtOTP3, edtOTP4, edtOTP5, edtOTP6;
    private TextView verify_otp;

    public OtpInputFragment() {
        // Required empty public constructor
    }

    public static OtpInputFragment getInstance(String mobileno, boolean forVerification) {
        OtpInputFragment otpInputFragment = new OtpInputFragment();
        Bundle bundle = new Bundle();
        bundle.putString("mobileno", mobileno);
        bundle.putBoolean("forVerify", forVerification);
        otpInputFragment.setArguments(bundle);
        return otpInputFragment;
    }

    private MobileOtpListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (MobileOtpListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mobileno = bundle.getString("mobileno");
        forVerification = bundle.getBoolean("forVerify");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_otp_input, container, false);
        /*register now clickable*/
        verify_btn = rootView.findViewById(R.id.verify_btn);
        verify_otp = rootView.findViewById(R.id.verify_otp);

        TextView verify_otp_tv = rootView.findViewById(R.id.verify_otp_tv);
        verify_otp_tv.setText(getString(R.string.verify_otp_msg, mobileno));
        rootView.findViewById(R.id.back_icon_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        edtOTP1 = rootView.findViewById(R.id.edtOTP1);
        edtOTP2 = rootView.findViewById(R.id.edtOTP2);
        edtOTP3 = rootView.findViewById(R.id.edtOTP3);
        edtOTP4 = rootView.findViewById(R.id.edtOTP4);
        edtOTP5 = rootView.findViewById(R.id.edtOTP5);
        edtOTP6 = rootView.findViewById(R.id.edtOTP6);
        setTextWatcherInView();

        resendOtpTv = rootView.findViewById(R.id.resend_otp_tv);
        String registerNow = "Resend OTP";
        String resendOtpText = getString(R.string.resend_otp_text, registerNow);
        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(resendOtpText));
        spannableStringBuilder2.setSpan(new MySpannable(getActivity()) {
            @Override
            public void onClick(View widget) {
                listener.sendOtp(mobileno);
            }
        }, Html.fromHtml(resendOtpText).toString().indexOf(registerNow), Html.fromHtml(resendOtpText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        resendOtpTv.setMovementMethod(LinkMovementMethod.getInstance());
        resendOtpTv.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);


        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = getEnteredOTP();
                if (Utility.isConnectingToInternet(getActivity())) {
                    if (!TextUtils.isEmpty(otp)) {
                        if (!TextUtils.isEmpty(mobileno)) {
                            if (forVerification) {
                                listener.finishVerification(otp);
                            } else {
                                setVerifyOtp();
                            }
                        }
                        verify_otp.setText("");
                    } else {
                        verify_otp.setText("Wrong OTP");
                    }
                } else {
                    Utility.setNetworkErrorToast(getActivity());
                }
            }
        });

        return rootView;
    }

    private void setTextWatcherInView() {
        edtOTP1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edtOTP2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edtOTP3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtOTP4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        edtOTP5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String txt = String.valueOf(s);
                if (txt.length() > 0) {
                    edtOTP6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void clearOTP() {
        edtOTP1.setText("");
        edtOTP2.setText("");
        edtOTP3.setText("");
        edtOTP4.setText("");
        edtOTP5.setText("");
        edtOTP6.setText("");
        edtOTP1.requestFocus();
    }

    private String getEnteredOTP() {
        return String.format("%s%s%s%s%s%s", getProperText(edtOTP1), getProperText(edtOTP2), getProperText(edtOTP3), getProperText(edtOTP4), getProperText(edtOTP5), getProperText(edtOTP6));
    }

    public String getProperText(TextView textView) {
        if (textView == null)
            return "";
        return textView.getText().toString().trim();
    }

    private void setVerifyOtp() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("otp", otp);
            jsonObject.put("mobileno", mobileno);
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            jsonObject.put("user_id", EpaperPrefernces.getInstance(getActivity()).getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
        } catch (JSONException ignored) {
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.MOBILE_VERIFY_OTP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        listener.finishScreen();
                    } else {
                        clearOTP();
                    }
                    Utility.setToast(getActivity(), response.optString("msg"));
                } catch (Exception ignored) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                clearOTP();
                Systr.println("Signup Error : " + error);
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;

        public MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {

        }
    }


}
