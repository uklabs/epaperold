package com.bhaskar.epaper.mobile_verify;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.ui.MobileVerificationActivity;
import com.bhaskar.epaper.utils.AuthConst;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

public class MobileVerifyActivity extends BaseActivity implements MobileOtpListener {
    private MobileInputFragment mobileInputFragment;
    private AVLoadingIndicatorView progressBar;
    private EpaperPrefernces pref;

    private boolean forVerification = false;
    private String email;
    private String signUpId;
    private String name;
    private String mobileno;
    private String otp;
    private String profilePic;
    private int loginType;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_mobile_verify);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            name = bundle.getString("name");
            email = bundle.getString("email");
            loginType = bundle.getInt("login_type", 0);
            signUpId = bundle.getString("sign_up_id");
            profilePic = bundle.getString("profile_pic");
            forVerification = bundle.getBoolean("forVerification");
        }

        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        pref = EpaperPrefernces.getInstance(MobileVerifyActivity.this);

        mobileInputFragment = MobileInputFragment.getInstance(email, loginType, signUpId);
        changeFragment(mobileInputFragment);
    }

    private void changeFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.addToBackStack(fragment.getTag());
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 2) {
            getSupportFragmentManager().popBackStack();
        } else {
            if (forVerification) {
                skipMobileInput();
            } else {
                finishScreen();
            }
        }
    }

    @Override
    public void sendOtp(String mobileNumber) {
        mobileInputFragment.setSend_Otp(mobileNumber);
    }

    @Override
    public void verifyOtp(String mobileNumber) {
        mobileno = mobileNumber;
        Fragment otpInputFragment = OtpInputFragment.getInstance(mobileNumber, forVerification);
        changeFragment(otpInputFragment);
    }

    @Override
    public void finishVerification(String otp) {
        this.otp = otp;
        makeSignUp();
//        finishScreen();
    }

    @Override
    public void finishScreen() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void skipMobileInput() {
        if (forVerification) {
            makeSignUp();
        } else {
            finishScreen();
        }
    }

    @Override
    public void handleProgress(boolean isProgressShown) {
        if (isProgressShown) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void makeSignUp() {
        handleProgress(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("login_type", loginType);

            if (loginType == AuthConst.LoginType.GOOGLE) {
                jsonObject.put("g_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB) {
                jsonObject.put("fb_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB_MANUAL) {
                jsonObject.put("fb_id", signUpId);
            }

            if (!TextUtils.isEmpty(mobileno)) {
                jsonObject.put("mobileno", mobileno);
            }
            if (!TextUtils.isEmpty(otp)) {
                jsonObject.put("otp", otp);
            }

            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            jsonObject.put("app_version", Util.getAppVersion(MobileVerifyActivity.this));
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SIGNUP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("SignUp Response : " + response);
                handleProgress(false);
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        Constants.isUserComeFromRegistration = true;
                        JSONObject mainObject = response.getJSONObject("data");
                        Constants.FREE_TRIAL_DURATION_MESSAGE = response.optString("freeTrialDateMessage");

                        pref.setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, response.optBoolean("is_trial"));
                        pref.setStringValue(QuickPreferences.UserInfo.EMAIL, mainObject.optString("email"));
                        pref.setStringValue(QuickPreferences.UserInfo.NAME, mainObject.optString("name"));
                        pref.setStringValue(QuickPreferences.UserInfo.MOBILE, mainObject.optString("mobileno"));
                        pref.setStringValue(QuickPreferences.UserInfo.USER_ID, mainObject.optString("user_id"));
                        pref.setStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, mainObject.optString("subscription_edate"));
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, mainObject.optInt("email_verified") == 1);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_MOBILE_VERIFIED, mainObject.optInt("mobile_verified") == 1);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, mainObject.optString("subscribed").equalsIgnoreCase("1"));
                        pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, mainObject.optInt("reg_type"));

                        pref.setStringValue(QuickPreferences.UserInfo.PASSWORD, mainObject.optString("password"));
                        pref.setStringValue(QuickPreferences.UserInfo.PROFILE_PIC, profilePic);
                        pref.setStringValue(QuickPreferences.UserInfo.SIGN_UP_ID, signUpId);
                        pref.setIntValue(QuickPreferences.UserInfo.LOGIN_TYPE, loginType);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, true);

                        Epaper.getInstance().trackEvent(MobileVerifyActivity.this,GAConst.Category.REGISTER, GAConst.Action.CLICK, "" + loginType);
                        Intent intent = new Intent(QuickPreferences.USER_LOGGED_IN_INTENT);
                        LocalBroadcastManager.getInstance(MobileVerifyActivity.this).sendBroadcast(intent);
                        finishScreen();
                    } else {
                        verifyOtp(mobileno);
                    }
                    Utility.setToast(MobileVerifyActivity.this, response.optString("msg"));

                } catch (Exception ignored) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                handleProgress(false);
                Utility.setNetworkErrorToast(MobileVerifyActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(MobileVerifyActivity.this).addToRequestQueue(request);
    }
}
