package com.bhaskar.epaper.mobile_verify;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MobileInputFragment extends Fragment {


    private EditText verify_mobile;
    private String mobileno;
    private String email;
    private int loginType;

    public MobileInputFragment() {
        // Required empty public constructor
    }

    private MobileOtpListener listener;

    public static MobileInputFragment getInstance(String email, int loginType, String signUpId) {
        MobileInputFragment mobileInputFragment = new MobileInputFragment();
        Bundle argument = new Bundle();
        argument.putString("email", email);
        argument.putInt("login_type", loginType);
        argument.putString("sign_up_id", signUpId);
        mobileInputFragment.setArguments(argument);
        return mobileInputFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (MobileOtpListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        email = bundle.getString("email");
        loginType = bundle.getInt("login_type", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mobile_input, container, false);


        TextView skipBtn = view.findViewById(R.id.skip_btn);
        skipBtn.setText(Html.fromHtml(getString(R.string.skip_text)));
        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.skipMobileInput();
            }
        });

        verify_mobile = view.findViewById(R.id.verify_email);
        view.findViewById(R.id.send_Otp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOtp();
            }
        });
        return view;
    }

    private void sendOtp() {
        if (Utility.isConnectingToInternet(getActivity())) {
            mobileno = verify_mobile.getText().toString();
            if (checkValidation()) {
                makeGetOtpHit();
            } else {
                verify_mobile.setError("Please enter valid Mobile number");
            }
        } else {
            Utility.setNetworkErrorToast(getActivity());
        }
    }

    private boolean checkValidation() {
        if (TextUtils.isEmpty(mobileno)) {
            verify_mobile.setError("Please enter mobile");
            return false;
        } else if (TextUtils.isEmpty(mobileno)) {
            verify_mobile.setError("Please enter Mobile number");
            return false;
        } else if (mobileno.length() != 10) {
            verify_mobile.setError("Please enter valid Mobile number");
            return false;
        } else if (!Patterns.PHONE.matcher(mobileno).matches()) {
            verify_mobile.setError("Please enter valid mobile number");
            return false;
        }
        return true;
    }

    public void setSend_Otp(String mobile) {
        //set email from preference
        mobileno = mobile;
        makeGetOtpHit();
    }

    private void makeGetOtpHit() {
        listener.handleProgress(true);
        String userId = EpaperPrefernces.getInstance(getActivity()).getStringValue(QuickPreferences.UserInfo.USER_ID, "");
        if (TextUtils.isEmpty(email)) {
            email = EpaperPrefernces.getInstance(getActivity()).getStringValue(QuickPreferences.UserInfo.EMAIL, email);
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", email);
            jsonObject.put("mobileno", mobileno);
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            if (loginType != 0) {
                jsonObject.put("login_type", loginType);
            }

            if (!TextUtils.isEmpty(userId)) {
                jsonObject.put("user_id", userId);
            }
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SEND_OTP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        listener.handleProgress(false);
                        listener.verifyOtp(mobileno);
                    } else {
                        listener.handleProgress(false);
                    }
                    Utility.setToast(getActivity(), response.optString("msg"));
                } catch (Exception ignored) {
                    listener.handleProgress(false);
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.handleProgress(false);
                Systr.println("Signup Error : " + error);
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getActivity()).addToRequestQueue(request);
    }
}
