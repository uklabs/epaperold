package com.bhaskar.epaper.imagezoom.utils;

public interface IDisposable {

    void dispose();
}
