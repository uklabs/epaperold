package com.bhaskar.epaper.epaper_v3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MoreItemInfo;

import java.util.List;

public class MoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<MoreItemInfo> mList;
    private MoreCallbacks settingCallbacks;

    MoreAdapter(Context context, List<MoreItemInfo> mList, MoreCallbacks settingCallbacks) {
        Context mContext = context;
        this.mList = mList;
        this.settingCallbacks = settingCallbacks;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SettingViewHolder(inflater.inflate(R.layout.more_list_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SettingViewHolder) {
            SettingViewHolder settingViewHolder = (SettingViewHolder) holder;
            settingViewHolder.settingName.setText(mList.get(position).name);
            settingViewHolder.settingIcon.setImageResource(mList.get(position).imageId);
            settingViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != settingCallbacks) {
                        settingCallbacks.onSettingClick(mList.get(position));
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class SettingViewHolder extends RecyclerView.ViewHolder {
        private TextView settingName;
        private ImageView settingIcon;

        public SettingViewHolder(View itemView) {
            super(itemView);
            settingName = itemView.findViewById(R.id.setting_name);
            settingIcon = itemView.findViewById(R.id.setting_icon);
            itemView.setTag(itemView);
        }

    }
}
