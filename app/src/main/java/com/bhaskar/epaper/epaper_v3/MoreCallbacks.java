package com.bhaskar.epaper.epaper_v3;

import com.bhaskar.epaper.model.MoreItemInfo;

public interface MoreCallbacks {
    void onSettingClick(MoreItemInfo settingInfo);
}
