package com.bhaskar.epaper.epaper_v3;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.epaper.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeMagazineFragment extends Fragment {


    public HomeMagazineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_magazine, container, false);
    }

}
