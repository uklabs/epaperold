package com.bhaskar.epaper.epaper_v3;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.model.MoreItemInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment implements MoreCallbacks {
    RecyclerView moreItemsRv;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_more, container, false);

        moreItemsRv = view.findViewById(R.id.more_items_rv);
        moreItemsRv.setNestedScrollingEnabled(false);
        moreItemsRv.setLayoutManager(new LinearLayoutManager(getContext()));
        moreItemsRv.setAdapter(new MoreAdapter(getContext(), getAllMoreList(getContext()), MoreFragment.this));

        return view;
    }

    public List<MoreItemInfo> getAllMoreList(Context context) {
        List<MoreItemInfo> settingInfoList = new ArrayList<>();
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_change_date), R.drawable.ic_stg_change_date, Constants.MoreAction.CHANGE_DATE));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_bookmark), R.drawable.ic_stg_bookmark, Constants.MoreAction.BOOKMARK));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_contact_us), R.drawable.ic_stg_contact_us, Constants.MoreAction.CONTACT_US));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_rate_us), R.drawable.ic_stg_rate_us, Constants.MoreAction.RATE_US));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_t_and_c), R.drawable.ic_stg_term_and_condition, Constants.MoreAction.TERMS_AND_CONDITION));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_privacy_policy), R.drawable.ic_stg_privacy_policy, Constants.MoreAction.PRIVACY_POLICY));
        settingInfoList.add(new MoreItemInfo(context.getString(R.string.more_feedback), R.drawable.ic_stg_feedback, Constants.MoreAction.FEEDBACK));

        return settingInfoList;
    }

    @Override
    public void onSettingClick(MoreItemInfo settingInfo) {

    }
}
