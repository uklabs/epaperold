package com.bhaskar.epaper.epaperv2.profile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.services.BaseAppService;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.mobile_verify.MobileVerifyActivity;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AccountSettingsActivity extends BaseActivity {

    private boolean isEmailVerified;
    private boolean isMobileVerified;
    private boolean isEmailVerificationSent;
    private boolean isStatusActive;
    private static final int MOBILE_VERIFY = 1010;
    private Context context;
    private EpaperPrefernces pref;
    private AVLoadingIndicatorView progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_settings);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountSettingsActivity.this.finish();
            }
        });

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setTitle("Account Settings");
        }
        pref = EpaperPrefernces.getInstance(this);
        context = AccountSettingsActivity.this;
        progress = findViewById(R.id.progress);
        progress.setIndicatorColor(ContextCompat.getColor(AccountSettingsActivity.this, R.color.colorPrimaryDark));
        /*mobile verification in india only*/
        if (Constants.COUNTRY_IS_INDIA) {
            findViewById(R.id.mobile_verify_layout).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.mobile_verify_layout).setVisibility(View.GONE);
        }

        (findViewById(R.id.verify_now_textview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isEmailVerified && !isEmailVerificationSent) {
                    emailVerification(AccountSettingsActivity.this);

                    startService(new Intent(AccountSettingsActivity.this, BaseAppService.class));
                }
            }
        });

        (findViewById(R.id.mobile_verify_now_textview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isMobileVerified) {
                    startActivityForResult(new Intent(AccountSettingsActivity.this, MobileVerifyActivity.class), MOBILE_VERIFY);
                }
            }
        });

        fetchDataFromServer();

        Epaper.getInstance().trackScreenView(AccountSettingsActivity.this,GAConst.Screen.ACCOUNT_SETTING);
    }

    private void setView(String plan, String period) {
        if (isStatusActive) {
            ((TextView) findViewById(R.id.status_textview)).setText(getResources().getString(R.string.active));
        } else {
            ((TextView) findViewById(R.id.status_textview)).setText(getResources().getString(R.string.inactive));
        }

        findViewById(R.id.subcription_plan_ll).setVisibility(View.VISIBLE);
        findViewById(R.id.current_period_ll).setVisibility(View.VISIBLE);

        ((TextView) findViewById(R.id.subcription_plan_textview)).setText(plan);
        ((TextView) findViewById(R.id.current_period_textview)).setText(period);

        if (isEmailVerificationSent) {
            ((TextView) findViewById(R.id.verify_now_textview)).setText(getResources().getString(R.string.email_sent));
        } else if (isEmailVerified) {
            ((TextView) findViewById(R.id.verify_now_textview)).setText(getResources().getString(R.string.verified));
        } else {
            ((TextView) findViewById(R.id.verify_now_textview)).setText(Html.fromHtml(getResources().getString(R.string.verify_now)));
        }

        if (isMobileVerified) {
            ((TextView) findViewById(R.id.mobile_verify_now_textview)).setText(getResources().getString(R.string.verified));
        } else {
            ((TextView) findViewById(R.id.mobile_verify_now_textview)).setText(Html.fromHtml(getResources().getString(R.string.verify_now)));
        }
    }

    private void showErrorDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AccountSettingsActivity.this);
        alertDialog.setTitle("Error!");
        alertDialog.setMessage("Do you want to retry?");

        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                fetchDataFromServer();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        AlertDialog dialog = alertDialog.create();
        dialog.show();
    }

    private void fetchDataFromServer() {
        progress.setVisibility(View.VISIBLE);
        String userId = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.UserInfo.USER_ID, "");
        String subUrl = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.Urls.SUBS_INFO, Urls.ACCOUNT_SETTINGS_URL);
        String url = Urls.FEED_BASE_URL + subUrl + userId + "/" + (Urls.IS_TESTING ? "?testing=1" : "");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                progress.setVisibility(View.GONE);
                try {
                    if (response.getString("status").equalsIgnoreCase("Success")) {
                        isStatusActive = response.optInt("IsSubscribed") == 1;
                        isEmailVerified = response.optString("email_verified").equalsIgnoreCase("1");
                        isMobileVerified = response.optString("mobile_verified").equalsIgnoreCase("1");
                        pref.setStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, response.optString("subscription_edate"));
                        String plan = response.optString("subs_plan");
                        String period = response.optString("current_period");

                        setView(plan, period);

                        pref.setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, response.optBoolean("is_trial"));
//                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, isStatusActive);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, isEmailVerified);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_MOBILE_VERIFIED, isMobileVerified);

                    } else {
                        showErrorDialog();
                    }
                } catch (Exception e) {
                    showErrorDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                showErrorDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
    }

    private void emailVerification(final Context context) {
        progress.setVisibility(View.VISIBLE);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
        } catch (JSONException e) {
        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.EMAIL_VERIFICATION_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progress.setVisibility(View.GONE);

                try {
                    String msg = response.getString("msg");
                    if (response.getString("status").equalsIgnoreCase("Success")) {
                        isEmailVerificationSent = true;
                        ((TextView) findViewById(R.id.verify_now_textview)).setText(getResources().getString(R.string.email_sent));
                    }
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.setVisibility(View.GONE);
                Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MOBILE_VERIFY && resultCode == RESULT_OK) {
            fetchDataFromServer();
        }
    }
}
