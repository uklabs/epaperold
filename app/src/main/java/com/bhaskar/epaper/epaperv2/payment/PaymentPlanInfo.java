package com.bhaskar.epaper.epaperv2.payment;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentPlanInfo implements Serializable {

    public int id;
    public int amount;
    public String title;
    public String desc_1;
    public String desc_2;
}
