package com.bhaskar.epaper.epaperv2.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.Window;

public class ProgressbarDialog {

    private ProgressDialog progressDialog;

    public ProgressbarDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public void show(Activity activity, String message, boolean cancelable) {
        try {
            if (progressDialog != null && !progressDialog.isShowing() && !activity.isFinishing()) {
                progressDialog.setCancelable(cancelable);
                progressDialog.setMessage(message);
                progressDialog.show();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    public void show(String message, boolean cancelable) {
        try {
            if (progressDialog != null && !progressDialog.isShowing()) {
                progressDialog.setCancelable(cancelable);
                progressDialog.setMessage(message);
                progressDialog.show();
            }
        } catch (IllegalArgumentException e) {
        }
    }

    public void dismiss() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
        }
    }
}
