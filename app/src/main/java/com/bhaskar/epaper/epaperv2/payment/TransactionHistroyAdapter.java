package com.bhaskar.epaper.epaperv2.payment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;

public class TransactionHistroyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_TYPE_LOADING = 2;

    private ArrayList<TransactionInfo> list;
    private Context mContext;

    public TransactionHistroyAdapter(Context context) {
        this.mContext = context;
        list = new ArrayList<>();
    }

    public void setData(ArrayList<TransactionInfo> dataList) {
        list.clear();
        list.addAll(dataList);
        notifyDataSetChanged();
    }

    public void setDataInList(ArrayList<TransactionInfo> dataList) {
        list.addAll(dataList);
        notifyDataSetChanged();
    }

    public void addItem() {
        list.add(null);
        notifyDataSetChanged();
    }

    public void removeItem() {
        if (list.size() > 0 && list.get(list.size() - 1) == null)
            list.remove(list.size() - 1);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADING) {
            return new LoadingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_loading_item, parent, false));
        } else {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_transaction, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            TransactionInfo info = list.get(position);
            ViewHolder viewHolder = (ViewHolder) holder;

            viewHolder.orderNumberTextView.setText(info.orderNumber);
            viewHolder.dateTimeTextView.setText(info.dateTime);
            viewHolder.titleTextView.setText(info.title);
            viewHolder.periodTextView.setText(info.timePeriod);
            viewHolder.amountTextView.setText(info.amount);
            viewHolder.statusTextView.setText(info.status);

            if (!TextUtils.isEmpty(info.txnStatus) && info.txnStatus.equalsIgnoreCase("1")) {
                viewHolder.statusTextView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_success_bg));
            } else {
                viewHolder.statusTextView.setBackground(ContextCompat.getDrawable(mContext, R.drawable.btn_failed_bg));
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) == null)
            return VIEW_TYPE_LOADING;
        else
            return VIEW_TYPE_ITEM;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView orderNumberTextView;
        TextView dateTimeTextView;
        TextView titleTextView;
        TextView periodTextView;
        TextView amountTextView;
        TextView statusTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            orderNumberTextView = itemView.findViewById(R.id.order_no_textview);
            dateTimeTextView = itemView.findViewById(R.id.data_time_textview);
            titleTextView = itemView.findViewById(R.id.order_title_textview);
            periodTextView = itemView.findViewById(R.id.time_period_textview);
            amountTextView = itemView.findViewById(R.id.amount_textview);
            statusTextView = itemView.findViewById(R.id.status_textview);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private AVLoadingIndicatorView progressBar;

        private LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progress_bar);
        }
    }
}
