package com.bhaskar.epaper.epaperv2.profile;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnProfileFragmentListener;
import com.bhaskar.epaper.epaperv2.profile.fragments.FragmentProfileEdit;
import com.bhaskar.epaper.epaperv2.profile.fragments.FragmentProfileView;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.GAConst;

public class ProfileActivity extends BaseActivity implements OnProfileFragmentListener {

    Toolbar toolbar;
    private ImageView editItem;

    boolean isEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        toolbar = findViewById(R.id.toolbar);
        editItem = toolbar.findViewById(R.id.editBtn);
        editItem.setVisibility(View.VISIBLE);
        editItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isEdit = false;
                editItem.setVisibility(View.GONE);
                for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentProfileEdit()).commit();
            }
        });
        setToolbar();
        onProfileChange(true);

        Epaper.getInstance().trackScreenView(ProfileActivity.this, GAConst.Screen.PROFILE);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("My Profile");
    }


    @Override
    public void onProfileChange(boolean isEdit) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
        if (isEdit) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentProfileView()).commit();
            editItem.setVisibility(View.VISIBLE);
        }
    }

    public void hideKeyBoard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (!isEdit) {
            isEdit = !isEdit;
            editItem.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FragmentProfileView()).commit();
            hideKeyBoard();
        } else {
            finish();
        }
    }

}
