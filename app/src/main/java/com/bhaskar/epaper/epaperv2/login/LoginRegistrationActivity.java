package com.bhaskar.epaper.epaperv2.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnLoginListener;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.fragments.SignInFragment;
import com.bhaskar.epaper.fragments.SignUpFragment;
import com.bhaskar.epaper.mobile_verify.MobileVerifyActivity;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.AuthConst;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.bhaskar.epaper.utils.views.CustomViewPager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class LoginRegistrationActivity extends BaseActivity implements OnLoginListener, GoogleApiClient.OnConnectionFailedListener {

    public static final String GA_ACTION = "ga_action";

    private TabLayout tab;
    public CustomViewPager viewpager;
    private String gaAction;
    public ViewPagerAdapter viewPagerAdapter;
    private boolean isLogin = true;
    private EpaperPrefernces pref;
    private String password;
    private String email;
    private String mobileno;
    private String signUpId;
    private String profilePic;
    private int loginType;
    private ProgressDialog pd;
    private String serverMsg;
    private int MOBILE_VERIFY_REQUEST_CODE = 1212;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_registration);
        setToolbar();

        Intent intent = getIntent();
        if (intent != null) {
            gaAction = intent.getStringExtra(GA_ACTION);
            isLogin = intent.getBooleanExtra("IS_LOGIN", true);
        }
        tab = findViewById(R.id.tabs);
        viewpager = findViewById(R.id.login_viewpager);
        viewpager.setPagingEnabled(false);
        try {
            initGoogleAuth();
        } catch (IllegalStateException ignored) {
        }
        initFacebookSdk();

        pref = EpaperPrefernces.getInstance(LoginRegistrationActivity.this);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(viewPagerAdapter);
        tab.setupWithViewPager(viewpager);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    Epaper.getInstance().trackScreenView(LoginRegistrationActivity.this,GAConst.Screen.LOGIN);
                    setToolbarTitle("Login");
                } else {
                    Epaper.getInstance().trackScreenView(LoginRegistrationActivity.this,GAConst.Screen.REGISTRATION);
                    setToolbarTitle("Registration");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        viewpager.setCurrentItem((isLogin) ? 0 : 1);
        Epaper.getInstance().trackScreenView(LoginRegistrationActivity.this,GAConst.Screen.LOGIN);
    }

    private void setProgressbar() {
        pd = new ProgressDialog(LoginRegistrationActivity.this);
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        if (!LoginRegistrationActivity.this.isFinishing()) {
            pd.show();
        }

    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginRegistrationActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
    }

    private void setToolbarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void onGoogleClick() {
        signInWithGoogle();

    }

    @Override
    public void onFBClick() {
        signInWithFacebook();
    }

    @Override
    public void onLogin(String email, String password) {
        this.email = email;
        this.password = password;
        loginType = AuthConst.LoginType.MANUAL;
        makeSignIn();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            addFrag(SignInFragment.getInstance(gaAction), "Login");
            addFrag(SignUpFragment.getInstance(gaAction), "Registration");
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void finishLoginActivity(boolean status) {
        if (status) {
            setResult(RESULT_OK);
            Intent intent = new Intent(QuickPreferences.USER_LOGGED_IN_INTENT);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
        finish();
    }

    /********* FB & Gmail Login *******/

    private static final int RC_SIGN_IN = 5432;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;

    private void initGoogleAuth() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(LoginRegistrationActivity.this)
                .enableAutoManage(LoginRegistrationActivity.this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
//            mGoogleApiClient.clearDefaultAccountAndReconnect();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    firebaseValues(user);
                }
            }
        };
    }

    private void initFacebookSdk() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        AccessToken accessToken = loginResult.getAccessToken();
                        System.out.println("Access Token : " + accessToken);

                        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        System.out.println(response.toString());
                                        try {
                                            String signUpId = object.getString("id");
                                            String name = object.getString("name");

                                            String email = "";
                                            try {
                                                email = object.getString("email");
                                            } catch (JSONException ignored) {
                                            }

                                            Systr.println("Id: " + signUpId + ", name: " + name + ", email: " + email);

                                            String profile_pic = "https://graph.facebook.com/" + signUpId + "/picture";
                                            // send data to server
                                            if (TextUtils.isEmpty(email)) {
                                                signupRequestToServer(email, name, signUpId, profile_pic, AuthConst.LoginType.FB_MANUAL);
                                            } else {
                                                signupRequestToServer(email, name, signUpId, profile_pic, AuthConst.LoginType.FB);
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            fail(getResources().getString(R.string.login_failed_please_try_again), AuthConst.ResultType.FAIL, AuthConst.LoginType.FB);
                                        }
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        fail(getResources().getString(R.string.cancel), AuthConst.ResultType.SKIP, AuthConst.LoginType.FB);
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                        fail(getResources().getString(R.string.error), AuthConst.ResultType.FAIL, AuthConst.LoginType.FB);
                    }
                });
    }

    private void signupRequestToServer(String email, String name, String signUpId, String profilePic, int loginType) {
        this.email = email;
        this.name = name;
        this.signUpId = signUpId;
        this.profilePic = profilePic;
        this.loginType = loginType;
        if (!TextUtils.isEmpty(email)) {
            makeSignIn();
        } else {
            onLoginFailure();
        }
    }

    private void onLoginFailure() {
        Object fragmentInstance = viewPagerAdapter.instantiateItem(viewpager, 1);
        if (fragmentInstance instanceof SignUpFragment)
            ((SignUpFragment) fragmentInstance).setRegistrationData(email, signUpId, profilePic, loginType);
        viewpager.setCurrentItem(1);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        fail(getResources().getString(R.string.google_play_service_error), AuthConst.ResultType.FAIL, AuthConst.LoginType.GOOGLE);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        if (mAuth != null) {
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            mAuth.signInWithCredential(credential).addOnCompleteListener(LoginRegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        fail(getResources().getString(R.string.authentication_failed), AuthConst.ResultType.FAIL, AuthConst.LoginType.GOOGLE);
                    }
                }
            });
        }
    }

    private void firebaseValues(FirebaseUser user) {
        Uri uri = user.getPhotoUrl();
        String profile_pic = "";
        if (uri != null) {
            profile_pic = uri.toString();
        }

        String signUpId = user.getUid();
        String name = user.getDisplayName();
        String email = user.getEmail();
        signupRequestToServer(email, name, signUpId, profile_pic, AuthConst.LoginType.GOOGLE);
    }

    private void signInWithGoogle() {
        if (mAuth != null && mAuthListener != null) {
            mAuth.addAuthStateListener(mAuthListener);
            mAuth.signOut();
        }

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            mGoogleApiClient.clearDefaultAccountAndReconnect();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    private void signInWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private void fail(String msg, int resultType, int loginType) {
        Utility.setToast(LoginRegistrationActivity.this, msg);

    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(LoginRegistrationActivity.this);
        mGoogleApiClient.disconnect();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mAuth != null && mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }

        if (mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(LoginRegistrationActivity.this);
            mGoogleApiClient.disconnect();
        }
        if (!pref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)
                && AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }
        super.onDestroy();
    }


    public void makeSignIn() {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            if (loginType == AuthConst.LoginType.GOOGLE) {
                jsonObject.put("g_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB || loginType == AuthConst.LoginType.FB_MANUAL) {
                jsonObject.put("fb_id", signUpId);
            } else if (loginType == AuthConst.LoginType.MANUAL) {
                jsonObject.put("password", password);
            }
            jsonObject.put("login_type", loginType);
            jsonObject.put("email_mobile", email);
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            jsonObject.put("app_version", Util.getAppVersion(LoginRegistrationActivity.this));
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.LOGIN_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (pd != null && !isFinishing()) {
                    pd.dismiss();
                }
                try {
                    serverMsg = response.optString("msg");
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        Constants.isUserComeFromRegistration = false;
                        boolean isTrial = response.optBoolean("is_trial");
                        EpaperPrefernces.getInstance(LoginRegistrationActivity.this).setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, isTrial);
                        Epaper.getInstance().trackEvent(LoginRegistrationActivity.this,GAConst.Category.LOGIN, gaAction, "" + loginType);
                        setData(response.getJSONObject("data"));
                        Utility.setToast(LoginRegistrationActivity.this, serverMsg);
                    } else {
                        if (loginType != AuthConst.LoginType.MANUAL) {
                            checkMobileNumber();
                        } else {
                            Utility.setToast(LoginRegistrationActivity.this, serverMsg);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(LoginRegistrationActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(LoginRegistrationActivity.this).addToRequestQueue(request);
    }

    private void checkMobileNumber() {
        if (TextUtils.isEmpty(mobileno) && Constants.COUNTRY_IS_INDIA) {
            Intent intent = new Intent(LoginRegistrationActivity.this, MobileVerifyActivity.class);
            intent.putExtra("name", name);
            intent.putExtra("email", email);
            intent.putExtra("login_type", loginType);
            intent.putExtra("sign_up_id", signUpId);
            intent.putExtra("profile_pic", profilePic);
            intent.putExtra("forVerification", true);
            startActivityForResult(intent, MOBILE_VERIFY_REQUEST_CODE);
        } else {
            makeSignUp();
        }
    }


    private void setData(JSONObject mainObject) {
        try {
            pref.setStringValue(QuickPreferences.UserInfo.USER_ID, mainObject.optString("user_id"));
            pref.setStringValue(QuickPreferences.UserInfo.NAME, mainObject.optString("name"));
            pref.setStringValue(QuickPreferences.UserInfo.EMAIL, mainObject.optString("email"));
            pref.setStringValue(QuickPreferences.UserInfo.MOBILE, mainObject.optString("mobileno"));
            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, mainObject.optString("subscribed").equalsIgnoreCase("1"));
            pref.setStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, mainObject.optString("subscription_edate"));
            pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, mainObject.optInt("reg_type"));
            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, mainObject.optString("email_verified").equalsIgnoreCase("1"));
            Constants.FREE_TRIAL_DURATION_MESSAGE = mainObject.optString("freeTrialDateMessage");
            pref.setStringValue(QuickPreferences.UserInfo.PASSWORD, password);
            pref.setStringValue(QuickPreferences.UserInfo.PROFILE_PIC, profilePic);
            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, true);
            try {
                String d_user_verified = mainObject.getString("d_user_verified");
                pref.setBooleanValue(QuickPreferences.PaymentPref.IS_DOMAIN_USER_VERIFIED, d_user_verified.equalsIgnoreCase("1"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finishLoginActivity(true);

    }

    public void makeSignUp() {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("login_type", loginType);

            if (loginType == AuthConst.LoginType.GOOGLE) {
                jsonObject.put("g_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB) {
                jsonObject.put("fb_id", signUpId);
            } else if (loginType == AuthConst.LoginType.FB_MANUAL) {
                jsonObject.put("fb_id", signUpId);
            }
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            jsonObject.put("app_version", Util.getAppVersion(LoginRegistrationActivity.this));
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SIGNUP_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("SignUp Response : " + response);
                if (pd != null) {
                    pd.dismiss();
                }
                try {
                    if (response.optString("code").equalsIgnoreCase("200")) {
                        if (response.optString("status").equalsIgnoreCase("Success")) {
                            Constants.isUserComeFromRegistration = true;
                            JSONObject mainObject = response.getJSONObject("data");
                            Constants.FREE_TRIAL_DURATION_MESSAGE = response.optString("freeTrialDateMessage");

                            boolean isTrial = response.optBoolean("is_trial");
                            EpaperPrefernces.getInstance(LoginRegistrationActivity.this).setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, isTrial);

                            pref.setStringValue(QuickPreferences.UserInfo.EMAIL, mainObject.optString("email"));
                            pref.setStringValue(QuickPreferences.UserInfo.NAME, mainObject.optString("name"));
                            pref.setStringValue(QuickPreferences.UserInfo.MOBILE, mainObject.optString("mobileno"));
                            pref.setStringValue(QuickPreferences.UserInfo.USER_ID, mainObject.optString("user_id"));
                            pref.setStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, mainObject.optString("subscription_edate"));
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, mainObject.optInt("email_verified") == 1);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_MOBILE_VERIFIED, mainObject.optInt("mobile_verified") == 1);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, mainObject.optString("subscribed").equalsIgnoreCase("1"));
                            pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, mainObject.optInt("reg_type"));

                            pref.setStringValue(QuickPreferences.UserInfo.PASSWORD, mainObject.optString("password"));
                            pref.setStringValue(QuickPreferences.UserInfo.PROFILE_PIC, profilePic);
                            pref.setStringValue(QuickPreferences.UserInfo.SIGN_UP_ID, signUpId);
                            pref.setIntValue(QuickPreferences.UserInfo.LOGIN_TYPE, loginType);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, true);

                            Epaper.getInstance().trackEvent(LoginRegistrationActivity.this,GAConst.Category.REGISTER, GAConst.Action.CLICK, "" + loginType);
                            finishLoginActivity(true);
                        }
                    }
                    Utility.setToast(LoginRegistrationActivity.this, response.optString("msg"));

                } catch (Exception ignored) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(LoginRegistrationActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(LoginRegistrationActivity.this).addToRequestQueue(request);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result != null && result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                fail(getResources().getString(R.string.login_failed_please_try_again), AuthConst.ResultType.FAIL, AuthConst.LoginType.GOOGLE);
            }
        } else if (requestCode == MOBILE_VERIFY_REQUEST_CODE) {
            finishLoginActivity(resultCode == RESULT_OK);
        } else {
            if (callbackManager != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
