package com.bhaskar.epaper.epaperv2.util;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.BuildConfig;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.epaperv2.payment.PaymentSubscriptionBean;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.model.MainDataModel;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.PreferncesConstant;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestHandler {
    private static EpaperPrefernces pref;

    public interface OnRequestListener {
        void onCompleteRequest();
    }


    public static void requestForDBId(final Context context) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dev_id", Util.getDeviceId(context));
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("app_version", Util.getAppVersion(context));
            jsonObject.put("country_type", Constants.COUNTRY_IS_INDIA ? "1" : "0");
        } catch (JSONException ignored) {
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.SESSION_URL, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                String status = response.optString("status");
                if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
                    String dbId = response.optString("dev_id");
                    boolean isTrial = response.optBoolean("is_trial");
                    EpaperPrefernces.getInstance(context).setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, isTrial);
                    EpaperPrefernces.getInstance(context).setStringValue(QuickPreferences.UserInfo.DB_ID, dbId);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Systr.println("Session Error : " + error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(12000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
    }

    public static void requestForIsUserSubscribed(final Context context, final OnRequestListener listener) {
        final EpaperPrefernces pref = EpaperPrefernces.getInstance(context);
        String userId = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.UserInfo.USER_ID, "");

        if (!TextUtils.isEmpty(userId)) {
            String subUrl = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.Urls.IS_USER_SUBSCRIBED, Urls.IS_USER_SUBSCRIBED_URL);
            String url = Urls.FEED_BASE_URL + subUrl + userId + "/" + Constants.HOST_ID + "/" + (Urls.IS_TESTING ? "?testing=1" : "");
            Systr.println("Subscribed url : " + url);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Systr.println("Subscribed Response : " + response.toString());

                    try {
                        String status = response.optString("status");
                        if (!TextUtils.isEmpty(status) && status.equalsIgnoreCase("Success")) {
                            boolean isSubscribed = response.optBoolean("IsSubscribed");
                            int userRegType = response.optInt("reg_type");

                            boolean isTrial = response.optBoolean("is_trial");
                             pref.setBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, isTrial);
                            pref.setStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, response.optString("subscription_edate"));
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, isSubscribed);
                            pref.setIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, userRegType);
                            pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, response.optString("email_verified").equalsIgnoreCase("1"));
                            try {
                                String d_user_verified = response.getString("d_user_verified");
                                pref.setBooleanValue(QuickPreferences.PaymentPref.IS_DOMAIN_USER_VERIFIED, d_user_verified.equalsIgnoreCase("1"));
                            } catch (Exception ignored) {
                            }

                            String previousDate = pref.getStringValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_DATE, "");
                            String subscriptionMessage = response.optString("trialEndsMessage");
                            if (!TextUtils.isEmpty(subscriptionMessage) && !Util.getCurrentDate().equalsIgnoreCase(previousDate)) {
                                pref.setStringValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_DATE, Util.getCurrentDate());
                                Constants.isTrialEndsDialogShown = true;
                                pref.setStringValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_DAYS_MESSAGE, subscriptionMessage);
                            }

                        }
                    } catch (Exception ignored) {
                    }

                    if (listener != null) {
                        listener.onCompleteRequest();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Systr.println("Subscribed Error : " + error);

                    if (listener != null) {
                        listener.onCompleteRequest();
                    }
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(12000, 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);
        }
    }

    public static void appSourceTracking(final Context context) {
        String medium = EpaperPrefernces.getInstance(context).getStringValue(Constants.UTM_MEDIUM_TEMP, "direct");
        String campaign = EpaperPrefernces.getInstance(context).getStringValue(Constants.UTM_CAMPAIGN_TEMP, "direct");
        String source = EpaperPrefernces.getInstance(context).getStringValue(Constants.UTM_SOURCE_TEMP, "direct");
        String token = "";
        String appId = "17";
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("app_version", Util.getAppVersion(context));
            jsonObject.put("device_model", Build.BRAND + "_" + Build.MODEL);
            jsonObject.put("device_os", "ANDROID" + "_" + Build.VERSION.SDK_INT);
            jsonObject.put("device_id", Util.getDeviceId(context));
            jsonObject.put("device_token", token);
            jsonObject.put("medium", medium);
            jsonObject.put("campaign", campaign);
            jsonObject.put("source", source);
            jsonObject.put("aid", appId);
            jsonObject.put("app_vc", BuildConfig.VERSION_CODE);
            jsonObject.put("city", "");
            jsonObject.put("state", "");
            jsonObject.put("country", "");

        } catch (JSONException ignored) {

        }


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.APP_SOURCE_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        EpaperPrefernces.getInstance(context).setBooleanValue(QuickPreferences.CommonPref.APP_SOURCE_HIT, true);
                    }
                } catch (Exception ignored) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                Utility.setNetworkErrorToast(context);
            }
        });

        Log.d("Urls", Urls.APP_SOURCE_URL+"");
        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(request);

    }


    public static void makeYearlyPlanHit(Context context, String channelId) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, String.format(Urls.SUBSCRIPTION_YEARLY_PLAN_URL, Constants.PAYMENT_GATEWAY, (Constants.COUNTRY_IS_INDIA ? "1" : "0"), channelId),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                PaymentSubscriptionBean paymentSubscriptionBean = new Gson().fromJson(response.toString(), PaymentSubscriptionBean.class);
                                Epaper.getInstance().getModelManager().setYearlyPlan(paymentSubscriptionBean.getData().getPlans().get(0), Epaper.getInstance().getChannelSlno());
                            }
                        } catch (Exception ignored) {
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", Constants.API_KEY_VALUE);
                headers.put("User-Agent", Constants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    public static void makeDefaultDateRequest(Context context, final String date, final OnChangeDateListener listener) {
        pref = EpaperPrefernces.getInstance(context);

        String url = Urls.STATE_LIST_URL + ((TextUtils.isEmpty(date)) ? "" : date + "/");
        Systr.println("URL= > " + url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (!TextUtils.isEmpty(response.toString())) {
                            try {
                                parseDateParsing(response, date, listener);
                            } catch (Exception e) {
                                if (listener != null) {
                                    listener.fail();
                                }
                            }
                        } else {
                            if (listener != null) {
                                listener.fail();
                            }
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", Constants.API_KEY_VALUE);
                headers.put("User-Agent", Constants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    public static void parseDateParsing(JSONObject response, String setDate, OnChangeDateListener listener) {
        pref.setStringValue(PreferncesConstant.setDateAsDefault, setDate);
        Systr.println("DATE WISE DATA=> " + response.optJSONArray("data"));
        List<MainDataModel> stateList = Arrays.asList(new Gson().fromJson(response.optJSONArray("data").toString(), MainDataModel[].class));

        String setAsDefaultString = pref.getStringValue(PreferncesConstant.setAsDefaultData, "");
        if (stateList.size() > 0 && !TextUtils.isEmpty(setAsDefaultString)) {
            int index = stateList.indexOf(new MainDataModel(setAsDefaultString));
            if (index > -1) {
                MainDataModel mainDataModel = stateList.get(index);
                List<MainDataModel> localList = new ArrayList<>(stateList);
                localList.remove(mainDataModel);
                localList.add(0, mainDataModel);
                Epaper.getInstance().getModelManager().setStateList(localList);
            }
        } else {
            Epaper.getInstance().getModelManager().setStateList(stateList);
        }
        DateUtility.setDateForAll = setDate;

        if (listener != null) {
            listener.success();
        }
    }

    public interface OnChangeDateListener {

        void fail();

        void success();
    }

}
