package com.bhaskar.epaper.epaperv2.payment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;

import org.json.JSONObject;

public class StatusActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_status);

        String packId = "";
        String orderId = "";
        String html = "";
        boolean isSuccess = false;
        String msg = "";

        Intent mainIntent = getIntent();
        if (mainIntent != null) {
            orderId = mainIntent.getStringExtra("orderId");
            html = mainIntent.getStringExtra("html");
            packId = mainIntent.getStringExtra("packId");
        }

        try {
            String str = html.substring(html.indexOf("{"), html.indexOf("}") + 1);
            Systr.println("Substring : " + str);

            JSONObject jsonObject = new JSONObject(str);

            orderId = jsonObject.getString("order_id");
            isSuccess = jsonObject.getString("status").equalsIgnoreCase("Success");
            msg = jsonObject.getString("msg");

        } catch (Exception e) {
        }

        if (!TextUtils.isEmpty(msg)) {
            msg = msg.replace("&lt;", "<").replace("&gt;", ">");

            ((TextView) findViewById(R.id.textView1)).setText(Html.fromHtml(getString(R.string.default_html_string, msg)));
        }

        try {
            ImageView iv = findViewById(R.id.icon_imageview);
            if (isSuccess) {
                iv.setImageResource(R.drawable.ic_trans_tick);
            } else {
                iv.setImageResource(R.drawable.ic_trans_close);
            }
        } catch (Exception e) {
        }

        findViewById(R.id.back_home_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EpaperPrefernces.getInstance(getApplicationContext()).setHome();
            }
        });

        if (isSuccess) {
            EpaperPrefernces.getInstance(this).setBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, true);
            Epaper.getInstance().trackEvent(StatusActivity.this, GAConst.Category.ORDER, GAConst.Action.SUCCESS, packId + "_" + orderId);
        } else {
            Epaper.getInstance().trackEvent(StatusActivity.this, GAConst.Category.ORDER, GAConst.Action.FAIL, packId + "_" + orderId);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        EpaperPrefernces.getInstance(getApplicationContext()).setHome();
    }
}