package com.bhaskar.epaper.epaperv2.payment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.KeyBenefitsInfo;
import com.bhaskar.epaper.model.SubscriptionContactUs;
import com.bhaskar.epaper.model.SubscriptionHeader;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PaymentPlanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_SUBSCRIBE_HEADER = 1;
    private static final int TYPE_SUBSCRIBE_CONTACT = 2;
    private static final int TYPE_SUBSCRIBE_PLANS = 4;
    private static final int TYPE_KEY_BENEFITS = 5;
    private static final int TYPE_LOGIN = 6;
    private static final int TYPE_EMPTY = 7;
    private boolean isShowHeaderDialog = false;
    private EpaperPrefernces mPref;

    public void trialActivated(boolean isShowHeaderDialog) {
        this.isShowHeaderDialog = isShowHeaderDialog;
        notifyDataSetChanged();
    }

    public interface OnPaymentPlanListener {
        void onClickBuy(PaymentSubscriptionBean.Data.Plans info);

        void onClickLogin(boolean isLogin);
    }

    private List<Object> list;
    private OnPaymentPlanListener mListener;
    private Context context;

    public PaymentPlanAdapter(Context context, OnPaymentPlanListener listener) {
        this.context = context;
        list = new ArrayList<>();
        mListener = listener;
        mPref = EpaperPrefernces.getInstance(context);
    }

    public void setData(List<PaymentSubscriptionBean.Data.Plans> infoList, boolean isContactVisible) {
        list.clear();
        list.add(new SubscriptionHeader(TYPE_SUBSCRIBE_HEADER));
        list.addAll(infoList);
//        if (!mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
//            list.add(new SubscriptionHeader(TYPE_LOGIN));
//        }
        list.addAll(Epaper.getInstance().getModelManager().getStringList(Epaper.getInstance().getChannelSlno()));
        if (isContactVisible)
            list.add(new SubscriptionContactUs(TYPE_SUBSCRIBE_CONTACT));

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) instanceof SubscriptionHeader) {
            SubscriptionHeader subscriptionHeader = (SubscriptionHeader) list.get(position);
            if (subscriptionHeader.type == TYPE_SUBSCRIBE_HEADER) {
                return TYPE_SUBSCRIBE_HEADER;
            } else {
                return TYPE_LOGIN;
            }
        } else if (list.get(position) instanceof KeyBenefitsInfo) {
            return TYPE_KEY_BENEFITS;
        } else if (list.get(position) instanceof SubscriptionContactUs) {
            return TYPE_SUBSCRIBE_CONTACT;
        } else if (list.get(position) instanceof PaymentSubscriptionBean.Data.Plans) {
            return TYPE_SUBSCRIBE_PLANS;
        } else {
            return TYPE_EMPTY;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        switch (viewType) {
            case TYPE_SUBSCRIBE_HEADER:
                return new NewHeaderViewHolder(layoutInflater.inflate(R.layout.list_item_payment_header_wall, parent, false));
            case TYPE_SUBSCRIBE_CONTACT:
                return new ContactViewHolder(layoutInflater.inflate(R.layout.list_item_payment_contact, parent, false));
            case TYPE_KEY_BENEFITS:
                return new KeyBenefitViewHolder(layoutInflater.inflate(R.layout.list_item_key_benefit, parent, false));
            case TYPE_SUBSCRIBE_PLANS:
                return new PlansViewHolder(layoutInflater.inflate(R.layout.list_item_payment_plan, parent, false));
            case TYPE_LOGIN:
                return new LoginViewHolder(layoutInflater.inflate(R.layout.list_item_payment_login, parent, false));
            case TYPE_EMPTY:
            default:
                return new EmptyViewHolder(layoutInflater.inflate(R.layout.list_item_payment_empty, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_SUBSCRIBE_HEADER:
                handleTypeHeaderNew(holder);
                break;
            case TYPE_SUBSCRIBE_CONTACT:
                handleTypeContact(holder);
                break;
            case TYPE_SUBSCRIBE_PLANS:
                handleTypePlans(holder, position);
                break;
            case TYPE_LOGIN:
                handleTypeLogin(holder);
                break;
            case TYPE_KEY_BENEFITS:
                handleKeyBenefits(holder, position);
                break;
        }
    }

    private void handleTypeLogin(RecyclerView.ViewHolder holder) {
        LoginViewHolder loginViewHolder = (LoginViewHolder) holder;
        loginViewHolder.loginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Epaper.getInstance().trackEvent(context,GAConst.Category.SUBSCRIPTION, GAConst.Action.CLICK, GAConst.Label.LOGIN);
                if (mListener != null) {
                    mListener.onClickLogin(true);
                }
            }
        });

        loginViewHolder.registrationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Epaper.getInstance().trackEvent(context,GAConst.Category.SUBSCRIPTION, GAConst.Action.CLICK, GAConst.Label.REGISTER);
                if (mListener != null) {
                    mListener.onClickLogin(false);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class NewHeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAppInstallSubtitle;
        private TextView tvAppInstallHeader;
        private TextView tvOR;
        private TextView tvPlanDescription;
        private TextView loginTv;
        private ImageView ivAppInstallHeader;
        private View vOrDivider;

        public NewHeaderViewHolder(View itemView) {
            super(itemView);
            tvAppInstallHeader = itemView.findViewById(R.id.tvAppInstallHeader);
            tvAppInstallSubtitle = itemView.findViewById(R.id.tvAppInstallSubtitle);
            tvPlanDescription = itemView.findViewById(R.id.tvPlanDescription);
            ivAppInstallHeader = itemView.findViewById(R.id.ivAppInstallHeader);
            loginTv = itemView.findViewById(R.id.login_tv);
            tvOR = itemView.findViewById(R.id.tvOR);
            vOrDivider = itemView.findViewById(R.id.vOrDivider);

        }
    }

    private void handleTypeHeaderNew(@NonNull RecyclerView.ViewHolder holder) {
        NewHeaderViewHolder headerViewHolder = (NewHeaderViewHolder) holder;
        String channel=Epaper.getInstance().getChannelSlno();
        String appInstallHeader = mPref.getStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_HEADER, channel), "");
        String appInstallSubtitle = mPref.getStringValue(String.format(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_HEADER_DESC, channel), "");
        String bannerImageURL = mPref.getStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_IMG, channel), "");
        String divyaPlanDesc = mPref.getStringValue(String.format(QuickPreferences.PaymentPref.SUBSCRIPTION_HEADER_TEXT, channel), "");
        final String appInstallLink = mPref.getStringValue(String.format(QuickPreferences.PaymentPref.APP_INSTALL_LINK, channel), "");

        /*Login Button */
        if (!mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false)) {
            headerViewHolder.loginTv.setVisibility(View.VISIBLE);
            String registerNow = "Login";
            String resendOtpText = context.getString(R.string.login_text, registerNow);
            SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(resendOtpText));
            spannableStringBuilder2.setSpan(new MySpannable(context) {
                @Override
                public void onClick(View widget) {
                    Epaper.getInstance().trackEvent(context,GAConst.Category.SUBSCRIPTION, GAConst.Action.CLICK, GAConst.Label.LOGIN);
                    if (mListener != null) {
                        mListener.onClickLogin(true);
                    }
                }
            }, Html.fromHtml(resendOtpText).toString().indexOf(registerNow), Html.fromHtml(resendOtpText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            headerViewHolder.loginTv.setMovementMethod(LinkMovementMethod.getInstance());
            headerViewHolder.loginTv.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
        } else {
            headerViewHolder.loginTv.setVisibility(View.GONE);
        }

        if (channel != null && channel.equalsIgnoreCase(Constants.AppIds.DAINIK_CHANNEL)) {
            headerViewHolder.tvOR.setText("या");
        } else if (channel != null && channel.equalsIgnoreCase(Constants.AppIds.DIVYA_CHANNEL)) {
            headerViewHolder.tvOR.setText("OR");
        }

        if (!TextUtils.isEmpty(appInstallHeader)) {
            headerViewHolder.tvAppInstallHeader.setText(Html.fromHtml(appInstallHeader));
            headerViewHolder.tvAppInstallHeader.setVisibility(View.VISIBLE);
        } else {
            headerViewHolder.tvAppInstallHeader.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(appInstallSubtitle)) {
            headerViewHolder.tvAppInstallSubtitle.setText(Html.fromHtml(appInstallSubtitle));
            headerViewHolder.tvAppInstallSubtitle.setVisibility(View.VISIBLE);
            headerViewHolder.vOrDivider.setVisibility(View.VISIBLE);
        } else {
            headerViewHolder.tvAppInstallSubtitle.setVisibility(View.GONE);
            headerViewHolder.vOrDivider.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(divyaPlanDesc)) {
            headerViewHolder.tvPlanDescription.setText(Html.fromHtml(divyaPlanDesc));
            headerViewHolder.tvPlanDescription.setVisibility(View.VISIBLE);
        } else {
            headerViewHolder.tvPlanDescription.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(bannerImageURL)) {
            ImageUtil.setImage(context, bannerImageURL, 0, headerViewHolder.ivAppInstallHeader);
        }
        if (!TextUtils.isEmpty(appInstallLink)) {
            headerViewHolder.ivAppInstallHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(appInstallLink)));
                }
            });
        }
        if (!TextUtils.isEmpty(bannerImageURL)) {
            headerViewHolder.ivAppInstallHeader.setVisibility(View.VISIBLE);
        } else {
            headerViewHolder.ivAppInstallHeader.setVisibility(View.GONE);
        }

    }

    class PlansViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout specialHotDealsRelativeLayout;
        private TextView titleTextView, discountedPriceTv, txtRsSymbolDisc, amountTextView, desc1TextView, txtSubscrMonth, detailsTextView, specialHotDealsTextView, tvPercent;
        private Button buyButton;

        public PlansViewHolder(View view) {
            super(view);
            titleTextView = view.findViewById(R.id.txt_subscr_title);
            amountTextView = view.findViewById(R.id.txt_subscr_amount);
            txtSubscrMonth = view.findViewById(R.id.txt_subscr_month);
            desc1TextView = view.findViewById(R.id.txt_subscr_desc);
            detailsTextView = view.findViewById(R.id.txt_subscr_details);
            buyButton = view.findViewById(R.id.btn_subscr_buy);
            txtRsSymbolDisc = view.findViewById(R.id.txt_rs_symbol_disc);
            discountedPriceTv = view.findViewById(R.id.discounted_price_tv);
            specialHotDealsTextView = view.findViewById(R.id.special_hot_deals_textview);
            specialHotDealsRelativeLayout = view.findViewById(R.id.special_hot_deals_rl);
            tvPercent = view.findViewById(R.id.tv_percent);
        }
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View view) {
            super(view);
        }
    }

    class LoginViewHolder extends RecyclerView.ViewHolder {
        TextView registrationTv, loginTv;

        public LoginViewHolder(View itemView) {
            super(itemView);
            loginTv = itemView.findViewById(R.id.login_tv);
            registrationTv = itemView.findViewById(R.id.registration_tv);
        }
    }

    private void handleKeyBenefits(RecyclerView.ViewHolder holder, int position) {

        KeyBenefitsInfo keyBenefitsInfo = (KeyBenefitsInfo) list.get(position);
        KeyBenefitViewHolder keyBenefitViewHolder = (KeyBenefitViewHolder) holder;
        if (keyBenefitsInfo.isHeader) {
            keyBenefitViewHolder.keyBenefitHeader.setVisibility(View.VISIBLE);
            keyBenefitViewHolder.contentLayout.setVisibility(View.GONE);
            keyBenefitViewHolder.benefitSeparator.setVisibility(View.GONE);
        } else {
            keyBenefitViewHolder.keyBenefitHeader.setVisibility(View.GONE);
            keyBenefitViewHolder.contentLayout.setVisibility(View.VISIBLE);
            keyBenefitViewHolder.benefitSeparator.setVisibility(View.VISIBLE);
            keyBenefitViewHolder.benefitNameTV.setText(Html.fromHtml(keyBenefitsInfo.name));
            keyBenefitViewHolder.benefitDescTV.setText(Html.fromHtml(keyBenefitsInfo.desc));
            Glide.with(context).load(keyBenefitsInfo.imagePath).into(keyBenefitViewHolder.benefitIV);
        }
    }

    class KeyBenefitViewHolder extends RecyclerView.ViewHolder {
        ImageView benefitIV;
        TextView benefitNameTV, benefitDescTV, keyBenefitHeader;
        RelativeLayout contentLayout;
        View benefitSeparator;

        public KeyBenefitViewHolder(View view) {
            super(view);
            benefitIV = view.findViewById(R.id.benefit_iv);
            benefitNameTV = view.findViewById(R.id.benefit_name_tv);
            benefitDescTV = view.findViewById(R.id.benefit_desc_tv);
            keyBenefitHeader = view.findViewById(R.id.key_benefit_header);
            contentLayout = view.findViewById(R.id.content_layout);
            benefitSeparator = view.findViewById(R.id.benefit_separator);

        }
    }

    private void handleTypePlans(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder == null)
            return;

        final PaymentSubscriptionBean.Data.Plans info = (PaymentSubscriptionBean.Data.Plans) list.get(position);
        PlansViewHolder viewHolder = (PlansViewHolder) holder;

        if (viewHolder.titleTextView == null &&
                viewHolder.amountTextView == null &&
                viewHolder.txtSubscrMonth == null &&
                viewHolder.detailsTextView == null &&
                viewHolder.buyButton == null) {
            return;
        }

        if (viewHolder.titleTextView != null) {
            viewHolder.titleTextView.setText(info.top_label);
        }
        if (info != null) {
            if (info.getPlan_desc() != null && info.getPlan_desc().list.length > 0) {

                StringBuilder stringBuffer = new StringBuilder();
                for (int i = 0; i < info.getPlan_desc().list.length; i++) {
                    String s = context.getString(R.string.tick_icon);
                    String desc = info.getPlan_desc().list[i];
                    String s1 = s + " " + desc;
                    if (i < info.getPlan_desc().list.length - 1) {
                        s1 = s1 + "<br><br>";
                    }
                    stringBuffer.append(s1);
                }
                viewHolder.desc1TextView.setText(Html.fromHtml(stringBuffer.toString()));
            }
            viewHolder.amountTextView.setText(" " + info.plan_rs);
            viewHolder.txtSubscrMonth.setText(info.plan_days_label);

            if (info.plan_desc != null && !TextUtils.isEmpty(info.plan_desc.desc)) {
                viewHolder.detailsTextView.setVisibility(View.VISIBLE);
                viewHolder.detailsTextView.setText(info.plan_desc.desc);
            } else {
                viewHolder.detailsTextView.setVisibility(View.GONE);
            }
            viewHolder.buyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onClickBuy(info);
                    }
                }
            });

            int displayRs = 0;
            try {
                displayRs = Integer.parseInt(info.plan_display_rs);
            } catch (Exception ignored) {
            }
            if (displayRs > 0) {
                viewHolder.tvPercent.setVisibility(View.VISIBLE);
                viewHolder.tvPercent.setText(String.format("%s%s", Util.getPercentValue(displayRs, Integer.parseInt(info.plan_rs)), "% off"));
                viewHolder.discountedPriceTv.setText(String.format("%s", displayRs));
                viewHolder.discountedPriceTv.setVisibility(View.VISIBLE);
                viewHolder.txtRsSymbolDisc.setVisibility(View.VISIBLE);
            } else {
                viewHolder.tvPercent.setVisibility(View.GONE);
                viewHolder.discountedPriceTv.setVisibility(View.GONE);
                viewHolder.txtRsSymbolDisc.setVisibility(View.GONE);
            }

            if (TextUtils.isEmpty(info.offer_name)) {
                viewHolder.specialHotDealsRelativeLayout.setVisibility(View.GONE);
            } else {
                viewHolder.specialHotDealsRelativeLayout.setVisibility(View.VISIBLE);
                viewHolder.specialHotDealsTextView.setText(info.offer_name);
            }
        }
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {
        LinearLayout email_ll, feedback_ll,address_2_ll;
        LinearLayout address_1_ll;
        TextView email_title_tv, email_desc_tv, feedback_desc_tv, address_1_title_tv, address_1_desc_tv, address_2_title_tv, address_2_desc_tv;

        ContactViewHolder(View itemView) {
            super(itemView);

            email_ll = itemView.findViewById(R.id.email_ll);
            feedback_ll = itemView.findViewById(R.id.feedback_ll);
            address_1_ll = itemView.findViewById(R.id.address_1_ll);
            address_2_ll = itemView.findViewById(R.id.address_2_ll);
            email_title_tv = itemView.findViewById(R.id.email_title_tv);
            email_desc_tv = itemView.findViewById(R.id.email_desc_tv);
            feedback_desc_tv = itemView.findViewById(R.id.feedback_desc_tv);
            address_1_title_tv = itemView.findViewById(R.id.address_1_title_tv);
            address_1_desc_tv = itemView.findViewById(R.id.address_1_desc_tv);
            address_2_title_tv = itemView.findViewById(R.id.address_2_title_tv);
            address_2_desc_tv = itemView.findViewById(R.id.address_2_desc_tv);
        }
    }

    private void handleTypeContact(@NonNull RecyclerView.ViewHolder holder) {

        ContactViewHolder contactViewHolder = (ContactViewHolder) holder;

        String emailJson = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.SEND_EMAIL, "");
        String feedbackJson = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.FEEDBACK, "");
        String address1Json = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.ADDRESS_1, "");
        String address2Json = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.ADDRESS_2, "");

        // Email
        try {
            JSONObject jsonObject = new JSONObject(emailJson);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                contactViewHolder.email_ll.setVisibility(View.VISIBLE);
                contactViewHolder.email_title_tv.setText(Html.fromHtml(jsonObject.optString("title")));

                String desc = jsonObject.optString("desc");
                contactViewHolder.email_desc_tv.setMovementMethod(Util.privacyMovementMethod(context, 1));
                contactViewHolder.email_desc_tv.setText(Html.fromHtml(desc));
            } else {
                contactViewHolder.email_ll.setVisibility(View.GONE);
            }
        } catch (JSONException ignored) {
        }

        // Feedback
        try {
            JSONObject jsonObject = new JSONObject(feedbackJson);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                contactViewHolder.feedback_ll.setVisibility(View.VISIBLE);

                String desc = jsonObject.optString("desc");
                contactViewHolder.feedback_desc_tv.setMovementMethod(Util.privacyMovementMethod(context, 2));
                contactViewHolder.feedback_desc_tv.setText(Html.fromHtml(desc));
            } else {
                contactViewHolder.feedback_ll.setVisibility(View.GONE);
            }
        } catch (JSONException ignored) {
        }

        // Address 1
        try {
            JSONObject jsonObject = new JSONObject(address1Json);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                contactViewHolder.address_1_ll.setVisibility(View.VISIBLE);
                contactViewHolder.address_1_title_tv.setText(Html.fromHtml(jsonObject.optString("title")));
                contactViewHolder.address_1_desc_tv.setText(Html.fromHtml(jsonObject.optString("desc")));
            } else {
                contactViewHolder.address_1_ll.setVisibility(View.GONE);
            }
        } catch (JSONException ignored) {
        }

        // Address 2
        try {
            JSONObject jsonObject = new JSONObject(address2Json);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                contactViewHolder.address_2_ll.setVisibility(View.VISIBLE);
                contactViewHolder.address_2_title_tv.setText(Html.fromHtml(jsonObject.optString("title")));
                contactViewHolder.address_2_desc_tv.setText(Html.fromHtml(jsonObject.optString("desc")));
            } else {
                contactViewHolder.address_2_ll.setVisibility(View.GONE);
            }
        } catch (JSONException ignored) {
        }
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;

        public MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {

        }
    }
}
