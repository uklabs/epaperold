package com.bhaskar.epaper.epaperv2.payment.utility;

import com.bhaskar.epaper.utils.Urls;

public class Constants {

    public static String DAINIK_BHASKAR = "521";
    public static String DIVYA_BHASKAR = "960";
    public static String DIVYA_MARATHI_BHASKAR = "5483";
    // Payments
    public static final String PARAMETER_SEP = "&";
    public static final String PARAMETER_EQUALS = "=";

    // Live
    public static final String MERCHANT_ID = "180569";
    public static final String ACCESS_CODE = "AVAJ78FF78BW56JAWB";
    public static final String REDIRECT_URL = "https://appfeedlight.bhaskar.com/epaper/verifyTxnStatus/" + (Urls.IS_TESTING ? "?testing=1" : "");
    public static final String CANCEL_URL = "https://appfeedlight.bhaskar.com/epaper/verifyTxnStatus/" + (Urls.IS_TESTING ? "?testing=1" : "");
    public static final String RSA_KEY_URL = "https://appfeedlight.bhaskar.com/epaper/getRSA/" + (Urls.IS_TESTING ? "?testing=1" : "");
    public static final String TRANS_URL = "https://secure.ccavenue.com/transaction/initTrans" + (Urls.IS_TESTING ? "?testing=1" : "");

//    Testing
//    public static final String RSA_KEY_URL = "https://appfeedlight.bhaskar.com/epaper/getTRSA/";
//    public static final String TRANS_URL = "https://test.ccavenue.com/transaction/initTrans";
//    public static final String REDIRECT_URL = "http://122.182.6.216/merchant/ccavResponseHandler.jsp";
//    public static final String CANCEL_URL = "http://122.182.6.216/merchant/ccavResponseHandler.jsp";

    // UTM Campaign PREFERENCE
    public static final String UTM_DIRECT = "direct";

    public static final String UTM_SOURCE = "utm_source";
    public static final String UTM_MEDIUM = "utm_medium";
    public static final String UTM_CAMPAIGN = "utm_campaign";

    public static final String UTM_SOURCE_TEMP = "utm_source_temp";
    public static final String UTM_MEDIUM_TEMP = "utm_medium_temp";
    public static final String UTM_CAMPAIGN_TEMP = "utm_campaign_temp";


    public static final String USER_AGENT = "EpaperAndroid";
    public static String API_KEY_VALUE = "bh@@$k@r-D";
    public static String HOST_ID = "112";
    public static String PAYMENT_GATEWAY = "1";

    public static String FREE_TRIAL_DURATION_MESSAGE;
    public static boolean COUNTRY_IS_INDIA;
    public static boolean SUBSCRIBED_COUNTRY;


    public static boolean isTrialEndsDialogShown = false;
    public static boolean isUserComeFromRegistration = false;

    public static String[] feedbackTopics;

    public interface AppCommon {
        int UPDATE_POPUP_SESSION_COUNT = 15;
        int UPDATE_POPUP_DAYS_COUNT = 5;
        int MOBILE_POPUP_DAYS_COUNT = 7;
        int DEFAULT_VISIBLE_THRESHOLD = 5;
    }

    public interface SubscriptionStatus {
        int TRIAL_USER = 1;
        int PAID_USER = 2;
        int DOMAIN_USER = 3;
    }

    public interface AppIds {
        String DAINIK_CHANNEL = "521";
        String DIVYA_CHANNEL = "960";
    }

    public interface MoreAction {
        String CHANGE_DATE = "ActionChangeDate";
        String BOOKMARK = "ActionNotification";
        String CONTACT_US = "ActionContactUs";
        String RATE_US = "ActionRateus";
        String TERMS_AND_CONDITION = "ActionTermAndCondi";
        String PRIVACY_POLICY = "ActionPrivacyPolicy";
        String FEEDBACK = "ActionFeedback";
        String SHARE_APP = "ActionShareApp";
        String ACCOUNT_SETTING = "ActionAccountSetting";
    }

    public interface Gdpr {
        String CONSENT_VALUE = "gdprConsentValue";
        int USER_CONSENT = 1;
        int WD_CONSENT = 2;
        int WD_LOGIN_CONSENT = 3;
        int WD_SUBSCR_CONSENT = 4;
    }
}