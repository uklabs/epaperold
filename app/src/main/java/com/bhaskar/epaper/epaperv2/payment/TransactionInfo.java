package com.bhaskar.epaper.epaperv2.payment;

import com.google.gson.annotations.SerializedName;

public class TransactionInfo {

    @SerializedName("title")
    public String title;

    @SerializedName("datetime")
    public String dateTime;

    @SerializedName("amount")
    public String amount;

    @SerializedName("order_id")
    public String orderNumber;

    @SerializedName("time_period")
    public String timePeriod;

    @SerializedName("status")
    public String status;

    @SerializedName("txnstatus")
    public String txnStatus;
}
