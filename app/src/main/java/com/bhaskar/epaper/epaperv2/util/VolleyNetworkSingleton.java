package com.bhaskar.epaper.epaperv2.util;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bhaskar.epaper.utils.OkHttpStack;
import com.facebook.stetho.okhttp.StethoInterceptor;
import com.squareup.okhttp.OkHttpClient;

public class VolleyNetworkSingleton {
    private static VolleyNetworkSingleton mInstance;
    Context mContext;
    private RequestQueue mRequestQueue;

    private VolleyNetworkSingleton(Context context) {
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyNetworkSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyNetworkSingleton(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient.networkInterceptors().add(new StethoInterceptor());
            mRequestQueue = Volley.newRequestQueue(mContext, new OkHttpStack(okHttpClient));
//            mRequestQueue = Volley.newRequestQueue(mContext, new OkHttpStack(okHttpClient));
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setShouldCache(false);
        getRequestQueue().add(req);
    }
}
