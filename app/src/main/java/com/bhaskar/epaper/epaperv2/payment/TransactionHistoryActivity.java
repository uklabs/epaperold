package com.bhaskar.epaper.epaperv2.payment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.DividerItemDecoration;
import com.bhaskar.epaper.epaperv2.util.ProgressbarDialog;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TransactionHistoryActivity extends BaseActivity {

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private TransactionHistroyAdapter mAdapter;
    private ProgressbarDialog progressbarDialog;

    private boolean isLoading;
    private int visibleThreshold = 5;
    private int pageCount = 1;
    private boolean isLoadMoreEnable = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransactionHistoryActivity.this.finish();
            }
        });

        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setHomeButtonEnabled(true);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setTitle("Order History");
        }

        progressbarDialog = new ProgressbarDialog(this);

        mAdapter = new TransactionHistroyAdapter(this);
        mRecyclerView = findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this);
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int totalItemCount = mLayoutManager.getItemCount();
                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();

                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (isLoadMoreEnable) {
                            onLoadMore();
                            isLoading = true;
                        }
                    }
                }
            }
        });

        fetchTracnsactionHistory();

        Epaper.getInstance().trackScreenView(TransactionHistoryActivity.this,GAConst.Screen.ORDER_DETAIL);
    }

    public void onLoadMore() {
        mAdapter.addItem();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fetchTracnsactionHistory();
            }
        }, 200);
    }

    private void fetchTracnsactionHistory() {
        String userId = EpaperPrefernces.getInstance(TransactionHistoryActivity.this).getStringValue(QuickPreferences.UserInfo.USER_ID, "");

        String subUrl = EpaperPrefernces.getInstance(TransactionHistoryActivity.this).getStringValue(QuickPreferences.Urls.SUBS_HISTORY, Urls.TRANSACTION_HISTORY_URL);
        String url = Urls.FEED_BASE_URL + subUrl + userId + "/PG" + pageCount + "/"+(Urls.IS_TESTING?"?testing=1":"");
        fetchTracnsactionHistoryFromServer(TransactionHistoryActivity.this, url, pageCount == 1);
    }

    private void fetchTracnsactionHistoryFromServer(final Context context, String url, final boolean showProgress) {
        if (showProgress)
            progressbarDialog.show(getResources().getString(R.string.please_wait), false);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url,
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Systr.println("Order history Response : " + response.toString());

                        if (showProgress)
                            progressbarDialog.dismiss();

                        try {
                            if (response.getString("status").equalsIgnoreCase("Success")) {
                                JSONArray data = response.getJSONArray("data");
                                ArrayList<TransactionInfo> list = new ArrayList<>(Arrays.asList(new Gson().fromJson(data.toString(), TransactionInfo[].class)));

                                mAdapter.removeItem();
                                mAdapter.setDataInList(list);

                                if (data.length() == 0) {
                                    isLoadMoreEnable = false;
                                    isLoading = false;
                                } else {
                                    isLoading = false;
                                    pageCount++;
                                }

                            } else {
                                mAdapter.removeItem();
                                Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            mAdapter.removeItem();
                            Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again), Toast.LENGTH_SHORT).show();
                        }

                        if (mAdapter.getItemCount() > 0) {
                            findViewById(R.id.no_data_found_tv).setVisibility(View.GONE);
                        } else {
                            findViewById(R.id.no_data_found_tv).setVisibility(View.VISIBLE);
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (showProgress)
                    progressbarDialog.dismiss();
                mAdapter.removeItem();
                Toast.makeText(context, context.getResources().getString(R.string.sorry_error_found_please_try_again), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", Constants.API_KEY_VALUE);
                headers.put("User-Agent", Constants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(TransactionHistoryActivity.this).addToRequestQueue(jsonObjReq);
    }
}
