package com.bhaskar.epaper.epaperv2.profile.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnProfileFragmentListener;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class FragmentProfileEdit extends Fragment implements View.OnClickListener {

    private EditText profileEditEmail;
    private EditText profileEditMobile;
    private EditText profileEditName;
    private Button profileEditSubmit;
    private TextView mobileTv;

    private EpaperPrefernces mPref;
    private ProgressDialog mProgressDialog;

    private OnProfileFragmentListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (OnProfileFragmentListener) context;
        } catch (ClassCastException ignored) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        mPref = EpaperPrefernces.getInstance(getActivity());

        profileEditName = v.findViewById(R.id.edt_profile_edit_name);
        profileEditMobile = v.findViewById(R.id.edt_profile_edit_mobile);
        profileEditEmail = v.findViewById(R.id.edt_profile_edit_email);

        profileEditMobile.setEnabled(false);
        profileEditEmail.setEnabled(false);
        profileEditSubmit = v.findViewById(R.id.btn_profile_edit_submit);
        mobileTv = v.findViewById(R.id.mobile_tv);
        ((TextView) v.findViewById(R.id.prfile_title)).setText(getResources().getString(R.string.welcome_message, mPref.getStringValue(QuickPreferences.UserInfo.NAME, "")));

        setupDialog();
        initView();
        return v;
    }

    private void setupDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Profile Updating...");
        mProgressDialog.setCancelable(true);
    }

    private void showDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.show();
        }
    }

    private void hideDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private void initView() {
        profileEditSubmit.setOnClickListener(this);
        profileEditEmail.setText(mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""));
        if (Constants.COUNTRY_IS_INDIA) {
            profileEditMobile.setText(mPref.getStringValue(QuickPreferences.UserInfo.MOBILE, ""));
            profileEditMobile.setVisibility(View.VISIBLE);
            mobileTv.setVisibility(View.VISIBLE);
        } else {
            profileEditMobile.setVisibility(View.GONE);
            mobileTv.setVisibility(View.GONE);
        }
        profileEditName.setText(mPref.getStringValue(QuickPreferences.UserInfo.NAME, ""));
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_profile_edit_submit: {
                if (Utility.isConnectingToInternet(getActivity())) {
                    if (checkValidation()) {
                        editProfileData(profileEditName.getText().toString(), profileEditEmail.getText().toString());
                    }
                } else {
                    Utility.setNetworkErrorToast(getActivity());
                }

                break;
            }
        }
    }


    public void editProfileData(final String name, final String email) {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", mPref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
            jsonObject.put("name", name);
            jsonObject.put("email", email);
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("app_version", Util.getAppVersion(getContext()));

        } catch (JSONException e) {
        }

        Systr.println("Signup send json : " + jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.PROFILE_EDIT_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Signup response : " + response.toString());
                hideDialog();
                try {
                    EpaperPrefernces pref = EpaperPrefernces.getInstance(getActivity());
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        pref.setStringValue(QuickPreferences.UserInfo.NAME, name);
                        pref.setStringValue(QuickPreferences.UserInfo.EMAIL, email);
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, response.optString("email_verified").equalsIgnoreCase("1"));
                        if (listener != null) {
                            listener.onProfileChange(true);
                        }
                        Intent intent = new Intent(QuickPreferences.USER_LOGGED_IN_INTENT);
                        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                    } else {
                        pref.setBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, response.optString("email_verified").equalsIgnoreCase("1"));
                        initView();
                    }
                    Utility.setToast(getActivity(), response.optString("msg"));

                } catch (Exception e) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                hideDialog();
                Utility.setNetworkErrorToast(getActivity());
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(getContext()).addToRequestQueue(request);
    }

    private boolean checkValidation() {

        if (TextUtils.isEmpty(profileEditName.getText().toString())) {
            profileEditName.setError("Please enter full name");
            return false;
        } /*else if (TextUtils.isEmpty(profileEditMobile.getText().toString())) {
            profileEditMobile.setError("Please enter mobile number");
            return false;
        } else if (profileEditMobile.getText().toString().length() < 10) {
            profileEditMobile.setError("Please enter 10 digit mobile number");
            return false;
        } else if (TextUtils.isEmpty(profileEditEmail.getText().toString())) {
            profileEditEmail.setError("Please enter email");
            return false;
        } */ else {
            return true;
        }
    }
}
