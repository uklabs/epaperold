package com.bhaskar.epaper.epaperv2.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.provider.Settings;
import android.text.Layout;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.FeedbackActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.comscore.Analytics;
import com.comscore.PublisherConfiguration;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Util {
     private Context context;
    public static String getDeviceId(Context mContext) {
        String android_id = EpaperPrefernces.getInstance(mContext).getStringValue(QuickPreferences.UserInfo.DEVICE_ID, "");
        if (TextUtils.isEmpty(android_id)) {
            android_id = Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
            if (TextUtils.isEmpty(android_id)) {
                android_id = UUID.randomUUID().toString();
            }
            EpaperPrefernces.getInstance(mContext).setStringValue(QuickPreferences.UserInfo.DEVICE_ID, android_id);
        }

        return android_id;
    }

    public static boolean isEmailValid(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static void sendEmail(Context context, String subject, String email) {
        try {
            if (TextUtils.isEmpty(subject)) {
                subject = context.getString(R.string.feedback_mail_subject) + getAppVersion(context);
            }
            Intent sendIntent = new Intent(android.content.Intent.ACTION_SEND);
            sendIntent.setType("message/rfc822");
            sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{email});
            sendIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
            sendIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
            context.startActivity(Intent.createChooser(sendIntent, "Send email"));
        } catch (Exception e) {
        }
    }

    public static String getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo != null)
                return packageInfo.versionName;
        } catch (Exception e) {
        }

        return "5.2";
    }

    public static MovementMethod privacyMovementMethod(final Context context, final int type) {

        return new MovementMethod() {
            @Override
            public boolean onTrackballEvent(TextView widget, Spannable text, MotionEvent event) {
                return false;
            }

            @Override
            public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
                final int action = event.getAction();
                if (action == MotionEvent.ACTION_UP) {
                    final int x = (int) event.getX() - widget.getTotalPaddingLeft() + widget.getScrollX();
                    final int y = (int) event.getY() - widget.getTotalPaddingTop() + widget.getScrollY();
                    final Layout layout = widget.getLayout();
                    final int line = layout.getLineForVertical(y);
                    final int off = layout.getOffsetForHorizontal(line, x);
                    final ClickableSpan[] C = buffer.getSpans(off, off, ClickableSpan.class);

                    if (C.length != 0) {
                        switch (type) {
                            case 1:
//                                Toast.makeText(context, "Email Open", Toast.LENGTH_SHORT).show();
                                Util.sendEmail(context, "", buffer.toString());
                                break;
                            case 2:
//                                Toast.makeText(context, "Feedback Open", Toast.LENGTH_SHORT).show();
                                Epaper.getInstance().trackEvent(context,GAConst.Category.FEEDBACK, GAConst.Action.CLICK, GAConst.Label.CONTACT);
                                Intent intent = new Intent(context, FeedbackActivity.class);
                                context.startActivity(intent);
                                break;
                        }
                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTakeFocus(TextView widget, Spannable text, int direction) {// empty Method
            }

            @Override
            public boolean onKeyUp(TextView widget, Spannable text, int keyCode, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyOther(TextView view, Spannable text, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onKeyDown(TextView widget, Spannable text, int keyCode, KeyEvent event) {
                return false;
            }

            @Override
            public boolean onGenericMotionEvent(TextView widget, Spannable text, MotionEvent event) {
                return false;
            }

            @Override
            public void initialize(TextView widget, Spannable text) {
                widget.setLinkTextColor(Color.parseColor("#000000"));
            }

            @Override
            public boolean canSelectArbitrarily() {
                return false;
            }
        };
    }

    public static String encryptString(String str) {
        try {
            return Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (Exception e) {
        }

        return "";
    }

    public static String decryptString(String str) {
        try {
            return new String(Base64.decode(str, Base64.DEFAULT), "UTF-8");
        } catch (Exception ignored) {
        }

        return "";
    }

    public static boolean isShowAds(Context context) {
        EpaperPrefernces epaperPrefernces = EpaperPrefernces.getInstance(context);
        boolean isSubscribed = epaperPrefernces.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false);
        int userStatus = epaperPrefernces.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        return !Constants.SUBSCRIBED_COUNTRY || !isSubscribed || userStatus != Constants.SubscriptionStatus.PAID_USER;

    }

    public static boolean isShowMagazine(Context context) {
        EpaperPrefernces epaperPrefernces = EpaperPrefernces.getInstance(context);
        boolean isSubscribed = epaperPrefernces.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false);
        return !Constants.SUBSCRIBED_COUNTRY || !isSubscribed;


    }

    public static String getCurrentDate() {
        Date today = Calendar.getInstance().getTime();
        DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd");
        return mSDF.format(today);
    }

    /**********************************************************************
     *  Call in Application
     ***********************/

    public static void initialiseComscore(Context applicationContext, String publisherId, String secretId, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // ComScore
            PublisherConfiguration myPublisherConfig = new PublisherConfiguration.Builder()
                    .publisherId(publisherId)
                    .publisherSecret(secretId)
                    .vce(false)
                    .build();
            Analytics.getConfiguration().addClient(myPublisherConfig);
            if (applicationContext != null) {
                Analytics.start(applicationContext);
            }
        }
    }

    public static void onResume(Context context, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // Comscore
            Analytics.notifyEnterForeground();
        }
    }

    public static void onPause(Context context, boolean isBlockedCountry) {
        if (!isBlockedCountry) {
            // Comscore
            Analytics.notifyExitForeground();
        }
    }

    public static String getUserType(Context context) {
        int userType = EpaperPrefernces.getInstance(context).getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        String userTypeString = "";
        switch (userType) {
            case 1:
                userTypeString = "FREE";
                break;
            case 2:
                userTypeString = "PAID";
                break;
            case 3:
                userTypeString = "DOMAIN";
                break;

        }
        return userTypeString;
    }

    public static int getPercentValue(int mainValue, int discountedValue) {
        float dividedValue = (float) discountedValue / (float) (mainValue) * 100;
        return 100 - (int) dividedValue;
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static void checkDateAndClearCache(Context context) {
        EpaperPrefernces sharedPreferences = EpaperPrefernces.getInstance(context);
        Calendar c = Calendar.getInstance();

        int thisDay = c.get(Calendar.DAY_OF_YEAR); // GET THE CURRENT DAY OF THE YEAR
        int lastDay = sharedPreferences.getIntValue(QuickPreferences.CommonPref.LAST_USED_DAY, thisDay); //If we don't have a saved value, use 0.

        if (thisDay == lastDay) {
            sharedPreferences.setIntValue(QuickPreferences.CommonPref.LAST_USED_DAY, lastDay);
        } else {
            /*if (thisDay - lastDay > 2) {
                deleteCache(context);
                sharedPreferences.setIntValue(QuickPreferences.CommonPref.LAST_USED_DAY, thisDay);
            }*/
            int  value = Integer.parseInt(EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.CommonPref.CACHE_VALUE, "0"));
            if (value==0)
                return;
            if (thisDay - lastDay > value){
                deleteCache(context);
                sharedPreferences.setIntValue(QuickPreferences.CommonPref.LAST_USED_DAY, thisDay);
            }

        }
    }

    public static void shareApp(Context activity) {
        String message = activity.getResources().getString(R.string.app_share_message);
        String messageLink = Urls.APP_SHARE_URL;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String body = message + " \n" + messageLink;
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getString(R.string.app_share_subject));
        activity.startActivity(Intent.createChooser(intent, "Share"));
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

}
