package com.bhaskar.epaper.epaperv2.payment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class FamousEditionsAdapter extends RecyclerView.Adapter<FamousEditionsAdapter.ViewHolder> {

    public List<MasterModel> listdata;
    public String statename;
    Context context;

    public FamousEditionsAdapter(Context context, String statename) {
        this.statename = statename;
        this.context = context;
        if (listdata == null) {
            listdata = new ArrayList<>();
        } else {
            listdata.clear();
        }
    }

    public void setData(List<MasterModel> listdata) {
        if (this.listdata.size() > 0) {
            this.listdata.clear();
        }
        this.listdata = listdata;
    }

    @NonNull
    @Override
    public FamousEditionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_famous_edition, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final FamousEditionsAdapter.ViewHolder holder, final int position) {

        String imgThumb = Epaper.NO_IMG_FOUND;
        if (listdata.get(position) != null) {
            holder.itemCategorytxt.setText(listdata.get(position).description);
            imgThumb = listdata.get(position).imagethumb;
        } else {
            holder.itemCategorytxt.setText("unavailable");
        }

        try {
            ImageUtil.setImage(context,imgThumb,R.drawable.watermark_epaper,holder.itemImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        TextView itemCategoryBookmark;
        ImageView itemImageView;
        LinearLayout mainCardView;
        RelativeLayout ll_category_home;
        CardView cardview;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);
            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemCategoryBookmark = itemView.findViewById(R.id.item_category_bookmark);
            cardview = itemView.findViewById(R.id.cardview);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            if (!TextUtils.isEmpty(statename)) {
                itemCategoryBookmark.setVisibility(View.GONE);
            }
        }
    }
}

