package com.bhaskar.epaper.epaperv2.util;

public class QuickPreferences {

    public static String USER_LOGGED_IN_INTENT = "user_logged_in_intent";

    public interface PaymentPref {
        String IS_LOGIN = "isLogin";
        String IS_SUBSCRIBED = "isSubscribed";
        String USER_REGISTRATION_TYPE = "userSubscriptionStatus";

        String FREE_SUBSCRIPTION_DATE = "freeSubscriptionDaysCount";
        String FREE_SUBSCRIPTION_DAYS_MESSAGE = "freeSubscriptionEndMessage";
        String FREE_TRIAL_END_TEXT = "freeTrialEndText_%s";
        String SUBSCRIPTION_END_TEXT = "subscriptionEndText_%s";
        String SUBSCRIPTION_HEADER_TEXT = "subscriptionHeaderText_%s";
        String FREE_SUBSCRIPTION_ACTIVE = "freeSubscriptionActive";
        //        String FREE_SUBSCRIPTION_ACTIVE = "freeSubscriptionActive_%s";
        String FREE_SUBSCRIPTION_HEADER_TITLE = "subscriptionHeaderTitle_%s";
        String FREE_SUBSCRIPTION_HEADER_DESC = "subscriptionHeaderDesc_%s";
        String VIDEO_HELP_URL = "videoHelpUrl_%s";
        String VIDEO_HELP_TITLE = "videoHelpTitle_%s";

        String IS_EMAIL_VERIFIED = "isEmailVerified";
        String IS_MOBILE_VERIFIED = "isMobileVerified";
        String IS_DOMAIN_USER_VERIFIED = "isDomainUser";
        String APP_INSTALL_HEADER = "appInstallHeader_%s";
        String APP_INSTALL_LINK = "appInstallLink_%s";
        String APP_INSTALL_IMG = "appInstallImg_%s";
        String SUBSCRIPTION_END_DATE = "subscriptionEndText_%s";
    }

    public interface UserInfo {
        String DB_ID = "dbId";
        String DEVICE_ID = "deviceId";
        String USER_ID = "userId";
        String NAME = "name";
        String EMAIL = "email";
        String MOBILE = "mobile";
        String CITY = "city";
        String COUNTRY = "country";
        String PASSWORD = "password";
        String LOGIN_TYPE = "loginType";
        String PROFILE_PIC = "profilePic";
        String SIGN_UP_ID = "signUpId";
    }

    //update popup constants
    public interface AppUpdate {
        String APP_UPDATE_POP_UP_SHOWN_DATE = "updatePopupShownDate";
        String APP_UPDATE_SESSION_COUNT = "updatePopupSessionCount";
        String APP_CURRENT_VERSION = "appCurrentVersion";
        String UPDATE_POPUP_VERSION_CODE = "update_popup_version";
        String FORCE_UPDATE_VERSION = "force_update_version";
        String UPDATE_POPUP_MESSAGE = "force_update_message";
        String APP_RATEUS_SESSION_COUNT = "rateUsPopupSessionCount";
        String APP_LATEST_VERSION = "appLatestVersion";
        String IS_APP_RATED = "isAppRated";
    }

    public interface GdprConst {
        String IS_BLOCKED_COUNTRY = "isBlockedCountry";
        String GDPR_CONSENT_ACCEPTED = "gdprConsentAccepted";
        String GDPR_CONSENT_CHECKED = "gdprConsentChecked";

        String IS_FAIL_VERSION = "isFailVersion";
    }

    public interface CommonPref {
        String APP_SOURCE_HIT = "appSourceHit";
        String IS_TRIAL_ON = "isTrial";
        String MOBILE_VERIFY_DAY = "mobileVerify";
        String USER_ALERT_TITLE = "UserAlertTitle";
        String USER_ALERT_MSG = "UserAlertMsg";
        String USER_ALERT_SHOWN = "USerAlertShown";
        String IS_AD_ACTIVE = "isAdActive";
        String LAST_USED_DAY = "lastDayUsed";
        String CHANNEL_SLNO = "ChannelSlno";
        String CACHE_VALUE = "value";
    }

    public interface ContactInfo {
        String ADDRESS_1 = "address1";
        String ADDRESS_2 = "address2";
        String FEEDBACK = "feedback";
        String SEND_EMAIL = "sendemail";
    }

    public interface Urls {
        String SUBS_INFO = "subInfoUrl";
        String SUBS_HISTORY = "subsHistoryUrl";
        String IS_USER_SUBSCRIBED = "isUserSubscribedUrl";
    }
}
