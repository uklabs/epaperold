package com.bhaskar.epaper.epaperv2.profile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.interfaces.OnProfileFragmentListener;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.utils.EpaperPrefernces;

public class FragmentProfileView extends Fragment implements View.OnClickListener {

    private OnProfileFragmentListener listener;
    private TextView profileMobileTitleTTV;
    private TextView profileMobileValueTTV;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (OnProfileFragmentListener) context;
        } catch (ClassCastException e) {
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile_view, container, false);

        EpaperPrefernces mPref = EpaperPrefernces.getInstance(getActivity());

        profileMobileTitleTTV = v.findViewById(R.id.profile_mobile_txt);
        profileMobileValueTTV = v.findViewById(R.id.profile_mobile_value_txt);

//        ((TextView) v.findViewById(R.id.txt_profile_city)).setText(mPref.getStringValue(QuickPreferences.UserInfo.CITY, "Not Available"));
//        ((TextView) v.findViewById(R.id.txt_profile_country)).setText(mPref.getStringValue(QuickPreferences.UserInfo.COUNTRY, "Not Available"));
        ((TextView) v.findViewById(R.id.txt_profile_email)).setText(mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""));
        String mobileNo = mPref.getStringValue(QuickPreferences.UserInfo.MOBILE, " ");
        if (!TextUtils.isEmpty(mobileNo)) {
            profileMobileValueTTV.setText(mobileNo);
        } else {
            profileMobileValueTTV.setText(" ");
        }
        ((TextView) v.findViewById(R.id.txt_profile_name)).setText(mPref.getStringValue(QuickPreferences.UserInfo.NAME, ""));

        if (Constants.COUNTRY_IS_INDIA) {
            profileMobileTitleTTV.setVisibility(View.VISIBLE);
            profileMobileValueTTV.setVisibility(View.VISIBLE);
        } else {
            profileMobileTitleTTV.setVisibility(View.GONE);
            profileMobileValueTTV.setVisibility(View.GONE);
        }

        ((TextView) v.findViewById(R.id.prfile_title)).setText(getResources().getString(R.string.welcome_message, mPref.getStringValue(QuickPreferences.UserInfo.NAME, "")));

        (v.findViewById(R.id.txt_profile_edit)).setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.txt_profile_edit: {
                if (listener != null) {
                    listener.onProfileChange(false);
                }
                break;
            }
        }
    }
}

