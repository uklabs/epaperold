package com.bhaskar.epaper.epaperv2.payment;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class PaymentSubscriptionBean {

    String status, code;
    Data data = new Data();

    public PaymentSubscriptionBean() {
    }

    public String getStatus() {
        return status;
    }


    public String getCode() {
        return code;
    }


    public Data getData() {
        return data;
    }


    public static class Data {

        String subcription_desc, show_contact_us;
        List<Plans> plans = new ArrayList<>();

        public Data() {
        }

        public String getSubcription_desc() {
            return subcription_desc;
        }


        public String getShow_contact_us() {
            return show_contact_us;
        }


        public List<Plans> getPlans() {
            return plans;
        }


        public static class Plans implements Parcelable {

            int type;
            String id, top_label, plan_rs, plan_days, plan_days_label, discount, offer_name, plan_display_rs;
            PlanDesc plan_desc = new PlanDesc();

            public Plans(int type) {
                this.type = type;
            }


            public String getId() {
                return id;
            }

            public String getTop_label() {
                return top_label;
            }


            public String getPlan_rs() {
                return plan_rs;
            }


            public String getPlan_days() {
                return plan_days;
            }


            public String getPlan_days_label() {
                return plan_days_label;
            }

            public String getDiscount() {
                return discount;
            }

            public String getOffer_name() {
                return offer_name;
            }

            public String getPlanDisplayRs() {
                return plan_display_rs;
            }


            public PlanDesc getPlan_desc() {
                return plan_desc;
            }


            public static class PlanDesc implements Parcelable {


                String list[] = new String[0];
                String desc;

                public PlanDesc() {
                }

                public String[] getList() {
                    return list;
                }

                public String getDesc() {
                    return desc;
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeStringArray(this.list);
                    dest.writeString(this.desc);
                }

                protected PlanDesc(Parcel in) {
                    this.list = in.createStringArray();
                    this.desc = in.readString();
                }

                public static final Creator<PlanDesc> CREATOR = new Creator<PlanDesc>() {
                    @Override
                    public PlanDesc createFromParcel(Parcel source) {
                        return new PlanDesc(source);
                    }

                    @Override
                    public PlanDesc[] newArray(int size) {
                        return new PlanDesc[size];
                    }
                };
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(this.id);
                dest.writeString(this.top_label);
                dest.writeString(this.plan_rs);
                dest.writeString(this.plan_days);
                dest.writeString(this.plan_days_label);
                dest.writeString(this.discount);
                dest.writeString(this.offer_name);
                dest.writeParcelable(this.plan_desc, flags);
                dest.writeString(this.plan_display_rs);
            }

            protected Plans(Parcel in) {
                this.id = in.readString();
                this.top_label = in.readString();
                this.plan_rs = in.readString();
                this.plan_days = in.readString();
                this.plan_days_label = in.readString();
                this.discount = in.readString();
                this.offer_name = in.readString();
                this.plan_desc = in.readParcelable(PlanDesc.class.getClassLoader());
                this.plan_display_rs = in.readString();
            }

            public static final Parcelable.Creator<Plans> CREATOR = new Parcelable.Creator<Plans>() {
                @Override
                public Plans createFromParcel(Parcel source) {
                    return new Plans(source);
                }

                @Override
                public Plans[] newArray(int size) {
                    return new Plans[size];
                }
            };
        }

    }

}
