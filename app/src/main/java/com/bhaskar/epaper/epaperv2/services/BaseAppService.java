package com.bhaskar.epaper.epaperv2.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.RequestHandler;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.utils.EpaperPrefernces;

import java.util.Timer;
import java.util.TimerTask;

public class BaseAppService extends Service {

    private int count = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        count = 0;
        checkForCondition();

        return START_NOT_STICKY;
    }

    private void startTimer() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                checkForCondition();
            }
        }, 30000);
    }

    private void checkForCondition() {
        Systr.println("BASE SERVICE : check for condition");
        if (count > 6 || EpaperPrefernces.getInstance(this).getBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, false)) {
            Systr.println("BASE SERVICE : check for condition stop service");
            stopSelf();
        } else {
            count++;
            Systr.println("BASE SERVICE : check for condition called : " + count);
            RequestHandler.requestForIsUserSubscribed(this, new RequestHandler.OnRequestListener() {
                @Override
                public void onCompleteRequest() {
                    Systr.println("BASE SERVICE : check for condition start timer");

                    if (count > 6 || EpaperPrefernces.getInstance(BaseAppService.this).getBooleanValue(QuickPreferences.PaymentPref.IS_EMAIL_VERIFIED, false)) {
                        Systr.println("BASE SERVICE : check for condition stop service");
                        stopSelf();
                    } else {
                        startTimer();
                    }
                }
            });
        }
    }
}
