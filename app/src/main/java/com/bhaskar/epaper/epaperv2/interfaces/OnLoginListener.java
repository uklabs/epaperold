package com.bhaskar.epaper.epaperv2.interfaces;

public interface OnLoginListener {
    void onGoogleClick();

    void onFBClick();

    void onLogin(String email, String password);

}
