package com.bhaskar.epaper.epaperv2.payment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.login.LoginRegistrationActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.ui.MasterActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.Urls;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bhaskar.epaper.ui.DetailSwipeActivity.SUBSCRIBE_REQUEST_CODE;

public class PaymentPlanActivity extends BaseActivity implements PaymentPlanAdapter.OnPaymentPlanListener, View.OnClickListener {

    private static final int BILLING_REQUEST_CODE = 201;
    public final static int SUSCRS_BUY_LOGIN_REQUEST_CODE = 202;
    public final static int SUSCRS_HEAD_REG_REQUEST_CODE = 203;
    public final static int SUSCRS_HEAD_LOGIN_REQUEST_CODE = 204;

    private PaymentPlanAdapter mAdapter;
    private EpaperPrefernces mPref;
    private AVLoadingIndicatorView progressBar;
    private boolean mIsLoggedIn;
    private String selectedPlanId;
    private Dialog trialEndDialog;
    private EpaperPrefernces pref;

    private void init() {
        //setToolbar();
        pref = EpaperPrefernces.getInstance(this);
        ImageView backIview = findViewById(R.id.backIview);
        backIview.setOnClickListener(this);
        setSubscriptionView();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_plan);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        progressBar = findViewById(R.id.progress);
        PaymentPlanActivity mActivity = this;
        mPref = EpaperPrefernces.getInstance(mActivity);
        mIsLoggedIn = mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false);
        init();
        String gaLabel = getIntent().getStringExtra("GA_Label");

        int regType = mPref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        boolean isSubscribed = mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false);
        if (!isSubscribed && (regType == Constants.SubscriptionStatus.PAID_USER
                || regType == Constants.SubscriptionStatus.TRIAL_USER)) {
            Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION, GAConst.Action.VIEW, GAConst.Label.SUBSCRIPTION_END);
        } else {
            Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION, GAConst.Action.VIEW, gaLabel);
        }
        Epaper.getInstance().trackScreenView(PaymentPlanActivity.this, GAConst.Screen.SUBSCRIPTION_PLAN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SUSCRS_BUY_LOGIN_REQUEST_CODE
                || requestCode == SUSCRS_HEAD_LOGIN_REQUEST_CODE
                || requestCode == SUSCRS_HEAD_REG_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                mIsLoggedIn = mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false);
                int regType = mPref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
                if (requestCode == SUSCRS_HEAD_REG_REQUEST_CODE
                        || requestCode == SUSCRS_HEAD_LOGIN_REQUEST_CODE) {
                    afterLoginRegs(regType);
                } else {
                    if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)
                            && (regType == Constants.SubscriptionStatus.DOMAIN_USER
                            || regType == Constants.SubscriptionStatus.PAID_USER)) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        launchBilling(selectedPlanId);
                    }
                }
            }
        } else if (requestCode == BILLING_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            } else if (mIsLoggedIn) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void afterLoginRegs(int regType) {
        if ((regType == Constants.SubscriptionStatus.TRIAL_USER
                && mPref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false))

                || (regType != Constants.SubscriptionStatus.TRIAL_USER
                && mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false))) {
            setResult(RESULT_OK);
            finish();
        } else {
            if (mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false)) {
                if (mAdapter != null && Constants.isUserComeFromRegistration) {
                    mAdapter.trialActivated(true);
                } else {
                    setResult(RESULT_OK);
                    finish();
                }
            } else {
                if (mAdapter != null) {
                    mAdapter.trialActivated(false);
                }
            }
        }
    }

   /* private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleBack();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Subscription Packs");
    }*/

    private void showDialog() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideDialog() {
        progressBar.setVisibility(View.GONE);
    }

    private void setSubscriptionView() {
        mAdapter = new PaymentPlanAdapter(this, this);
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
//        String channelSno ="";//Epaper.getInstance().getChannelSlno().equalsIgnoreCase("")
        if (Epaper.getInstance().getChannelSlno().equalsIgnoreCase(""))
            showTrialEndDialog();
        else
        getSubscriptionPackage();
    }

    private void getSubscriptionPackage() {
        showDialog();
        JSONObject jsonObject = new JSONObject();
        try {
            /*new*/
            jsonObject.put("pmt_gateway", Constants.PAYMENT_GATEWAY);
            jsonObject.put("isIndia", (Constants.COUNTRY_IS_INDIA ? "1" : "0"));
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            jsonObject.put("user_id", EpaperPrefernces.getInstance(PaymentPlanActivity.this).getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
        } catch (JSONException ignored) {
        }
        //String url = String.format(Urls.SUBSCRIPTION_PLAN_URL, Constants.PAYMENT_GATEWAY, (Constants.COUNTRY_IS_INDIA ? "1" : "0"), Epaper.getInstance().getChannelSlno());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Urls.SUBSCRIPTION_PLAN_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {
                            if (!TextUtils.isEmpty(response.toString())) {
                                Epaper.getInstance().getModelManager().setmPaymentSubscriptionBean(new Gson().fromJson(response.toString(), PaymentSubscriptionBean.class));
                                parseSubscriptionPlans();
                            }
                        } catch (Exception ignored) {
                        }
                    }
                }, new com.android.volley.Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("api-key", Constants.API_KEY_VALUE);
                headers.put("User-Agent", Constants.USER_AGENT);
                headers.put("encrypt", "0");
                headers.put("user-id", mPref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
                headers.put("app-version", Util.getAppVersion(PaymentPlanActivity.this));
                headers.put("host-id", Constants.HOST_ID);
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(12000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(PaymentPlanActivity.this).addToRequestQueue(jsonObjReq);
    }

    private void parseSubscriptionPlans() {
        if (Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean() != null) {
            if (Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean().getCode().equals("200")) {
                if (Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean().getData() != null) {
                    if (Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean().getData().getPlans() != null) {
                        List<PaymentSubscriptionBean.Data.Plans> plans = Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean().getData().getPlans();
                        boolean isContactVisible = Epaper.getInstance().getModelManager().getmPaymentSubscriptionBean().getData().show_contact_us.equals("1");
                        mAdapter.setData(plans, isContactVisible);
                    }
                }
            }
        }
    }

    @Override
    public void onClickBuy(PaymentSubscriptionBean.Data.Plans info) {
        mIsLoggedIn = mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_LOGIN, false);
        selectedPlanId = info.id;
        Systr.println("ON Click Buy : " + selectedPlanId);
        if (mIsLoggedIn) {
            launchBilling(selectedPlanId);
        } else {
            Intent intent1 = new Intent(PaymentPlanActivity.this, LoginRegistrationActivity.class);
            intent1.putExtra("IS_LOGIN", false);
            intent1.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.SUBSCRIPTION_PAGE);
            startActivityForResult(intent1, SUSCRS_BUY_LOGIN_REQUEST_CODE);
        }
    }

    @Override
    public void onClickLogin(boolean isLogin) {
        Intent intent = new Intent(PaymentPlanActivity.this, LoginRegistrationActivity.class);
        intent.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.SUBSCRIPTION_PAGE);
        intent.putExtra("IS_LOGIN", isLogin);
        startActivityForResult(intent, (isLogin) ? SUSCRS_HEAD_LOGIN_REQUEST_CODE : SUSCRS_HEAD_REG_REQUEST_CODE);
    }


    private void launchBilling(String selectedPlanId) {
        Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION, GAConst.Action.CLICK, "pack_" + selectedPlanId);

        Intent intent = new Intent(PaymentPlanActivity.this, BillingInfoActivity.class);
        intent.putExtra(BillingInfoActivity.EXTRA_ID, selectedPlanId);
        startActivityForResult(intent, BILLING_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    private void handleBack() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.backIview) {
            handleBack();
        }
    }

    //    private void intiView() {
//        txtSubscrLoginCheck = findViewById(R.id.txt_subscr_login_check);
//
//
//        /*register now clickable*/
//        String signInString = "Sign in";
//        String dontHaveAccountText = getString(R.string.already_a_subscribed_sign_in, signInString);
//        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(Html.fromHtml(dontHaveAccountText));
//        spannableStringBuilder2.setSpan(new MySpannable(PaymentPlanActivity.this) {
//            @Override
//            public void onClick(View widget) {
//                Intent intent = new Intent(mActivity, LoginRegistrationActivity.class);
//                intent.putExtra(LoginRegistrationActivity.GA_ACTION, GAConst.Action.SUBSCRIPTION_PAGE);
//                startActivityForResult(intent, SUSCRS_REQUEST_CODE);
//            }
//        }, Html.fromHtml(dontHaveAccountText).toString().indexOf(signInString), Html.fromHtml(dontHaveAccountText).toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        txtSubscrLoginCheck.setMovementMethod(LinkMovementMethod.getInstance());
//        txtSubscrLoginCheck.setText(spannableStringBuilder2, TextView.BufferType.SPANNABLE);
//
//        txtSubscrLoginCheck.setVisibility(View.VISIBLE);
//    }

    /*public class MySpannable extends ClickableSpan {
        private Context mContext;

        public MySpannable(Context context) {
            this.mContext = context;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.md_white_1000));
        }

        @Override
        public void onClick(View widget) {

        }
    }*/

    boolean epaperChoosed = false;
    private void showTrialEndDialog() {
        trialEndDialog = new Dialog(PaymentPlanActivity.this);
        trialEndDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        trialEndDialog.setContentView(R.layout.subscription_and_trial_end);
        trialEndDialog.setCancelable(false);

        TextView txtTrialEndsMsg = trialEndDialog.findViewById(R.id.txt_trial_ends_msg);
        TextView btnBuySubscriptionHindi = trialEndDialog.findViewById(R.id.buy_subscription_hindi);
        TextView btnBuySubscriptionGujrati = trialEndDialog.findViewById(R.id.buy_subscription_gujrati);
        TextView btnLogin = trialEndDialog.findViewById(R.id.not_now);
        final ImageView iv_dainik_epaper = trialEndDialog.findViewById(R.id.iv_dainik_epaper);
        final ImageView iv_divya_epaper = trialEndDialog.findViewById(R.id.iv_divya_epaper);
        // text come from back end in appcontrol
        String trialEndsDaysMessage = pref.getStringValue(QuickPreferences.PaymentPref.FREE_SUBSCRIPTION_DAYS_MESSAGE, "");
        txtTrialEndsMsg.setText("Choose Epaper");
        LinearLayout dainik_bhaskar_epaper = trialEndDialog.findViewById(R.id.dainik_bhaskar_epaper);
        LinearLayout divya_bhaskar_epaper = trialEndDialog.findViewById(R.id.divya_bhaskar_epaper);
        TextView buy_subscription = trialEndDialog.findViewById(R.id.buy_subscription);

        buy_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (epaperChoosed) {
                    Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                    startActivityForResult(new Intent(PaymentPlanActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                    trialEndDialog.dismiss();
                    getSubscriptionPackage();
                } else {
                    Toast.makeText(PaymentPlanActivity.this, "Please Choose Epaper", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dainik_bhaskar_epaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = true;
                Epaper.getInstance().setChannelSlno(Constants.DAINIK_BHASKAR);
                iv_dainik_epaper.setImageResource(R.drawable.ic_radio_checked);
                iv_divya_epaper.setImageResource(R.drawable.ic_radio_unchecked);
            }
        });
        divya_bhaskar_epaper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = true;
                Epaper.getInstance().setChannelSlno(Constants.DIVYA_BHASKAR);
                iv_dainik_epaper.setImageResource(R.drawable.ic_radio_unchecked);
                iv_divya_epaper.setImageResource(R.drawable.ic_radio_checked);
            }
        });
        btnBuySubscriptionHindi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Epaper.getInstance().setChannelSlno(Constants.DAINIK_BHASKAR);
                Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                startActivityForResult(new Intent(PaymentPlanActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                trialEndDialog.dismiss();
            }
        });
        btnBuySubscriptionGujrati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Epaper.getInstance().setChannelSlno(Constants.DIVYA_BHASKAR);
                Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.CLICK, GAConst.Label.BUY_NOW);
                startActivityForResult(new Intent(PaymentPlanActivity.this, PaymentPlanActivity.class), SUBSCRIBE_REQUEST_CODE);
                trialEndDialog.dismiss();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                epaperChoosed = false;
                trialEndDialog.dismiss();
                startActivity(new Intent(PaymentPlanActivity.this,MasterActivity.class));
            }
        });
        trialEndDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    epaperChoosed = false;
                    trialEndDialog.dismiss();
                }
                return true;
            }
        });
        trialEndDialog.show();
        int regType = pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        Epaper.getInstance().trackEvent(PaymentPlanActivity.this, GAConst.Category.SUBSCRIPTION_POPUP, GAConst.Action.VIEW + "_" + regType, GAConst.Label.IMPRESSION);
        Constants.isTrialEndsDialogShown = false;
    }
}
