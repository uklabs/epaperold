package com.bhaskar.epaper.epaperv2.interfaces;

public interface OnSetHomePageListener {
    void removeHomeState();

    void changeHomeState(String stateName);

    void showProgress();

    void hideProgress();
}
