package com.bhaskar.epaper.epaperv2.interfaces;

public interface OnSavePdfListener {
    void saveAsPdf(int position);
}
