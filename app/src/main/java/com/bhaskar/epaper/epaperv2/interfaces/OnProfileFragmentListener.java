package com.bhaskar.epaper.epaperv2.interfaces;

public interface OnProfileFragmentListener {
    void onProfileChange(boolean isEdit);
}
