package com.bhaskar.epaper.epaperv2.util;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpacesDecoration extends RecyclerView.ItemDecoration {

    private final int mLeft;
    private final int mRight;
    private final int mTop;
    private final int mBottom;

    public SpacesDecoration(int left, int right, int top, int bottom) {
        this.mLeft = left;
        this.mRight = right;
        this.mTop = top;
        this.mBottom = bottom;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = mLeft;
        outRect.right = mRight;
        outRect.top = mTop;
        outRect.bottom = mBottom;
    }
}
