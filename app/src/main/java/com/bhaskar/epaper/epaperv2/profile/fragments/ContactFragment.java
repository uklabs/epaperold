package com.bhaskar.epaper.epaperv2.profile.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.utils.EpaperPrefernces;

import org.json.JSONException;
import org.json.JSONObject;

public class ContactFragment extends Fragment {
    private View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_contact, container, false);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        parseData(getContext());
    }

    private void parseData(Context context) {
        String emailJson = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.SEND_EMAIL, "");
        String feedbackJson = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.FEEDBACK, "");
        String address1Json = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.ADDRESS_1, "");
        String address2Json = EpaperPrefernces.getInstance(context).getStringValue(QuickPreferences.ContactInfo.ADDRESS_2, "");

        // Email
        try {
            JSONObject jsonObject = new JSONObject(emailJson);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                view.findViewById(R.id.email_ll).setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.email_title_tv)).setText(Html.fromHtml(jsonObject.optString("title")));

                String desc = jsonObject.optString("desc");
                TextView tv = view.findViewById(R.id.email_desc_tv);
                tv.setMovementMethod(Util.privacyMovementMethod(context, 1));
                tv.setText(Html.fromHtml(desc));
            } else {
                view.findViewById(R.id.email_ll).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Feedback
        try {
            JSONObject jsonObject = new JSONObject(feedbackJson);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                view.findViewById(R.id.feedback_ll).setVisibility(View.VISIBLE);

                String desc = jsonObject.optString("desc");
                TextView tv = (TextView) view.findViewById(R.id.feedback_desc_tv);
                tv.setMovementMethod(Util.privacyMovementMethod(context, 2));
                tv.setText(Html.fromHtml(desc));
            } else {
                view.findViewById(R.id.feedback_ll).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Address 1
        try {
            JSONObject jsonObject = new JSONObject(address1Json);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                view.findViewById(R.id.address_1_ll).setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.address_1_title_tv)).setText(Html.fromHtml(jsonObject.optString("title")));
                ((TextView) view.findViewById(R.id.address_1_desc_tv)).setText(Html.fromHtml(jsonObject.optString("desc")));
            } else {
                view.findViewById(R.id.address_1_ll).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Address 2
        try {
            JSONObject jsonObject = new JSONObject(address2Json);
            if (jsonObject.optString("is_active").equalsIgnoreCase("1")) {
                view.findViewById(R.id.address_2_ll).setVisibility(View.VISIBLE);
                ((TextView) view.findViewById(R.id.address_2_title_tv)).setText(Html.fromHtml(jsonObject.optString("title")));
                ((TextView) view.findViewById(R.id.address_2_desc_tv)).setText(Html.fromHtml(jsonObject.optString("desc")));
            } else {
                view.findViewById(R.id.address_2_ll).setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
