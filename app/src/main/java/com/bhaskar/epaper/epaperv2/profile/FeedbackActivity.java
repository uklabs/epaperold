package com.bhaskar.epaper.epaperv2.profile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackActivity extends BaseActivity {

    private Spinner spnTopics;
    private EditText edtFeedback;
    private EditText edtFullname;
    private EditText edtMobile;
    private EditText edtEmail;

    private ProgressDialog pd;
    private String topic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        setToolbar();

        spnTopics = findViewById(R.id.spn_topics);
        edtFeedback = findViewById(R.id.edt_feedback);
        edtFullname = findViewById(R.id.edt_fullname);
        edtMobile = findViewById(R.id.edt_mobile);
        edtEmail = findViewById(R.id.edt_email);


        initTopicSpinner();

        findViewById(R.id.submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isConnectingToInternet(FeedbackActivity.this)) {
                    if (checkValidation()) {
                        sendFeedbackToServer(edtFeedback.getText().toString(), edtFullname.getText().toString(), "", edtEmail.getText().toString());
                    }
                } else {
                    Utility.setNetworkErrorToast(FeedbackActivity.this);
                }
            }
        });
    }


    private void sendFeedbackToServer(String feedback, String fullName, String mobileNumber, String email) {
        setProgressbar();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("topic", topic);
            jsonObject.put("feedback_desc", feedback);
            jsonObject.put("name", fullName);
//            jsonObject.put("mobileno", mobileNumber);
            jsonObject.put("email", email);
            jsonObject.put("host_id", Constants.HOST_ID);
        } catch (JSONException e) {
        }

        Systr.println("Signup send json : " + jsonObject.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Urls.FEED_BACK_URL, jsonObject, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Systr.println("Signup response : " + response.toString());
                if (pd != null) {
                    pd.dismiss();
                }
                try {
                    if (response.optString("status").equalsIgnoreCase("Success")) {
                        finish();
                    }
                    Utility.setToast(FeedbackActivity.this, response.optString("msg"));

                } catch (Exception e) {
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Signup Error : " + error);
                if (pd != null) {
                    pd.dismiss();
                }
                Utility.setNetworkErrorToast(FeedbackActivity.this);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(FeedbackActivity.this).addToRequestQueue(request);
    }

    private boolean checkValidation() {
        if (spnTopics.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please select topic", Toast.LENGTH_LONG).show();
            spnTopics.performClick();
            return false;
        } else if (TextUtils.isEmpty(edtFeedback.getText().toString().trim())) {
            edtFeedback.setError("Please enter your concern");
            return false;
        } else if (TextUtils.isEmpty(edtFullname.getText().toString().trim())) {
            edtFullname.setError("Please enter full name");
            return false;
        }/* else if (TextUtils.isEmpty(edtMobile.getText().toString().trim())) {
            edtMobile.setError("Please enter mobile number");
            return false;
        } else if (edtMobile.getText().toString().length() < 10) {
            edtMobile.setError(getString(R.string.password_validation_message));
            return false;
        }*/ else if (TextUtils.isEmpty(edtEmail.getText().toString().trim())) {
            edtEmail.setError("Please enter email");
            return false;
        } else if (!Util.isEmailValid(edtEmail.getText().toString())) {
            edtEmail.setError("Please enter valid email");
            return false;
        }
        return true;
    }

    private void setProgressbar() {
        pd = new ProgressDialog(FeedbackActivity.this);
        pd.setMessage("Please Wait");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FeedbackActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");
    }

    public void initTopicSpinner() {
        if (!(Constants.feedbackTopics != null && Constants.feedbackTopics.length > 0)) {
            Constants.feedbackTopics = getResources().getStringArray(R.array.topics);
        }
        spnTopics.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                topic = Constants.feedbackTopics[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter<String> aa = new ArrayAdapter<String>(FeedbackActivity.this, android.R.layout.simple_spinner_item, Constants.feedbackTopics) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == 0)
                    ((TextView) v).setTextColor(getResources().getColorStateList(R.color.md_grey_400));
                return v;
            }
        };
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spnTopics.setAdapter(aa);
        spnTopics.setSelection(0);
    }
}
