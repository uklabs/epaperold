package com.bhaskar.epaper.epaperv2.payment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.AvenuesParams;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.payment.utility.ServiceUtility;
import com.bhaskar.epaper.epaperv2.util.ProgressbarDialog;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Systr;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.epaperv2.util.VolleyNetworkSingleton;
import com.bhaskar.epaper.ui.BaseActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.bhaskar.epaper.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BillingInfoActivity extends BaseActivity {

    private static final int BILLPAY_REQUEST_CODE = 1012;
    public static final String EXTRA_ID = "id";

    private ProgressbarDialog progressbarDialog;

    private String orderId;
    private String title;
    private String desc;
    private String price;
    private String discount;
    private String subtotal;
    private String taxes, taxes2;
    private String grandTotal;
    private String currency = "INR";

    private String priceTitle;
    private String discountTitle;
    private String subtotalTitle;
    private String taxesTitle;
    private String grandTotalTitle;

    private String subsId;
    private String userId;
    private JSONArray descJsonArray;
    private Spinner stateSpinner;
    private RelativeLayout stateLayout;
    private Spinner countrySpinner;

    private EditText stateET;
    private EditText nameET;
    private EditText mobileET;
    private EditText addressET;
    private EditText zipCodeET;
    private EditText cityET;

    private TextView fullNameErrorTv;
    private TextView mobileNumberErrorTv;
    private TextView addressErrorTv;
    private TextView zipCodeErrorTv;
    private TextView cityErrorTv;
    private TextView countryErrorTv;
    private TextView stateErrorTv;
    private EpaperPrefernces pref;
    private Epaper mApp;
    private boolean checkClick=false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_info);
        mApp = Epaper.getInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkClick){
                    findViewById(R.id.user_input_layout).setVisibility(View.VISIBLE);
                    findViewById(R.id.billing_layout).setVisibility(View.GONE);
                    checkClick=false;
                }
                else
                    BillingInfoActivity.this.finish();
            }
        });

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Required Details");

        Intent intent = getIntent();
        if (intent != null) {
            subsId = intent.getStringExtra(EXTRA_ID);
        }
        pref = EpaperPrefernces.getInstance(BillingInfoActivity.this);
        userId = pref.getStringValue(QuickPreferences.UserInfo.USER_ID, "");
        progressbarDialog = new ProgressbarDialog(this);

        findViewById(R.id.paynow_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateOrderId();

            }
        });
        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isConnectingToInternet(BillingInfoActivity.this)) {
                    if (checkValidation()) {
                        fetchDataFromServer(BillingInfoActivity.this, subsId);
                        checkClick = true;
                    }
                } else {
                    Utility.setNetworkErrorToast(BillingInfoActivity.this);
                }

            }
        });

        fullNameErrorTv = findViewById(R.id.full_name_error_tv);
        mobileNumberErrorTv = findViewById(R.id.mobile_number_error_tv);
        addressErrorTv = findViewById(R.id.address_error_tv);
        zipCodeErrorTv = findViewById(R.id.zip_code_error_tv);
        cityErrorTv = findViewById(R.id.city_error_tv);
        countryErrorTv = findViewById(R.id.country_error_tv);

        stateLayout = findViewById(R.id.state_layout);
        stateSpinner = findViewById(R.id.state_spinner);
        stateET = findViewById(R.id.state_et);
        stateErrorTv = findViewById(R.id.state_error_tv);
        countrySpinner = findViewById(R.id.country_spinner);
        nameET = findViewById(R.id.name_et);
        mobileET = findViewById(R.id.mobile_et);
        addressET = findViewById(R.id.address_et);
        zipCodeET = findViewById(R.id.zip_code_et);
        cityET = findViewById(R.id.city_et);

        ((EditText) findViewById(R.id.email_et)).setText(pref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""));
        nameET.setText(pref.getStringValue(QuickPreferences.UserInfo.NAME, ""));
        if (Constants.COUNTRY_IS_INDIA) {
            mobileET.setText(pref.getStringValue(QuickPreferences.UserInfo.MOBILE, ""));
        }

        setCountrySpinnerData();
    }

    boolean isIndia;
    boolean isUP;
    String fullName, number, zipCode, city, country, state, stateCode, address;

    private boolean checkValidation() {
        boolean returnValue;
        fullName = nameET.getText().toString();
        number = mobileET.getText().toString();
        address = addressET.getText().toString();
        zipCode = zipCodeET.getText().toString();
        city = cityET.getText().toString();
        ViewParent parentView = fullNameErrorTv.getParent();
        if (!isIndia)
            state = stateET.getText().toString();

        if (TextUtils.isEmpty(fullName) && fullName.length() < 10) {
            fullNameErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(fullNameErrorTv, fullNameErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(number)) {
            fullNameErrorTv.setVisibility(View.GONE);
            mobileNumberErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(mobileNumberErrorTv, mobileNumberErrorTv);
            returnValue = false;
        } else if ((isIndia && number.length() != 10) || (!isIndia && (number.length() < 5 || number.length() > 14))) {
            fullNameErrorTv.setVisibility(View.GONE);
            mobileNumberErrorTv.setVisibility(View.VISIBLE);
            if (isIndia) {
                mobileNumberErrorTv.setText(R.string.india_mobile_number_error_text);
            } else {
                mobileNumberErrorTv.setText(R.string.outside_india_mobile_number_error_text);
            }
            parentView.requestChildFocus(mobileNumberErrorTv, mobileNumberErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(address)) {
            mobileNumberErrorTv.setVisibility(View.GONE);
            addressErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(addressErrorTv, addressErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(zipCode)) {
            addressErrorTv.setVisibility(View.GONE);
            zipCodeErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(zipCodeErrorTv, zipCodeErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(city)) {
            zipCodeErrorTv.setVisibility(View.GONE);
            cityErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(cityErrorTv, cityErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(country)) {
            cityErrorTv.setVisibility(View.GONE);
            countryErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(countryErrorTv, countryErrorTv);
            returnValue = false;
        } else if (TextUtils.isEmpty(state)) {
            countryErrorTv.setVisibility(View.GONE);
            stateErrorTv.setVisibility(View.VISIBLE);
            parentView.requestChildFocus(stateErrorTv, stateErrorTv);
            if (!isIndia) {
                stateErrorTv.setText(R.string.state_error_text);
            }
            returnValue = false;
        } else {
            fullNameErrorTv.setVisibility(View.GONE);
            mobileNumberErrorTv.setVisibility(View.GONE);
            addressErrorTv.setVisibility(View.GONE);
            zipCodeErrorTv.setVisibility(View.GONE);
            cityErrorTv.setVisibility(View.GONE);
            countryErrorTv.setVisibility(View.GONE);
            stateErrorTv.setVisibility(View.GONE);
            returnValue = true;
        }
        return returnValue;
    }

    private String[] countryList;

    private void setCountrySpinnerData() {
        countryList = getResources().getStringArray(R.array.country_list);
        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(BillingInfoActivity.this,
                android.R.layout.simple_spinner_item, countryList);
        countrySpinner.setAdapter(countryAdapter);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position != 0) {
                    country = countryList[position];
                    isIndia = country.equalsIgnoreCase("India");
                    setStateSpinnerData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        int indiaIndex = getIndiaIndex();
        countrySpinner.setSelection((indiaIndex == -1) ? 0 : indiaIndex);
    }

    private void setStateSpinnerData() {
        if (!isIndia) {
            stateLayout.setVisibility(View.GONE);
            stateET.setVisibility(View.VISIBLE);
        } else {
            stateLayout.setVisibility(View.VISIBLE);
            stateET.setVisibility(View.GONE);
        }
        final String[] stateList = getResources().getStringArray(R.array.state_list);
        final String[] stateCodeList = getResources().getStringArray(R.array.state_gstcode_list);
        ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(BillingInfoActivity.this,
                android.R.layout.simple_spinner_item, stateList);
        stateSpinner.setAdapter(stateAdapter);
        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    state = stateList[i];
                    stateCode = stateCodeList[i];
                    isUP = stateCode.equalsIgnoreCase("9");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setView(String title, String desc, String price, String discount, String tax, String tax2, String subTotal, String grandTotal) {
        ((TextView) findViewById(R.id.summary_title_tv)).setText(title);

        if (TextUtils.isEmpty(desc)) {
            (findViewById(R.id.summary_desc_tv)).setVisibility(View.GONE);
        } else {
            (findViewById(R.id.summary_desc_tv)).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.summary_desc_tv)).setText(desc);
        }

        if (descJsonArray == null) {
            (findViewById(R.id.summary_desc_detail_tv)).setVisibility(View.GONE);
        } else {
            (findViewById(R.id.summary_desc_detail_tv)).setVisibility(View.VISIBLE);

            StringBuilder stringBuffer = new StringBuilder();
            for (int i = 0; i < descJsonArray.length(); i++) {
                stringBuffer.append(getString(R.string.tick_icon)).append(" ").append(descJsonArray.optString(i));
                if (i < descJsonArray.length() - 1)
                    stringBuffer.append("<br><br>");
            }
            ((TextView) findViewById(R.id.summary_desc_detail_tv)).setText(Html.fromHtml(stringBuffer.toString()));
        }


        ((TextView) findViewById(R.id.price_tv)).setText(String.format("%s %s", currency, price));
        ((TextView) findViewById(R.id.discount_tv)).setText(discount);
        ((TextView) findViewById(R.id.taxes_tv)).setText(String.format("%s %s", currency, tax));
        if (!TextUtils.isEmpty(tax2)) {
            findViewById(R.id.taxes2_tv).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.taxes2_tv)).setText(String.format("%s %s", currency, tax2));
        } else {
            findViewById(R.id.taxes2_tv).setVisibility(View.GONE);
        }
        ((TextView) findViewById(R.id.subtotal_tv)).setText(String.format("%s %s", currency, subTotal));
        ((TextView) findViewById(R.id.grand_total_tv)).setText(String.format("%s %s", currency, grandTotal));

        ((TextView) findViewById(R.id.price_title_tv)).setText(priceTitle);
        ((TextView) findViewById(R.id.discount_title_tv)).setText(discountTitle);
        ((TextView) findViewById(R.id.taxes_title_tv)).setText(taxesTitle);
        ((TextView) findViewById(R.id.subtotal_title_tv)).setText(subtotalTitle);
        ((TextView) findViewById(R.id.grand_total_title_tv)).setText(grandTotalTitle);
    }


    private void showLoginErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(BillingInfoActivity.this).create();
        alertDialog.setTitle("Sorry!");
        alertDialog.setMessage("Login is required.");
        alertDialog.setCancelable(false);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        alertDialog.show();
    }


    private void afterResponse() {
        findViewById(R.id.user_input_layout).setVisibility(View.GONE);
        findViewById(R.id.billing_layout).setVisibility(View.VISIBLE);
        checkClick=true;
    }

    private void showErrorDialog() {
        final AlertDialog alertDialog = new AlertDialog.Builder(BillingInfoActivity.this).create();
        alertDialog.setTitle("Error!");
        alertDialog.setMessage("Check your network connection and try again.");
        alertDialog.setCancelable(false);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "RETRY", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                fetchDataFromServer(BillingInfoActivity.this, subsId);
            }
        });

        alertDialog.show();
    }

    private void showOrderErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(BillingInfoActivity.this).create();
        alertDialog.setTitle("Error!");
        alertDialog.setMessage("Check your network connection and try again.");
        alertDialog.setCancelable(false);

        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "RETRY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                generateOrderId();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        if (!BillingInfoActivity.this.isFinishing()) {
            alertDialog.show();
        }

    }

    private void generateOrderId() {
        if (TextUtils.isEmpty(userId)) {
            showLoginErrorDialog();
        } else {
            generateOrderIdFromServer(BillingInfoActivity.this, userId, subsId, grandTotal);
        }
    }

    private void generateOrderIdFromServer(final Context context, final String userId, String subsId, final String grandTotal) {

        progressbarDialog.show(getResources().getString(R.string.please_wait), false);

        JSONObject jsonObject = new JSONObject();
        try {
            /*new*/
            jsonObject.put("name", fullName);
            jsonObject.put("mobile", number);
            jsonObject.put("address", address);
            jsonObject.put("zip_code", zipCode);
            jsonObject.put("city", city);
            jsonObject.put("country", country);
            jsonObject.put("state", state);
            jsonObject.put("state_code", stateCode);
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
            /*old */
            jsonObject.put("user_id", userId);
            jsonObject.put("subs_id", subsId);
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("country_type", (isIndia ? "1" : "0"));
            jsonObject.put("app_version", Util.getAppVersion(BillingInfoActivity.this));
        } catch (JSONException ignored) {
        }

        Systr.println("Order send json : " + jsonObject.toString());

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Urls.PAYMENT_PROCESS_ORDER_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Systr.println("Order response : " + response.toString());
                        progressbarDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("Success")) {
                                orderId = response.getString("order_id");

                                if (TextUtils.isEmpty(orderId)) {
                                    showOrderErrorDialog();
                                } else {
                                    launchPayment(orderId, grandTotal, currency);
                                }
                            } else {
                                showOrderErrorDialog();
                            }
                        } catch (Exception e) {
                            showOrderErrorDialog();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Systr.println("Order error : " + error);
                progressbarDialog.dismiss();
                showOrderErrorDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }

    private void launchPayment(String orderId, String amount, String currency) {
        // Amount/Currency/Access code/Merchant Id & RSA key Url

        Intent intent = new Intent(BillingInfoActivity.this, WebViewActivity2.class);
        intent.putExtra(AvenuesParams.ORDER_ID, orderId);
        intent.putExtra(AvenuesParams.CURRENCY, currency);
        intent.putExtra(AvenuesParams.AMOUNT, Urls.IS_TESTING ? "1" : amount);
//        intent.putExtra(AvenuesParams.AMOUNT, "1");

        intent.putExtra(AvenuesParams.ACCESS_CODE, Constants.ACCESS_CODE);
        intent.putExtra(AvenuesParams.MERCHANT_ID, Constants.MERCHANT_ID);
        intent.putExtra(AvenuesParams.RSA_KEY_URL, ServiceUtility.chkNull(Constants.RSA_KEY_URL).toString().trim());
        intent.putExtra(AvenuesParams.REDIRECT_URL, ServiceUtility.chkNull(Constants.REDIRECT_URL).toString().trim());
        intent.putExtra(AvenuesParams.CANCEL_URL, ServiceUtility.chkNull(Constants.CANCEL_URL).toString().trim());

        // Billing info
        intent.putExtra(AvenuesParams.BILLING_NAME, fullName);
        intent.putExtra(AvenuesParams.BILLING_ADDRESS, address);
        intent.putExtra(AvenuesParams.BILLING_CITY, city);
        intent.putExtra(AvenuesParams.BILLING_STATE, state);
        intent.putExtra(AvenuesParams.BILLING_COUNTRY, country);
        intent.putExtra(AvenuesParams.BILLING_ZIP, zipCode);
        intent.putExtra(AvenuesParams.BILLING_TEL, number);
        intent.putExtra(AvenuesParams.BILLING_EMAIL, ServiceUtility.chkNull(pref.getStringValue(QuickPreferences.UserInfo.EMAIL, "")).toString().trim());

        intent.putExtra(AvenuesParams.PACK_ID, subsId);

        startActivityForResult(intent, BILLPAY_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == BILLPAY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                //finish();
            }
        }
    }

    public int getIndiaIndex() {
        for (int index = 102; index < countryList.length; index++) {
            if (countryList[index].equalsIgnoreCase("India")) {
                return index;
            }
        }

        return -1;
    }

    static boolean active = false;

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    //API_ORDER_DETAIL
    private void fetchDataFromServer(final Context context, String subsId) {
        progressbarDialog.show(getResources().getString(R.string.please_wait), false);

        JSONObject jsonObject = new JSONObject();
        try {
            /*new*/
            jsonObject.put("host_id", Constants.HOST_ID);
            jsonObject.put("subs_id", subsId);
            jsonObject.put("country_type", (isIndia ? "1" : "0"));
            jsonObject.put("state_code", stateCode);
            jsonObject.put("channel_slno", Epaper.getInstance().getChannelSlno());
        } catch (JSONException ignored) {
        }
        // String url = String.format(Urls.PAYMENT_ORDER_URL, Constants.HOST_ID, subsId, (isIndia ? "1" : "0"), stateCode, Epaper.getInstance().getChannelSlno());
        Systr.println("Order send json : " + jsonObject.toString());
/*JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url,
                new Response.Listener<JSONObject>() {*/
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, Urls.PAYMENT_ORDER_URL, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        afterResponse();
                        progressbarDialog.dismiss();
                        try {
                            if (response.getString("status").equalsIgnoreCase("Success")) {
                                title = response.optString("title");

                                JSONObject descObject = response.optJSONObject("desc");
                                if (descObject != null) {
                                    desc = descObject.optString("desc");
                                    descJsonArray = descObject.optJSONArray("list");
                                }

                                price = response.optString("price");
                                discount = response.optString("discount");
                                subtotal = response.optString("subtotal");
                                taxes = response.optString("taxes");
                                taxes2 = response.optString("taxes2");
                                grandTotal = response.optString("total");
                                currency = response.optString("currency");

                                priceTitle = response.optString("price_title");
                                discountTitle = response.optString("discount_title");
                                subtotalTitle = response.optString("subtotal_title");
                                taxesTitle = response.optString("taxes_title");
                                grandTotalTitle = response.optString("total_title");

                                if (TextUtils.isEmpty(grandTotal)) {
                                    showErrorDialog();
                                } else {
                                    setView(title, desc, price, discount, taxes, taxes2, subtotal, grandTotal);
                                }

                            } else {
                                showErrorDialog();
                            }
                        } catch (Exception e) {
                            showErrorDialog();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressbarDialog.dismiss();
                showErrorDialog();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json charset=utf-8");
                headers.put("Accept", "application/json");
                headers.put("user-id", pref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
                headers.put("app-version", Util.getAppVersion(BillingInfoActivity.this));
                headers.put("host-id", Constants.HOST_ID);
                return headers;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(15000, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyNetworkSingleton.getInstance(context).addToRequestQueue(jsonObjReq);
    }



/* private void fetchDataFromServer(final Context context, String subsId) {
        try {

            final ProgressDialog pd = new ProgressDialog(BillingInfoActivity.this);
            pd.setMessage("Please Wait ...");
            pd.setCanceledOnTouchOutside(false);

            String countryType;
            String host_id = Constants.HOST_ID;
            String subs_id = subsId;
            if (isIndia)
                countryType = "1";
            else
                countryType = "0";

            String state_code = stateCode;
            String channel_slno = Epaper.getInstance().getChannelSlno();

            ApiInterface apiInterface = ApiClientConnection.getInstance().createApiInterface();
            Call<OrderDetialModel> call = apiInterface.orderDetailData(host_id, subs_id, countryType, state_code, channel_slno);
            call.enqueue(new retrofit2.Callback<OrderDetialModel>() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onResponse(Call<OrderDetialModel> call, retrofit2.Response<OrderDetialModel> response) {
                    pd.dismiss();
                    if (response.isSuccessful()) {
                        afterResponse();
                        OrderDetialModel orderDetialModel = response.body();
                        Log.e("orderDetailService", "onResponse:" + orderDetialModel.toString());
                        if (orderDetialModel.getStatus().equalsIgnoreCase("Success")) {
                            Toast.makeText(getApplicationContext(), "" + orderDetialModel.getDiscount(), Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getApplicationContext(), "" + orderDetialModel.getPrice(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Server Error!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<OrderDetialModel> call, Throwable t) {
                    t.printStackTrace();
                    pd.dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onBackPressed() {
        if (checkClick){
            findViewById(R.id.user_input_layout).setVisibility(View.VISIBLE);
            findViewById(R.id.billing_layout).setVisibility(View.GONE);
            checkClick=false;
        }
        else{
            BillingInfoActivity.this.finish();
            super.onBackPressed();
        }

    }
}
