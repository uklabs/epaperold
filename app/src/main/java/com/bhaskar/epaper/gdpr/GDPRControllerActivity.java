package com.bhaskar.epaper.gdpr;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.profile.TermConditionActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.Urls;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.bhaskar.epaper.epaperv2.payment.utility.Constants.Gdpr.USER_CONSENT;
import static com.bhaskar.epaper.epaperv2.payment.utility.Constants.Gdpr.WD_CONSENT;
import static com.bhaskar.epaper.epaperv2.payment.utility.Constants.Gdpr.WD_LOGIN_CONSENT;
import static com.bhaskar.epaper.epaperv2.payment.utility.Constants.Gdpr.WD_SUBSCR_CONSENT;

public class GDPRControllerActivity extends AppCompatActivity {
    private EpaperPrefernces pref;
    private final int privacyOption = 1;
    private final int cookieOption = 2;
    private final int refundOption = 3;
    private final int termsOption = 4;
    private String privacyPolicy = "Privacy Policy";
    private String cookiePolicy = "Cookie Policy";
    private String refundPolicy = "Refund Policy";
    private String termsAndCond = "Terms & Conditions";
    private AVLoadingIndicatorView progress;
    private boolean checkedValue = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_gdprcontroller);
        progress = findViewById(R.id.progress);
        Bundle bundle = getIntent().getExtras();
        int consentValue = bundle.getInt(Constants.Gdpr.CONSENT_VALUE);
        pref = EpaperPrefernces.getInstance(this);
        handleDialog(consentValue);
    }

    private void handleDialog(int consentValue) {

        switch (consentValue) {
            case USER_CONSENT:
                showUserConsentDialog();
                break;
            case WD_CONSENT:
                showWDUserConsentDialog();
                break;
            case WD_LOGIN_CONSENT:
                showWDLoginConsentDialog();
                break;
            case WD_SUBSCR_CONSENT:
                showWDSubsConsentDialog();
                break;

        }
    }

    private void showUserConsentDialog() {
        final AlertDialog.Builder gdprConsentBuilder = new AlertDialog.Builder(this);
        gdprConsentBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_gdpr_consent_dialog, null);

        dialogView.findViewById(R.id.cross_layout).setVisibility(View.GONE);
        dialogView.findViewById(R.id.terms_layout).setVisibility(View.GONE);
        TextView contentDesc = dialogView.findViewById(R.id.txt_content_desc);

        TextView termsTv = dialogView.findViewById(R.id.term_condition_tv);
        RadioGroup enabDisRadioGroup = dialogView.findViewById(R.id.enab_dis_radio_group);
        CheckBox agreeCheckBox = dialogView.findViewById(R.id.agree_check_box);
        final Button continueBtn = dialogView.findViewById(R.id.continue_btn);
        gdprConsentBuilder.setView(dialogView);

        final AlertDialog gdprDialog = gdprConsentBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        gdprDialog.setCanceledOnTouchOutside(false);

        // do some work here

        String contentDescText = getString(R.string.gdpr_content_description, cookiePolicy);
        SpannableStringBuilder spannableStringBuilder1 = new SpannableStringBuilder(Html.fromHtml(contentDescText));
        int cookiePoIndx1 = Html.fromHtml(contentDescText).toString().indexOf(cookiePolicy);
        spannableStringBuilder1.setSpan(new MySpannable(GDPRControllerActivity.this, cookieOption), cookiePoIndx1, cookiePoIndx1 + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        contentDesc.setMovementMethod(LinkMovementMethod.getInstance());
        contentDesc.setText(spannableStringBuilder1, TextView.BufferType.SPANNABLE);


        if (pref.getBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_CHECKED, false)) {
            dialogView.findViewById(R.id.terms_layout).setVisibility(View.GONE);
            continueBtn.setEnabled(true);
            continueBtn.setClickable(true);
            continueBtn.setBackground(ContextCompat.getDrawable(GDPRControllerActivity.this, R.drawable.gdpr_btn_bg));
            checkedValue = true;
        } else {
            dialogView.findViewById(R.id.terms_layout).setVisibility(View.VISIBLE);
        }

        String agreeTermsConditionText = getString(R.string.gdpr_user_consent_terms, termsAndCond, privacyPolicy, cookiePolicy);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(agreeTermsConditionText));
        int priPoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(privacyPolicy);
        int cookiePoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(cookiePolicy);
        int termsIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(termsAndCond);

        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, termsOption), termsIndx, termsIndx + termsAndCond.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, privacyOption), priPoIndx, priPoIndx + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, cookieOption), cookiePoIndx, cookiePoIndx + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsTv.setMovementMethod(LinkMovementMethod.getInstance());
        termsTv.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

        enabDisRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.enable_cons) {
                    dialogView.findViewById(R.id.continue_layout).setVisibility(View.VISIBLE);
                } else {
                    dialogView.findViewById(R.id.continue_layout).setVisibility(View.GONE);
                }
            }
        });
        agreeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkedValue = isChecked;
                if (isChecked) {
                    continueBtn.setEnabled(true);
                    continueBtn.setClickable(true);
                    continueBtn.setBackground(ContextCompat.getDrawable(GDPRControllerActivity.this, R.drawable.gdpr_btn_bg));
                } else {
                    continueBtn.setEnabled(false);
                    continueBtn.setClickable(false);
                    continueBtn.setBackground(ContextCompat.getDrawable(GDPRControllerActivity.this, R.drawable.gdpr_btn_grey_bg));
                }
            }
        });
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedValue) {
                    pref.setBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, true);
                    pref.setBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_CHECKED, true);
                    setResult(RESULT_OK);
                    GDPRControllerActivity.this.finish();
                } else {
                    Toast.makeText(GDPRControllerActivity.this, "Please acknowledge Policy", Toast.LENGTH_SHORT).show();
                }
            }
        });
        gdprDialog.show();
    }

    private void showWDUserConsentDialog() {
        final AlertDialog.Builder gdprConsentBuilder = new AlertDialog.Builder(this);
        gdprConsentBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_gdpr_consent_dialog, null);
        dialogView.findViewById(R.id.cross_layout).setVisibility(View.VISIBLE);
        dialogView.findViewById(R.id.continue_layout).setVisibility(View.VISIBLE);
        dialogView.findViewById(R.id.terms_layout).setVisibility(View.GONE);
        TextView contentDesc = dialogView.findViewById(R.id.txt_content_desc);
        final RadioGroup enabDisRadioGroup = dialogView.findViewById(R.id.enab_dis_radio_group);
        final Button continueBtn = dialogView.findViewById(R.id.continue_btn);
        RadioButton radioButton = dialogView.findViewById(R.id.enable_cons);
        radioButton.setChecked(true);

        gdprConsentBuilder.setView(dialogView);
        final AlertDialog gdprDialog = gdprConsentBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogView.findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(gdprDialog);
            }
        });

        gdprDialog.setCanceledOnTouchOutside(false);

        // do some work here
        /*content desc*/
        String contentDescText = getString(R.string.gdpr_content_description, cookiePolicy);
        SpannableStringBuilder spannableStringBuilder1 = new SpannableStringBuilder(Html.fromHtml(contentDescText));
        int cookiePoIndx1 = Html.fromHtml(contentDescText).toString().indexOf(cookiePolicy);
        spannableStringBuilder1.setSpan(new MySpannable(GDPRControllerActivity.this, cookieOption), cookiePoIndx1, cookiePoIndx1 + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        contentDesc.setMovementMethod(LinkMovementMethod.getInstance());
        contentDesc.setText(spannableStringBuilder1, TextView.BufferType.SPANNABLE);
        continueBtn.setBackground(ContextCompat.getDrawable(GDPRControllerActivity.this, R.drawable.gdpr_btn_bg));
        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (enabDisRadioGroup.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(GDPRControllerActivity.this, "Please choose one", Toast.LENGTH_SHORT).show();
                } else {
                    if (enabDisRadioGroup.getCheckedRadioButtonId() == R.id.disable_cons) {
                        withDrawConsent();
                    } else {
                        //dismiss
                        closeActivity(gdprDialog);
                    }
                }
            }
        });
        gdprDialog.show();
    }

    private void withDrawConsent() {
        pref.setBooleanValue(QuickPreferences.GdprConst.GDPR_CONSENT_ACCEPTED, false);
        setResult(RESULT_OK);
        finish();
    }

    private void showWDLoginConsentDialog() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        exitDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_gdpr_wd_consent_dialog, null);


        TextView desc_tv = dialogView.findViewById(R.id.desc_tv);
        Button confirmBtn = dialogView.findViewById(R.id.confirm_btn);

        exitDialogBuilder.setView(dialogView);
        final AlertDialog gdprDialog = exitDialogBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        gdprDialog.setCanceledOnTouchOutside(false);
        /*do some work here*/
        dialogView.findViewById(R.id.cross_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(gdprDialog);
            }
        });

        String cookiePolicy = "cookie policy";
        String privacyPolicy = "privacy policy";
        String agreeTermsConditionText = getString(R.string.gdpr_wd_login_consent, privacyPolicy, cookiePolicy);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(agreeTermsConditionText));
        int priPoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(privacyPolicy);
        int cookiePoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(cookiePolicy);

        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, privacyOption), priPoIndx, priPoIndx + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, cookieOption), cookiePoIndx, cookiePoIndx + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        desc_tv.setMovementMethod(LinkMovementMethod.getInstance());
        desc_tv.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitGdprWDConsentAPI(gdprDialog);
            }
        });
        gdprDialog.show();
    }

    private void showWDSubsConsentDialog() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        exitDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_gdpr_wd_consent_dialog, null);

        TextView desc_tv = dialogView.findViewById(R.id.desc_tv);
        Button confirmBtn = dialogView.findViewById(R.id.confirm_btn);

        exitDialogBuilder.setView(dialogView);
        final AlertDialog gdprDialog = exitDialogBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        gdprDialog.setCanceledOnTouchOutside(false);
        /*do some work here*/
        dialogView.findViewById(R.id.cross_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(gdprDialog);
            }
        });
        int regType = pref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0);
        boolean isDomainUser = regType == Constants.SubscriptionStatus.DOMAIN_USER;
        String subscriptionEndDate = pref.getStringValue(QuickPreferences.PaymentPref.SUBSCRIPTION_END_DATE, "");
        String subscriptionDate = (isDomainUser && TextUtils.isEmpty(subscriptionEndDate)) ? "" : " ending on <b>" + DateUtility.parseDateToddMMyyyy(subscriptionEndDate) + "</b>";
        String agreeTermsConditionText = getString(R.string.gdpr_wd_subs_consent, subscriptionDate, privacyPolicy, cookiePolicy, refundPolicy);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(Html.fromHtml(agreeTermsConditionText));
        int priPoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(privacyPolicy);
        int cookiePoIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(cookiePolicy);
        int refundIndx = Html.fromHtml(agreeTermsConditionText).toString().indexOf(refundPolicy);

        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, privacyOption), priPoIndx, priPoIndx + privacyPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, cookieOption), cookiePoIndx, cookiePoIndx + cookiePolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new MySpannable(GDPRControllerActivity.this, refundOption), refundIndx, refundIndx + refundPolicy.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        desc_tv.setMovementMethod(LinkMovementMethod.getInstance());
        desc_tv.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gdprDialog.dismiss();
                confirmWDConsentDialog();
            }
        });

        gdprDialog.show();
    }

    private void confirmWDConsentDialog() {
        final AlertDialog.Builder exitDialogBuilder = new AlertDialog.Builder(this);
        exitDialogBuilder.setCancelable(false);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_gdpr_confirm_wd_consent_dialog, null);

        Button cancelBtn = dialogView.findViewById(R.id.cancel_btn);
        Button confirmBtn = dialogView.findViewById(R.id.confirm_btn);

        exitDialogBuilder.setView(dialogView);
        final AlertDialog gdprDialog = exitDialogBuilder.create();
        gdprDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        gdprDialog.setCanceledOnTouchOutside(false);
        /*do some work here*/

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeActivity(gdprDialog);
            }
        });
        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitGdprWDConsentAPI(gdprDialog);
            }
        });

        gdprDialog.show();
    }

    private void hitGdprWDConsentAPI(final AlertDialog dialog) {
        progress.setVisibility(View.VISIBLE);
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("db_id", pref.getStringValue(QuickPreferences.UserInfo.DB_ID, ""));
            jsonObject.put("user_id", pref.getStringValue(QuickPreferences.UserInfo.USER_ID, ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Urls.GDPR_WD_CONSENT_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(Util.decryptString(response));
                            if (jsonObject1.optString("status").equalsIgnoreCase("Success")) {
                                dialog.dismiss();
                                progress.setVisibility(View.GONE);
                                withDrawConsent();
                            } else {
                                progress.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progress.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progress.setVisibility(View.GONE);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("encrypt", "1");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() {
                String data = Util.encryptString(jsonObject.toString());
                Map<String, String> params = new HashMap<>();
                params.put("edata", data);
                params.put("Content-Type", "application/json");
                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public class MySpannable extends ClickableSpan {
        private Context mContext;
        private int position;

        public MySpannable(Context context, int position) {
            this.mContext = context;
            this.position = position;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            ds.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        @Override
        public void onClick(View widget) {
            switch (position) {
                case privacyOption:
                    openPolicy(privacyPolicy, Urls.PRIVACY_POLICY_URL);
                    break;
                case cookieOption:
                    openPolicy(cookiePolicy, Urls.COOKIE_POLICY_URL);
                    break;
                case refundOption:
                    openPolicy(refundPolicy, Urls.REFUND_POLICY_URL);
                    break;
                case termsOption:
                    openPolicy(termsAndCond, Urls.TERM_AND_CONDITION_URL);
                    break;
            }
        }
    }

    private void openPolicy(String title, String url) {
        startActivity(new Intent(GDPRControllerActivity.this, TermConditionActivity.class).putExtra("title", title).putExtra("url", url));
    }

    private void closeActivity(AlertDialog gdprDialog) {
        gdprDialog.dismiss();
        GDPRControllerActivity.this.finish();
    }
}
