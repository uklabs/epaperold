package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.ImageZoomActivity;
import com.bhaskar.epaper.ui.MyBookmarkActivity;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by db on 5/2/2016.
 */
public class MyBookmarkAdapter extends RecyclerView.Adapter<MyBookmarkAdapter.ViewHolder> {

    private List<MasterModel> listdata;
    private Activity context;

    public MyBookmarkAdapter(Activity context, List<MasterModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    @NonNull
    @Override
    public MyBookmarkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mybookmark_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyBookmarkAdapter.ViewHolder holder, final int position) {

        String url = listdata.get(position).imagethumb.replace(" ", "%20");
        try {
            ImageUtil.setImage(context, url, R.drawable.watermark_epaper, holder.imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.item_bookmark_name_txt.setText(listdata.get(position).description);
        holder.item_bookmark_edition_txt.setText("Page No:" + listdata.get(position).editioncode);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageZoomActivity.clickModel = listdata.get(position);
                context.startActivity(new Intent(context, ImageZoomActivity.class)
                        .putExtra("largeUrl", listdata.get(position).imagepath)
                        .putExtra("editioncode", listdata.get(position).editioncode)
                        .putExtra("editionname", listdata.get(position).description)
                );
            }
        });
        holder.delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeBookMark(listdata.get(position).id, position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, delete_iv;
        TextView item_bookmark_name_txt, item_bookmark_edition_txt;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.item_category_image);
            item_bookmark_name_txt = itemView.findViewById(R.id.item_bookmark_name_txt);
            item_bookmark_edition_txt = itemView.findViewById(R.id.item_bookmark_edition_txt);
//            delete_txt = itemView.findViewById(R.id.delete_txt);
            delete_iv = itemView.findViewById(R.id.delete_iv);

        }
    }

    public void removeBookMark(String id, final int position) {
        final MyBookmarkActivity activity = (MyBookmarkActivity) context;

        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();

        Epaper mApp = (Epaper) context.getApplicationContext();
        mApp.getServices().removeMyBookmarkData(id, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                Log.e("Response", "=>" + jsonElement.toString());
                String status = "", msg = "";
                try {
                    JSONObject jb = new JSONObject(jsonElement.toString());
                    status = jb.getString("status");
                    msg = jb.getString("msg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.contains("1")) {
                    Utility.setToast(activity, msg);
                    removeItemPostion(position);

                } else {
                    Utility.setToast(activity, msg);
                }
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {

                pd.dismiss();
            }
        });

    }

    public void removeItemPostion(int position) {
        listdata.remove(position);
        MyBookmarkAdapter.this.notifyItemRemoved(position);
        MyBookmarkAdapter.this.notifyItemRangeChanged(position, listdata.size());
        MyBookmarkAdapter.this.notifyDataSetChanged();
    }
}