package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.PaymentPlanActivity;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.homeutils.HomeContent;
import com.bhaskar.epaper.model.MasterModel;
import com.bhaskar.epaper.ui.MasterActivity;
import com.bhaskar.epaper.ui.SubMasterActivity;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class HomeItemAdapter extends RecyclerView.Adapter<HomeItemAdapter.ViewHolder> {

    public List<MasterModel> listdata;
    public String statename;
    private Context context;
    private EpaperPrefernces mPref;
    private String channelSlno="";
    private String isPaid;

    public HomeItemAdapter(Context context, EpaperPrefernces prefernces, String statename, String channelSlno, String isPaid) {
        this.listdata = HomeContent.getHomeContent().listMain.get(statename);
        this.statename = statename;
        this.context = context;
        this.mPref = prefernces;
        this.channelSlno = channelSlno;
        this.isPaid = isPaid;
    }

    @NonNull
    @Override
    public HomeItemAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeItemAdapter.ViewHolder holder, final int position) {
        final MasterModel masterModel = listdata.get(position);
        holder.todayDateTxt.setText(DateUtility.parseDateToddMMyyyy(DateUtility.setDateForAll));
        holder.todayDateTxt.setVisibility(View.VISIBLE);
        String imgThumb = Epaper.NO_IMG_FOUND;
        if (masterModel != null) {
            holder.itemCategorytxt.setText(masterModel.description);
            imgThumb = masterModel.imagethumb;
        } else {
            holder.itemCategorytxt.setText("unavailable");
        }
        int placeholder = R.drawable.water_mark_dainik;
        if (channelSlno.equalsIgnoreCase(Constants.DIVYA_BHASKAR)) {
            placeholder = R.drawable.water_mark_divya;
        } else if (channelSlno.equalsIgnoreCase(Constants.DIVYA_MARATHI_BHASKAR)) {
            placeholder = R.drawable.water_mark_marathi;
        }
        try {
            ImageUtil.setImage(context, imgThumb, placeholder, holder.itemImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Epaper.getInstance().setChannelSlno(channelSlno);
                if (isShowSubscription()) {
                    ((Activity) context).startActivityForResult(
                            new Intent(context, PaymentPlanActivity.class).putExtra("GA_Label", GAConst.Label.LANDING), MasterActivity.REQUEST_CODE_PAYMENT);

                } else {
                    context.startActivity(
                            new Intent(context, SubMasterActivity.class)
                                    .putExtra("statename", statename)
                                    .putExtra("description", masterModel.description)
                                    .putExtra("editioncode", masterModel.editioncode)
                    );
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        LinearLayout itemCategoryBookmark;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        CardView cardview;
        TextView todayDateTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            todayDateTxt = itemView.findViewById(R.id.today_date_txt);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);
            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemCategoryBookmark = itemView.findViewById(R.id.item_category_bookmark);
            cardview = itemView.findViewById(R.id.cardview);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            if (!TextUtils.isEmpty(statename)) {
                itemCategoryBookmark.setVisibility(View.GONE);
            }
        }
    }

    private boolean isShowSubscription() {
        return (isPaid.equalsIgnoreCase("1")
                && !mPref.getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false)
                && mPref.getIntValue(QuickPreferences.PaymentPref.USER_REGISTRATION_TYPE, 0) != Constants.SubscriptionStatus.DOMAIN_USER
                && !mPref.getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false));
    }
}

