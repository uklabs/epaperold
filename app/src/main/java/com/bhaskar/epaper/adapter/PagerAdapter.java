package com.bhaskar.epaper.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.fragments.ViewPagerFragment;
import com.bhaskar.epaper.manager.ModelManager;
import com.bhaskar.epaper.model.MasterPageModel;

import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {

    private final List<MasterPageModel> pages;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        this.pages = Epaper.getInstance().getModelManager().getListdata();
    }


    @Override
    public Fragment getItem(int position) {
        return ViewPagerFragment.getPagerFragment(pages.get(position), position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int pageitem = position;
        pageitem = pageitem + 1;
        return "PAGE " + pageitem;
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}