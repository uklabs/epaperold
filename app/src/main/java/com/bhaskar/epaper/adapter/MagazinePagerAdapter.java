package com.bhaskar.epaper.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.bhaskar.epaper.fragments.MagazinePagerFragment;
import com.bhaskar.epaper.model.MagazineMasterModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagazinePagerAdapter extends FragmentPagerAdapter {

    //    private boolean isSubscribed;
    private List<MagazineMasterModel> GujratiModelList = new ArrayList<>();
    private List<MagazineMasterModel> MarathiModelList = new ArrayList<>();
    private List<MagazineMasterModel> HindiModelList = new ArrayList<>();
    private List<String> title = new ArrayList<>();

    public MagazinePagerAdapter(FragmentManager fm, List<MagazineMasterModel> pages, Context context) {
        super(fm);
        setMasterDetail(pages);
    }

    private void setMasterDetail(List<MagazineMasterModel> pages) {
        title.add("Hindi");
        title.add("Gujarati");
        title.add("Marathi");

        for (MagazineMasterModel mmm : pages) {
            if (mmm != null) {
                if (mmm.getMagazine().equals("1")) {
                    HindiModelList.add(mmm);
                } else if (mmm.getMagazine().equals("2")) {
                    GujratiModelList.add(mmm);
                } else {
                    MarathiModelList.add(mmm);
                }
            }
        }

    }

    @Override
    public Fragment getItem(int position) {
        if (title.get(position).equalsIgnoreCase("Hindi")) {
            return new MagazinePagerFragment(HindiModelList,"Hindi");
        } else if (title.get(position).equalsIgnoreCase("Gujarati")) {
            return new MagazinePagerFragment(GujratiModelList,"Gujarati");
        } else {
            return new MagazinePagerFragment(MarathiModelList,"Marathi");
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title.get(position);
    }

    @Override
    public int getCount() {
        return title.size();
    }


}