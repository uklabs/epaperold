package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.utility.Constants;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.model.SubMasterModel;
import com.bhaskar.epaper.ui.EditionDetailActivity;
import com.bhaskar.epaper.ui.SubMasterActivity;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by db on 5/2/2016.
 */
public class HomeSubMasterAdapter extends RecyclerView.Adapter<HomeSubMasterAdapter.ViewHolder> {

    public ArrayList<SubMasterModel> listdata;
    Activity context;
    String statename,cityName;
    Epaper mApp;
    EpaperPrefernces mPref;

    public HomeSubMasterAdapter(Activity context, ArrayList<SubMasterModel> filterEdition, String stateName,String cityName) {
        this.listdata = filterEdition;
        this.context = context;
        this.statename = stateName;
        this.cityName = cityName;
        mApp = (Epaper) context.getApplicationContext();
        mPref = EpaperPrefernces.getInstance(context);
    }

    @NonNull
    @Override
    public HomeSubMasterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final HomeSubMasterAdapter.ViewHolder holder, final int position) {
        int placeholder = R.drawable.water_mark_dainik;
        String channel=Epaper.getInstance().getChannelSlno();
        if (!TextUtils.isEmpty(channel)) {
            if (channel.equalsIgnoreCase(Constants.DIVYA_BHASKAR)) {
                placeholder = R.drawable.water_mark_divya;
            } else if (channel.equalsIgnoreCase(Constants.DIVYA_MARATHI_BHASKAR)) {
                placeholder = R.drawable.water_mark_marathi;
            }
        }
        if (listdata.get(position) != null) {
            holder.itemCategorytxt.setText(listdata.get(position).description);
            ImageUtil.setImage(context, listdata.get(position).imagethumb, placeholder, holder.itemImageView);
        } else {
            holder.itemCategorytxt.setText("unavailable");
            ImageUtil.setImage(context, Epaper.NO_IMG_FOUND, placeholder, holder.itemImageView);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(statename)) {
                    context.startActivity(new Intent(context, EditionDetailActivity.class)
                            .putExtra("statename", statename)
                            .putExtra("city_name", cityName)
                            .putExtra("check_comes", "homeSubmaster")
                            .putParcelableArrayListExtra("pages", listdata)
                            .putExtra("editioncode", listdata.get(position).editioncode)
                            .putExtra("editionname", listdata.get(position).description));
                }

            }
        });

        holder.itemCategoryBookmark.setVisibility(View.VISIBLE);
        holder.itemCategoryBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, ""))) {
                    setMyEdition(listdata.get(position));
                } else {
                    Utility.setToast(context, "Please Login First");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        LinearLayout itemCategoryBookmark;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);

            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            itemCategoryBookmark = itemView.findViewById(R.id.item_category_bookmark);
        }
    }

    public void setMyEdition(SubMasterModel sm) {

        SubMasterActivity activity = (SubMasterActivity) context;
        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();

        String email = mPref.getStringValue(QuickPreferences.UserInfo.EMAIL, "");

        mApp.getServices().setMyEditionData(email, statename, statename, sm.description, sm.editioncode,
                sm.imagepath, sm.imagethumb, sm.imagelarge, new Callback<JsonElement>() {
                    @Override
                    public void success(JsonElement jsonElement, Response response) {
                        try {
                            JSONObject jb = new JSONObject(jsonElement.toString());
                            if (jb.getString("status").contains("1")) {
                                Utility.setToast(context, "Edition Successfully Saved..");
                            } else {
                                Utility.setToast(context, "Edition Already Exist..");
                            }
                        } catch (JSONException e) {
                          e.printStackTrace();
                        }
                        pd.dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        pd.dismiss();
                    }
                });

    }

}


/*

if (listdata.get(position).getColor_dec().contains("1")) {
        Log.i("declaration", "=>" + listdata.get(position).getColor_dec());
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

final ImageDataList imgDataModel = listdata.get(position);

        if (imgDataModel.color_dec.contains("1")) {
        Log.i("declaration", "=>" + imgDataModel.color_dec);
        holder.color_yes.setChecked(true);
        } else {
        holder.color_no.setChecked(true);
        }

        if (imgDataModel.reg_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.reg_dec);
        holder.reg_yes.setChecked(true);
        } else {
        holder.reg_no.setChecked(true);
        }

        if (imgDataModel.center_dec.equals("1")) {
        Log.i("declaration", "=>" + imgDataModel.center_dec);
        holder.center_yes.setChecked(true);
        } else {
        holder.center_no.setChecked(true);
        }*/
