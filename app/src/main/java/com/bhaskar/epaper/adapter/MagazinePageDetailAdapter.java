package com.bhaskar.epaper.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bhaskar.epaper.fragments.MagazinePageDetailViewpager;
import com.bhaskar.epaper.model.MagazineMasterModel;

import java.util.List;

public class MagazinePageDetailAdapter extends FragmentPagerAdapter {

    private final List<MagazineMasterModel> pages;

    public MagazinePageDetailAdapter(FragmentManager fm, List<MagazineMasterModel> pages) {
        super(fm);
        this.pages = pages;
    }

    @Override
    public Fragment getItem(int position) {
        return new MagazinePageDetailViewpager(pages.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        int pageitem = position;
        pageitem = pageitem + 1;
        pages.get(position).setPageNo("Page " + pageitem);
        return "Page " + pageitem;
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}