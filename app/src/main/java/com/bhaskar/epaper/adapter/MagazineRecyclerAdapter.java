package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.payment.PaymentPlanActivity;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.bhaskar.epaper.model.MagazineMasterModel;
import com.bhaskar.epaper.ui.ConnectionDetector;
import com.bhaskar.epaper.ui.MagazineDetailActivity;
import com.bhaskar.epaper.ui.MagazineMasterActivity;
import com.bhaskar.epaper.ui.MagazineSubActivity;
import com.bhaskar.epaper.utils.DateUtility;
import com.bhaskar.epaper.utils.EpaperPrefernces;
import com.bhaskar.epaper.utils.GAConst;
import com.bhaskar.epaper.utils.ImageUtil;

import java.util.List;

/**
 * Created by db on 5/2/2016.
 */
public class MagazineRecyclerAdapter extends RecyclerView.Adapter<MagazineRecyclerAdapter.ViewHolder> {

    private List<MagazineMasterModel> listdata;
    private Activity context;
    private Boolean isClickable;
    private String language="";

    public MagazineRecyclerAdapter(Activity context, List<MagazineMasterModel> listdata,String lang, Boolean isClickable) {
        this.listdata = listdata;
        this.context = context;
        this.language = lang;
        this.isClickable = isClickable;
    }


    @NonNull
    @Override
    public MagazineRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_home, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final MagazineRecyclerAdapter.ViewHolder holder, final int position) {
        final MagazineMasterModel magazineMasterModel = listdata.get(position);
        int placeholder = R.drawable.watermark_epaper;
        if (magazineMasterModel != null) {
            holder.itemCategorytxt.setText(magazineMasterModel.getDescription());
            ImageUtil.setImage(context, magazineMasterModel.getImagethumb(), placeholder, holder.itemImageView);
            holder.todayDateTxt.setText(DateUtility.parseDateToddMMyyyy(magazineMasterModel.getPagedate()));
            holder.todayDateTxt.setVisibility(View.VISIBLE);

        } else {
            holder.itemCategorytxt.setText("unavailable");
            ImageUtil.setImage(context, Epaper.NO_IMG_FOUND, placeholder, holder.itemImageView);
            holder.todayDateTxt.setText("");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isConnectingToInternet(context)) {
                    MagazineMasterModel magazine = magazineMasterModel;
                    if (isShowSubscripionLoginDialog(magazine.getMagazine())) {
                        Epaper.getInstance().setChannelSlno(magazine.getChannelSlno());
                        context.startActivityForResult(
                                new Intent(context, PaymentPlanActivity.class).putExtra("GA_Label", GAConst.Label.LANDING), MagazineMasterActivity.REQUEST_CODE_PAYMENT);
                    } else {
                        if (isClickable) {
                            context.startActivity(new Intent(context, MagazineSubActivity.class).putExtra("editioncode", magazineMasterModel.getEditioncode()).putExtra("magazinelang", language).putExtra("magazinename", magazineMasterModel.getDescription()));
                        } else {
                            context.startActivity(new Intent(context, MagazineDetailActivity.class).putExtra("editioncode", magazineMasterModel.getEditioncode()).putExtra("editiondate", magazineMasterModel.getPagedate()).putExtra("magazine", magazineMasterModel.getMagazine()).putExtra("magazinelang",language).putExtra("magazinename", magazineMasterModel.getDescription()));
                        }
                    }
                } else {
                    Toast.makeText(context, "Please Check Network Connection", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemCategorytxt;
        ImageView itemImageView;
        RelativeLayout mainCardView;
        RelativeLayout ll_category_home;
        TextView todayDateTxt;

        public ViewHolder(View itemView) {
            super(itemView);
            mainCardView = itemView.findViewById(R.id.mainCardView);
            ll_category_home = itemView.findViewById(R.id.ll_category_home);
            itemCategorytxt = itemView.findViewById(R.id.item_category_txt);
            itemImageView = itemView.findViewById(R.id.item_category_image);
            todayDateTxt = itemView.findViewById(R.id.today_date_txt);
        }

    }

    private boolean isShowSubscripionLoginDialog(String magazineId) {
        boolean isSubs = EpaperPrefernces.getInstance(context).getBooleanValue(QuickPreferences.PaymentPref.IS_SUBSCRIBED, false);
        boolean isTrial = EpaperPrefernces.getInstance(context).getBooleanValue(QuickPreferences.CommonPref.IS_TRIAL_ON, false);
        return !isTrial & !Epaper.getInstance().getModelManager().getAllowMagazineList().contains(magazineId) && !isSubs;
    }
}