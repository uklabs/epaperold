package com.bhaskar.epaper.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.epaperv2.interfaces.OnSetHomePageListener;
import com.bhaskar.epaper.fragments.MasterPageStateFragment;
import com.bhaskar.epaper.model.MainDataModel;

import java.util.List;

public class MasterPageStateAdapter extends FragmentStatePagerAdapter {

    private List<MainDataModel> stateList;
    private OnSetHomePageListener onSetHomePageListener;

    public MasterPageStateAdapter(FragmentManager fm, OnSetHomePageListener onSetHomePageListener) {
        super(fm);
        this.onSetHomePageListener = onSetHomePageListener;
        this.stateList = Epaper.getInstance().getModelManager().getStateList();

    }

    @Override
    public Fragment getItem(int position) {
        MasterPageStateFragment fragment = MasterPageStateFragment.getInstance(position);
        fragment.setHomeListener(onSetHomePageListener);
        return fragment;


    }

    @Override
    public CharSequence getPageTitle(int position) {
        return stateList.get(position).statename;
    }

    @Override
    public int getCount() {
        return stateList.size();
    }


}