package com.bhaskar.epaper.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bhaskar.epaper.Epaper;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.model.EditionModel;
import com.bhaskar.epaper.ui.EditionDetailActivity;
import com.bhaskar.epaper.ui.MyEditionActivity;
import com.bhaskar.epaper.utils.ImageUtil;
import com.bhaskar.epaper.utils.Utility;
import com.google.gson.JsonElement;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by db on 5/2/2016.
 */
public class MyEditionAdapter extends RecyclerView.Adapter<MyEditionAdapter.ViewHolder> {

    public List<EditionModel> listdata;
    Activity context;

    public MyEditionAdapter(Activity context, List<EditionModel> listdata) {
        this.listdata = listdata;
        this.context = context;
    }

    @NonNull
    @Override
    public MyEditionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mybookmark_home, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyEditionAdapter.ViewHolder holder, final int position) {

        String url = listdata.get(position).getThumb_path().replace(" ", "%20");
        try {
            if (url.isEmpty()) {
                url = Epaper.NO_IMG_FOUND;
            }
            ImageUtil.setImage(context, url, R.drawable.watermark_epaper, holder.imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.item_bookmark_name_txt.setText(listdata.get(position).getEdtname());
        holder.item_bookmark_edition_txt.setText(listdata.get(position).getState());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, EditionDetailActivity.class).putExtra("statename", listdata.get(position).getState()).putExtra("editionname",listdata.get(position).getEdtname())
                        .putExtra("editioncode", listdata.get(position).edtcode).putExtra("currentdate", "currentdate")
                        .putExtra("check_comes", ""));
            }
        });

        holder.delete_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeMyEdition(listdata.get(position).id, position);
            }
        });

        Utility.logE("image", "=>" + listdata.get(position).getThumb_path());
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView, delete_iv;
        TextView item_bookmark_name_txt, item_bookmark_edition_txt;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.item_category_image);
            item_bookmark_name_txt = itemView.findViewById(R.id.item_bookmark_name_txt);
            item_bookmark_edition_txt = itemView.findViewById(R.id.item_bookmark_edition_txt);
            delete_iv = itemView.findViewById(R.id.delete_iv);

        }
    }

    public void removeMyEdition(String id, final int position) {
        final MyEditionActivity activity = (MyEditionActivity) context;

        final ProgressDialog pd = new ProgressDialog(activity);
        pd.setMessage("Please Wait ..");
        pd.show();

        Epaper mApp = (Epaper) context.getApplicationContext();
        mApp.getServices().removeMyEditionData(id, new Callback<JsonElement>() {
            @Override
            public void success(JsonElement jsonElement, Response response) {
                Log.e("Response", "=>" + jsonElement.toString());
                String status = "", msg = "";
                try {
                    JSONObject jb = new JSONObject(jsonElement.toString());
                    status = jb.getString("status");
                    msg = jb.getString("msg");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.contains("1")) {
                    Utility.setToast(activity, msg);
                    removeItemPostion(position);

                } else {
                    Utility.setToast(activity, msg);
                }
                pd.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                pd.dismiss();
            }
        });

    }

    public void removeItemPostion(int position) {
        listdata.remove(position);
        MyEditionAdapter.this.notifyItemRemoved(position);
        MyEditionAdapter.this.notifyItemRangeChanged(position, listdata.size());
        MyEditionAdapter.this.notifyDataSetChanged();
    }
}