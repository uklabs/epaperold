package com.bhaskar.epaper.utils;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import com.facebook.ads.AbstractAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSize;
import com.facebook.ads.AdView;
import com.facebook.ads.InterstitialAd;

/**
 * Created by db on 12/30/2016.
 */
public class FBAdUtility {
    private InterstitialAd interstitialAd;
    Context context;

    public FBAdUtility(Context context) {
        this.context = context;
    }

    public void loadInterstitialAd() {
        interstitialAd = new InterstitialAd(context, "1585293375111281_1612761979031087");

        interstitialAd.setAdListener(new AbstractAdListener() {
            @Override
            public void onAdLoaded(Ad ad) {
                super.onAdLoaded(ad);
                interstitialAd.show();
                Log.e("Load", "=>" + ad.getPlacementId());
            }

            @Override
            public void onError(Ad ad, AdError error) {
                // Ad failed to load
                Log.e("LoadError", "=>" + error.getErrorCode());
                Log.e("LoadError", "=>" + error.getErrorMessage());

            }
        });
        interstitialAd.loadAd();

    }

    public void loadBannerAd(ViewGroup viewGroup) {
        AdView adView = new AdView(context, "1585293375111281_1612761979031087", AdSize.BANNER_320_50);
        viewGroup.addView(adView);
        adView.loadAd();
    }

}
