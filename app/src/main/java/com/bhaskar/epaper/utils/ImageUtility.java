package com.bhaskar.epaper.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.FileUriExposedException;
import android.os.TransactionTooLargeException;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bhaskar.epaper.BuildConfig;
import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.Util;
import com.github.jksiezni.permissive.PermissionsGrantedListener;
import com.github.jksiezni.permissive.PermissionsRefusedListener;
import com.github.jksiezni.permissive.Permissive;


public class ImageUtility {
    static String path = "";
    static String AppUrl = "";
    static File shared = null;
    static File myDir;
    static File share = null;

    static String ViralStringMSG = "";

    private static final float BITMAP_SCALE = 0.4f;
    private static final float BLUR_RADIUS = 7.5f;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }

    @SuppressLint("NewApi")
    public static Bitmap blurRenderScript(Bitmap smallBitmap, Context context) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(2); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);
        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public static class ImageUtilityWork extends AsyncTask<String, String, String> {

        Context context;
        Bitmap bitmap;
        ProgressDialog pd;
        String methodname;

        public ImageUtilityWork(Context context, String methodname, Bitmap bitmap) {
            this.context = context;
            pd = new ProgressDialog(context);
            pd.setMessage("Please Wait");
            this.bitmap = bitmap;


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.show();

        }

        @Override
        protected String doInBackground(String... params) {

            switch (methodname) {
                case "saveimage":
                    saveImage(bitmap, context);
                    break;
                case "shareimage":
                    saveSharedImage(bitmap, context);
                    break;

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pd.dismiss();
        }

    }

    public static void saveImage(final Bitmap finalBitmap, final Context c) {
//        if (requestFileWritePermission(c)) {
        final Activity act = (Activity) c;

        if (checkPermission(act))//Save
        {
            Log.e("permission", "=>Save_Image");
            writeSaveImage(finalBitmap, c);
        } else {
            //requestPermission(act);
            new Permissive.Request(Manifest.permission.WRITE_EXTERNAL_STORAGE).whenPermissionsGranted(new PermissionsGrantedListener() {
                @Override
                public void onPermissionsGranted(String[] permissions) throws SecurityException {
                    Log.e("granded", "=>");
                    writeSaveImage(finalBitmap, c);
                }
            }).whenPermissionsRefused(new PermissionsRefusedListener() {
                @Override
                public void onPermissionsRefused(String[] permissions) {
//                writeSaveImage(finalBitmap, c);
                    Toast.makeText(act, "Please Allow the file write", Toast.LENGTH_SHORT).show();
                }
            }).execute(act);
        }



//        } else {
//            Toast.makeText(c, "Please Allow the permission", Toast.LENGTH_SHORT).show();
//        }
    }

    public static boolean checkPermission(Activity act) {
        int result = ContextCompat.checkSelfPermission(act, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    public static void requestPermission(Activity act) {

        if (ActivityCompat.shouldShowRequestPermissionRationale(act, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            Toast.makeText(act, "Write External Storage permission allows us to do store images. Please allow this permission in App Settings.", Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(act, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    public static void writeSaveImage(final Bitmap finalBitmap, final Context c) {
        final ProgressDialog pd = new ProgressDialog(c);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Please Wait");

        class saveImageAsync extends AsyncTask<String, String, String> {
            @Override
            protected String doInBackground(String... params) {
                if (hasStorage(true)) {
                    String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
                    myDir = new File(root + "/" + c.getString(R.string.foldername));
                } else {
                    ContextWrapper cw = new ContextWrapper(c);
                    myDir = cw.getDir(c.getString(R.string.foldername), Context.MODE_PRIVATE);
                }

                myDir.mkdirs();
                Random generator = new Random();
                int n = 100000;
                n = generator.nextInt(n);
                String fname;
//        if (edited != null)
//            fname = edited;
//        else
                fname = "Image-" + n + ".jpg";
                File file = new File(myDir, fname);
                if (file.exists()) file.delete();
                try {
                    FileOutputStream out = new FileOutputStream(file);
                    finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    galleryAddPic(file.getAbsolutePath(), c);
                    out.flush();
                    out.close();
                    path = file.getAbsolutePath();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return path;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                Toast.makeText(c, "Saved To: " + s, Toast.LENGTH_SHORT).show();


            }
        }
        new saveImageAsync().execute();
    }

    public static void saveSharedImage(final Bitmap finalBitmap, final Context c) {

        final Activity act = (Activity) c;

       /* if (checkPermission(act)) {

            writeShareImageFile(finalBitmap, c, "");
        } else {
            ActivityCompat.requestPermissions(act, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }*/


        new Permissive.Request(Manifest.permission.WRITE_EXTERNAL_STORAGE).whenPermissionsGranted(new PermissionsGrantedListener() {
            @Override
            public void onPermissionsGranted(String[] permissions) throws SecurityException {
                Log.e("granded", "=>");
                writeShareImageFile(finalBitmap, c, "");
            }
        }).whenPermissionsRefused(new PermissionsRefusedListener() {
            @Override
            public void onPermissionsRefused(String[] permissions) {
                ActivityCompat.requestPermissions(act, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                Toast.makeText(act, "Please Allow the file write", Toast.LENGTH_SHORT).show();
            }
        }).execute(act);

    }

    public boolean isStoragePermissionGranted(Activity act) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (act.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("IMAGE", "Permission is granted");
                return true;
            } else {

                Log.v("IMAGE", "Permission is revoked");
                ActivityCompat.requestPermissions(act, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("IMAGE", "Permission is granted");
            return true;
        }
    }

    public static void saveSharedImage(final Bitmap finalBitmap, final Context c, final String desciption) {

        final Activity act = (Activity) c;
        Log.e("permission", "=>Save_share_img");
        if (checkPermission(act))//Share
        {
            writeShareImageFile(finalBitmap, c, desciption);
        } else {
            //requestPermission(act);
            new Permissive.Request(Manifest.permission.WRITE_EXTERNAL_STORAGE).whenPermissionsGranted(new PermissionsGrantedListener() {
                @Override
                public void onPermissionsGranted(String[] permissions) throws SecurityException {
                    Log.e("granded", "=>");

                }
            }).whenPermissionsRefused(new PermissionsRefusedListener() {
                @Override
                public void onPermissionsRefused(String[] permissions) {
                    Toast.makeText(act, "Please Allow the file write", Toast.LENGTH_SHORT).show();
                    requestPermission(act);
                }
            }).execute(act);
        }




    }

    public static void writeShareImageFile(final Bitmap finalBitmap, final Context c, final String desciption) {
        final ProgressDialog pd = new ProgressDialog(c);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Please Wait");

        class shareImageAsync extends AsyncTask<String, String, String> {
            @Override
            protected String doInBackground(String... params) {

                if (hasStorage(true)) {
                    String targetPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + c.getString(R.string.sharecropfoldername);
                    share = new File(targetPath);
                } else {
                    ContextWrapper cw = new ContextWrapper(c);
                    share = cw.getDir(c.getString(R.string.sharecropfoldername), Context.MODE_PRIVATE);
                }
                share.mkdirs();

                Random generator = new Random();
                int n = 10000;
                n = generator.nextInt(n);
                String fname = "Image-shared.jpg";
                shared = new File(share, fname);

                if (shared.exists()) shared.delete();
                try {
                    FileOutputStream out = new FileOutputStream(shared);
                    finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    galleryAddPic(shared.getAbsolutePath(), c);
                    out.flush();
                    out.close();
                    path = shared.getAbsolutePath();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
                return shared.toString();
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pd.show();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                pd.dismiss();
                String shareText = c.getString(R.string.app_share_message);//" Download the Bhaskar E-Paper all-in-1 App to read Dainik bhaskar, Divya bhaskar & Divya marathi digital papers.\n" +"Click";
                Uri photoURI = FileProvider.getUriForFile(c, BuildConfig.APPLICATION_ID + ".provider", shared);
                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, c.getString(R.string.feedback_mail_subject1) + Util.getAppVersion(c));
                intent.putExtra(Intent.EXTRA_STREAM, photoURI);
                intent.putExtra(Intent.EXTRA_TEXT, ViralStringMSG + shareText + "\n" + Urls.APP_SHARE_URL);
                intent.setType("image/*");
                c.startActivity(intent);

            }
        }
        new shareImageAsync().execute();
    }

    public static void setQueryFileDownload(Context context, String ur1) {
        if (Utility.isConnectingToInternet(context)) {
            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri Download_Uri = Uri.parse(ur1);
            DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
            request.setAllowedOverRoaming(false);
            request.setDescription("MultipleWallpaper Download using Desktopper.");

            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, setFileLocation(context));
            downloadManager.enqueue(request);
        } else {
            Toast.makeText(context, "Network Error..", Toast.LENGTH_SHORT).show();
        }
    }

    public static String setFileLocation(Context c) {
        if (hasStorage(true)) {
            String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            myDir = new File(root + "/" + c.getString(R.string.foldername));
        } else {
            ContextWrapper cw = new ContextWrapper(c);
            myDir = cw.getDir(c.getString(R.string.foldername), Context.MODE_PRIVATE);
        }

        myDir.mkdirs();
        Random generator = new Random();
        int n = 100000;
        n = generator.nextInt(n);
        String fname;
//        if (edited != null)
//            fname = edited;
//        else
        fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);

        return file.toString();
    }

    public static boolean hasStorage(boolean requireWriteAccess) {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        } else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public static void galleryAddPic(String mCurrentPhotoPath, Context context) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);

    }

    /*private static boolean requestFileWritePermission(Context c) {
        Activity act = (Activity) c;
        Perm writepermission = new Perm(act, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return writepermission.isGranted();
    }*/


    public static Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static Bitmap takeScreenShot(ImageView view) {

        final Drawable drawable = view.getDrawable();
        if (drawable == null || !(drawable instanceof BitmapDrawable)) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;

        /*view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(AccessibilityNodeInfoCompat.ACTION_COLLAPSE);
        view.buildDrawingCache();
        if (view.getDrawingCache() == null) {
            return null;
        }
        Bitmap snapshot = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        view.destroyDrawingCache();
        return snapshot;
*/

       /* // Get image matrix values and place them in an array.
        final float[] matrixValues = new float[9];
        view.getImageMatrix().getValues(matrixValues);

        // Extract the scale and translation values. Note, we currently do not handle any other transformations (e.g. skew).
        final float scaleX = matrixValues[Matrix.MSCALE_X];
        final float scaleY = matrixValues[Matrix.MSCALE_Y];
        final float transX = matrixValues[Matrix.MTRANS_X];
        final float transY = matrixValues[Matrix.MTRANS_Y];

        // Ensure that the left and top edges are not outside of the ImageView bounds.
        final float bitmapLeft = (transX < 0) ? Math.abs(transX) : 0;
        final float bitmapTop = (transY < 0) ? Math.abs(transY) : 0;

        // Get the original bitmap object.
        final Bitmap originalBitmap = ((BitmapDrawable) drawable).getBitmap();

        // Calculate the top-left corner of the crop window relative to the ~original~ bitmap size.
        final float cropX = (bitmapLeft + Edge.LEFT.getCoordinate()) / scaleX;
        final float cropY = (bitmapTop + Edge.TOP.getCoordinate()) / scaleY;

        // Calculate the crop window size relative to the ~original~ bitmap size.
        // Make sure the right and bottom edges are not outside the ImageView bounds (this is just to address rounding discrepancies).
        final float cropWidth = Math.min(Edge.getWidth() / scaleX, originalBitmap.getWidth() - cropX);
        final float cropHeight = Math.min(Edge.getHeight() / scaleY, originalBitmap.getHeight() - cropY);

        // Crop the subset from the original Bitmap.
        return Bitmap.createBitmap(originalBitmap,
                (int) cropX,
                (int) cropY,
                (int) cropWidth,
                (int) cropHeight);*/
    }



}
