package com.bhaskar.epaper.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.bhaskar.epaper.R;
import com.bhaskar.epaper.epaperv2.util.QuickPreferences;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


/**
 * Created by siddharth on 6/28/2016.
 */
public class AdUtility {

    InterstitialAd mInterstitialAd;

    Context context;

    boolean flag = false;

    public AdUtility(Activity context) {
        this.context = context;
    }

    public void loadBannerAd(AdView adview) {
        if (EpaperPrefernces.getInstance(context).getBooleanValue(QuickPreferences.CommonPref.IS_AD_ACTIVE, false)) {
            AdRequest adRequest = new AdRequest.Builder()
                    .build();
            adview.loadAd(adRequest);
            adview.setVisibility(View.VISIBLE);
        } else {
            adview.setVisibility(View.GONE);
        }
    }

    public void loadInterstitial() {
      /*  // Disable the next level button and load the ad.
        AdRequest request = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
                .addTestDevice("CE265DA0223B8BB621DD8CF95F09280E")
                .build();

        mInterstitialAd = newInterstitialAd();
        mInterstitialAd.setAdListener(new AdListener() {
            public void onAdLoaded() {
                showInterstitial();
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                new FBAdUtility(context).loadInterstitialAd();
            }
        });

//        AdRequest.Builder.addTestDevice("CE265DA0223B8BB621DD8CF95F09280E");
        mInterstitialAd.loadAd(request);*/

    }

    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and reload the ad.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            loadInterstitial();
        }
    }

    private InterstitialAd newInterstitialAd() {

        InterstitialAd interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId(context.getResources().getString(R.string.interstitial_ad_unit));

        // Begin loading your interstitial.
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailedToLoad(int errorCode) {

            }

            @Override
            public void onAdClosed() {
                // Proceed to the next level.

            }
        });
        return interstitialAd;
    }
}
