package com.bhaskar.epaper.utils;


public class Urls {

    public static boolean IS_TESTING = true;
    public static String FEED_BASE_URL = IS_TESTING ? "https://appfeedlight.bhaskar.com/" : "https://appfeedlight.bhaskar.com/";
    public static String BASE_URL = IS_TESTING ? "https://appfeedlight.bhaskar.com/epaper/" : "https://appfeedlight.bhaskar.com/epaper/";

    public static String APP_CONTROL_API_URL = "https://appfeedlight.bhaskar.com/epaper/appcontrol/%s/" + (IS_TESTING ? "?testing=1" : "");
    public static String STATE_LIST_URL = BASE_URL + "jx-default/json/";
    public static String SESSION_URL = BASE_URL + "getsession/" + (IS_TESTING ? "?testing=1" : "");

    public static final String SIGNUP_URL = BASE_URL + "signupV2/" + (IS_TESTING ? "?testing=1" : "");
    public static final String VERIFY_OTP_URL = BASE_URL + "verifyOtpV2/" + (IS_TESTING ? "?testing=1" : "");
    public static final String MOBILE_VERIFY_OTP_URL = BASE_URL + "verifyMobileOtp/" + (IS_TESTING ? "?testing=1" : "");
    public static final String SEND_OTP_URL = BASE_URL + "getOtpV3/" + (IS_TESTING ? "?testing=1" : "");
    public static final String SIGN_UP_VERIFY_URL = BASE_URL + "signupVerify/" + (IS_TESTING ? "?testing=1" : "");
    public static final String LOGIN_URL = BASE_URL + "loginV2/" + (IS_TESTING ? "?testing=1" : "");
    public static final String RESET_PASSWORD_URL = BASE_URL + "resetPasswordV2/" + (IS_TESTING ? "?testing=1" : "");
    public static final String PROFILE_EDIT_URL = BASE_URL + "profileUpdate/" + (IS_TESTING ? "?testing=1" : "");

    public static final String FEED_BACK_URL = BASE_URL + "feedBackForm/";
    public static final String ALL_EDITION_LIST_URL = BASE_URL + "magazine-alledition/json/";

    public static final String TERM_AND_CONDITION_URL = "https://epaper.divyabhaskar.co.in/terms-and-conditions/?ref=app";
    public static final String PRIVACY_POLICY_URL = "https://epaper.divyabhaskar.co.in/privacy-policy/?ref=app";
    public static final String REFUND_POLICY_URL = "https://epaper.divyabhaskar.co.in/refund-policy/?ref=app";
    public static final String COOKIE_POLICY_URL = "https://epaper.divyabhaskar.co.in/cookie-policy/?ref=app";

    /*{paymentGateway,Country check,Channel}*/
    public static final String SUBSCRIPTION_PLAN_URL = BASE_URL + "getSubsPlan/" + (IS_TESTING ? "?testing=1" : "");
    /*{paymentGateway,Country check,Channel}*/
    public static final String SUBSCRIPTION_YEARLY_PLAN_URL = BASE_URL + "getSubsPlanYearly/%s/%s/%s/" + (IS_TESTING ? "?testing=1" : "");
    /*{host_id, subs_id, isIndia, stateCode,Channel_id}*/
    public static final String PAYMENT_ORDER_URL = BASE_URL + "orderDetail/" + (IS_TESTING ? "?testing=1" : "");
    public static final String PAYMENT_PROCESS_ORDER_URL = BASE_URL + "processOrderDetail/" + (IS_TESTING ? "?testing=1" : "");
    public static final String EMAIL_VERIFICATION_URL = BASE_URL + "sendEmailToken/" + (IS_TESTING ? "?testing=1" : "");

    public static final String APP_SOURCE_URL = BASE_URL + "app_source/" + (IS_TESTING ? "?testing=1" : "");

    public static final String ACCOUNT_SETTINGS_URL = "webfeed/ttl1m/epaper/getSubsInfo/"; // userId
    public static final String TRANSACTION_HISTORY_URL = "webfeed/ttl1m/epaper/userSubsHistory/"; // userId, pageCount
    public static final String IS_USER_SUBSCRIBED_URL = "webfeed/ttl1m/epaper/isUserSubscribed/"; // userId

    public static final String APP_SHARE_URL = "https://nh3p6.app.goo.gl/H3Ed/";
    public static final String GDPR_WD_CONSENT_URL = BASE_URL + "resetGdprUser" + (IS_TESTING ? "?testing=1" : "");

    public static final String IS_IMAGE_SAVE = BASE_URL + "isImageSave/" + (IS_TESTING ? "?testing=1" : "");
    //public static final String HOLIDAY_MSZ = BASE_URL + "holidayMessage/" + (IS_TESTING ? "?testing=1" : "");
    public static final String HOLIDAY_MSZ = BASE_URL + "holidayMessage/" + (IS_TESTING ? "?testing=1" : "");
    /*POST*/
//    public static final String SAVE_BOOKMARK = BASE_URL + "/bookmark.php";
//    public static final String GET_BOOKMARK = BASE_URL + "/view_bookmarks.php";
//    public static final String REMOVE_BOOKMARK = BASE_URL + "/remove_bookmarks.php";
//    public static final String SAVE_EDITION = BASE_URL + "/myedition.php";
//    public static final String GET_EDITION = BASE_URL + "/view_myedition.php";
//    public static final String REMOVE_EDITION = BASE_URL + "/remove_myedition.php";
//    public static final String SAVE_BEST_STORY = BASE_URL + "/set_best_story.php";

    /*GET*/
//    public static final String GET_SEARCH_DATA = BASE_URL + "/datajson.php";
//    public static final String GET_MAGAZINE_MASTER_DATA = BASE_URL + "/magazine-master.json.php";
//    public static final String GET_MAGAZINE_SUBMASTER_DATA = BASE_URL + "/magazine-submaster.json.php";
//    public static final String GET_MAGAZINE_DETAIL_DATA = BASE_URL + "/magazine-details.json.php";
}
