package com.bhaskar.epaper.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestOptions;

public class ImageUtil {

    private static final String defaultURL = "";

    @SuppressLint("CheckResult")
    public static void setImage(Context context, String imageUrl, int placeholder, ImageView imageView) {
        try {
            if (context == null)
                return;

            GlideUrl url = HttpHeader.getUrl(imageUrl);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);

            if (placeholder == 0) {
                Glide.with(context)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .into(imageView);
            } else {
                requestOptions.placeholder(placeholder);
                Glide.with(context)
                        .applyDefaultRequestOptions(requestOptions)
                        .asBitmap()
                        .load(url == null ? defaultURL : url)
                        .into(imageView);
            }
        } catch (Exception ignored) {
        }
    }

    public static class HttpHeader {
        private static final String USER_AGENT = "EPAPER_USER_AGENT";
        private static LazyHeaders lazyHeaders;

        public static GlideUrl getUrl(String url) {
            try {
                if (lazyHeaders == null) {
                    lazyHeaders = new LazyHeaders.Builder().addHeader("User-Agent", USER_AGENT).build();
                }

                return new GlideUrl(url, lazyHeaders);
            } catch (Exception e) {
                try {
                    return new GlideUrl(url);
                } catch (Exception ignored) {
                }
            }

            return null;
        }
    }
}
