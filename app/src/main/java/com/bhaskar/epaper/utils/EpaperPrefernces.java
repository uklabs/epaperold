package com.bhaskar.epaper.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.bhaskar.epaper.ui.MasterActivity;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;

public class EpaperPrefernces {

    Context mContext;
    public static final String NAME = "DB_APP_Preference";
    private SharedPreferences preferences = null;
    private static EpaperPrefernces instance = null;

    private EpaperPrefernces(Context ctx) {
        if (ctx != null) {
            this.mContext = ctx;
            preferences = ctx.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        }
    }

    public static EpaperPrefernces getInstance(Context ctx) {
        if (instance == null) {
            instance = new EpaperPrefernces(ctx);
        }
        return instance;
    }

    public void removePreferences() {
        if (preferences != null)
            preferences.edit().clear().commit();
    }

    public void setIntValue(String key, int value) {
        if (preferences != null)
            preferences.edit().putInt(key, value).commit();
    }

    public int getIntValue(String key, int defaultValue) {
        if (preferences != null)
            return preferences.getInt(key, defaultValue);
        else
            return defaultValue;
    }

    public void setStringValue(String key, String value) {
        if (preferences != null)
            preferences.edit().putString(key, value).commit();
    }

    public String getStringValue(String key, String defaultValue) {
        if (preferences != null)
            return preferences.getString(key, defaultValue);
        else
            return defaultValue;
    }

    public void setBooleanValue(String key, Boolean value) {
        if (preferences != null)
            preferences.edit().putBoolean(key, value).commit();
    }

    public Boolean getBooleanValue(String key, Boolean defaultValue) {
        if (preferences != null)
            return preferences.getBoolean(key, defaultValue);
        else
            return defaultValue;
    }

    public void setFloatValue(String key, Float value) {
        if (preferences != null)
            preferences.edit().putFloat(key, value).commit();
    }

    public Float getFlotValue(String key, Float defaultValue) {
        if (preferences != null)
            return preferences.getFloat(key, defaultValue);
        else
            return defaultValue;
    }

    public void setLongValue(String key, Long value) {
        if (preferences != null)
            preferences.edit().putLong(key, value).commit();
    }

    public long getLongValue(String key, Long defaultValue) {
        if (preferences != null)
            return preferences.getLong(key, defaultValue);
        else
            return defaultValue;
    }

    public void logOut(Context context) {
//        removePreferences();
//        FirebaseAuth mAuth = FirebaseAuth.getInstance();
//        if (mAuth != null) {
//            mAuth.signOut();
//        }
//        LoginManager.getInstance().logOut();
//        Intent i = new Intent(context, MasterActivity.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        context.startActivity(i);
    }

    public void setHome() {
        Intent i = new Intent(mContext, MasterActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(i);
    }

    public void delete(String key) {
        preferences.edit().remove(key).commit();
    }

    public void safelogOut(Context context) {
        removePreferences();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        if (mAuth != null) {
            mAuth.signOut();
        }
        LoginManager.getInstance().logOut();
    }
}
