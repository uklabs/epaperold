package com.bhaskar.epaper.utils;

public class GAConst {

    public interface Screen {
        String ACCOUNT_SETTING = "account_setting";
        String PROFILE = "profile";
        String ORDER_DETAIL = "order_detail";
        String SUBSCRIPTION_PLAN = "subscription_plan";
        String REGISTRATION = "registration";
        String LOGIN = "login";
    }

    public interface Category {
        String LOGIN = "login";
        String REGISTER = "signup";
        String SUBSCRIPTION = "subscription";
        String ORDER = "order";
        String OTP = "otp";
        String FEEDBACK = "feedback";
        String SUBSCRIPTION_POPUP = "subscription_popup";
    }

    public interface Action {
        String LEFT_MENU = "left_menu";
        String SUBSCRIPTION_POPUP = "subscription_popup";
        String SUBSCRIPTION_PAGE = "subscription_page";

        String CLICK = "click";

        String SUCCESS = "success";
        String FAIL = "fail";
        String SKIP = "skip";

        String SEND = "send";
        String RECIEVE = "recieve";

        String VIEW = "view";

    }

    public interface Label {
        String FACEBOOK = "F";
        String GMAIL = "G";
        String MANUAL = "M";
        String IMPRESSION = "impression";
        String BUY_NOW = "buynow";
        String CONTACT = "contact";
        String LEFT_MENU = "left_menu";
        String LOGIN = "login";
        String REGISTER = "register";
        String REGISTER_STRIP = "register_strip";

        String LANDING = "landing";
        String SUBSCRIPTION_POPUP = "subscription_popup_";
        String SUBSCRIPTION_END = "subscription_end";
    }
}
