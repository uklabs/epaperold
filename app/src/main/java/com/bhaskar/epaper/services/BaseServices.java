package com.bhaskar.epaper.services;


import com.google.gson.JsonElement;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;


/**
 * Created by db on 3/21/2016.
 */
public interface BaseServices {

    @FormUrlEncoded
    @POST("/bookmark.php")
    void setBookmarkData(@Field("email") String email, @Field("edition_name") String edition_name, @Field("edition_date") String edition_date, @Field("org_url") String org_url, @Field("thumb_url") String thumb_url, @Field("large_url") String large_url, @Field("pageno") String pageno, Callback<JsonElement> callback);

    @FormUrlEncoded
    @POST("/view_bookmarks.php")
    void getMyBookmarkData(@Field("email") String email, Callback<JsonElement> callback);

    @FormUrlEncoded
    @POST("/remove_bookmarks.php")
    void removeMyBookmarkData(@Field("id") String id, Callback<JsonElement> callback);

    @FormUrlEncoded
    @POST("/myedition.php")
    void setMyEditionData(@Field("email") String email, @Field("state") String state, @Field("center") String center, @Field("edtname") String edtname, @Field("edtcode") String edtcode,
                          @Field("org_path") String imagepath, @Field("thumb_path") String thumbimage, @Field("large_path") String imagelarge, Callback<JsonElement> callback);

    @FormUrlEncoded
    @POST("/view_myedition.php")
    void getMyEditionData(@Field("email") String email, Callback<JsonElement> callback);

    @FormUrlEncoded
    @POST("/remove_myedition.php")
    void removeMyEditionData(@Field("id") String id, Callback<JsonElement> callback);

    @GET("/datajson.php")
    void getSearchData(Callback<Response> callback);

    @GET("/magazine-master.json.php")
    void getMagazineMasterData(Callback<Response> callback);

    @GET("/magazine-submaster.json.php")
    void getMagazineSubMasterData(Callback<Response> callback);

    @GET("/magazine-details.json.php")
    void getMagazineDetailData(Callback<Response> callback);

    @Multipart
    @POST("/set_best_story.php")
    void setBestStory(@Part("comment") String comment, @Part("edition_code") String edition_code, @Part("edition_name") String edition_name, @Part("img_origanal") String img_org_path, @Part("img_cropped") TypedFile file, Callback<JsonElement> callback);


}
